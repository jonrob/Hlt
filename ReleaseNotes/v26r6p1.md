2018-02-15 Hlt v26r6p1
========================================

Wrap-up release for 2017
----------------------------------------
Based on Gaudi v28r2, LHCb v42r6p1, Lbcom v20r6p1, Rec v21r6p1, Phys v23r7p1.
This version is released on the 2017-patches branch.

### Line developments
- 5 TeV settings wrap-up, !446 (@rmatev) [LBHLT-377]
- Monitoring histograms for the CharmHad cross-section lines, !447 (@spradlin) [LBHLT-367,LBHLT-371]
- Updated CharmHad settings for 5 TeV, !448 (@spradlin) [LBHLT-367,LBHLT-371,LBHLT-376]
- Add monitoring histograms for SMOG trigger lines, !445 (@robbep)
- Add Etac2PP SMOG trigger lines, !443 (@zhangy) [LBHLT-373]
- Add 2017 5TeV pp EW lines as a straight copy from 2017 pp settings, !442 (@weisser) [LBHLT-372]
- CharmHad settings for 5 TeV 2017 running, !441 (@apearce) [LBHLT-371]
- SL lines for 5TeV run, !440 (@sely) [LBHLT-375]
- B&Q DiMuon lines for the 5 TeV run, !438 (@apiucci) [LBHLT-370]
- Add initial settings for 5 TeV run, !439 (@rmatev) [LBHLT-367]
- HLT2 lines for charmed-baryon decays to final states with a Lambda or a Xim, !416 (@lohenry) [LBHLT-139]
- Monitoring for CaloPIDEta2MuMuGamma, !430 (@chefdevi)

### New features
- Add 2D histos to HltMassMonitor, !459, !467 (@kgizdov, @rmatev)
- Add HltDecReportsCreator that writes TCK in HltDecReports, !455 (@egovorko)
- Add routing bits to flow dump, !444 (@rmatev)
- Trigger settings for Xenon-Xenon run in 2017, !433 (@zhangy) [LBHLT-362]

### Enhancements
- Always clone CaloDigit objects associated to CaloHypo objects in the simulation, !481 (@apearce)
- Flush stdout and stderr in TCKUtils, !480 (@rmatev)
- Update MC TCK creation for 2017 Turbo support, !479 (@apearce)
- Miscellaneous improvements to TCKUtils, !464 (@rmatev)
- Configure CALO object cloners to have consistent behaviour, !453 (@apearce)

### Bug fixes
- Fix createMCversion and add tests, !473 (@rmatev) [LBHLT-120]
- Protect HistoWrapper from segfault, !454 (@sstahl)
