/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// =============================================================================
// STD & STL
// =============================================================================
#include <sstream>
// =============================================================================
// LoKi
// =============================================================================
#include "LoKi/HltTool.h"
#include "LoKi/ToCpp.h"
// =============================================================================
/** @file
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date   2009-03-31
 */
// =============================================================================
/*  constructor from the tool type/name & "public" flag
 *  @param tool the tool type/name
 *  @param flag public?
 */
// =============================================================================
LoKi::Hlt1::Tool::Tool
( const std::string& tool ,
  const bool         flag )
  : m_tool   ( tool )
  , m_public ( flag )
{}
// =============================================================================
// conversion to string
// =============================================================================
std::string LoKi::Hlt1::Tool::toString    () const
{
  std::ostringstream s ;
  fillStream ( s )  ;
  return s.str() ;
}
// =============================================================================

// =============================================================================
std::string Gaudi::Utils::toCpp
( const LoKi::Hlt1::Tool&                o )
{
  return " LoKi::Hlt1::Tool("
    + toCpp ( o.tool () ) + ","
    + toCpp ( o.flag () ) + ") " ;
}
// =============================================================================
// The END
// =============================================================================
