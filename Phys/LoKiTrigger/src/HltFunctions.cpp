/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// TrackEvent
// ============================================================================
#include "Event/Track.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/HltFunctions.h"
#include "LoKi/HltBinders.h"
#include "LoKi/Constants.h"
// ============================================================================
// HltBase
// ============================================================================
#include "HltBase/HltUtils.h"
// ============================================================================
/** @file
 *  implementation file for function from the file LoKi/HltFunctions.h
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-08-13
 */
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
double
LoKi::Tracks::DistanceOfClosestApproach::operator()
  ( LoKi::Tracks::DistanceOfClosestApproach::argument a ) const
{
  // try to calculate DOCA...
  return HltUtils::closestDistanceMod ( a.first , a.second ) ;
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
double
LoKi::Tracks::DeltaP::operator()
  ( LoKi::Tracks::DeltaP::argument a ) const
{
  // unpack argument:
  const LHCb::Track& t1 = a.first  ;
  const LHCb::Track& t2 = a.second ;
  //
  if      ( &t1 == &t2 ) { return 0 ; }                        // RETURN
  const double p1 = t1.p() ;
  const double p2 = t2.p() ;
  // trivial checks:
  if      ( p1  ==  p2 ) { return 0 ; }                        // RETURN
  else if ( 0   ==  p2 )
  {
    Warning ( "The second momentum is equal to zero, return -inf").ignore();
    return LoKi::Constants::NegativeInfinity ;                 // RETURN
  }
  //
  return (p1-p2)/p2 ;                                          // RETURN
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
double
LoKi::Tracks::DeltaE::operator()
  ( LoKi::Tracks::DeltaE::argument t ) const
{
  if ( !t )
  {
    Error ( "LHCb::Track* points to NULL, return -inf" ).ignore();
    return LoKi::Constants::NegativeInfinity ;                // RETURN
  }
  // get the state:
  const LHCb::State* s  = t->stateAt(LHCb::State::MidHCal) ;
  if ( !s )
  {
    Warning("No state at MidHcal, return +inf").ignore();
    return LoKi::Constants::PositiveInfinity ;                // RETURN
  }
  // calculate delta E:
  const double p  = t -> p () ;
  const double e  = s -> p () ;
  const double de = e * ( sqrt ( 0.60 * 0.60 + 0.70 * 0.70 / e ) ) ;
  //
  return (e-p)/de ;                                            // REUTRN
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
double
LoKi::Tracks::ImpactParameter::operator()
  ( LoKi::Tracks::ImpactParameter::argument a ) const
{
  return HltUtils::impactParameter ( a.second , a.first ) ;
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
double
LoKi::Tracks::MatchIDsFraction::operator()
  ( LoKi::Tracks::MatchIDsFraction::argument a ) const
{
  return HltUtils::matchIDsFraction ( a.first , a.second ) ;
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
double
LoKi::Tracks::DeltaAngle::operator()
  ( LoKi::Tracks::DeltaAngle::argument a ) const
{
  return HltUtils::deltaAngle ( a.first , a.second ) ;
}
// ============================================================================
// The END
// ============================================================================
