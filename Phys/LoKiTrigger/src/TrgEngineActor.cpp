/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/TrgEngineActor.h"
#include "LoKi/Report.h"
// ============================================================================
/** @file
 *  Implementation file for class LoKi::Hybrid::TrgEngineActor
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @date 2004-06-29
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *
 */
// ============================================================================
// helper method to descrease number of lines
// ============================================================================
template <class TYPE>
inline StatusCode LoKi::Hybrid::TrgEngineActor::_add
( const std::string& name , const TYPE& cut ) const
{
  if ( m_stack.empty() ) 
  {
    return LoKi::Report::Error
      ("LoKi::Hybrid::TrgEngineActor::addCut/Fun(): empty stack!") ;  
  }
  //
  const Entry& entry = m_stack.top() ;
  // check the tool
  if ( !entry.first ) 
  {
    return LoKi::Report::Error
      ("LoKi::Hybrid::TrgEngineActor::addCut/Fun(): LoKi::ITrgFunctorAntiFactory* is invalid!") ;  
  }
  // one more check 
  if ( name != entry.first->name() )
  {
    return LoKi::Report::Error
      ("LoKi::Hybrid::TrgEngineActor::addCut/Fun() : mismatch in LoKi::ITrgFunctorAntiFactory name!") ;  
  }
  // set the cut for the tool 
  entry.first->set ( cut ) ;
  // 
  return StatusCode::SUCCESS ;
}
// ============================================================================
// accessor to the static instance
// ============================================================================
LoKi::Hybrid::TrgEngineActor&
LoKi::Hybrid::TrgEngineActor::instance()
{
  static LoKi::Hybrid::TrgEngineActor s_holder ;
  return s_holder ;
}
// ============================================================================
// disconnect the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrgEngineActor::disconnect
( const LoKi::ITrgFunctorAntiFactory* factory )
{
  
  if ( m_stack.empty() ) 
  { return LoKi::Report::Error ("LoKi::Hybrid::TrgEngineActor::disconnect: empty stack!") ; }
  //
  const Entry& entry = m_stack.top () ;
  //
  if ( entry.first == factory  ) { m_stack.pop() ; } /// remove the last entry
  else
  {
    return LoKi::Report::Error
      ("LoKi::Hybrid::TrgEngineActor::disconnect: mismatch in tools " ) ;
  } 
  ///
  return StatusCode::SUCCESS ;
}
// ============================================================================
// connect the hybrid tool for code translation
// ============================================================================
StatusCode LoKi::Hybrid::TrgEngineActor::connect
( const LoKi::ITrgFunctorAntiFactory* factory ,
  const LoKi::Context&                context )
{
  //
  if ( !factory ) 
  {
    return LoKi::Report::Error
      ( "LoKi::Hybrid::TrgEngineActor::connect: Invalid factory" ) ;
  }
  m_stack.emplace ( factory , context ) ;
  //
  return StatusCode::SUCCESS ;
}
// ============================================================================
/*  get the current context
 *  contex is valid only in between <code>connect/disconnect</code>
 *  @return the current active context 
 */
// ============================================================================
const LoKi::Context* LoKi::Hybrid::TrgEngineActor::context () const
{
  if ( m_stack.empty() ) 
  {
    LoKi::Report::Error ( "LoKi::Hybrid::TrgEngineActor::context: empty stack" ) ;
    return nullptr ;
  }
  const Entry& last = m_stack.top() ;
  return &last.second ;
}  
// ============================================================================
// propagate the cut to the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrgEngineActor::process
( const std::string&          name ,
  const LoKi::Types::TC_Cuts&  cut  ) const { return _add ( name , cut  ) ; }
// ============================================================================
// propagate the function to the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrgEngineActor::process
( const std::string&          name ,
  const LoKi::Types::TC_Func&  func ) const { return _add ( name , func ) ; }
// ============================================================================
// functional part
// ============================================================================
// propagate the function to the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrgEngineActor::process
( const std::string&             name ,
  const LoKi::Types::TC_Maps&     func ) const
{ return _add ( name , func ) ; }
// ============================================================================
// propagate the function to the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrgEngineActor::process
( const std::string&             name ,
  const LoKi::Types::TC_Pipes&    func ) const
{ return _add ( name , func ) ; }
// ============================================================================
// propagate the function to the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrgEngineActor::process
( const std::string&             name ,
  const LoKi::Types::TC_FunVals&  func ) const
{ return _add ( name , func ) ; }
// ============================================================================
// propagate the function to the tool
// ============================================================================
StatusCode LoKi::Hybrid::TrgEngineActor::process
( const std::string&             name ,
  const LoKi::Types::TC_Sources&  func ) const
{ return _add ( name , func ) ; }
// ============================================================================

// ============================================================================
// The END
// ============================================================================
