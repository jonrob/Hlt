/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_LOKITRIGGER_H
#define LOKI_LOKITRIGGER_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
/** @file
 *  This file is part of LoKi project:
 *   ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *
 */
// ============================================================================
#include "LoKi/TriggerTypes.h"
//
// #include "LoKi/HltCuts.h"
#include "LoKi/HltCandidates.h"
#include "LoKi/HltStages.h"
//
#include "LoKi/RecVertices.h"
#include "LoKi/MultiTracks.h"
//
#include "LoKi/HltL0.h"
//
#include "LoKi/Hlt1CombinerConf.h"
#include "LoKi/Hlt1Combiner.h"
#include "LoKi/Hlt1DiPhotonMaker.h"
#include "LoKi/ToParticles.h"
#include "LoKi/ToProtoParticles.h"
#include "LoKi/L0CaloToParticles.h"
#include "LoKi/Hlt1Functions.h"
#include "LoKi/Hlt1Wrappers.h"
//
#include "LoKi/TrackCutAsRecVertexCut.h"
//
#include "LoKi/TrSources.h"
#include "LoKi/TrFilter.h"
#include "LoKi/TrMatch.h"
#include "LoKi/TrUpgrade.h"
//
#include "LoKi/VxMaker.h"
#include "LoKi/VxUpgrade.h"
#include "LoKi/VxCreator.h"
#include "LoKi/UpgradeVertices.h"
//
#include "LoKi/TrgSources.h"
#include "LoKi/MatchTracks.h"
#include "LoKi/CacheFuncs.h"
// ============================================================================
#include "LoKi/ITrgFunctorFactory.h"
#include "LoKi/ITrgFunctorAntiFactory.h"
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_LOKITRACK_H
// ============================================================================
