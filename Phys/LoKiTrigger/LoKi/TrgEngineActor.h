/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ===========================================================================
#ifndef LOKI_TRGENGINEACTOR_H
#define LOKI_TRGENGINEACTOR_H 1
// ===========================================================================
// Include files
// ===========================================================================
// STD&STL
// ===========================================================================
#include <stack>
// ===========================================================================
// LoKi
// ===========================================================================
#include "LoKi/Interface.h"
#include "LoKi/ITrgFunctorAntiFactory.h"
#include "LoKi/Context.h"
// ===========================================================================
namespace LoKi
{
  // =========================================================================
  namespace Hybrid
  {
    // =======================================================================
    /** @class TrgEngineActor LoKi/TrgEngineActor.h
     *
     *  This file is a part of LoKi project -
     *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
     *
     *  The package has been designed with the kind help from
     *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
     *  contributions and advices from G.Raven, J.van Tilburg,
     *  A.Golutvin, P.Koppenburg have been used in the design.
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date   2004-06-29
     *
     */
    class TrgEngineActor final
    {
    public:
      // ======================================================================
      // get the static instance
      static TrgEngineActor& instance() ;
      // ======================================================================
      /// connect the hybrid tool for code translation
      StatusCode connect  
        ( const LoKi::ITrgFunctorAntiFactory* tool    ,
          const LoKi::Context&                context ) ;
      // ======================================================================
      /// disconnect the tool
      StatusCode disconnect 
        ( const LoKi::ITrgFunctorAntiFactory* tool    ) ;
      // ======================================================================
      /** get the current context
       *  contex is valid only in between <code>connect/disconnect</code>
       *  @return the current active context 
       */
      const LoKi::Context* context () const ;
      // ======================================================================
    public:
      // ======================================================================      
      /// propagate the cut to the tool
      StatusCode process
        ( const std::string&           name ,
          const LoKi::Types::TC_Cuts&  cut  ) const ;
      // ======================================================================
      /// propagate the function to the tool
      StatusCode process
        ( const std::string&           name ,
          const LoKi::Types::TC_Func&  func ) const ;
      // ======================================================================
    public:
      // ======================================================================
      // functional part for LHCb::TC_ack
      // ======================================================================
      /// add the function
      StatusCode process
        ( const std::string&              name ,
          const LoKi::Types::TC_Maps&     fun  ) const ;
      // ======================================================================
      /// add the function
      StatusCode process
        ( const std::string&              name ,
          const LoKi::Types::TC_Pipes&    fun  ) const ;
      // ======================================================================
      /// add the function
      StatusCode process
        ( const std::string&              name ,
          const LoKi::Types::TC_FunVals&  fun  ) const ;
      // ======================================================================
      /// add the function
      StatusCode process
        ( const std::string&              name ,
          const LoKi::Types::TC_Sources&  fun  ) const ;
      // ======================================================================
    protected:
      // ======================================================================
      /// Standard constructor
      TrgEngineActor() = default;
      // ======================================================================
    private:
      // ======================================================================
      /// just to save some lines
      template <class TYPE>
        inline StatusCode _add
        ( const std::string& name ,
          const TYPE&        cut )  const ; // save some lines
      // ======================================================================
    private:
      // ======================================================================
      /// the copy contructor is disabled
      TrgEngineActor           ( const TrgEngineActor& ); // no-copy
      /// the assignement operator is disabled
      TrgEngineActor& operator=( const TrgEngineActor& ); // no-assignement
      // ======================================================================
    private:
      // ======================================================================
      // the tool itself
      typedef LoKi::Interface<LoKi::ITrgFunctorAntiFactory>   Tool  ;
      typedef std::pair<Tool,LoKi::Context>                   Entry ;
      typedef std::stack<Entry>                               Stack ;
      ///  the stack of active factories 
      Stack m_stack {} ; // the stack of active factories 
      // ======================================================================
    } ;
    // ========================================================================
  } //                                            end of namespace LoKi::Hybrid
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_CUTSHOLDER_H
// ============================================================================
