/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_MULTITRACKS_H
#define LOKI_MULTITRACKS_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
// ============================================================================
// Event
// ============================================================================
#include "Event/HltMultiTrack.h"
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/TriggerTypes.h"
// ============================================================================
/** @file
 *  Couple of functions to deal with multi-tracks
 *  @see Hlt::MultiTrack
 *
 *  This file is part of LoKi project:
 *   ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@cern.ch
 *  @date 2011-03-27
 *
 */
namespace LoKi
{
  // ==========================================================================
  namespace Candidates
  {
    // ========================================================================
    class GAUDI_API MtFun : public LoKi::BasicFunctors<const Hlt::Candidate*>::Function
    {
    public:
      // ======================================================================
      /// constructor form the functor, index  and bad-value
      MtFun ( const LoKi::BasicFunctors<const LHCb::Track*>::Function& fun   ,
              const unsigned int                                       index ,
              const double                                             bad   ) ;
      /// constructor form the functor, index  and bad-value
      MtFun ( const LoKi::BasicFunctors<const LHCb::Track*>::Function& fun   ,
              const unsigned int                                       index ) ;
      /// MANDATORY: clone method ("virtual constructor")
      MtFun* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator() ( argument c ) const override;
      /// OPTIONAL: the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      /// the actual function
      LoKi::FunctorFromFunctor<const LHCb::Track*,double> m_fun  ; // the cut
      /// the index
      unsigned int                                       m_index ; // the index
      /// the bad-value
      const double m_bad ; // the bad-value
      // ======================================================================
   } ;
    // ========================================================================
  } //                                        end of namespace LoKi::Candidates
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_MULTITRACKS_H
// ============================================================================
