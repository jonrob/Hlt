/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_TRGLOCK_H
#define LOKI_TRGLOCK_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "LoKi/Interface.h"
#include "LoKi/ITrgFunctorAntiFactory.h"
#include "LoKi/Context.h"
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  namespace Hybrid
  {
    // ========================================================================
    /** @class TrgFactoryLock LoKi/TrgFactoryLock.h
     *
     *  Helper class (sentry) to connect ITrackFunctorAntiFactory to TrackEngine
     *
     *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
     *  @date 2007-06-09
     *
     */
    class TrgFactoryLock
    {
    public:
      // ======================================================================
      /// contructor : Lock
      TrgFactoryLock
      ( const LoKi::ITrgFunctorAntiFactory* tool    ,
        const LoKi::Context&                context ) ; 
      /// destrcutor : UnLock
      virtual ~TrgFactoryLock () ;                    // destrcutor : UnLock
      // ======================================================================
      /// no copy constructor
      TrgFactoryLock ( const TrgFactoryLock& ) = delete; // no copy constructor
      /// no assignement operator
      TrgFactoryLock& operator = ( const TrgFactoryLock& ) = delete;
      // ======================================================================
    private:
      // ======================================================================
      /// the tool itself
      LoKi::Interface<LoKi::ITrgFunctorAntiFactory> m_tool ;        // the tool
      // ======================================================================
    } ;
    // ========================================================================
  } //                                            end of namespace LoKi::Hybrid
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_HYBRIDLOCK_H
// ============================================================================
