/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_HLTBINDERSDICTS_H
#define LOKI_HLTBINDERSDICTS_H 1
// ============================================================================
// Include Files
// ============================================================================
#include "LoKi/TriggerTypes.h"
// ============================================================================
/** @file
 *  Collection of binders for Hlt-functions
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2007-08-19
 */
// ============================================================================
#endif // LOKI_HLTBINDERSDICTS_H
// ============================================================================
