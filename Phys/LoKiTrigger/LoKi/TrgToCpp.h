/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_TRGTOCPP_H
#define LOKI_TRGTOCPP_H 1
// ============================================================================
// Include files
// ============================================================================
// STD&STL
// ============================================================================
#include <string>
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/Kernel.h"
// ============================================================================
namespace LoKi
{
  // ==========================================================================
  namespace Hlt1
  {
    // ========================================================================
    class Tool                ;
    class TrackTool           ;
    class MatchConf           ;
    class UpgradeConf         ;
    class UpgradeTool         ;
    class VxMakerConf         ;
    class Hlt1CombinerConf    ;
    // ========================================================================
  }
  // ==========================================================================
}
// ============================================================================
namespace Gaudi
{
  // ==========================================================================
  namespace Utils
  {
    // ========================================================================
    GAUDI_API std::string toCpp ( const LoKi::Hlt1::Tool&                o ) ;
    GAUDI_API std::string toCpp ( const LoKi::Hlt1::MatchConf&           o ) ;
    GAUDI_API std::string toCpp ( const LoKi::Hlt1::TrackTool&           o ) ;
    GAUDI_API std::string toCpp ( const LoKi::Hlt1::UpgradeConf&         o ) ;
    GAUDI_API std::string toCpp ( const LoKi::Hlt1::UpgradeTool&         o ) ;
    GAUDI_API std::string toCpp ( const LoKi::Hlt1::VxMakerConf&         o ) ;
    GAUDI_API std::string toCpp ( const LoKi::Hlt1::Hlt1CombinerConf&    o ) ;
    // ========================================================================
  }
  // ==========================================================================
}
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_TRGTOCPP_H
// ============================================================================
