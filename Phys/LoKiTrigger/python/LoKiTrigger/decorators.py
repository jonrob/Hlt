#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# $Id$ 
# =============================================================================
## @file 
#  The set of basic decorations for objects from LoKiTrigger library
#
#        This file is a part of LoKi project - 
#    "C++ ToolKit  for Smart and Friendly Physics Analysis"
#
#  The package has been designed with the kind help from
#  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas, 
#  contributions and advices from G.Raven, J.van Tilburg, 
#  A.Golutvin, P.Koppenburg have been used in the design.
#
#  @author Vanya BELYAEV ibelyaev@physics.syr.edu
#  @date 2007-06-09
#
#                   $Revision$
# Last modification $Date$
#                by $Author$
# =============================================================================
"""
The set of basic decorations for objects from LoKiTrigger library

      This file is a part of LoKi project - 
``C++ ToolKit  for Smart and Friendly Physics Analysis''

The package has been designed with the kind help from
Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas, 
contributions and advices from G.Raven, J.van Tilburg, 
A.Golutvin, P.Koppenburg have been used in the design.

 """
# =============================================================================
__date__     = "2007-05-29"
__version__  = "SVN version $Revision$"
__author__   = "Vanya BELYAEV ibelyaev@physics.syr.edu" 
# =============================================================================
from   LoKiTrigger.functions   import *

_name = __name__

# =============================================================================
## make the decoration of all objects fomr this module
def _decorate ( name = _name  ) :
    """
    Make the decoration of all objects from this module
    """
    import LoKiCore.decorators as _LoKiCore
    
    tC = 'const Hlt::Candidate*'
    tS = 'const Hlt::Stage*' 
    vC = 'std::vector<const Hlt::Candidate*>'
    vD = 'std::vector<double>'
    #
    ## "function" : Hlt::Candidate -> double 
    
    _decorated  = _LoKiCore.getAndDecorateFunctions (  
        name                                   , ## module name  
        LoKi.Functor        ( tC ,'double'  )  , ## the base
        LoKi.Dicts.FunCalls ( Hlt.Candidate )  , ## call-traits
        LoKi.Dicts.FuncOps  ( tC       )  ) ## operators&operations
    
    ## "predicate/cut" :  Hlt::Candidate -> bool
    
    _decorated |= _LoKiCore.getAndDecoratePredicates ( 
        name                                    , ## module name  
        LoKi.Functor        ( tC , bool     )   , ## the base
        LoKi.Dicts.CutCalls ( Hlt.Candidate )   , ## call-traits
        LoKi.Dicts.CutsOps  ( tC       )   ) ## operators&operations

    ## "function" : Hlt::Stage -> double 
    
    _decorated  = _LoKiCore.getAndDecorateFunctions (  
        name                                   , ## module name  
        LoKi.Functor        ( tS ,'double'  )  , ## the base
        LoKi.Dicts.FunCalls ( Hlt.Stage     )  , ## call-traits
        LoKi.Dicts.FuncOps  ( tS       )  ) ## operators&operations
    
    ## "predicate/cut" :  Hlt::Stage -> bool
    
    _decorated |= _LoKiCore.getAndDecoratePredicates ( 
        name                                    , ## module name  
        LoKi.Functor        ( tS , bool     )   , ## the base
        LoKi.Dicts.CutCalls ( Hlt.Stage     )   , ## call-traits
        LoKi.Dicts.CutsOps  ( tS       )   ) ## operators&operations

    ## functional part:
    
    # "map" : vector<T> -> vector<double>
    
    _decorated |= _LoKiCore.getAndDecorateMaps (
        name                                   , ## module name  
        LoKi.Functor       ( vC , vD )         , ## the base
        LoKi.Dicts.MapsOps ( tC      )         ) ## call-traits
    
    # "pipe" : vector<T> -> vector<T>
    
    _decorated |= _LoKiCore.getAndDecoratePipes (
        name                                   , ## module name  
        LoKi.Functor       ( vC , vC )         , ## the base
        LoKi.Dicts.PipeOps ( tC )         ) ## call-traits
    
     # "funval" : vector<T> -> double 
    
    _decorated |= _LoKiCore.getAndDecorateFunVals ( 
        name                                   , ## module name  
        LoKi.Functor         ( vC , 'double' ) , ## the base
        LoKi.Dicts.FunValOps ( tC )            ) ## call-traits

    # "cutval" : vector<T> -> bool

    _decorated |= _LoKiCore.getAndDecorateCutVals (
        name                                   , ## module name  
        LoKi.Functor         ( vC , bool )     , ## the base
        LoKi.Dicts.CutValOps ( tC        )     ) ## call-traits


    # 'source' : void -> vector<T>
    
    _decorated |= _LoKiCore.getAndDecorateSources  (  
        name                                   , ## module name  
        LoKi.Functor         ( 'void' , vC )   , ## the base
        LoKi.Dicts.SourceOps (  tC )   ) ## call-traits

    
    ## primitive voids 
    _decorated |= _LoKiCore.getAndDecoratePrimitiveVoids ( name )


    # =========================================================================
    ## LHCb::Track remnants ATTENTION !!! 
    # =========================================================================
    
    tT = 'const LHCb::Track*'
    vT = 'std::vector<const LHCb::Track*>'
    
    # function 
    _decorated |= _LoKiCore.getAndDecorateFunctions (  
        name                                   , ## module name  
        LoKi.Functor        ( tT ,'double'  )  , ## the base
        LoKi.Dicts.FunCalls ( LHCb.Track    )  , ## call-traits
        LoKi.Dicts.FuncOps  ( tT       )  ) ## operators&operations
    
    # "pipe" : vector<T> -> vector<T>    
    _decorated |= _LoKiCore.getAndDecoratePipes (
        name                                   , ## module name  
        LoKi.Functor       ( vT , vT )         , ## the base
        LoKi.Dicts.PipeOps ( tT )         ) ## call-traits
    

    # 'source' : void -> vector<T>    
    _decorated |= _LoKiCore.getAndDecorateSources  (  
        name                                   , ## module name  
        LoKi.Functor         ( 'void' , vT )   , ## the base
        LoKi.Dicts.SourceOps (  tT )   ) ## call-traits

    # =========================================================================
    ## LHCb::VertexBase specifics 
    # =========================================================================
    tV = 'const LHCb::VertexBase*'
    vV = 'std::vector<const LHCb::VertexBase*>'
    
    # function 
    _decorated |= _LoKiCore.getAndDecorateFunctions (  
        name                                     , ## module name  
        LoKi.Functor        ( tV ,'double'    )  , ## the base
        LoKi.Dicts.FunCalls ( LHCb.VertexBase )  , ## call-traits
        LoKi.Dicts.FuncOps  (  tV         )  ) ## operators&operations
    
    # predicates 
    _decorated |= _LoKiCore.getAndDecoratePredicates (  
        name                                     , ## module name  
        LoKi.Functor        ( tV , bool       )  , ## the base
        LoKi.Dicts.CutCalls ( LHCb.VertexBase )  , ## call-traits
        LoKi.Dicts.CutsOps  (  tV         )  ) ## operators&operations
    
    # "pipe" : vector<T> -> vector<T>    
    _decorated |= _LoKiCore.getAndDecoratePipes (
        name                                   , ## module name  
        LoKi.Functor       ( vV , vV )         , ## the base
        LoKi.Dicts.PipeOps (  tV )         ) ## call-traits

    # 'source' : void -> vector<T>    
    _decorated |= _LoKiCore.getAndDecorateSources  (  
        name                                   , ## module name  
        LoKi.Functor         ( 'void' , vV )   , ## the base
        LoKi.Dicts.SourceOps (  tV )   ) ## call-traits

    # =========================================================================
    ## various "info" operations
    # =========================================================================
    
    ## decorate HltCandidates:
    _decorated |= _LoKiCore.getAndDecorateInfos (
        name                                   ,
        LoKi.Functor        ( tC ,'double'  )  , ## the base
        LoKi.Dicts.InfoOps  ( tC               ) ## info-opeartion
        )
    ## decorate HltCandidates:
    _decorated |= _LoKiCore.getAndDecorateInfos (
        name                                   ,
        LoKi.Functor        ( tC , bool     )  , ## the base
        LoKi.Dicts.InfoOps  ( tC               ) ## info-opeartion
        )
    ## decorate HltStages:
    _decorated |= _LoKiCore.getAndDecorateInfos (
        name                                   , 
        LoKi.Functor        ( tS ,'double'     )  , ## the base
        LoKi.Dicts.InfoOps  ( tS               ) ## info-opeartion
        )
    ## decorate HltStages:
    _decorated |= _LoKiCore.getAndDecorateInfos (
        name                                   , 
        LoKi.Functor        ( tS , bool        )  , ## the base
        LoKi.Dicts.InfoOps  ( tS               ) ## info-opeartion
        )
    
    ## 
    return _decorated                            ## RETURN

# =============================================================================
## perform the decoration 
_decorated = _decorate ()                         ## ATTENTION 
# =============================================================================

from LoKiTrack.decorators import *

# =============================================================================
if __name__ == '__main__' :
    print '*'*120
    print                               __doc__
    print ' Author    : %s '        %   __author__    
    print ' Version   : %s '        %   __version__
    print ' Date      : %s '        %   __date__
    print ' Decorated : %s symbols' %   len ( _decorated )
    for t in _decorated  : print t 
    print '*'*120
    
# =============================================================================
# The END 
# =============================================================================

