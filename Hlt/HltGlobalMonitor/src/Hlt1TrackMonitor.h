/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HLT1TRACKMONITOR_H
#define HLT1TRACKMONITOR_H 1

#include "GaudiKernel/HistoDef.h"
#include "GaudiKernel/ObjectContainerBase.h"
#include "HltBase/HltBaseAlg.h"

#include <unordered_map>

namespace AIDA {
  class IHistorgram2D;
}

/** @class Hlt1TrackMonitor Hlt1TrackMonitor.h
 *
 *  functionality:
 *        monitor simple track quantities
 *        make histograms with Hlt diagnostics info
 *
 *  @author Sascha Stahl
 *  @date   2015-05-21
 */
class Hlt1TrackMonitor : public HltBaseAlg {
public:

  /// Standard constructor
  Hlt1TrackMonitor(const std::string& name, ISvcLocator* pSvcLocator);

  virtual ~Hlt1TrackMonitor(); ///< Destructor

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution

private:

  void monitorTracks(LHCb::Track::Range tracks);
  void monitorFittedTracks(LHCb::Track::Range tracks);
  void monitorKinematics(LHCb::Track::Range tracksVelo,LHCb::Track::Range tracksForward);

  template < typename T > T* fetch(const std::string& location)
  {
    T* t = this->exist<T>( location ) ?  this->get<T>( location ) : 0;
    if ( t == 0 && this->msgLevel( MSG::WARNING ) ) {
      Warning( " Could not retrieve " + location, StatusCode::SUCCESS, 10 ).ignore();
    }
    return t;
  }

  void monitor();

  std::string m_VeloTrackLocation;
  std::string m_VeloTTTrackLocation;
  std::string m_ForwardTrackLocation;
  std::string m_FittedTrackLocation;

  AIDA::IHistogram1D* m_VeloTrackMult = nullptr;
  AIDA::IHistogram1D* m_VeloTTTrackMult = nullptr;
  AIDA::IHistogram1D* m_ForwardTrackMult = nullptr;
  AIDA::IHistogram1D* m_nTTHits = nullptr;
  AIDA::IHistogram1D* m_nITHits = nullptr;
  AIDA::IHistogram1D* m_nOTHits = nullptr;
  AIDA::IHistogram1D* m_nVeloHits = nullptr;
  AIDA::IHistogram1D* m_trackChi2DoF = nullptr;
  AIDA::IHistogram1D* m_hitResidual = nullptr;
  AIDA::IHistogram1D* m_hitResidualPull = nullptr;
  AIDA::IHistogram1D* m_VeloPhi = nullptr;
  AIDA::IHistogram1D* m_VeloEta = nullptr;
  AIDA::IHistogram1D* m_forwardPhi = nullptr;
  AIDA::IHistogram1D* m_forwardEta = nullptr;
  AIDA::IHistogram1D* m_forwardPt = nullptr;


  struct EnumClassHash{
    template <typename T>
    std::size_t operator()(T t) const{
      return static_cast<std::size_t>(t);
    }
  };

  std::unordered_map<LHCb::Measurement::Type,AIDA::IHistogram1D*,EnumClassHash> m_hitResidualPerDet;
  std::unordered_map<LHCb::Measurement::Type,AIDA::IHistogram1D*,EnumClassHash> m_hitResidualPullPerDet;

};
#endif // HLT1TRACKMONITOR_H
