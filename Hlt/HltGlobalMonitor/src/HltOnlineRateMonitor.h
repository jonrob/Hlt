/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HLTGLOBALMONITOR_HLTONLINERATEMONITOR_H
#define HLTGLOBALMONITOR_HLTONLINERATEMONITOR_H 1

// Include files
#include <array>
#include <functional>

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/GaudiException.h"
#include "DetDesc/Condition.h"

#include "Kernel/IHltMonitorSvc.h"

#include "LoKi/CoreCuts.h"
#include "LoKi/OdinTypes.h"
#include "LoKi/L0Types.h"
#include "LoKi/HLTTypes.h"

#include "HltDAQ/HltEvaluator.h"

/** @class HltOnlineRateMonitor HltOnlineRateMonitor.h
 *
 *
 *  @author Gerhard Raven
 *  @date   2008-07-29
 */
class HltOnlineRateMonitor : public HltEvaluator {
public:
   /// Standard constructor
   HltOnlineRateMonitor( const std::string& name, ISvcLocator* pSvcLocator );

   ~HltOnlineRateMonitor() override;

   StatusCode initialize() override;
   StatusCode execute   () override;

protected:

   /// Decode
   StatusCode decode() override;

private:

   std::string m_stage;
   std::map<std::string, std::string> m_rates;
   std::map<std::string, std::vector<std::string>> m_combined;

   void zeroEvaluators();
   void updateRates( Property& /* p */ );

   std::unordered_map<std::string, EvalVariant> m_evaluators;
   std::map<std::string, std::string> m_stages;

   std::unordered_map<std::string, std::pair<std::vector<std::string>,
                                             HltEval>> m_combEvals;


};
#endif // HLTGLOBALMONITOR_HLTONLINERATEMONITOR_H
