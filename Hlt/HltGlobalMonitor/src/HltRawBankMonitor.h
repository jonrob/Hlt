/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: HltRawBankMonitor.h,v 1.1 2009-03-09 21:18:07 jvantilb Exp $
#ifndef HLTRAWBANKMONITOR_H
#define HLTRAWBANKMONITOR_H 1

// STD & STL
#include <string>
#include <unordered_set>
#include <unordered_map>

// DetDesc
#include "DetDesc/Condition.h"

// GaudiKernel
#include "GaudiKernel/IIncidentListener.h"

// from Gaudi
#include "GaudiAlg/GaudiHistoAlg.h"

/** @class HltRawBankMonitor HltRawBankMonitor.h
 *
 *  Algorithm that monitors raw bank types
 *
 *  @author Roel Aaij
 *  @date   2016-10-18
 *
 */
class HltRawBankMonitor : public extends1<GaudiHistoAlg, IIncidentListener> {
public:

   /// Standard constructor
   HltRawBankMonitor(const std::string& name, ISvcLocator* pSvcLocator);

   StatusCode execute() override;    ///< Algorithm execution
   StatusCode initialize() override; ///< Algorithm initialization

private:

   StatusCode times(double& t, double& w, double& et) const;

   StatusCode i_updateConditions();
   void handle(const Incident&) override;

   // Properties
   std::string m_inputLocation;          ///< Location in TES of the RawEvent
   std::vector<std::string> m_bankNames; ///< List of banks to look for.
   std::string m_odin_location;
   bool m_useCondDB;

   // data members
   Condition *m_runpars;
   mutable unsigned long long m_startOfRun = 0;
   double m_binWidth; // in seconds!
   double m_timeSpan; // in seconds!

   /// Internal enum vector selected bank types
   std::unordered_set<int> m_bankTypes;

   ///< Histogram to monitor banks
   AIDA::IHistogram1D* m_histogram = nullptr;

   ///< Bin indices and counters to count banks
   std::unordered_map<int, std::tuple<size_t, StatEntity*, AIDA::IHistogram1D*>> m_counters;


};
#endif // HLTRAWBANKMONITOR_H
