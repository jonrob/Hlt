/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include <vector>
#include <map>

// from Boost
#include <boost/format.hpp>
#include <boost/algorithm/string.hpp>

#include <range/v3/view/transform.hpp>

// from Gaudi
#include "GaudiKernel/IIncidentSvc.h"
#include "AIDA/IHistogram1D.h"

// Event
#include "Event/RawEvent.h"
#include "Event/RawBank.h"
#include "Event/HltDecReports.h"
#include "Event/L0DUReport.h"
#include "Event/ODIN.h"

// Hlt Interfaces
#include "Kernel/RateCounter.h"

// local
#include "HltOnlineRateMonitor.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltOnlineRateMonitor
//
// 2016-05-01 : Roel Aaij
//-----------------------------------------------------------------------------

namespace {
   using std::pair;
   using std::string;
   using std::map;
   using std::vector;
   using std::set;
   using std::unordered_map;

   namespace view = ranges::view;
}

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( HltOnlineRateMonitor )

//=============================================================================
StatusCode HltOnlineRateMonitor::decode() {
   zeroEvaluators();

   // Create the right type of evaluator and build it
   auto build = [this](const string t, const string expr) -> pair<string, StatusCode> {
     if (expr.empty()) return make_pair(t, StatusCode(StatusCode::SUCCESS));

      auto stageIt = m_stages.find(t);
      auto stage = (stageIt != m_stages.end()) ? stageIt->second : m_stage;

      string title = boost::str(boost::format("%s:%s") % t % expr);
      string htitle = boost::str(boost::format("HltRate_%s") % t);

      decltype(m_evaluators)::iterator it; bool placed{false};
      if (stage == "ODIN") {
         std::tie(it, placed) = m_evaluators.emplace(title, ODINEval{m_odin_location});
      } else if (stage == "L0") {
         std::tie(it, placed) = m_evaluators.emplace(title, L0Eval{m_l0_location});
      } else if (stage == "HLT1") {
         std::tie(it, placed) = m_evaluators.emplace(title, HltEval{m_hlt_location[0]});
      } else if (stage == "HLT2") {
         std::tie(it, placed) = m_evaluators.emplace(title, HltEval{m_hlt_location[1]});
      } else {
         return make_pair(t, Error(string{"Bad stage: "} + stage, StatusCode::FAILURE));
      }
      assert(placed && it->first == title);
      auto builder = Builder{this, expr, title, htitle};
      return make_pair(title, std::visit(builder, it->second));
   };

   map<string, string> titles;

   // Build the routing bits
   for (const auto& entry : m_rates) {
      auto r = build(entry.first, entry.second);
      if (!r.second.isSuccess()) return r.second;
      titles.emplace(entry.first, r.first);
   }

   // Build the routing bits
   for (const auto& entry : m_combined) {
      string htitle = boost::str(boost::format("HltCombRate_%s") % entry.first);
      auto builder = Builder{this, "HLT_TRUE", entry.first, htitle};
      decltype(m_combEvals)::iterator it; bool placed = false;
      vector<string> keys = entry.second
         | view::transform([&titles](string t) { return titles[t]; });
      auto val = make_pair(std::move(keys), HltEval{m_hlt_location[0]});
      std::tie(it, placed) = m_combEvals.emplace(entry.first, std::move(val));
      if (!placed) return StatusCode::FAILURE;
      auto sc = builder(it->second.second);
      if (!sc.isSuccess()) return sc;
   }

   m_evals_updated = false;
   m_preambulo_updated = false;
   return StatusCode::SUCCESS;
}

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
HltOnlineRateMonitor::HltOnlineRateMonitor( const string& name,
                                            ISvcLocator* pSvcLocator)
   : HltEvaluator( name , pSvcLocator )
{
   declareProperty("Rates", m_rates)->declareUpdateHandler( &HltOnlineRateMonitor::updateRates, this);
   declareProperty("CombinedRates", m_combined)->declareUpdateHandler( &HltOnlineRateMonitor::updateRates, this);
   declareProperty("Stages", m_stages);
   declareProperty("Stage", m_stage, "ODIN, L0, HLT1 or HLT2");
}

//=============================================================================
HltOnlineRateMonitor::~HltOnlineRateMonitor()
{
   zeroEvaluators();
}

//=============================================================================
void HltOnlineRateMonitor::zeroEvaluators() {
   Deleter deleter;
   for (auto& entry : m_evaluators) {
      std::visit(deleter, entry.second);
   }
}

//=============================================================================
// update handlers
//=============================================================================
void HltOnlineRateMonitor::updateRates( Property& /* p */ )
{
   /// mark as "to-be-updated"
   m_evals_updated = true;
   // no action if not yet initialized
   if (Gaudi::StateMachine::INITIALIZED > FSMState()) return;
   // postpone the action
   if ( !m_preambulo_updated ) return;
   // perform the actual immediate decoding
   StatusCode sc = decode();
   Assert(sc.isFailure(), "Error from HltOnlineRateMonitor::decode()", sc);
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode HltOnlineRateMonitor::initialize() {
   auto sc = HltEvaluator::initialize(); // must be executed first
   if (!sc.isSuccess()) return sc;

   // Check if the Stage property is either Hlt1 or Hlt2
   if (!set<string>{{"ODIN", "L0", "HLT1", "HLT2"}}.count(m_stage)) {
      sc = Error(string{"Stage property must be ODIN, L0, Hlt1 or Hlt2, while it is '"} +
                 m_stage + "'.", StatusCode::FAILURE);
   }

   return sc;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode HltOnlineRateMonitor::execute() {
   StatusCode sc = StatusCode::SUCCESS;
   if (m_evals_updated || m_preambulo_updated) {
      sc = decode();
      Error(" Unable to Decode ???? ", sc).ignore();
      if (!sc.isSuccess()) return sc;
   }

   // Get the fill time, weight and event time
   double t = 0, weight = 0, evt_time = 0;
   sc = times(t, weight, evt_time);
   if (!sc.isSuccess()) return sc;

   // Create the evaluator
   Evaluator evaluator{this, t, weight, evt_time};

   unordered_map<string, bool> results;

   // Evaluate stuff
   for (auto& entry : m_evaluators) {
      results[entry.first] = std::visit(evaluator, entry.second);
   }

   // Evaluate the combined stuff
   for (const auto& entry : m_combEvals) {
      auto& combEval = entry.second.second;
      bool result = true;
      for (const auto& key : entry.second.first) {
         result &= results[key];
      }
      *(combEval.counter) += result;
      if (result) {
         combEval.hist->fill(t, weight);
         if (combEval.rate) combEval.rate->count(evt_time);
      }
   }

   return sc;
}
