/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HLTRECCHECKVERTICES_H
#define HLTRECCHECKVERTICES_H 1

// Include files
#include "HltBase/HltSelectionContainer.h"
#include "HltBase/HltAlgorithm.h"
#include "Event/MCParticle.h"
#include "ERelations.h"

/** @class HltRecCheckVertices HltRecCheckVertices.h
 *
 *
 *  @author Jose A. Hernando
 *  @date   2006-05-24
 */
class HltRecCheckVertices : public HltAlgorithm {
public:
  /// Standard constructor
  HltRecCheckVertices( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

protected:

  void relateVertices();

  void checkVertices();

private:

  std::string m_linkName;

  std::string m_TESInputVerticesName;
  LHCb::RecVertices* m_TESInputVertices = nullptr;

  Hlt::SelectionContainer<LHCb::Track,LHCb::Track> m_selections { *this };


  AIDA::IHistogram1D* m_histoNRCV = nullptr;
  AIDA::IHistogram1D* m_histoNMCV = nullptr;
  AIDA::IHistogram1D* m_histoNDV = nullptr;

  AIDA::IHistogram1D* m_histoX = nullptr;
  AIDA::IHistogram1D* m_histoY = nullptr;
  AIDA::IHistogram1D* m_histoZ = nullptr;

  AIDA::IHistogram1D* m_histoDX = nullptr;
  AIDA::IHistogram1D* m_histoDY = nullptr;
  AIDA::IHistogram1D* m_histoDZ = nullptr;

  zen::relation<LHCb::Track*,LHCb::MCVertex*> m_relTrackMCVertex;
  zen::relation<LHCb::MCVertex*,LHCb::Track*> m_relMCVertexTrack;

  zen::relation<LHCb::RecVertex*,LHCb::MCVertex*> m_relVertexMCVertex;
  zen::relation<LHCb::MCVertex*,LHCb::RecVertex*> m_relMCVertexVertex;



};
#endif // HLTMCMONITOR_H
