/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HLT2SELREPORTSMAKER_H
#define HLT2SELREPORTSMAKER_H 1

// STD & STL
#include <string>

// Include files
#include "HltSelReportsMaker.h"

// Forward declarations
namespace LHCb {
   class HltSelReports;
   class HltDecReports;
}

/** @class Hlt2SelReportsMaker Hlt2SelReportsMaker.h
 *
 *
 *  @author Roel Aaij
 *  @date   2015-10-27
 */
class Hlt2SelReportsMaker : public HltSelReportsMaker {
public:
   /// Standard constructor
   Hlt2SelReportsMaker(const std::string& name,
                       ISvcLocator* svcLocator);

   virtual ~Hlt2SelReportsMaker( ); ///< Destructor

protected:

   StatusCode postExecute(LHCb::HltSelReports* outputSummary,
                          const LHCb::HltDecReports* decReports) override;


private:

   std::string m_RecSummaryLoc;
   std::string m_summaryName;

};
#endif // HLT2SELREPORTSMAKER_H
