/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/HltDecReports.h"
#include "GaudiAlg/Producer.h"
#include "Kernel/TCK.h"
#include <algorithm>

class HltDecReportsCreator
    : public Gaudi::Functional::Producer<LHCb::HltDecReports()> {
public:
  HltDecReportsCreator(const std::string &name, ISvcLocator *svcLoc)
      : Producer(name, svcLoc,
                 KeyValue("HltDecReportsLocation",
                          {LHCb::HltDecReportsLocation::Default})){};

  LHCb::HltDecReports operator()() const override;

private:
  Gaudi::Property<TCK> m_tck{this, "TCK"};
};

LHCb::HltDecReports HltDecReportsCreator::operator()() const {
  LHCb::HltDecReports outputs;
  outputs.setConfiguredTCK(m_tck.value().uint());
  return outputs;
}

DECLARE_COMPONENT(HltDecReportsCreator)