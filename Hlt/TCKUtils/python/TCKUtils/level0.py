###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function

__all__ = [
    'getL0Config',
    'getL0Configs',
    'getL0Channels',
    'getL0DUOptions',
    'dumpL0',
    'getL0Prescales',
    'updateL0TCK',
]

import re
import itertools
import operator
from multiprocessing.managers import BaseManager

import helpers
import trees
import backend
from backend import default_cas, accept_id
import backend
from .pureutils import tck_to_int

import cppyy
LHCb = cppyy.gbl.LHCb
SmartRef = cppyy.gbl.SmartRef


def _parseL0Setting(setting):
    """Parse a line from the L0 settings.

    Example:
        >>> s = ['name=[DiMuon]', 'rate==[100]', 'conditions= [Spd_DiMu(Mult)<900]  && [Muon12(Pt)>900]', 'MASK=[001]']
        >>> _parseL0setting(s)
        {'MASK': '001', 'conditions': ['Spd_DiMu(Mult)<900', 'Muon12(Pt)>900'], 'name': 'DiMuon', 'rate': '100'}

    """
    p = re.compile(r'([^ ]+) *= *(\[.+\])')
    result = {}
    for s in setting:
        key, value = p.match(s).group(1, 2)
        values = [re.match(r'^ *\[(.*)\] *$', i).group(1)
                  for i in value.split('&&')]
        # adapt to ideosyncracies
        if key == 'rate=':
            key = 'rate'
        if key == 'conditions':
            result[key] = values
        else:
            assert len(values) == 1
            result[key] = values[0]
    return result


def _parseL0Settings(settings):
    """Parse multiple L0 settings.

    Example:
        >>> ss = [['name=[Electron(Et)>20]', 'data=[Electron(Et)]', 'comparator=[>]', 'threshold=[20]'],
                  ['name=[Electron(Et)>50]', 'data=[Electron(Et)]', 'comparator=[>]', 'threshold=[50]']]
        >>> _parseL0settings(ss)
        {'Electron(Et)>20': {'comparator': '>', 'data': 'Electron(Et)', 'name': 'Electron(Et)>20', 'threshold': '20'},
         'Electron(Et)>50': {'comparator': '>', 'data': 'Electron(Et)', 'name': 'Electron(Et)>50', 'threshold': '50'}}

    """
    parsed = (_parseL0Setting(line) for line in settings)
    return {s['name']: s for s in parsed}


def _parseL0Config(leaf):
    """Parse L0DUConfigProvider PropCfg."""
    return {
        'TCK': leaf.props['TCK'],
        'Conditions': _parseL0Settings(eval(leaf.props['Conditions'])),
        'Channels': _parseL0Settings(eval(leaf.props['Channels'])),
    }


@accept_id
def getL0Config(tree):
    """Return the L0 configuration.

    If there are multiple L0 configurations an exception is raised.

    Args:
        tree (obj): Configuration tree or id.

    """
    l0 = trees.findInTree(tree, type='^L0DUConfigProvider$')
    return _parseL0Config(l0.leaf)


@accept_id
def getL0Configs(tree):
    """Return all L0 configurations in a dictionary.

    Args:
        tree (obj): Configuration tree or id.

    """
    l0s = trees.matchInTree(tree, type='^L0DUConfigProvider$')
    parsed = (_parseL0Config(l0.leaf) for l0 in l0s)
    return {i['TCK']: i for i in parsed}


@accept_id
def getL0Channels(tree):
    """Return the list of L0 channels.

    Args:
        tree (obj): Configuration tree or id.

    """
    return sorted(getL0Config(tree)['Channels'].keys())


@accept_id
def dumpL0(tree, file=None):
    """Print all L0 configurations for the given tree/id.

    Args:
        tree (obj): Configuration tree or id.
        file (str or file): Filename or file-like object.

    """
    l0s = getL0Configs(tree)
    with helpers.smart_open(file) as f:
        for l0 in l0s.itervalues():
            f.write('{:*^80}\n'.format(' TCK = {} '.format(l0['TCK'])))
            f.write('{:*^80}\n'.format(' Channels '))
            for name, value in l0['Channels'].iteritems():
                f.write('{}: {}\n'.format(name, value))
            f.write('{:*^80}\n'.format(' Conditions '))
            for name, value in l0['Conditions'].iteritems():
                f.write('{}: {}\n'.format(name, value))
            f.write('{:*^80}\n'.format(''))


@accept_id
def getL0Prescales(tree):
    """Return the prescales for all L0 configurations for the given tree/id.

    Args:
        tree (obj): Configuration tree or id.

    """
    l0s = getL0Configs(tree)
    return {tck: {name: ch['rate'] for name, ch in l0['Channels'].iteritems()}
            for tck, l0 in l0s.iteritems()}


def getL0DUOptions(l0tck):
    """Obtain the L0DU options as given in the .opts files.

    Args:
        l0tck (str/int): L0 TCK.

    Returns:
        A dictionary of property names and values.

    """
    l0tck = '0x{:04X}'.format(tck_to_int(l0tck))
    from Gaudi.Configuration import importOptions
    importOptions('$L0TCK/L0DUConfig.opts')
    from Configurables import L0DUMultiConfigProvider, L0DUConfigProvider
    if l0tck not in L0DUMultiConfigProvider('L0DUConfig').registerTCK:
        raise KeyError('Requested L0 TCK {} is not known to TCK/L0TCK'.format(l0tck))
    configProvider = L0DUConfigProvider('ToolSvc.L0DUConfig.TCK_' + l0tck)
    l0config = configProvider.getValuedProperties()
    l0config['TCK'] = l0tck
    return l0config


def updateL0TCK(id, l0tck, label, cas=default_cas, extra=None):
    """Update the L0 TCK for a given configuration.

    Args:
        id (str/int): Configuration id or TCK.
        l0tck (str/int): New L0 TCK.
        label (str): Label for the new configuration.
        cas (obj): Writable ConfigAccessSvc.
        extra (dict): Dictionary of extra properties to be updated.

    Returns:
        String id (digest) of the updated configuration.

    """
    if not label:
        raise ValueError('Provide a reasonable label for the new configuration')
    l0config = getL0DUOptions(l0tck)
    ret = backend.AccessProxy().access(cas).rupdateL0TCK(id, l0config, label, extra)
    if ret:
        backend.AccessProxy().flush()
    return ret


class RemoteL0DUConfig(object):
    def __init__(self):
        import logging
        from Gaudi.Configuration import importOptions, ERROR, log
        from Configurables import ApplicationMgr, LHCbApp
        import GaudiPython
        log.setLevel(logging.ERROR)
        importOptions('$L0TCK/L0DUConfig.opts')
        LHCbApp()
        ApplicationMgr().AppName = ""
        ApplicationMgr().OutputLevel = ERROR
        app = GaudiPython.AppMgr()
        app.initialize()
        self._app = app

    def config(self, tck):
        provider = self._app.toolsvc().create('L0DUConfigProvider/L0DUConfig.TCK_' + tck,
                                              interface='IL0DUConfigProvider')
        config = provider.config(int(tck, 16))
        return config


class L0DUConfigManager(BaseManager):
    pass
L0DUConfigManager.register('L0DUConfig', RemoteL0DUConfig)


def getL0DUConfig(l0tck):
    """Return the L0DUConfig for a given L0 TCK."""
    import atexit
    if not getL0DUConfig._remote:
        manager = L0DUConfigManager()
        manager.start()
        getL0DUConfig._remote = manager.L0DUConfig()
        atexit.register(manager.shutdown)
    return getL0DUConfig._remote.config(l0tck)
getL0DUConfig._remote = None


def createL0DUReport(l0tck):
    """Return an empty L0DUReport given a L0 TCK."""
    try:
        report = createL0DUReport._reports[l0tck]
    except KeyError:
        config = getL0DUConfig(l0tck)
        report = LHCb.L0DUReport()
        report.setConfiguration(SmartRef(LHCb.L0DUConfig)(config))
        createL0DUReport._reports[l0tck] = report
    # Return a copy of the cached empty report
    return LHCb.L0DUReport(report)
createL0DUReport._reports = {}


def dummyL0DUReport(l0tck, channels):
    report = createL0DUReport(l0tck)
    decision = 0
    for ch in channels:
        report.setChannelDecision(ch, True)
        decision |= report.configuration().channels()[ch].decisionType()
    report.setDecisionValue(decision)
    return report


class L0_TRUE_DATA(object):
    """Dummy L0_DATA-like functor that always gives L0_TRUE when used."""

    def __init__(self, *args, **kwargs):
        from LoKiHlt.decorators import L0_TRUE
        self.L0_TRUE = L0_TRUE
        pass

    def __lt__(self, other):
        return self.L0_TRUE
    __le__ = __eq__ = __ne__ = __gt__ = _ge__ = __lt__


def defaultL0Overrides():
    return {'L0_DATA': L0_TRUE_DATA}


def applyL0DUFunctor(l0tck, functor):
    """Return the list of channels that pass the L0DU filter.

    Args:
        l0tck (str): L0 TCK in hex representation.
        functor (obj): L0DU functor.

    """
    config = getL0DUConfig(l0tck)
    return [ch for ch, _ in config.channels()
            if functor(dummyL0DUReport(l0tck, [ch]))]

ODIN_EVENT_TYPES = [
    'VeloOpen',
    'Physics',
    'NoBias',
    'Lumi',
    'Beam1Gas',
    'Beam2Gas',
    # 'et_bit_06',
    # 'et_bit_07',
    # 'et_bit_08',
    # 'et_bit_09',
    # 'TriggerMaskPhysics',
    # 'TriggerMaskNoBias',
    # 'TriggerMaskBeam1Gas',
    # 'TriggerMaskBeam2Gas',
    'SequencerTrigger',
    # 'HltFlaggingMode',
]
ODIN_BX_TYPES = [(0, 'ee'), (1, 'be'), (2, 'eb'), (3, 'bb')]


def dummyODIN(eventtypes, bxtype):
    evttyp = reduce(operator.or_, (getattr(LHCb.ODIN, name)
                    for name in eventtypes))
    odin = LHCb.ODIN()
    odin.setEventType(evttyp)
    odin.setBunchCrossingType(bxtype)
    return odin


def applyODINFunctor(functor, eventtypes=[]):
    """Return the list of event types that pass the ODIN filter.

    Args:
        functor (obj): ODIN functor.
        eventtypes (list): A list of event types to consider.
            If empty (default), all known trigger event types are used.

    """
    def passes(ets, bxs=range(4)):
        return any(functor(dummyODIN(ets, bx)) for bx in bxs)

    allflags = ['VeloOpen']
    eventtypes = eventtypes or ODIN_EVENT_TYPES[1:]
    results = []
    for et in eventtypes:
        bxs = [name for bx, name in ODIN_BX_TYPES
               if passes([et] + allflags, [bx])]
        flags = {f: (passes([et]), passes([et, f])) for f in allflags}
        flags = [('!' if p0 else '') + f
                 for f, (p0, p1) in flags.iteritems() if not (p0 and p1)]
        # TODO maybe we should the combination of all flags (i.e. VeloOpen + bb, etc.)
        if bxs:
            results.append('{}:{}:{}'.format(et, ','.join(bxs), ','.join(flags)))
    return results


def _l0masks_to_eventtypes(masks):
    mapping = ['Beam2Gas', 'Beam1Gas', 'Physics']
    return set(mapping[i] for i, bits in enumerate(zip(*masks))
               if any(map(int, bits)))
