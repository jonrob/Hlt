###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import re
import contextlib
import trees


@contextlib.contextmanager
def smart_open(filename=None):
    if not filename:
        import sys
        fh, close = sys.stdout, False
    elif isinstance(filename, basestring):
        fh, close = open(filename, 'w'), True
    else:
        fh, close = filename, False

    try:
        yield fh
    finally:
        if close:
            fh.close()


def findHybridFilterFactory(tree, name):
    """Return the factory of a LoKi hybrid filter.

    Args:
        name (obj): Factory name

    """
    type_, name = re.match(r'^(.*)/(.*):PUBLIC$', name).group(1, 2)
    return trees.findInTree(tree, type=r"^{}$".format(type_),
                            name=r"^ToolSvc\.{}$".format(name)).leaf


def defaultFunctorOverrides():
    from LoKiNumbers.decorators import FALL, FNONE
    from LoKiHlt.functions import L0_TRUE
    return {
        'L0_ODINCUT': lambda x: L0_TRUE,
        'SKIP': lambda x: FALL,
        'SCALE': lambda x: FALL if x > 0 else FNONE,
        'RATE': lambda x: FALL if x > 0 else FNONE,
    }


def functorFromFactory(code, preambulo, factory, overrides=None):
    """Return an evaluatable functor corresponding to the hybrid filter.

    Args:
        code (str): Functor code.
        preambulo (list): List of preambulo lines.
        factory (obj): Factory
        tree (obj): Configuration tree where factories are searched.
        overrides (dict): Override variables in the functors' environment.

    """
    locals_ = defaultFunctorOverrides()
    if overrides:
        locals_.update(overrides)
    globals_ = {}

    modules = eval(factory.properties()['Modules'])
    for module in modules:
        exec('from {} import *'.format(module), globals_)
    lines = eval(factory.properties()['Lines'])
    exec('\n'.join(lines), globals_)

    exec('\n'.join(preambulo), globals_)

    return eval(code, globals_, locals_)


def functorFromHybridFilter(hfilter, tree, overrides=None):
    """Return an evaluatable functor corresponding to the hybrid filter.

    Args:
        hfilter (obj): Hybrid filter leaf.
        tree (obj): Configuration tree where factories are searched.
        overrides (dict): Override variables in the functors' environment.

    """
    if not hfilter:
        return lambda x: True

    factory = findHybridFilterFactory(tree, hfilter.properties()['Factory'])
    return functorFromFactory(
        hfilter.properties()['Code'], eval(hfilter.properties()['Preambulo']),
        factory, overrides=overrides)


def functorFromHltCode(code, tree):
    if 'Hlt1' in code:
        factory_name = 'LoKi::Hybrid::HltFactory/Hlt1HltFactory:PUBLIC'
    elif 'Hlt2' in code:
        factory_name = 'LoKi::Hybrid::HltFactory/Hlt2HltFactory:PUBLIC'
    else:
        factory_name = 'LoKi::Hybrid::HltFactory/HltFactory:PUBLIC'

    factory = findHybridFilterFactory(tree, factory_name)
    return functorFromFactory(code, [], factory)
