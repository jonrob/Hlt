###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import GaudiPython
from Gaudi.Configuration import*
from Configurables import ConfigStackAccessSvc, ConfigTarFileAccessSvc, ConfigFileAccessSvc, ConfigTreeEditor, PropertyConfigSvc

from TCKUtils.utils import *


def createMCversion( tck, newtck = None, cas = ConfigTarFileAccessSvc() ) :
    if not newtck : newtck = tck | 0x40000000
    props = getProperties(tck,'DeterministicPrescaler/.*','AcceptFraction', cas=cas)
    exceptions = { 'LumiStripper.*'     : '0',
         '.*Debug.*'          : None,
         'Hlt1ODINTechnical.*': None,
         'Hlt1Lumi.*'         : None,
         'Hlt1L0.*'           : None,
         'Hlt1MBMicroBias.*'  : None}
    update = { } 
    for n,v in props.iteritems() :
       v = v['AcceptFraction']
       if float(v) in [0., 1.]: continue  # do not modify these
       vorig = v
       m = 0  # count # of matching exceptions
       for (e,o) in exceptions.iteritems() :
          import re
          if re.match(e,n) : 
             if o : v = o
             m = m+1
       if m == 0 : v = '1'   # if no exception, put at 1
       if vorig == v : continue  # if not 
       update[ n ] = { 'AcceptFraction' : '%s'%v }

    # Always clone associated CALO objects for Turbo objects
    props = getProperties(tck, 'Calo(Cluster|Hypo)Cloner', 'Clone(Clusters|Digits|Entries)Always', cas=cas)
    for n,v in props.iteritems():
        update[ n.split('/')[-1] ] = {prop: True for prop in v}

    from pprint import pprint
    print "==================> dump update <===================="
    pprint(update)
    label = None
    for (i,j) in getConfigurations(cas).iteritems() :
       if tck in j['TCK'] : label = j['label']
    id  = updateProperties( resolveTCK(tck,cas),update,label='%s - MC version of 0x%08x' % (label,tck), cas=cas)
    createTCKEntries({newtck:id},cas)


    print "==================> diff data / MC TCK <===================="
    diff(tck, newtck, cas=cas)
