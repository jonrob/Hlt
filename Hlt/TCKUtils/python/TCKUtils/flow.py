###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function

__all__ = [
    'getLineTree',
    'getLineTrees',
    'getLineInputFilters',
    'getHltLineInputs',
    'getRoutingBitInputs',
]

import re
from functools import wraps
from collections import OrderedDict

import trees
import level0
import helpers
from backend import accept_id, default_cas, getConfigTree


_LINE_FILTER_TYPES = {
    'ODIN': 'LoKi::ODINFilter',
    'L0DU': 'LoKi::L0Filter',
    'HLT': 'LoKi::HDRFilter',
    'HLT1': 'LoKi::HDRFilter',
    'HLT2': 'LoKi::HDRFilter',
    'TISTOS': 'TisTosParticleTagger',
}

_LINE_FILTER_ALLOW_MULTIPLE = {
    'ODIN': False,
    'L0DU': False,
    'HLT': False,
    'HLT1': False,
    'HLT2': False,
    'TISTOS': True,
}


@accept_id
def getLineTree(tree, line):
    """Return the subtree for a line with a given name.

    Args:
        tree (obj): Configuration tree or id.
        line (str): Line name.

    """
    try:
        return trees.findInTree(tree, type='^Hlt::Line$', name='^{}$'.format(line))
    except KeyError:
        raise KeyError('Line "{}" not found'.format(line))


@accept_id
def getLineTrees(tree, parent_name=None):
    """Return a list of trees for all (some) lines.

    Args:
        tree (obj): Configuration tree or id.
        parent_name (str): Name of a parent to restrict the results (e.g. Hlt1)

    """
    if parent_name:
        try:
            subtree = trees.findInTree(tree, name='^' + parent_name + '$')
        except KeyError:
            return []
    return list(trees.matchInTree(subtree, type='^Hlt::Line$'))


def _getLineInputFilters(linetree, which):
    """Return the leafs of all filters identified by `which`."""
    props = linetree.leaf.properties()
    if which in props:  # `which` is a property of the Hlt::Line
        if not props[which]:  # no filter was specified
            return []
        # Find the sequence where the filter is
        tree = trees.findInTree(linetree, fqn='^' + props[which] + ' ')
    else:  # search in the full line tree
        tree = linetree
    # Find the filter algorithms
    matches = trees.matchInTree(tree, type='^' + _LINE_FILTER_TYPES[which] + '$')
    leafs = [i.leaf for i in matches]
    return leafs


@accept_id
def getLineInputFilters(tree, line):
    """Return a dict of filter component leaf(s) for a given line.

    Normally, the returned values are a single component with the
    exception of the TISTOS, where a list is returned.

    Args:
        tree (obj): Configuration tree or id.
        line (str): Line name or type/name.

    """
    linetree = getLineTree(tree, line)
    filters = {}
    for which in _LINE_FILTER_TYPES:
        leafs = _getLineInputFilters(linetree, which)
        if _LINE_FILTER_ALLOW_MULTIPLE[which]:
            filters[which] = leafs
        else:
            if len(leafs) > 1:
                raise RuntimeError("Found more than one {} filter: {}"
                                   .format(which, [x.fqn() for x in leafs]))
            filters[which] = leafs[0] if leafs else None
    return filters


def dummyDecReports(decName, turbo, stage=0, eb=0, noc=1, decID=1001):
    from cppyy.gbl import LHCb
    if turbo:
        stage = stage | 0x80
    dr = LHCb.HltDecReport(True, stage, eb, noc, decID)
    drs = LHCb.HltDecReports()
    drs.insert(decName, dr)
    return drs


def applyHDRFunctor(linetrees, functor):
    """Yield the input line trees that pass the HDR filter functor.

    Args:
        linetrees (iterable): List of trees of lines.
        functor (obj): HDR filter functor.

    """
    if not functor:
        for t in linetrees:
            yield t
        raise StopIteration()
    for t in linetrees:
        decname = t.leaf.properties()['DecisionName']
        turbo = eval(t.leaf.properties()['Turbo'])
        drs = dummyDecReports(decname, turbo)
        if functor(drs):
            yield t


def _parseTisTosSpecs(specs):
    """Parse TisTosSpecs.

    Parse according to the logic in TisTosParticleTagger. Normally,
    an event passes if _any_ filter passess for _all_ stages.

    """
    res = {'L0': [], 'HLT1': [], 'HLT2': []}
    for spec, key in eval(specs).iteritems():
        assert key == 0
        triggerInput, tisTosSpec = spec.split('%')
        triggerStage = 'HLT1'
        if 'Hlt2' in triggerInput:
            triggerStage = 'HLT2'
        elif 'L0' in triggerInput:
            triggerStage = 'L0'
        res[triggerStage].append((triggerInput, tisTosSpec))
        if len(res[triggerStage]) > 1:
            raise NotImplementedError('More than one TISTOS spec for a stage?\n'
                                      'Specs are: {}'.format(specs))
    return res


def prettyTisTosFilters(tistosfilters, stage):
    x = []
    for tistosfilter in tistosfilters:
        props = tistosfilter.properties()
        specs = _parseTisTosSpecs(props['TisTosSpecs'])
        x.extend(specs[stage])
    return map(str, x)


def applyTisTosHltFilter(linetrees, tistosfilter, stage):
    """Yield the input line trees that pass the TISTOS filter.

    Args:
        linetrees (iterable): List of trees of lines.
        tistosfilter (obj): TisTosParticleTagger leaf.
        stage (str): HLT stage ("HLT1" or "HLT2")

    """
    if stage not in ['HLT1', 'HLT2']:
        raise ValueError('Stage must be either HLT1 or HLT2')
    props = tistosfilter.properties()
    if props['PassOnAll'] == 'True':
        for t in linetrees:
            yield t
        raise StopIteration()
    assert props['NoRegex'] == 'False'
    assert props['SatisfiedOnFirstSpec'] == 'False'
    tistos = _parseTisTosSpecs(props['TisTosSpecs'])
    for t in linetrees:
        decname = t.leaf.properties()['DecisionName']
        if any(tistos[k] for k in ['HLT1', 'HLT2'] if k != stage):
            raise NotImplementedError("Don't know how to deal with tistos specs "
                                      "having multiple stages: {}".format(tistos))
        if any(re.match(i[0], decname) for i in tistos[stage]) or not tistos[stage]:
            yield t


def applyTisTosHltFilters(linetrees, tistosfilters, stage):
    """Yield the input line trees that pass all TISTOS filters.

    In general, we don't know that a line decision is the logical and of all
    TisTos filters, so this is a heuristic.

    Args:
        linetrees (iterable): List of trees of lines.
        tistosfilters (list): List of TisTosParticleTagger leafs.
        stage (str): HLT stage ("HLT1" or "HLT2")

    """
    for f in tistosfilters:
        linetrees = applyTisTosHltFilter(linetrees, f, stage=stage)
    return linetrees


def applyTisTosL0Filter(l0channels, tistosfilter):
    """Yield the l0 channels that pass the TISTOS filter.

    Args:
        l0channels (list): List of L0 channel names.
        tistosfilter (obj): TisTosParticleTagger leaf.

    """
    props = tistosfilter.properties()
    if props['PassOnAll'] == 'True':
        for t in linetrees:
            yield t
        raise StopIteration()
    assert props['NoRegex'] == 'False'
    assert props['SatisfiedOnFirstSpec'] == 'False'
    tistos = _parseTisTosSpecs(props['TisTosSpecs'])
    for ch in l0channels:
        decname = 'L0{}Decision'.format(ch)
        if any(re.match(i[0], decname) for i in tistos['L0']) or not tistos['L0']:
            yield ch


def applyTisTosL0Filters(l0channels, tistosfilters):
    """Yield the l0 channels that pass the all TISTOS filter.

    In general, we don't know that a line decision is the logical and of all
    TisTos filters, so this is a heuristic.

    Args:
        l0channels (list): List of L0 channel names.
        tistosfilters (list): List of TisTosParticleTagger leafs.

    """
    for f in tistosfilters:
        l0channels = list(applyTisTosL0Filter(l0channels, f))
    return l0channels


def _getHltLineInputs(tree, line, tree1=None, use_l0_masks=True, ret_filter=False):
    """Return the HLT inputs of an HLT line.

    Args:
        tree (obj): Configuration tree or id.
        line (str): Line name or type/name.
        tree1 (obj): Configuration tree or id containing the Hlt1 lines.
        use_l0_masks (bool): Whether to consider l0 masks for ODIN inputs.
        ret_filter (bool): Whether to return (sources, filter) or just sources.

    Returns:
        A dictionary {type: list of input sources} or
        {type: (list of input sources, formatted filter)}.

    """
    filters = getLineInputFilters(tree, line)
    inputs = {}

    odin_eventtypes = []
    if filters['L0DU']:
        l0 = level0.getL0Config(tree)
        enabled_channels = set(name for name, ch in l0['Channels'].iteritems()
                               if ch['MASK'] != '000')
        l0functor = helpers.functorFromHybridFilter(
            filters['L0DU'], tree, overrides=level0.defaultL0Overrides())
        l0channels = level0.applyL0DUFunctor(l0['TCK'], l0functor)
        l0channels = applyTisTosL0Filters(l0channels, filters['TISTOS'])
        l0channels = set(l0channels) & enabled_channels
        inputs['L0DU'] = (l0channels,
                          ' AND '.join([filters['L0DU'].properties()['Code']] +
                                        prettyTisTosFilters(filters['TISTOS'], 'L0')
                                        ))
        # The masks of the required L0DU channels imply requirements on the
        # ODIN event types
        if use_l0_masks:
            masks = [l0['Channels'][ch]['MASK'] for ch in l0channels]
            odin_eventtypes = level0._l0masks_to_eventtypes(masks)
    if filters['ODIN']:
        odinfunctor = helpers.functorFromHybridFilter(filters['ODIN'], tree)
        odin_evttyps = level0.applyODINFunctor(odinfunctor, odin_eventtypes)
        inputs['ODIN'] = (set(odin_evttyps), filters['ODIN'].properties()['Code'])
    if filters['HLT']:
        raise NotImplementedError("Don't know how to deal with HLT filters")
    if filters['HLT1']:
        lines1 = getLineTrees(tree1 or tree, 'Hlt1')
        if not lines1:
            raise ValueError("No Hlt1 lines found in the second TCK")
        functor = helpers.functorFromHybridFilter(filters['HLT1'], tree)
        matches = applyHDRFunctor(lines1, functor)
        matches = applyTisTosHltFilters(matches, filters['TISTOS'], stage='HLT1')
        inputs['HLT1'] = (set(i.leaf.name for i in matches),
                          ' AND '.join([filters['HLT1'].properties()['Code']] +
                                       prettyTisTosFilters(filters['TISTOS'], 'HLT1')
                                       ))
    if filters['HLT2']:
        lines2 = getLineTrees(tree, 'Hlt2')
        if not lines2:
            raise ValueError("No Hlt2 lines found in the TCK")
        functor = helpers.functorFromHybridFilter(filters['HLT2'], tree)
        matches = applyHDRFunctor(lines2, functor)
        matches = applyTisTosHltFilters(matches, filters['TISTOS'], stage='HLT2')
        inputs['HLT2'] = (set(i.leaf.name for i in matches),
                          ' AND '.join([filters['HLT2'].properties()['Code']] +
                                       prettyTisTosFilters(filters['TISTOS'], 'HLT2')
                                       ))
    return OrderedDict((i, inputs[i] if ret_filter else inputs[i][0])
                       for i in ['ODIN', 'L0DU', 'HLT1', 'HLT2'] if i in inputs)


@wraps(_getHltLineInputs)
def getHltLineInputs(id, line, id1=None, use_l0_masks=True, ret_filter=False, cas=default_cas):
    id1 = id1 or id
    tree = getConfigTree(id, cas=cas)
    tree1 = getConfigTree(id1, cas=cas)
    return _getHltLineInputs(tree, line, tree1, use_l0_masks, ret_filter)


@accept_id
def getRoutingBitInputs(tree, bit, ret_filter=False):
    """Return the inputs of a routing bit filter.

    Args:
        tree (obj): Configuration tree or id.
        bit (int): Routing bit.
        ret_filter (bool): Whether to return (sources, filter) or just sources.

    Returns:
        A dictionary {type: list of input sources} or
        {type: (list of input sources, formatted filter)}.

    """
    from .utils import getRoutingBits
    try:
        code = getRoutingBits(tree)[bit]
    except KeyError:
        raise ValueError("Routing bit {} not found".format(bit))

    inputs = {}
    if 'ODIN' in code:
        pass
        # inputs['ODIN'] = # TODO
    if 'L0' in code:
        pass
        # inputs['L0DU'] = # TODO
    for stage in ['Hlt1', 'Hlt2']:
        if stage in code:
            lines = getLineTrees(tree, stage)
            if not lines:
                raise ValueError("No {} lines found in tree".format(stage))
            functor = helpers.functorFromHltCode(code, tree)
            matches = applyHDRFunctor(lines, functor)
            inputs[stage.upper()] = (set(i.leaf.name for i in matches), code)
    return OrderedDict((i, inputs[i] if ret_filter else inputs[i][0])
                       for i in ['ODIN', 'L0DU', 'HLT1', 'HLT2'] if i in inputs)
