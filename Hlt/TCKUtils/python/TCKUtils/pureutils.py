###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__all__ = (
    'tck_to_int',
    'tck_to_str',
    'tck_flags',
)


def tck_to_int(x):
    """Return a TCK normalized to an int.

    Args:
        x: A TCK-like value to be normalized.

    """
    if isinstance(x, basestring):
        return int(x, 16)
    elif isinstance(x, int):
        if x <= 0 or x > 0xffffffff:
            raise ValueError('Integer {!r} is not a valid TCK'.format(x))
        return x
    else:
        raise ValueError('Cannot normalize {!r} ({}) to TCK'
                         .format(x, str(type(x))))


def tck_to_str(x):
    """Return a TCK normalized to a "0x12345678"  format.

    Args:
        x: A TCK-like value to be normalized.

    """
    return '{:#010x}'.format(tck_to_int(x))


def tck_flags(tck):
    """Interpret special TCK bits (flags).

    - bit 31 is assigned to "technical"
    - bit 30 is assigned to "for MC"
    - bit 29 is assigned to "HLT2 only"
    - bit 28 is assigned to "HLT1 only"
    - neither 29 nor 28 is "old style" HLT1 + HLT2

    Args:
        tck: A TCK-like value.

    Returns:
        A dict with the TCK flags.

    """
    tck = tck_to_int(tck)
    hlt1 = bool((tck >> 28) & 1)
    hlt2 = bool((tck >> 29) & 1)
    if hlt1 and hlt2:
        raise ValueError('TCK has both bit 28 and 29 set')
    elif not hlt1 and not hlt2:
        hlt1 = hlt2 = True
    return {
        'Hlt1': hlt1,
        'Hlt2': hlt2,
        'Split': '' if hlt1 and hlt2 else ('Hlt1' if hlt1 else 'Hlt2'),
        'MC': bool((tck >> 30) & 1),
        'Technical': bool((tck >> 31) & 1),
    }
