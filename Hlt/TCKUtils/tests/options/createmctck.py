###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os, errno
from TCKUtils.utils import (default_cas, ConfigCDBAccessSvc, ConfigStackAccessSvc,
                            updateProperties, createTCKEntries, diff)
from TCKUtils.createMCversion import createMCversion

try:
    os.remove('createmctck.cdb')
except OSError as e:
    if e.errno != errno.ENOENT:
        raise
cas_rw = ConfigCDBAccessSvc('WriteConfigAccessSvc', File='createmctck.cdb', Mode='ReadWrite')
cas = ConfigStackAccessSvc('StackConfigAccessSvc', ConfigAccessSvcs=[cas_rw, default_cas])

createMCversion(0x21611709, newtck=0x61661709, cas=cas)
