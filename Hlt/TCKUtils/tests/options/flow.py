###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import unittest
import itertools
from TCKUtils.utils import *


class TestFlowMethods(unittest.TestCase):
    TCK1 = 0x1138160f
    TCK2 = 0x2139160f

    def test_getLineInputFilters(self):
        expected_keys = set(['HLT', 'HLT1', 'HLT2', 'L0DU', 'ODIN', 'TISTOS'])
        expected_fqn1 = 'LoKi::HDRFilter/Hlt2Topo2BodyHlt1Filter (IAlgorithm)'
        expected_fqn2 = 'TisTosParticleTagger/Hlt2Topo2BodyTosTisTosTagger (IAlgorithm)'

        filters = getLineInputFilters(self.TCK2, 'Hlt2Topo2Body')
        self.assertEqual(set(filters), expected_keys)
        self.assertIsInstance(filters['TISTOS'], list)
        for k in expected_keys - set(['TISTOS']):
            self.assertIsInstance(filters[k], (backend.PropCfg, type(None)))
        self.assertEqual(filters['HLT1'].fqn(), expected_fqn1)
        self.assertEqual(filters['TISTOS'][0].fqn(), expected_fqn2)

    def test_getHltLineInputs_Hlt1(self):
        expected1 = {'ODIN': {'Beam1Gas:be:VeloOpen'}, 'L0DU': {'B1gas'}}
        inputs1 = getHltLineInputs(self.TCK1, 'Hlt1BeamGasBeam1VeloOpen')
        self.assertDictEqual(inputs1, expected1)

        expected11 = {'ODIN': {'Beam1Gas:be:VeloOpen',
                               'Beam2Gas:be:VeloOpen',
                               'Lumi:be:VeloOpen',
                               'NoBias:be:VeloOpen',
                               'Physics:be:VeloOpen',
                               'SequencerTrigger:be:VeloOpen'},
                      'L0DU': {'B1gas'}}
        inputs11 = getHltLineInputs(self.TCK1, 'Hlt1BeamGasBeam1VeloOpen', use_l0_masks=False)
        self.assertDictEqual(inputs11, expected11)

        expected2 = {'ODIN': {'Lumi:ee,be,eb,bb:'}}
        inputs2 = getHltLineInputs(self.TCK1, 'Hlt1Lumi')
        self.assertDictEqual(inputs2, expected2)

        expected3 = {'ODIN': {'Beam1Gas:ee,be,eb,bb:VeloOpen',
                              'Beam2Gas:ee,be,eb,bb:VeloOpen',
                              'Lumi:ee,be,eb,bb:VeloOpen',
                              'NoBias:ee,be,eb,bb:VeloOpen',
                              'Physics:ee,be,eb,bb:VeloOpen',
                              'SequencerTrigger:ee,be,eb,bb:VeloOpen'}}
        inputs3 = getHltLineInputs(self.TCK1, 'Hlt1VeloClosingMicroBias')
        self.assertDictEqual(inputs3, expected3)

        expected4 = {'ODIN': {'Lumi:ee,be,eb:'}}
        inputs4 = getHltLineInputs(self.TCK1, 'Hlt1NoBiasNonBeamBeam')
        self.assertDictEqual(inputs4, expected4)

    def test_getHltLineInputs_Hlt2(self):
        inputs1 = getHltLineInputs(self.TCK2, 'Hlt2Topo2Body', self.TCK1)
        expected1 = {'HLT1': {'Hlt1TrackMVA',
                              'Hlt1TrackMVALoose',
                              'Hlt1TwoTrackMVA',
                              'Hlt1TwoTrackMVALoose'}}
        self.assertDictEqual(inputs1, expected1)
        inputs2 = getHltLineInputs(self.TCK2, 'Hlt2Lumi', self.TCK1)
        expected2 = {'HLT1': {'Hlt1Lumi'}}
        self.assertDictEqual(inputs2, expected2)
        inputs3 = getHltLineInputs(self.TCK2, 'Hlt2PassThrough', self.TCK1)
        expected3 = {'HLT1': (set(getHlt1Lines(self.TCK1)) -
                              set(['Hlt1Global', 'Hlt1Lumi']))}
        self.assertDictEqual(inputs3, expected3)

    def test_getRoutingBitInputs(self):
        inputs1 = getRoutingBitInputs(self.TCK1, 33)
        expected1 = {'HLT1': {'Hlt1Lumi'}}
        self.assertDictEqual(inputs1, expected1)
        inputs2 = getRoutingBitInputs(self.TCK2, 92)
        expected2 = {'HLT2': {
            'Hlt2DiMuonDetachedJPsi',
            'Hlt2PIDD02KPiTagTurboCalib',
            'Hlt2PIDDetJPsiMuMuNegTaggedTurboCalib',
            'Hlt2PIDDetJPsiMuMuPosTaggedTurboCalib',
            'Hlt2PIDDs2PiPhiKKNegTaggedTurboCalib',
            'Hlt2PIDDs2PiPhiKKPosTaggedTurboCalib',
            'Hlt2PIDLambda2PPiLLTurboCalib',
            'Hlt2PIDLambda2PPiLLhighPTTurboCalib',
            'Hlt2PIDLambda2PPiLLveryhighPTTurboCalib'}}
        self.assertDictEqual(inputs2, expected2)
        # bit 95 is special, and for now an empty dict is returned
        self.assertDictEqual(getRoutingBitInputs(self.TCK2, 95), {})


if __name__ == '__main__':
    unittest.main()
