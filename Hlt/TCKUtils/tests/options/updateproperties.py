###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os, errno
from TCKUtils.utils import (default_cas, ConfigCDBAccessSvc, ConfigStackAccessSvc,
                            updateProperties, createTCKEntries, diff)

try:
    os.remove('updateproperties.cdb')
except OSError as e:
    if e.errno != errno.ENOENT:
        raise
cas_rw = ConfigCDBAccessSvc('WriteConfigAccessSvc', File='updateproperties.cdb', Mode='ReadWrite')
cas = ConfigStackAccessSvc('StackConfigAccessSvc', ConfigAccessSvcs=[cas_rw, default_cas])

updates = {
    'Hlt2B2HH_B2HHPreScaler': {'AcceptFraction': '0.01'},
    'Hlt2BeamGasPreScaler': {'AcceptFraction': 0.01},  # repr(0.01) is called
}

new_id = updateProperties(0x212c1605, updates, label='An updated TCK.', cas=cas)
new_tck = 0x2fff1605
createTCKEntries({new_tck: new_id}, cas=cas)

diff(0x212c1605, 0x2fff1605, human=True, cas=cas)
