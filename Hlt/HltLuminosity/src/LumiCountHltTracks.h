/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LUMICOUNTHLTTRACKS_H
#define LUMICOUNTHLTTRACKS_H 1

// Include files
// from Gaudi
#include <string>
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/StringKey.h"
#include "HltBase/HltBaseAlg.h"
#include "HltBase/HltSelection.h"

/** @class LumiCountHltTracks LumiCountHltTracks.h
 *
 *
 *  Note:

 *  @author Jaap Panman
 *  @date   2008-07-22
 */
class LumiCountHltTracks : public HltBaseAlg
{
  public:
    /// Standard constructor
    LumiCountHltTracks( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution

  private:
    const Hlt::Selection* m_input = nullptr;
    Gaudi::StringKey m_InputSelectionName;
    std::string m_CounterName;
    std::string m_OutputContainerName;
    int m_Counter = -1;
};
#endif // LUMICOUNTHLTTRACKS_H
