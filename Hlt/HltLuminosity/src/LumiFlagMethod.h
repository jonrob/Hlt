/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LUMIFLAGMETHOD_H
#define LUMIFLAGMETHOD_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class LumiFlagMethod LumiFlagMethod.h
 *
 *
 *  @author Jaap Panman
 *  @date   2008-07-21
 */
class LumiFlagMethod : public GaudiAlgorithm
{
  public:
    /// Standard constructor
    LumiFlagMethod( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution

  private:
    std::string m_OutputContainerName;

    std::string m_ValueName;
    std::string m_CounterName;
    int m_Counter = -1;
    int m_Value = -1;
};
#endif // LUMIFLAGMETHOD_H
