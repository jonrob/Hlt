/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LUMIPUTRESULT_H
#define LUMIPUTRESULT_H 1

// Include files
#include <memory>

// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class LumiPutResult LumiPutResult.h
 *
 *
 *  @author Jaap Panman
 *  @date   2008-08-27
 */
class LumiPutResult : public GaudiAlgorithm
{
  public:
    /// Standard constructor
    LumiPutResult( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    StatusCode finalize() override;   ///< Algorithm finalization

  private:
    std::string m_InputContainerName;
    unsigned int m_size;
    std::vector<double> m_means;
    std::vector<double> m_thresholds;
    std::vector<unsigned int> m_infoKeys;
};
#endif // LUMIPUTRESULT_H
