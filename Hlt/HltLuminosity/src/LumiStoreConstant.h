/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LUMISTORECONSTANT_H
#define LUMISTORECONSTANT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class LumiStoreConstant LumiStoreConstant.h
 *
 *
 *  @author Rosen Matev
 *  @date   2015-11-09
 */
class LumiStoreConstant : public GaudiAlgorithm
{
  public:
    /// Standard constructor
    LumiStoreConstant( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution

  private:
    std::string m_OutputContainerName;

    std::string m_CounterName;
    int m_Counter = -1;
    int m_Value;
};
#endif // LUMISTORECONSTANT_H
