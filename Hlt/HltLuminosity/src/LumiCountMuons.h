/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LUMICOUNTMUONS_H
#define LUMICOUNTMUONS_H 1

#include "GaudiAlg/GaudiAlgorithm.h"

/** @class LumiCountMuons LumiCountMuons.h
 *
 *  @author Vladislav Balagura
 *  @date   2011-03-01
 *
 *  (copied from LumiCountTracks)
 */
class LumiCountMuons : public GaudiAlgorithm
{
  public:
    LumiCountMuons( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override;
    StatusCode execute() override;

  protected:
    std::string m_CounterName;
    std::string m_InputSelectionName;
    std::string m_OutputContainerName;
    double m_Threshold;
    int m_Counter = -1;
};
#endif
