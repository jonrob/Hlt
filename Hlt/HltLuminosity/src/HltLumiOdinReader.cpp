/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// from LHCb
#include "Event/ODIN.h"
#include "Event/L0DUReport.h"
// local
#include "HltLumiOdinReader.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : HltLumiOdinReader
//
// 2008-07-15 : Jaap Panman
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( HltLumiOdinReader )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
HltLumiOdinReader::HltLumiOdinReader( const std::string& name,
                                      ISvcLocator* pSvcLocator )
    : HltAlgorithm( name, pSvcLocator ), m_selection( *this )
{
    //  declareProperty("L0DULocation", m_l0Location = L0DUReportLocation::Default );
}
//=============================================================================
// Initialization
//=============================================================================
StatusCode HltLumiOdinReader::initialize()
{
    if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;
    StatusCode sc = HltAlgorithm::initialize(); // must be executed first
    if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm

    // default configuration
    m_selection.registerSelection();

    return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode HltLumiOdinReader::execute()
{
    // get ODIN
    LHCb::ODIN* odin = getIfExists<LHCb::ODIN>( LHCb::ODINLocation::Default );
    ;
    if ( !odin ) {
        return Error( "ODIN cannot be loaded", StatusCode::FAILURE );
    }

    if ( msgLevel( MSG::DEBUG ) ) {
        std::ostringstream m;
        m << " Trigger Type : " << (LHCb::ODIN::TriggerType)odin->triggerType()
          << " BXType : " <<  (LHCb::ODIN::BXTypes)odin->bunchCrossingType();
        debug() << m.str() << endmsg;
    }
    m_selection.output()->setDecision( true );

    return StatusCode::SUCCESS;
}

