/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LUMIPREPARERESULT_H
#define LUMIPREPARERESULT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"

/** @class LumiPrepareResult LumiPrepareResult.h
 *
 *
 *  @author Jaap Panman
 *  @date   2008-08-27
 */
class LumiPrepareResult : public GaudiAlgorithm
{
  public:
    /// Standard constructor
    LumiPrepareResult( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override ; ///< Algorithm initialization
    StatusCode execute() override ;    ///< Algorithm execution

  private:
    std::string m_OutputContainerName;
};
#endif // LUMIPREPARERESULT_H
