/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef LUMICOUNTVELOWITHZRCUTS_H
#define LUMICOUNTVELOWITHZRCUTS_H 1

#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/Point3DTypes.h"
#include <cmath>

/** @class LumiCountVeloWithZRCuts LumiCountVeloWithZRCuts.h
 *
 *  @author Vladislav Balagura
 *  @date   2011-09-20
 *
 *  (copied from LumiCountMuons)
 */
class LumiCountVeloWithZRCuts : public GaudiAlgorithm
{
  public:
    LumiCountVeloWithZRCuts( std::string name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override;
    StatusCode execute() override;

  private:
    std::string m_TrackCounterName, m_VertexCounterName;
    std::string m_TrackInputSelectionName, m_VertexInputSelectionName;
    std::string m_OutputContainerName;
    double m_AbsZCut, m_RCut;
    int m_TrackCounter = -1;
    int m_VertexCounter = -1;

    inline bool fiducial(const Gaudi::XYZPoint&  p) const {
         return std::fabs( p.z() )   < m_AbsZCut &&
                           p.Perp2() < m_RCut * m_RCut;
    }

    template <typename T, typename P> int count_if(const std::string& loc, P predicate) const {
        auto* c = getIfExists<T>( loc );
        return c ? std::count_if( std::begin( *c ), std::end( *c ), predicate )
                 : -1;
    }
};
#endif
