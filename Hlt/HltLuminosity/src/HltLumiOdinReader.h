/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HLTLUMIODINREADER_H
#define HLTLUMIODINREADER_H 1

// Include files
// from Gaudi

#include "HltBase/HltSelectionContainer.h"
#include "HltBase/HltAlgorithm.h"

/** @class HltLumiOdinReader HltLumiOdinReader.h
 *
 *  functionality:
 *      Read some values from ODIN bank
 *  Options:
 *
 *  @author Jaap Panman
 *  @date   2008-07-15
 */
class HltLumiOdinReader : public HltAlgorithm
{
  public:
    /// Standard constructor
    HltLumiOdinReader( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution

  private:
    Hlt::SelectionContainer<void> m_selection;
};
#endif // HLTLUMIODINREADER_H
