/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "Event/HltLumiResult.h"
// local
#include "LumiPrepareResult.h"

using namespace LHCb;

//-----------------------------------------------------------------------------
// Implementation file for class : LumiPrepareResult
//
// 2008-08-27 : Jaap Panman
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( LumiPrepareResult )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
LumiPrepareResult::LumiPrepareResult( const std::string& name,
                                      ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator )
{
    declareProperty( "OutputContainer",
                     m_OutputContainerName = LHCb::HltLumiResultLocation::Default );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode LumiPrepareResult::initialize()
{
    StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
    if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm
    if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;
    info() << "OutputContainer        " << m_OutputContainerName << endmsg;

    return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode LumiPrepareResult::execute()
{

    if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
    // get container
    getOrCreate<HltLumiResult, HltLumiResult>( m_OutputContainerName );
    setFilterPassed( true );

    return StatusCode::SUCCESS;
}

