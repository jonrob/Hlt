###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from functools import partial
from LHCbKernel.Configuration import LHCbConfigurableUser
from HltTracking.Hlt2TrackingConfigurations import (
    Hlt2BiKalmanFittedForwardTracking, Hlt2BiKalmanFittedDownstreamTracking)
from HltTracking.Hlt2ProbeTrackingConfigurations import (
    Hlt2MuonTTTracking, Hlt2VeloMuonTracking, Hlt2FullDownstreamTracking)
from Configurables import ChargedProtoANNPIDConf


class Hlt2AllTrackings(LHCbConfigurableUser):
    """Dummy configurable to help dependency resolution.

    Notice the explicit mention of ChargedProtoANNPIDConf, which must
    be applied before that uses Hlt2Tracking, but after Hlt2Tracking.
    The only proper way to do this is with an extra configurable,
    hence this.
    """

    # python configurables to be applied before me
    __queried_configurables__ = [
        Hlt2BiKalmanFittedForwardTracking,
        partial(ChargedProtoANNPIDConf, "Hlt2LongTracking_ChargedProtoANNPIDConf"),
        Hlt2BiKalmanFittedDownstreamTracking,
        partial(ChargedProtoANNPIDConf, "Hlt2DownstreamTracking_ChargedProtoANNPIDConf"),
        Hlt2MuonTTTracking,
        Hlt2VeloMuonTracking,
        Hlt2FullDownstreamTracking,
    ]
