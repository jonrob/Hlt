###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# ========================================================================#
# vertex-version of track upgrade strings defined in Hlt1Tracking         #
# for Hlt1 displaced vertex line
# ========================================================================#

def LooseForward(allowFail=False):
    if allowFail:
        return "LooseForward  = ( execute(decodeIT) * TC_UPGRADE_VX ( '', HltTracking.Hlt1StreamerConf.LooseForward , True ) )"
    else:
        return "LooseForward  = ( execute(decodeIT) * TC_UPGRADE_VX ( '', HltTracking.Hlt1StreamerConf.LooseForward  ) )"

def FitTracks(allowFail=False):
    if allowFail:
        return "FitTracks     = TC_UPGRADE_VX ( '', HltTracking.Hlt1StreamerConf.FitTrack , True )"
    else:
        return "FitTracks     = TC_UPGRADE_VX ( '', HltTracking.Hlt1StreamerConf.FitTrack )"

def VeloOnlyFitTracks(allowFail=False):
    if allowFail:
        return "VeloOnlyFitTracks = TC_UPGRADE_VX ( '', HltTracking.Hlt1StreamerConf.VeloOnlyFitTrack , True )"
    else:
        return "VeloOnlyFitTracks = TC_UPGRADE_VX ( '', HltTracking.Hlt1StreamerConf.VeloOnlyFitTrack )"
