#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# $Id: HltPVs.py,v 1.11 2010-08-23 20:32:30 witekma Exp $
# =============================================================================
## @file HltTracking/HltPVs.py
#  Define the 2D and 3D primary vertex making algorithms for the Hlt
#  @author V. Gligorov vladimir.gligorov@cern.ch
#  @date 2010-02-21
# =============================================================================
#
# the (ordered) list of (configurables corresponding to the) algorithms needed to
# obtain the Hlt1 selection 'PV3D' can be obtained by doing
#
#   from HltTracking.HltPVs import PV3D
#   PV3D.members()
#
# in addition, the 'outputselection' of the last member can be
# obtained by doing:
#
#   PV3D.outputSelection()
#
# which should return 'PV3D' of course...
# The returned object can be used in the 'algos' list used by Hlt1Line,
# and, given that 'outputSelection' is properly set, partakes in the automated
# daisychaining present in Hlt1Line. In case you do _not_ want this
# daisychaining, you can do:
#
#  PV3D.ignoreOutputSelection()
#
# which 'skips' PV3D for the daisychaining (but will insert the right configurables)
#

__all__ = ( 'PV3D'            # bindMembers instance with algorithms needed to get 'PV3D'
          )
#############################################################################################
# Import Configurables
#############################################################################################
from Gaudi.Configuration import *
#############################################################################################
# Configure PV algorithms
#############################################################################################
from HltLine.HltLine import bindMembers
from HltVertexNames import Hlt3DPrimaryVerticesName as PV3DSelection
from HltVertexNames import _vertexLocation
from HltTracking.HltRecoConf import HltRecoConf

def _PV3D():
    #from HltTrackNames import HltSharedRZVeloTracksName, HltSharedTracksPrefix, _baseTrackLocation
    from HltVertexNames import HltSharedVerticesPrefix
    from HltVertexNames import HltGlobalVertexLocation
    from Configurables import PatPV3D, HltRecoConf
    from Configurables import PVOfflineTool, LSAdaptPV3DFitter
    from Configurables import SimplifiedMaterialLocator, TrackMasterExtrapolator

    if HltRecoConf().getProp("FitVelo"):
      from HltSharedTracking import FittedVelo
      velo = FittedVelo
    else:
      raise RuntimeError("HltPVs: HltRecoConf().FitVelo = False not implemented")
      from HltSharedTracking import RevivedVelo
      velo = RevivedVelo

    #TODO: Move these options to HltRecoConf
    output3DVertices = _vertexLocation(HltSharedVerticesPrefix, HltGlobalVertexLocation, PV3DSelection)
    recoPV3D = PatPV3D('HltPVsPV3D' )
    recoPV3D.InputTracks = velo.outputSelection()
    recoPV3D.OutputVerticesName = output3DVertices
    recoPV3D.addTool(PVOfflineTool,"PVOfflineTool")
    recoPV3D.PVOfflineTool.PVSeedingName = "PVSeed3DTool"
    recoPV3D.PVOfflineTool.PVFitterName = "LSAdaptPV3DFitter"
    recoPV3D.PVOfflineTool.addTool(LSAdaptPV3DFitter, "LSAdaptPV3DFitter")

    offlineTool = recoPV3D.PVOfflineTool
    fitter = recoPV3D.PVOfflineTool.LSAdaptPV3DFitter
    # Set options from HltRecoConf
    for opt, value in HltRecoConf().getProp("PVOptions").iteritems():
        found = False
        for tool in (fitter, offlineTool):
            if opt in tool.getProperties():
                tool.setProp(opt, value)
                found = True
        if not found:
            raise AttributeError("HltPVs.py: no tool has property %s" % opt)

    ## OL: this is from the old Hlt code, not imported from RecoUpgradeTracking
    ## Simplified Material for TrackMasterExtrapolator
    # recoPV3D.PVOfflineTool.LSAdaptPV3DFitter.addTool(TrackMasterExtrapolator, "TrackMasterExtrapolator")
    # recoPV3D.PVOfflineTool.LSAdaptPV3DFitter.TrackMasterExtrapolator.addTool(SimplifiedMaterialLocator, name="MaterialLocator")
    ## RM: for some reason having the first line above causes a weird warning/error
    ## Removing them has no effect on the produced options.
    # ERROR: attempt to redefine type of "HltPVsPV3D.PVOfflineTool.LSAdaptPV3DFitter.TrackMasterExtrapolator" (was: TrackMasterExtrapolator, new: TrackMasterExtrapolator)
    # ERROR:      ==> After this line, a hack will be executed, attempting to rectify the
    # ERROR:      ==> problem just reported. Since this hack may fail, and since it is a
    # ERROR:      ==> rather temporary measure hack, this is an ERROR, not a WARNING.

    ## Simplified Material for Recalculate. Not used, but let's not load
    from Configurables import PVOfflineRecalculate
    recoPV3D.PVOfflineTool.addTool(PVOfflineRecalculate, "PVOfflineRecalculate")
    # recoPV3D.PVOfflineTool.PVOfflineRecalculate.addTool(TrackMasterExtrapolator, "TrackMasterExtrapolator")
    # recoPV3D.PVOfflineTool.PVOfflineRecalculate.TrackMasterExtrapolator.addTool(SimplifiedMaterialLocator, name="MaterialLocator")
    # RM: see comment above
    if HltRecoConf().getProp("FitVelo"):
        recoPV3D.PVOfflineTool.LSAdaptPV3DFitter.UseFittedTracks = True

    pv3dAlgos = ','.join( [ "'%s'"%m.getFullName() for m in velo.members() + [ recoPV3D ] ] )
    from Configurables import LoKi__HltUnit as HltUnit
    ## Hlt vertex beamspot filter
    ##-- todo: can we integrate this in the main streamers directly, using 'tee' ?
    ## TODO: Make this a configurable.
    name = "HltPV3D"
    makePV3DSel = HltUnit(
        name,
        Preambulo = [ 'from LoKiPhys.decorators import *',
                      'from LoKiTrigger.decorators import *',
                      'from LoKiHlt.algorithms import *' ],
        Code = """
        execute( %(algo)s ) * VSOURCE( '%(tesInput)s' )
        >> VX_SINK( '%(hltFinal)s' )
        >> ~VEMPTY
        """ % { 'algo'     : pv3dAlgos,
                'tesInput' : recoPV3D.OutputVerticesName,
                'hltFinal' : PV3DSelection   }
        )

    pv3d = bindMembers( name + 'Sequence', [ makePV3DSel ] ).setOutputSelection( PV3DSelection )
    pv3d.output = output3DVertices
    return pv3d

def PV3D(stage):
    # TODO remove 'stage'
    if stage.upper() not in ['HLT1','HLT2']:
        raise ValueError('PV3D: where must be either HLT1 or HLT2')
    try:
        return PV3D._cache
    except AttributeError:
        PV3D._cache = _PV3D()
        return PV3D._cache
