###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Hlt2 Tracking and PID.

Based on code by P. Koppenburg.
"""

from Gaudi.Configuration import *
from LHCbKernel.Configuration import *
from HltTrackNames import TrackName, Hlt2TrackRoot, Hlt2TrackLoc, Hlt1TrackLoc, HltSharedTrackLoc, Hlt2TracksPrefix

from HltTrackNames import Hlt2ChargedProtoParticleSuffix, Hlt2TrackingRecognizedTrackTypes
from HltTrackNames import Hlt2TrackingRecognizedFitTypes
from HltTrackNames import _baseTrackLocation, _baseProtoPLocation
from HltTrackNames import HltMuonTracksName, HltAllMuonTracksName
from HltTrackNames import HltMuonIDSuffix, HltRICHIDSuffix, HltCALOIDSuffix, HltSharedPIDPrefix
from HltTrackNames import (HltNoPIDSuffix, HltAllPIDsSuffix, HltCaloProtosSuffix, HltMuonProtosSuffix,
                           HltRichProtosSuffix, HltAllPIDsProtosSuffix, HltCaloAndMuonProtosSuffix)

from HltTrackNames import HltDefaultFitSuffix
from HltTrackNames import HltGlobalTrackLocation
from HltTrackNames import Hlt2ChargedProtoParticleSuffix, Hlt2NeutralProtoParticleSuffix

from Configurables import CaloProcessor, CaloPIDsConf, CaloDigitConf
from Configurables import TrackSelector
from Configurables import TrackSys
from Configurables import ChargedProtoANNPIDConf
from HltTracking.HltRecoConf import HltRecoConf

__author__ = "V. Gligorov (vladimir.gligorov@cern.ch), S. Stahl (sascha.stahl@cern.ch)"

#################################################################################################
#
# Hlt2 Tracking
#
class Hlt2Tracking(LHCbConfigurableUser):
    # python configurables that I configure
    __used_configurables__ = [
        (ChargedProtoANNPIDConf, "Hlt2LongTracking_ChargedProtoANNPIDConf"),
        (ChargedProtoANNPIDConf, "Hlt2DownstreamTracking_ChargedProtoANNPIDConf")
        ]
    # python configurables that I use
    __queried_configurables__ = [
        HltRecoConf,
        TrackSys,  # used indirectly via HltSharedTracking
        ]

    __slots__ = { "DataType"                        : '2012' # datatype  2009, MC09, DC06...
                , "EarlyDataTracking"               : False
                , "Hlt2Tracks"                      : "Long" # type of HLT2 tracks
                , "Prefix"                          : "Hlt2"                 # Why should we need this? It's never changed
                , "FastFitType"                     : HltDefaultFitSuffix
                , "DoSeeding"                       : False  # TODO: Remove not used anymore
                , "DoCleanups"                      : True   # Intended default True. Development options
                , "RestartForward"                  : False  # Intended default False. Development options
                , "CreateBestTracks"                : True  # Intended default True. Development options
                , "UseTrackBestTrackCreator"        : True  # Intended default True. Development options
                                                            # This one disables separate fitting of Hlt2 Forward, Match
                                                            # and Downstream tracks. Leave DoCleanups true for Hlt1 track filtering
                , "FitTTracks"                      : True
                , "FindV0s"                         : True
                , "Hlt2ForwardMaxVelo"              : 0
                # TODO : make these variables, not slots
                , "__hlt2ChargedProtosSeq__"        : 0
                , "__hlt2ChargedCaloProtosSeq__"    : 0
                , "__hlt2ChargedRichProtosSeq__"    : 0
                , "__hlt2ChargedMuonProtosSeq__"    : 0
                , "__hlt2ChargedMuonWithCaloProtosSeq__": 0
                , "__hlt2ChargedAllPIDsProtosSeq__": 0
                , "__hlt2NeutralProtosSeq__"        : 0
                , "__hlt2PhotonsFromL0Seq__"        : 0
                , "__hlt2Pi0FromL0Seq__"            : 0
                , "__hlt2ElectronsFromL0Seq__"      : 0
                , "__hlt2VeloTrackingSeq__"         : 0
                , "__hlt2ForwardTrackingSeq__"      : 0
                , "__hlt2SeedTrackingSeq__"         : 0
                , "__hlt2MatchTrackingSeq__"        : 0
                , "__hlt2DownstreamTrackingSeq__"   : 0
                , "__hlt2TrackingSeq__"             : 0
                , "__hlt2StagedFastFitSeq__"        : 0
                , "__hlt2MuonIDSeq__"               : 0
                , "__hlt2RICHIDSeq__"               : 0
                , "__hlt2CALOIDSeq__"               : 0
                , "__caloProcessor"                 : 0
                , "__allTracks"                     : 0
                }

    #############################################################################################
    #############################################################################################
    #
    # Externally visible methods to return the created bindMembers objects
    #
    #############################################################################################
    #########################################################################################
    #
    # No Charged Protos
    #
    def hlt2ChargedProtos(self, suffix) :
        """
        Charged No PID protoparticles
        """
        # Fill the sequence
        return self.__getSequence("__hlt2ChargedProtosSeq__", self.__hlt2ChargedProtos, suffix)
    #############################################################################################
    #
    # Charged ProtoParticles with no PID information
    #
    def hlt2ChargedNoPIDsProtos(self):
        """
        Charged protoparticles from Calo (=electrons)
        """
        assert False
        return self.hlt2ChargedProtos(HltNoPIDSuffix)
    #############################################################################################
    #
    # Charged ProtoParticles with CALO ID
    #
    def hlt2ChargedCaloProtos(self):
        """
        Charged protoparticles from Calo (=electrons)
        """
        assert False, "Do not use, use hlt2ChargedAllPIDsProtos"
        return self.__getSequence("__hlt2ChargedCaloProtosSeq__", self.__hlt2ChargedCaloProtos)
    #############################################################################################
    #
    # Charged ProtoParticles with RICH ID
    #
    def hlt2ChargedRichProtos(self):
        """
        Charged protoparticles using RICH (=pions,protons,kaons)
        """
        assert False, "Do not use, use hlt2ChargedAllPIDsProtos"
        return self.__getSequence("__hlt2ChargedRichProtosSeq__", self.__hlt2ChargedRichProtos)
    #############################################################################################
    #
    # Charged ProtoParticles with Muon ID
    #
    def hlt2ChargedMuonProtos(self):
        """
        Charged protoparticles using Muon (=Muons)
        """
        assert False, "Do not use, use hlt2ChargedAllPIDsProtos"
        return self.__getSequence("__hlt2ChargedMuonProtosSeq__", self.__hlt2ChargedMuonProtos)

    #############################################################################################
    #
    # Charged ProtoParticles with Muon and CALO ID
    #
    def hlt2ChargedMuonWithCaloProtos(self):
        """
        Charged protoparticles using Muon (=Muons)
        """
        assert False, "Do not use, use hlt2ChargedAllPIDsProtos"
        return self.__getSequence("__hlt2ChargedMuonWithCaloProtosSeq__", self.__hlt2ChargedMuonWithCaloProtos)
    #############################################################################################
    #
    # Charged ProtoParticles with all PID information
    #
    def hlt2ChargedAllPIDsProtos(self):
        """
        Charged protoparticles all PID information
        """
        return self.__getSequence("__hlt2ChargedAllPIDsProtosSeq__", self.__hlt2ChargedAllPIDsProtos)
    #############################################################################################
    #
    # Neutral ProtoParticles
    #
    def hlt2NeutralProtos(self):
        """
        Neutral protoparticles
        """
        return self.__getSequence("__hlt2NeutralProtosSeq__", self.__getCALOSeq, "Neutral")
    #############################################################################################
    #
    # Photons from L0 Candidates
    #
    def hlt2PhotonsFromL0(self):
        """
        Neutral protoparticles
        """
        return self.__getSequence("__hlt2PhotonsFromL0Seq__", self.__getNewCALOSeq, 'photon')
    #############################################################################################
    #
    # Electrons from L0 Candidates
    #
    def hlt2ElectronsFromL0(self):
        """
        Charged protoparticles
        """
        log.warning( '##################################################################################')
        log.warning( '## Think of getting rid of hlt2ElectronsFromL0  ')
        log.warning( '##################################################################################')
        #assert False, "Do not use, use hlt2ChargedAllPIDsProtos"
        return self.__getSequence("__hlt2ElectronsFromL0Seq__", self.__hlt2ElectronsFromL0)
    #############################################################################################
    #
    # Pi0 from L0 Candidates
    #
    def hlt2Pi0FromL0(self):
        """
        Neutral protoparticles
        """
        return self.__getSequence("__hlt2Pi0FromL0Seq__", self.__getNewCALOSeq, 'pi0')
    #############################################################################################
    #
    # Velo tracking for the PV making sequence
    #
    def hlt2VeloTracking(self):
        """
        Velo tracks
        """
        return self.__getSequence("__hlt2VeloTrackingSeq__", self.__hlt2VeloTracking)
    #############################################################################################
    #
    # Forward tracking
    #
    def hlt2ForwardTracking(self):
        """
        Forward tracks
        """
        return self.__getSequence("__hlt2ForwardTrackingSeq__", self.__hlt2ForwardTracking)
    #############################################################################################
    #
    # Seed tracking
    #
    def hlt2SeedTracking(self):
        """
        Seed tracks
        """
        return self.__getSequence("__hlt2SeedTrackingSeq__", self.__hlt2SeedTracking)
    #############################################################################################
    #
    # Match tracking
    #
    def hlt2MatchTracking(self):
        """
        Match tracks
        """
        return self.__getSequence("__hlt2MatchTrackingSeq__", self.__hlt2MatchTracking)
    #############################################################################################
    #
    # Downstream tracking
    #
    def hlt2DownstreamTracking(self):
        """
        Downstream tracks
        """
        return self.__getSequence("__hlt2DownstreamTrackingSeq__", self.__hlt2DownstreamTracking)
    #############################################################################################
    #
    # Full tracking sequence
    #
    def hlt2Tracking(self):
        """
        Velo tracks
        """
        return self.__getSequence("__hlt2TrackingSeq__", self.__hlt2Tracking)
    #############################################################################################
    #
    # Full tracking sequence
    #
    def hlt2StagedFastFit(self):
        """
        Fitted Tracks
        """
        return self.__getSequence("__hlt2StagedFastFitSeq__", self.__hlt2StagedFastFit)
    #############################################################################################
    #
    # The Muon ID sequence: makes the tracks and Muon IDs them
    #
    def hlt2MuonID(self):
        """
        Muon PID
        """
        return self.__getSequence("__hlt2MuonIDSeq__", self.__hlt2MuonID)
    #############################################################################################
    #
    # The RICH ID sequence: makes the tracks and RICH IDs them
    #
    def hlt2RICHID(self):
        """
        RICH PID
        """
        return self.__getSequence("__hlt2RICHIDSeq__", self.__hlt2RICHID)
    #############################################################################################
    #
    # The CALO ID: makes the tracks and CALO IDs them
    #
    def hlt2CALOID(self):
        """
        CALO PID
        """
        return self.__getSequence("__hlt2CALOIDSeq__", self.__hlt2CALOID)
    #############################################################################################
    #
    # Track preparation
    #
    # This will automatically do the required tracking and then, if required
    # do the specified fast-fit as well; this is the only one of the tracking sequences
    # whose output is used in the protoparticle making
    #
    def hlt2PrepareTracks(self):
        """
        The staged fast fit
        """
        return self.hlt2StagedFastFit()
    #############################################################################################
    #
    # Return all track locations that are filled
    #
    def allTrackLocations(self):
        """
        All track locations
        """
        return self.__allTracks
    #############################################################################################
    #
    # The apply configuration method, makes the bindMembers objects which can then
    # be exported, and also checks that the configuration is not inconsistent
    #
    #############################################################################################
    #############################################################################################
    def __apply_configuration__(self) :
        log.debug('############################################################')
        log.debug('## INFO You have configured an instance of the Hlt2 tracking')
        log.debug('## INFO ----------------------------------------------------')
        log.debug('## INFO The data type is %s'     % self.getProp("DataType"       ))
        log.debug('## INFO Early data tuning? %s'   % self.getProp("EarlyDataTracking"))
        log.debug('## INFO Tracks to make are %s'   % self.getProp("Hlt2Tracks"     ))
        #log.debug('## INFO The prefix is %s'        % self.getProp("Prefix"         ))
        log.debug('## INFO The fit type is %s'      % self.getProp("FastFitType"    ))
        log.debug('## INFO Seeding? = %s'           % self.getProp("DoSeeding"      ))
        log.debug('#################################%##########################')
        #
        # First of all check that I have been called with a sensible set of options
        #
        outputOfHlt2Tracking = self.__trackType()
        if (outputOfHlt2Tracking == 'Unknown') :
            self.__fatalErrorUnknownTrackType()
            raise ValueError('Unknown track type')
        #
        if (self.getProp("FastFitType") not in Hlt2TrackingRecognizedFitTypes) :
            self.__fatalErrorUnknownTrackFitType()
            raise ValueError('Unknown track fit type')
        if (self.getProp("UseTrackBestTrackCreator") and not self.getProp("CreateBestTracks")):
            log.fatal( '#############################################################')
            log.fatal( 'Set CreateBestTracks to true if UseTrackBestTrackCreator true')
            raise ValueError('Incompatible properties')

        # TODO call everything here
        self.hlt2ChargedAllPIDsProtos()

    #############################################################################################
    #############################################################################################
    #
    # Actual implementation of the functions
    #
    #############################################################################################
    #
    # Start with the naming conventions
    #
    # The track type for the derived tracks
    # This function checks that the track asked for by the Hlt2Tracking instance
    # is in the recognised track types, and returns "Unknown" or the correct
    # suffix based on the configuration of the Hlt2Tracking.
    #
    def __getSequence(self, prop, fun, *args):
        if self.isPropertySet(prop):
            k = tuple(args)
            p = self.getProp(prop)
            if k not in p:
                p[k] = fun(*args)
                self.setProp(prop, p)
            return p[k]
        else:
            r = fun(*args)
            self.setProp(prop, {tuple(args) : r})
            return r

    def __trackType(self) :
        if ( self.getProp("Hlt2Tracks") not in Hlt2TrackingRecognizedTrackTypes) :
            return "Unknown"
        elif ( self.getProp("Hlt2Tracks") == "Long") :
            if self.getProp("DoSeeding") :
                return "Long"
            else :
                self.__warningAskLongGetForward()
                return "Long"
        elif ( self.getProp("Hlt2Tracks") == "Forward") :
            if self.getProp("DoSeeding") :
                self.__warningAskForwardGetLong()
                return "Long"
            else :
                return "Long"
        elif ( self.getProp("Hlt2Tracks") == "Downstream") :
            return "Downstream"

    def trackType(self) :
        return self.__trackType()
    #
    # Now the "long" track location, for the tracks which will be used to
    # make particles, protoparticles, etc. baseTrack/ProtoPLocation live in
    # HltTrackNames.py   WE NEED TO REWORK THIS
    #

    def __trackLocationByType(self, type):
        thisTrackLocation     = self.getProp("FastFitType") + "/" + \
                                TrackName[type]
        return Hlt2TrackRoot +"/"+ thisTrackLocation

    def __trackLocation(self):
        return self.__trackLocationByType(self.__trackType())

    def trackLocation(self):
        return self.__trackLocation();


    ## For protos, the format is e.g. Hlt2/ProtoP/Unfitted/Forward/Charged #
    def __protosLocation(self,protosType):
        thisProtosLocation = self.__trackType() + "/" + protosType
        return _baseProtoPLocation(self.getProp("Prefix"), thisProtosLocation)

    def __neutralProtosLocation(self):
        if self.__trackType()!="Long":
            assert False, "We should fix this call..."
        return _baseProtoPLocation(self.getProp("Prefix"), "Neutrals")

    #
    # The hadron protos need subdirectories depending on the type of PID requested
    #
    def __chargedRichProtosSuffix(self):
        baseSuffix = HltRichProtosSuffix+"/"
        baseSuffix += HltRICHIDSuffix
        return baseSuffix

    #
    # The trackified Muon ID location
    #
    def _trackifiedMuonIDLocation(self) :
        return self.__trackLocationByType("Muon")

    #
    # The trackified AllMuon ID location
    #
    def _trackifiedAllMuonIDLocation(self) :
        return self.__hltBasePIDLocation() + "/"+ HltAllMuonTracksName


    #
    # The PID objects themselves
    #
    def __hltBasePIDLocation(self) :
        return self.__trackLocation() + "/" + HltSharedPIDPrefix
    #
    def __muonIDLocation(self) :
        #muonBase = self.__hltBasePIDLocation() + "/" + HltMuonIDSuffix
        # This is now the same for all Hlt2Tracking instances
        muonBase = self.getProp("Prefix")+"/"+HltSharedPIDPrefix+"/"+HltMuonIDSuffix
        return muonBase

    #
    def __richIDLocation(self) :
        return self.__hltBasePIDLocation() + "/" + HltRICHIDSuffix
    #
    def _caloIDLocation(self, withSuffix = False) :
        # This is now the same for all Hlt2Tracking instances
        if not withSuffix:
            caloBase = self.getProp("Prefix")+"/"+HltSharedPIDPrefix
        else:
            # The calo reconstruction adds automatically /Calo,
            # when asking for the location this return the full path
            caloBase = self.getProp("Prefix")+"/"+HltSharedPIDPrefix+"/Calo"
        return caloBase
    def __caloL0IDLocation(self) :
        caloBase =  self.__hltBasePIDLocation() + "/L0Calo" + HltCALOIDSuffix
        return caloBase
    #
    # The prefixes for the various tools and algorithms used
    #
    def __trackingAlgosAndToolsPrefix(self) :
        return self.getProp("Prefix") + self.__trackType()
    #
    def __trackfitAlgosAndToolsPrefix(self) :
        return self.getProp("Prefix") + self.getProp("FastFitType") + self.__trackType()
    #
    def __pidAlgosAndToolsPrefix(self) :
        return self.name()
    # Explicitly tell users about the new long/forward naming convention: long tracks are the
    # forward+match tracks (seeding required), forward tracks are just PatForward, no seeding.
    # Since we are not bastards, if someone asks for forward tracks and turns the seeding on we
    # will give them long tracks, but in this case we should print a warning to tell them what
    # happened.
    #
    def __warningAskForwardGetLong(self) :
        log.warning( '##################################################################################')
        log.warning( '## WARNING You asked for Forward tracks with seeding on. I will assume that you   ')
        log.warning( '## WARNING would like both forward and match tracks, and I will put the combined  ')
        log.warning( '## WARNING output of these into the long track container. You can still retrieve  ')
        log.warning( '## WARNING the forward tracks alone by asking for the forward track container.    ')
        log.warning( '##################################################################################')
    #
    def __warningAskLongGetForward(self) :
        log.warning( '##################################################################################')
        log.warning( '## WARNING You asked for long tracks with seeding off. I will assume that you     ')
        log.warning( '## WARNING would like only forward tracks, which will be placed in the forward    ')
        log.warning( '## WARNING forward track container. The long track container will be left empty.  ')
        log.warning( '##################################################################################')
    #
    # In case something went wrong when specifying the options, warn me before you die
    #
    def __fatalErrorUnknownTrackType(self) :
        log.fatal( '##################################################################################')
        log.fatal( '## FATAL You specified an unknown track type %s for the Hlt2 Reconstruction' % self.getProp("Hlt2Tracks") )
        log.fatal( '## FATAL I will now die, you need to make Hlt2Tracking aware of this track type!  ')
        log.fatal( '##################################################################################')
    #
    def __fatalErrorUnknownTrackFitType(self) :
        log.fatal( '################################################################################')
        log.fatal( '## FATAL You specified an unknown fit type %s for the Hlt2 Reconstruction' % self.getProp("FastFitType") )
        log.fatal( '## FATAL I will now die, you need to make Hlt2Tracking aware of this fit type!  ')
        log.fatal( '################################################################################')

    #########################################################################################
    #
    # Electron Protos
    #
    def __hlt2ChargedCaloProtos( self ) :
        """
        Charged Calo protoparticles = electrons
        """

        #
        # The charged protoparticles and their output location
        #
        allTracks                   = self.hlt2Tracking()
        tracks                      = self.hlt2StagedFastFit()
        chargedProtos               = self.hlt2ChargedProtos(HltCaloProtosSuffix)
        chargedProtosOutputLocation = chargedProtos.outputSelection()

        doCaloReco              = self.hlt2CALOID()

        #
        # Add the Calo info to the DLL
        #
        from Configurables import ( ChargedProtoParticleAddEcalInfo,
                                    ChargedProtoParticleAddBremInfo,
                                    ChargedProtoParticleAddHcalInfo,
                                    )
        from Configurables import GaudiSequencer
        addCaloInfo = GaudiSequencer( self.__pidAlgosAndToolsPrefix() + HltCaloProtosSuffix + "Sequence")
        caloPidLocation = self._caloIDLocation()
        prefix = self.__pidAlgosAndToolsPrefix()
        suffix =  HltCaloProtosSuffix
        ecal = ChargedProtoParticleAddEcalInfo(prefix+"ChargedProtoPAddEcal"+suffix)
        brem = ChargedProtoParticleAddBremInfo(prefix+"ChargedProtoPAddBrem"+suffix)
        hcal = ChargedProtoParticleAddHcalInfo(prefix+"ChargedProtoPAddHcal"+suffix)
        for alg in (ecal,brem,hcal):
            alg.setProp("ProtoParticleLocation",chargedProtosOutputLocation)
            alg.setProp("Context",caloPidLocation)
            addCaloInfo.Members += [ alg  ]
        # For the charged we need a combined DLL
        from Configurables import ChargedProtoCombineDLLsAlg
        combine_name                    = self.__pidAlgosAndToolsPrefix()+"ChargedCaloProtoPCombDLLs"
        combine                         = ChargedProtoCombineDLLsAlg(combine_name)
        combine.ProtoParticleLocation   = chargedProtosOutputLocation

        sequenceToReturn  = [ allTracks, tracks, chargedProtos]
        sequenceToReturn += [ doCaloReco ]
        sequenceToReturn += [ addCaloInfo ]
        sequenceToReturn += [ combine ]

        from HltLine.HltLine import bindMembers
        # Build the bindMembers
        bm_name         = self.__pidAlgosAndToolsPrefix()+"ChargedCaloProtosSeq"
        bm_members      = sequenceToReturn
        bm_output       = chargedProtosOutputLocation

        return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)

    #########################################################################################
    #
    # Muons with CALO ID added, these seem to be obsolete and probably don't work
    #
    def __hlt2ChargedMuonWithCaloProtos(self) :
        """
        Muons which have the CALO PID information filled for them
        """
        #
        # The charged protoparticles and their output location
        #
        allTracks                   = self.hlt2Tracking()
        tracks                      = self.hlt2StagedFastFit()
        chargedProtos               = self.hlt2ChargedProtos(HltCaloAndMuonProtosSuffix)
        chargedProtosOutputLocation = chargedProtos.outputSelection()
        doCaloReco              = self.hlt2CALOID()

        #
        # Add the Calo info to the DLL
        # TODO: This is now used several times, create an own function
        from Configurables import ( ChargedProtoParticleAddEcalInfo,
                                    ChargedProtoParticleAddBremInfo,
                                    ChargedProtoParticleAddHcalInfo,
                                    )
        from Configurables import GaudiSequencer
        addCaloInfo = GaudiSequencer( self.__pidAlgosAndToolsPrefix() + HltCaloAndMuonProtosSuffix + "Sequence")
        caloPidLocation = self._caloIDLocation()
        prefix = self.__pidAlgosAndToolsPrefix()
        suffix =   HltCaloAndMuonProtosSuffix
        ecal = ChargedProtoParticleAddEcalInfo(prefix+"ChargedProtoPAddEcal"+suffix)
        brem = ChargedProtoParticleAddBremInfo(prefix+"ChargedProtoPAddBrem"+suffix)
        hcal = ChargedProtoParticleAddHcalInfo(prefix+"ChargedProtoPAddHcal"+suffix)
        for alg in (ecal,brem,hcal):
            alg.setProp("ProtoParticleLocation",chargedProtosOutputLocation)
            alg.setProp("Context",caloPidLocation)
            addCaloInfo.Members += [ alg  ]

        from Configurables import ChargedProtoParticleAddMuonInfo

        # Now add the muon information
        # The muon ID
        muonID                          = self.hlt2MuonID()
        muon_name                       = self.__pidAlgosAndToolsPrefix()+HltCaloAndMuonProtosSuffix
        muon                            = ChargedProtoParticleAddMuonInfo(muon_name)
        muon.ProtoParticleLocation      = chargedProtosOutputLocation
        muon.InputMuonPIDLocation       = muonID.outputSelection()


        # For the charged we need a combined DLL
        from Configurables import ChargedProtoCombineDLLsAlg
        combine_name                    = self.__pidAlgosAndToolsPrefix()+HltCaloAndMuonProtosSuffix+ "CombDLLs"
        combine                         = ChargedProtoCombineDLLsAlg(combine_name)
        combine.ProtoParticleLocation   = chargedProtosOutputLocation

        sequenceToReturn  = [ allTracks, tracks, chargedProtos]
        sequenceToReturn += [ doCaloReco, addCaloInfo]
        sequenceToReturn += [ muonID, muon ]
        sequenceToReturn += [ combine ]

        from HltLine.HltLine import bindMembers

        # Build the bindMembers
        bm_name         = self.__pidAlgosAndToolsPrefix()+"MwCSeq"
        bm_members      = sequenceToReturn
        bm_output       = chargedProtosOutputLocation

        return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)


    #########################################################################################
    #
    # Muon Protos
    #
    def __hlt2ChargedMuonProtos(self):
        """
        Charged muon protoparticles
        Requires chargedProtos and muon ID
        """
        from Configurables import ( ChargedProtoParticleAddMuonInfo,
                                    ChargedProtoCombineDLLsAlg )
        #The different add PID algorithms
        #
        # The charged protoparticles and their output location
        chargedProtos                   = self.hlt2ChargedProtos(HltMuonProtosSuffix)
        chargedProtosOutputLocation     = chargedProtos.outputSelection()
        #
        muon_name           = self.__pidAlgosAndToolsPrefix()+"ChargedProtoPAddMuon"

        muon                = ChargedProtoParticleAddMuonInfo(muon_name)
        muon.ProtoParticleLocation  = chargedProtosOutputLocation
        # Get the MuonID from the MuonID sequence
        muonID                      = self.hlt2MuonID()
        muon.InputMuonPIDLocation   = muonID.outputSelection()

        from HltLine.HltLine import bindMembers
        # Build the bindMembers

        bm_name         = self.__pidAlgosAndToolsPrefix()+"ChargedMuonProtosSeq"

        bm_members      = [ muonID, chargedProtos, muon ]
        bm_output       = chargedProtosOutputLocation

        return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)
    #########################################################################################
    #
    # Charged hadron ProtoParticles
    # Does not necessarily use RICH or CALO but can do if so configured
    #
    def __hlt2ChargedRichProtos(self):
        """
        Charged hadron protoparticles = pion, kaon, proton
        If RICH is on, then requires rich (this is off by default)
        """
        from Configurables import ( ChargedProtoParticleAddRichInfo,
                                    ChargedProtoCombineDLLsAlg)
        #The different add PID algorithms
        #
        # The charged protoparticles and their output location
        #
        chargedProtos                   = self.hlt2ChargedProtos(self.__chargedRichProtosSuffix())
        chargedProtosOutputLocation     = chargedProtos.outputSelection()
        #
        # Now set up the RICH sequence
        #
        doRICHReco              = self.hlt2RICHID()
        #
        # Add the RICH info to the DLL
        #
        richDLL_name            = self.__pidAlgosAndToolsPrefix()+"ChargedHadronProtoPAddRich"
        richDLL                 = ChargedProtoParticleAddRichInfo(richDLL_name)
        richDLL.InputRichPIDLocation    = doRICHReco.outputSelection()
        richDLL.ProtoParticleLocation   = chargedProtosOutputLocation
        #
        # What are we returning?
        #
        sequenceToReturn = []
        sequenceToReturn += [chargedProtos]
        sequenceToReturn += [doRICHReco]
        sequenceToReturn += [richDLL]
        #
        # The combined DLL is needed for cutting on later
        #
        from Configurables import ChargedProtoCombineDLLsAlg
        combine_name                    = self.__pidAlgosAndToolsPrefix()+"RichCombDLLs"
        combine                         = ChargedProtoCombineDLLsAlg(combine_name)
        combine.ProtoParticleLocation   = chargedProtosOutputLocation

        sequenceToReturn += [combine]

        from HltLine.HltLine import bindMembers
        # Build the bindMembers
        bm_name         = self.__pidAlgosAndToolsPrefix()+"ChargedRichProtosSeq"
        bm_members      = sequenceToReturn
        bm_output       = chargedProtosOutputLocation

        return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)

    #########################################################################################
    #
    # Charged ProtoParticles with all pid information
    #
    #
    def __hlt2ChargedAllPIDsProtos(self):
        """
        Charged hadron protoparticles = pion, kaon, proton
        If RICH is on, then requires rich (this is off by default)
        """
        #
        # The charged protoparticles and their output location
        #
        allTracks                   = self.hlt2Tracking()
        tracks                      = self.hlt2StagedFastFit()
        chargedProtos               = self.hlt2ChargedProtos(HltAllPIDsProtosSuffix)
        chargedProtosOutputLocation = chargedProtos.outputSelection()

        from Configurables import ( ChargedProtoParticleAddRichInfo,ChargedProtoParticleAddMuonInfo,
                                    ChargedProtoCombineDLLsAlg)

        #
        # Set up the pid sequence
        #
        doRICHReco              = self.hlt2RICHID()
        muonID                  = self.hlt2MuonID()
        doCaloReco              = self.hlt2CALOID()

        #
        # Add the rich info to the DLL
        #
        richDLL_name                    = self.__pidAlgosAndToolsPrefix()+"ChargedHadronProtoPAddRich"+HltAllPIDsSuffix
        richDLL                         = ChargedProtoParticleAddRichInfo(richDLL_name)
        richDLL.InputRichPIDLocation    = doRICHReco.outputSelection()
        richDLL.ProtoParticleLocation   = chargedProtosOutputLocation
        #
        # Add the muon info to the DLL
        #
        muon_name                   = self.__pidAlgosAndToolsPrefix()+"ChargedProtoPAddMuon"+HltAllPIDsSuffix
        muon                        = ChargedProtoParticleAddMuonInfo(muon_name)
        muon.ProtoParticleLocation  = chargedProtosOutputLocation
        muon.InputMuonPIDLocation   = str(muonID.outputSelection())

        #
        # Add the Calo info to the DLL
        #
        from Configurables import ( ChargedProtoParticleAddEcalInfo,
                                    ChargedProtoParticleAddBremInfo,
                                    ChargedProtoParticleAddHcalInfo,
                                    ChargedProtoParticleAddPrsInfo,
                                    ChargedProtoParticleAddSpdInfo
                                    )
        from Configurables import GaudiSequencer
        addCaloInfo = GaudiSequencer( self.__pidAlgosAndToolsPrefix() + HltAllPIDsSuffix + "Sequence")
        caloPidLocation = self._caloIDLocation()
        prefix = self.__pidAlgosAndToolsPrefix()
        suffix =  HltAllPIDsSuffix
        ecal = ChargedProtoParticleAddEcalInfo(prefix+"ChargedProtoPAddEcal"+suffix)
        brem = ChargedProtoParticleAddBremInfo(prefix+"ChargedProtoPAddBrem"+suffix)
        hcal = ChargedProtoParticleAddHcalInfo(prefix+"ChargedProtoPAddHcal"+suffix)
        for alg in (ecal,brem,hcal):
            alg.setProp("ProtoParticleLocation",chargedProtosOutputLocation)
            alg.setProp("Context",caloPidLocation)
            addCaloInfo.Members += [ alg  ]


        # Combine the DLLs
        from Configurables import ChargedProtoCombineDLLsAlg
        combine_name                    = self.__pidAlgosAndToolsPrefix()+"CombDLLs"+HltAllPIDsSuffix
        combine                         = ChargedProtoCombineDLLsAlg(combine_name)
        combine.ProtoParticleLocation   = chargedProtosOutputLocation


        #
        # What are we returning?
        #
        sequenceToReturn  = [ allTracks, tracks, chargedProtos]
        sequenceToReturn += [ doRICHReco, muonID, doCaloReco ]
        sequenceToReturn += [ richDLL, muon , addCaloInfo ]
        sequenceToReturn += [combine]

        # Calculate ProbNN variables
        if HltRecoConf().getProp("CalculateProbNN") == True:
            probNNSeqName         = self._instanceName(ChargedProtoANNPIDConf)
            probNNSeq             = GaudiSequencer(probNNSeqName+"Seq")
            annconf = ChargedProtoANNPIDConf(probNNSeqName)
            annconf.DataType = self.getProp( "DataType" )
            annconf.TrackTypes              = [ self.__trackType() ]
            annconf.RecoSequencer = probNNSeq
            annconf.ProtoParticlesLocation = chargedProtosOutputLocation
            sequenceToReturn += [probNNSeq]

        from HltLine.HltLine import bindMembers
        # Build the bindMembers
        bm_name          = self.__pidAlgosAndToolsPrefix()+"ChargedAllPIDsProtosSeq"
        bm_members       = sequenceToReturn
        bm_output        = chargedProtosOutputLocation
        return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)

    #########################################################################################
    #
    # Charged ProtoParticles
    #
    def __hlt2ChargedProtos(self,outputLocation):
        """
        Charged protoparticles

        Requires tracks, fitted if necessary
        """
        from Configurables import ChargedProtoParticleMaker
        from HltLine.HltLine import bindMembers
        # Build the bindMembers
        chargedProtosOutputLocation = self.__protosLocation(Hlt2ChargedProtoParticleSuffix) + "/" + outputLocation
        charged_name = self.__pidAlgosAndToolsPrefix()+outputLocation.replace("/", "")+'ChargedProtoPAlg'
        charged      = ChargedProtoParticleMaker(charged_name)
        charged.TrackSelectorType = "TrackSelector"
        charged.addTool(TrackSelector, name="TrackSelector")
        ts=charged.TrackSelector
        ts.setProp("MinChi2Cut", 0.)
        ts.setProp("MaxChi2Cut", HltRecoConf().getProp("MaxTrCHI2PDOF"))
        if HltRecoConf().getProp("ApplyGHOSTPROBCut") == True:
            ts.setProp("MinGhostProbCut", 0.)
            ts.setProp("MaxGhostProbCut", HltRecoConf().getProp("MaxTrGHOSTPROB"))

        if self.__trackType() == "Long":
            ts.TrackTypes = ["Long"]
        elif self.__trackType() == "Downstream":
            ts.TrackTypes = ["Downstream"]

        # Need to allow for fitted tracks
        # This is now done inside the staged fast fit based on the fastFitType passed
        tracks          = self.hlt2StagedFastFit()
        charged.Inputs  = [tracks.outputSelection()]
        charged.Output  = chargedProtosOutputLocation

        bm_name         = self.__pidAlgosAndToolsPrefix() + outputLocation.replace("/", "") + "ChargedProtosSeq"
        bm_members      = [tracks, charged]
        bm_output       = chargedProtosOutputLocation
        return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)


    #########################################################################################
    #
    # Photons built from L0
    #
    def __hlt2ElectronsFromL0(self):
        """
        Photons coming from L0
        """
        # Configure CaloElectron
        caloElectronsFromL0 = self.__getNewCALOSeq('electron')
        from Configurables import ChargedProtoCombineDLLsAlg
        combine_name                    = self.__pidAlgosAndToolsPrefix()+"LowEtChargedCaloProtoPCombDLLs"
        combine                         = ChargedProtoCombineDLLsAlg(combine_name)
        combine.ProtoParticleLocation   = caloElectronsFromL0.outputSelection()

        from HltLine.HltLine import bindMembers
        # Build the bindMembers
        bm_name         = self.__pidAlgosAndToolsPrefix()+"LowEtChargedCaloProtosSeq"
        bm_members      = [caloElectronsFromL0, combine]
        bm_output       = caloElectronsFromL0.outputSelection()

        return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)

    #########################################################################################
    #
    # MuonID
    #
    def __hlt2MuonID(self) :
        """
        Muon ID options
        Requires tracks
        """

        from MuonID import ConfiguredMuonIDs
        from Configurables import MuonRec, MuonIDAlgLite
        from Configurables import PrepareMuonHits
        from HltLine.HltLine import bindMembers

        cm = ConfiguredMuonIDs.ConfiguredMuonIDs(data=self.getProp("DataType"))

        #HltMuonIDAlg_name   = self.__pidAlgosAndToolsPrefix()+"MuonIDAlgLite"
        HltMuonIDAlg = cm.configureMuonIDAlgLite(self.getProp("Prefix") + "MuonIDAlgLite")

        # Configure moun ID tools explicitly, would be better if the ConfiguredMuonIDs class
        # provided a comprehensive method. All tools are public, but should configure
        # everywhere, where they are used to be safe.
        import Configurables
        for tool, fun in (("CommonMuonTool" ,"IsMuonTool"),
                     ("DLLMuonTool", "DLLMuonTool"),
                     ("MakeMuonTool", "MakeMuonTool")):
            tool = getattr(Configurables, tool)()
            getattr(cm, "configure" + fun)(tool)

        #The tracks to use
        #tracks              = self.__hlt2StagedFastFit()
        # We want all tracks
        tracks              = self.hlt2Tracking()

        # merge these into a single list because the muon ID can't take multiple input locations anymore
        from Configurables import TrackListMerger
        merger = TrackListMerger(self.getProp("Prefix") + "TrackMergerForMuonID")
        merger.InputLocations = tracks.outputSelection()
        merger.OutputLocation = "Rec/Track/MergedTracksForMuonID"

        #Enforce naming conventions
        HltMuonIDAlg.TracksLocations        = merger.OutputLocation
        HltMuonIDAlg.MuonIDLocation         = self.__muonIDLocation()
        HltMuonIDAlg.MuonTrackLocation      = self._trackifiedMuonIDLocation()
        #HltMuonIDAlg.MuonTrackLocationAll   = self._trackifiedAllMuonIDLocation()
        # CRJ : Disable FindQuality in HLT since it increases CPU time for MuonID by
        #       a factor 3-4
        # TODO: Do we want to keep this? Which difference does it make.
        #HltMuonIDAlg.FindQuality            = False

        # Build the bindMembers
        bm_name         = self.__pidAlgosAndToolsPrefix()+"MuonIDSeq"
        bm_members      = [ tracks, MuonRec(), PrepareMuonHits(), merger, HltMuonIDAlg ]
        bm_output       = HltMuonIDAlg.MuonIDLocation

        return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)

    #########################################################################################
    #
    # Calo ID
    #
    def __hlt2CALOID(self) :
        """
        Calo ID
        Does calo reconstruction and Calo ID
        """
        # the sequence to run
        myCALOProcessorPIDSeq   = self.__getCALOSeq("PID")

        return myCALOProcessorPIDSeq
    #########################################################################################
    #
    # RICH ID
    #
    def __hlt2RICHID(self) :
        """
        Set up the sequence for doing the RICH PID on the tracks
        """
        from HltLine.HltLine import bindMembers
        from Configurables import Rich__Future__RawBankDecoder as RichDecoder
        richDecode = RichDecoder( "RichFutureDecode" )

        prefix = "Hlt2Rich"

        tracks = self.hlt2Tracking()

        from Configurables import Rich__Future__Rec__TrackFilter as TrackFilter
        tkFiltLong = TrackFilter(prefix+"TrackLongFilter")
        tkFiltLong.InTracksLocation      = tracks.outputSelection()[0]
        tkFiltLong.OutLongTracksLocation = tracks.outputSelection()[0]+"RichLong"
        tkFiltLong.OutDownTracksLocation = tracks.outputSelection()[0]+"RichLongDown"
        tkFiltLong.OutUpTracksLocation   = tracks.outputSelection()[0]+"RichLongUp"

        tkFiltDown = TrackFilter(prefix+"TrackDownFilter")
        tkFiltDown.InTracksLocation      = tracks.outputSelection()[1]
        tkFiltDown.OutLongTracksLocation = tracks.outputSelection()[1]+"RichDownLong"
        tkFiltDown.OutDownTracksLocation = tracks.outputSelection()[1]+"RichDown"
        tkFiltDown.OutUpTracksLocation   = tracks.outputSelection()[1]+"RichDownUp"

        tkLocs  = { "Long" : tkFiltLong.OutLongTracksLocation,
                    "Downstream" : tkFiltDown.OutDownTracksLocation }

        PIDLoc = "{}/PID/{}/".format(Hlt2TracksPrefix,HltRICHIDSuffix)
        pidLocs = { "Long" : PIDLoc +"Long",
                    "Downstream" : PIDLoc+"Downstream"}

        richSeq = GaudiSequencer(prefix+"FutureSeq")
        # Workaround to configure only once
        if self.__trackType()=="Long":
            from RichFutureRecSys.ConfiguredRichReco import RichRecoSequence
            Hlt2RichRec = RichRecoSequence( GroupName = prefix+"Future",
                                            dataType = self.getProp("DataType"),
                                            onlineBrunelMode = False,
                                            inputTrackLocations = tkLocs,
                                            outputPIDLocations  = pidLocs,
                                            mergedOutputPIDLocation = "" ) # set to empty otherwise inserting into keyed container fails
            richSeq.Members = [richDecode, tkFiltLong, tkFiltDown, Hlt2RichRec]

        from HltLine.HltLine import bindMembers
        # Build the bindMembers
        bm_name         = prefix + "FutureIDSeq"
        bm_members      = [tracks, richSeq]
        bm_output       = pidLocs[self.__trackType()]

        return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)
    #########################################################################################
    #
    # Staged fast fit
    #
    def __hlt2StagedFastFit(self) :
        """
        The staged fast-fit. Currently set to
        be bidirectional and with smoothing to allow PID information
        to be used; special cases need to be justified separately
        """
        from HltTracking.HltSharedTracking import LongBest, DownBest
        if self.__trackType() == "Long":
          return LongBest
        elif self.__trackType() == "Downstream":
          return DownBest
        else:
          raise AttributeError("__hlt2StagedFastFit() can't cope with trackType "
              + self.__trackType())

    #########################################################################################
    #
    # Track reconstruction
    # TODO: Separate long and downstream tracks?
    def __hlt2Tracking(self) :
        """
        Track reconstruction
        """
        from HltTracking.HltSharedTracking import AllBest
        return AllBest

    #########################################################################################
    #
    # Hlt2 Velo Reconstruction
    #
    def __hlt2VeloTracking(self):
        """
        Velo track reconstruction for Hlt2
        """
        #HltSharedTracking decides which Velo sequence is used
        from HltSharedTracking        import RevivedVelo, MinimalVelo
        from Configurables  import Hlt2Conf
        from HltLine.HltLine    import bindMembers

        veloTracksOutputLocation = HltSharedTrackLoc["Velo"]

        Velo = RevivedVelo

        ## for debugging:
        #from Configurables import DumpTracks
        #veloDumper = DumpTracks('VeloDumper',TracksLocation = veloTracksOutputLocation )
        #veloDumper.StatPrint = True

        # Build the bindMembers
        bm_name         = self.getProp("Prefix")+"VeloTracking"
        bm_members      =  Velo.members()
        bm_output       = veloTracksOutputLocation

        return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)


    #########################################################################################
    #
    # Hlt2 Forward Reconstruction
    #
    def __hlt2ForwardTracking(self) :
        """
        Forward track reconstruction for Hlt2
        """
        from HltTracking.HltSharedTracking import LongBest
        return LongBest

    #########################################################################################
    #
    # Hlt2 Seeding for forward,downstream reconstruction
    #
    def __hlt2SeedTracking(self):
        """
        Seeding in the trackers for later use in Match Forward reconstruction
        or in downstream tracking.
        """
        from Configurables    import PatSeeding
        from Configurables      import PatSeedingTool

        from HltLine.HltLine    import bindMembers

        # We depend on the forward tracking
        fwdtracks = self.hlt2ForwardTracking()
        # Now our output location
        seedTrackOutputLocation    = Hlt2TrackLoc["Seeding"]

        #### Seeding
        vetoTrackLocations = [ fwdtracks.outputSelection() ]
        if HltRecoConf().getProp("OfflineSeeding"):
            vetoTrackLocations = None
        from HltTracking.HltSharedTracking import ConfiguredPatSeeding
        recoSeeding = ConfiguredPatSeeding(self.getProp("Prefix")+'Seeding',
                                           OutputTracksName = seedTrackOutputLocation,
                                           VetoTrackLocations = vetoTrackLocations )

        # Build the bindMembers
        bm_name         = self.getProp("Prefix")+"SeedTracking"
        bm_members      = self.hlt2VeloTracking().members() + self.__hlt2TrackerDecoding().members() + \
                          self.hlt2ForwardTracking().members() + [recoSeeding]
        bm_output       = seedTrackOutputLocation

        return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)
    #########################################################################################
    #
    # Hlt2 Match Forward Reconstruction
    #
    def __hlt2MatchTracking(self):
        """
        Forward track reconstruction for Hlt2 using seeding
        """
        from Configurables    import PatMatch
        from HltLine.HltLine    import bindMembers
        fwdtracks = self.hlt2ForwardTracking()
        matchTrackOutputLocation = Hlt2TrackLoc["Match"]
        # TODO: We can restrict the input of the matching by vetoing already used velo tracks.
        #### Matching
        recoMatch         = PatMatch(self.getProp("Prefix")+'Match'
                                     , VeloInput = self.hlt2VeloTracking().outputSelection()
                                     , SeedInput = self.hlt2SeedTracking().outputSelection()
                                     , MatchOutput = matchTrackOutputLocation)
        from Configurables import PatMatchTool
        recoMatch.addTool(PatMatchTool, name="PatMatchTool")
        # We depend on the forward tracking
        if HltRecoConf().getProp("OfflineSeeding"):
            recoMatch.PatMatchTool.VeloVetoTracksName = []
        else:
            recoMatch.PatMatchTool.VeloVetoTracksName = [ fwdtracks.outputSelection() ]
            recoMatch.PatMatchTool.MinMomentum = HltRecoConf().getProp("Forward_LPT_MinP")
            recoMatch.PatMatchTool.MinPt = HltRecoConf().getProp("Forward_LPT_MinPt")


        # Build the bindMembers
        bm_name         = self.getProp("Prefix")+"MatchTracking"
        bm_members      = self.hlt2VeloTracking().members() + self.hlt2ForwardTracking().members() +self.hlt2SeedTracking().members()
        if self.getProp("DoCleanups") and not self.getProp("UseTrackBestTrackCreator"):
            from HltTracking.HltSharedTracking import ( ConfiguredHltEventFitter, ConfiguredGoodTrackFilter )
            fitHlt2MatchTracks = ConfiguredHltEventFitter(name = self.getProp("Prefix") + 'FitHlt2MatchTracks',
                                                          TracksInContainer = matchTrackOutputLocation)
            filterHlt2MatchTracks = ConfiguredGoodTrackFilter( self.getProp("Prefix") + 'FilterHlt2MatchTracks',
                                                               InputLocation = matchTrackOutputLocation )
            matchTrackOutputLocation = filterHlt2MatchTracks.outputLocation
            bm_members += [ recoMatch, fitHlt2MatchTracks, filterHlt2MatchTracks ]
        else:
            bm_members += [ recoMatch ]
        if self.getProp("EarlyDataTracking") :
            # Do something special in case of early data
            # For the moment just a dummy setting
            pass

        bm_output       = matchTrackOutputLocation

        return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)
    #########################################################################################
    #
    # Hlt2 Downstream Forward Reconstruction
    #
    def __hlt2DownstreamTracking(self):
        """
        Downstream track reconstruction for Hlt2 using seeding
        """
        from HltTracking.HltSharedTracking import DownBest
        return DownBest

    #########################################################################################
    #
    # Hlt2 tracker decoding
    #
    def __hlt2TrackerDecoding(self):
        """
        Decode the ST for Hlt2
        """
        from HltLine.HltDecodeRaw     import DecodeTT, DecodeIT
        from HltLine.HltLine        import bindMembers
        return bindMembers(self.getProp("Prefix")+"DecodeSTSeq", DecodeTT.members() + DecodeIT.members())

    #########################################################################################
    #
    # Helper function to set up the CALO processor and return the correct sequence
    #
    def __getNewCALOSeq(self, mode ):
        from HltLine.HltLine    import bindMembers
        # Load tracks
        tracks        = self.hlt2StagedFastFit()

        #neutralProtosLowEtOutputLocation = self.__protosLocation(Hlt2LowEtNeutralProtoParticleSuffix)
        #neutralProtosHighEtOutputLocation = self.__protosLocation(Hlt2HighEtNeutralProtoParticleSuffix)

        #outputCALOPID            = self.__caloIDLocation()
        caloName = "HLT2CaloLines"+ mode.capitalize() + self.__trackType()
        # Create configurable
        from Configurables import CaloLines
        caloLines = CaloLines(caloName)
        bm_name    = self.__pidAlgosAndToolsPrefix() + mode.capitalize() + self.__trackType()
        bm_members = [tracks]
        bm_output = ''
        suffix = None
        if mode.lower() == 'photon':
            caloLines.HighPhoton = True
            caloLines.LowPhoton = False
            caloLines.LowElectron = False
            bm_name  += 'PhotonsFromL0'
            bm_output = '/Event/'+caloName+'HighPhoton/ProtoP/Neutrals'
            suffix = 'HighPhoton'
        elif mode.lower() == 'electron':
            caloLines.HighPhoton = False
            caloLines.LowPhoton = False
            caloLines.LowElectron = True
            from Configurables import ChargedProtoParticleMaker
            bm_name  += 'ElectronsFromL0'
            bm_output = '/Event/' + caloName + 'LowElectron/ProtoP/Charged'
            chargedName = self.__pidAlgosAndToolsPrefix() + bm_output.replace("/", "") + 'ChargedProtoPAlg'
            charged     = ChargedProtoParticleMaker(chargedName)
            charged.TrackSelectorType = "TrackSelector"
            charged.addTool(TrackSelector, name="TrackSelector")
            ts=charged.TrackSelector
            ts.setProp("MinChi2Cut", 0.)
            ts.setProp("MaxChi2Cut", HltRecoConf().getProp("MaxTrCHI2PDOF"))
            if HltRecoConf().getProp("ApplyGHOSTPROBCut") == True:
                ts.setProp("MinGhostProbCut", 0.)
                ts.setProp("MaxGhostProbCut", HltRecoConf().getProp("MaxTrGHOSTPROB"))

            if self.__trackType() == "Long":
                ts.TrackTypes = ["Long"]
            elif self.__trackType() == "Downstream":
                ts.TrackTypes = ["Downstream"]

            charged.Inputs = [tracks.outputSelection()]
            charged.Output = bm_output
            bm_members += [charged]
            suffix = 'LowElectron'
        elif mode.lower() == 'pi0':
            caloLines.HighPhoton = False
            caloLines.LowPhoton = True
            caloLines.LowElectron = False
            bm_name  += 'Pi0FromL0'
            bm_output = '/Event/'+caloName+'LowPhoton/ProtoP/Neutrals'
            suffix = 'LowPhoton'

        stopCalo = self._startCalo(
            caloLines,
            CaloProcessor(caloName + suffix),
            CaloPIDsConf('CaloPIDsFor' + caloName + suffix),
            )
        seq = caloLines.sequence(tracks=tracks.outputSelection())
        stopCalo()

        bm_members += [seq]
        return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)

    #########################################################################################
    #
    # Helper functions to set up the CALO processor and return the correct sequences
    #

    def caloProcessor(self):
        if not hasattr(self, '__caloProcessor') or not self.__caloProcessor:
            caloProcessorName = "SharedCaloProcessor"
            self.__caloProcessor = CaloProcessor(caloProcessorName)
            self.__caloProcessor.Context = self._caloIDLocation()
            self.__caloProcessor.NoSpdPrs = True
            # Do the reconstruction and the PID but do not make or update the protoparticles here!
            self.__caloProcessor.CaloReco     = True
            self.__caloProcessor.CaloPIDs     = True
            from LoKiCore.basic import LHCb
            self.__caloProcessor.CaloPIDTrTypes = [LHCb.Track.Long,LHCb.Track.Downstream]
            #
            # Check if we are making neutrals or not
            #
            self.__caloProcessor.SkipNeutrals    = False
            self.__caloProcessor.SkipCharged     = False
        return self.__caloProcessor

    def __getCALOSeq(self,mode):
        """
        Defines the CALO processor and, depending on the mode,
        returns the PID, charged proto, or neutral proto sequences
        """
        # We need all tracks: Long, Downstream and T-tracks
        tracks = self.hlt2Tracking()
        #trackLocations = tracks.outputSelection()
        #CaloReco is modified to not take multiple track locations under new Gaudi framework
        trackLocation = "Rec/Track/MergedTracksForCaloReco"
        from Configurables import TrackListMerger
        merger = TrackListMerger(self.getProp("Prefix") + "TrackMergerForCaloReco")
        merger.InputLocations = tracks.outputSelection()
        merger.OutputLocation = trackLocation 

        # One single CaloProcessor for all Hlt2Tracking instances
        myCALOProcessor         = self.caloProcessor()
        outputCALOPID           = myCALOProcessor.Context

        # The sequences are given the track and protoparticle locations when initializing
        from HltLine.HltLine    import bindMembers
        bm_name = self.__pidAlgosAndToolsPrefix()

        if ( mode == "PID" ) :
            stopCalo = self._startCalo(
                myCALOProcessor,
                CaloDigitConf(),
                CaloPIDsConf('CaloPIDsFor' + myCALOProcessor.getName()),
                )
            myPIDSeq        = myCALOProcessor.caloSequence(  trackLocation  )
            stopCalo()
            bm_name         += "CALOPIDSeq"
            bm_members      = [tracks, myPIDSeq]
            bm_output       = outputCALOPID
            return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)

        if ( mode == "Neutral" ) :
            bm_name         += "CaloNeutralSeq"
            neutralProtosOutputLocation = self.__neutralProtosLocation()
            stopCalo = self._startCalo(
                myCALOProcessor,
                CaloDigitConf(),
                CaloPIDsConf('CaloPIDsFor' + myCALOProcessor.getName()),
                )
            myNeutralSeq    = myCALOProcessor.neutralProtoSequence(     trackLocation ,    neutralProtosOutputLocation    )
            stopCalo()
            bm_members      = self.__getCALOSeq("PID").members()+ [ myNeutralSeq ]
            bm_output       = myCALOProcessor.NeutralProtoLocation
            return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)

        if ( mode == "Charged" ) :
            assert False, "This function call is deprecated. Please use AllPIDs protos"
            bm_name         += "CaloChargedSeq"
            bm_members      = [tracks, myPIDSeq, chargedProtos, myChargedSeq]
            bm_output       = chargedProtosOutputLocation
            return bindMembers(bm_name, bm_members).setOutputSelection(bm_output)

    # Damn you calo configurables!
    @staticmethod
    def _startCalo(*args):
        """Pretend that ConfigurableUser-s are being applied.

        The CALO configurables are applying themselves, thus if we are
        to do some automatic dependency enforcement, we should emulate
        their application.
        The ConfigurableUser._applying member is not in Gaudi v28r2, but
        is something needed for the enforcment. (RM: it is in my fork).

        """
        from functools import partial

        def f(applying, applied):
            for c in args:
                if applying:
                    log.info("emulate applying configuration of %s", c.name())
                try:
                    c._applying = applying
                except AttributeError:
                    pass
                c._applied = applied

        f(True, False)
        return partial(f, False, True)
