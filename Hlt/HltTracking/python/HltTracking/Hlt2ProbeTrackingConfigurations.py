###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Tag and probe tracking."""

from HltTracking.Hlt2ProbeTracking import Hlt2ProbeTracking


def Hlt2MuonTTTracking(_enabled=True):
    return Hlt2ProbeTracking('Hlt2MuonTTTracking',
                             _enabled=_enabled,
                             ProbeTrack='MuonTT')


def Hlt2VeloMuonTracking(_enabled=True):
    return Hlt2ProbeTracking('Hlt2VeloMuonTracking',
                             _enabled=_enabled,
                             ProbeTrack='VeloMuon')


def Hlt2FullDownstreamTracking(_enabled=True):
    return Hlt2ProbeTracking('Hlt2FullDownstreamTracking',
                             _enabled=_enabled,
                             ProbeTrack='FullDownstream')
