#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## @file HltTracking/HltSharedTracking.py
#  was HltReco.py
#  Collection of predefined algorithms to perform reconstruction
#  @author Sebastian Neubert sebastian.neubert@cern.ch, Gerhard Raven Gerhard.Raven@nikhef.nl
# =============================================================================
#

__all__ = ( 'MinimalVelo'
          , 'RevivedVelo'
          , 'RevivedForward'
          , 'Hlt1Seeding'
          , 'HltHPTTracking'
          , 'VeloTTTracking'
          )

############################################################################################
# Option to decide which pattern to use
############################################################################################
#############################################################################################
# Import Configurables
#############################################################################################
from Gaudi.Configuration import *
from Configurables import GaudiSequencer
from Configurables import FastVeloTracking
from Configurables import PVOfflineTool
from HltLine.HltLine import bindMembers
from Configurables import PatSeeding, PatSeedingTool

#############################################################################################
# Configure pattern recognition algorithms
#############################################################################################

from HltTrackNames import Hlt1TrackLoc, HltSharedTrackLoc, Hlt2TrackLoc

from Configurables import TrackStateInitAlg, TrackStateInitTool
from Configurables import ToolSvc, TrackMasterExtrapolator, TrackStateProvider, TrackInterpolator, StateThickMSCorrectionTool
from Configurables import SimplifiedMaterialLocator
from HltTracking.HltRecoConf import HltRecoConf

#### Velo Tracking
# the full Velo reconstruction
def recoVelo(OutputTracksName=HltSharedTrackLoc["Velo"]):
    recoVelo = FastVeloTracking( 'FastVeloHlt', OutputTracksName = OutputTracksName)
    recoVelo.StatPrint = True
    extendVelo = HltRecoConf().getProp("BeamGasMode")
    if extendVelo:
        recoVelo.ZVertexMin = HltRecoConf().getProp("VeloTrackingZMin")
        recoVelo.ZVertexMax = HltRecoConf().getProp("VeloTrackingZMax")
    recoVelo.VetoObjects = [ OutputTracksName ]
    return recoVelo


#### filter the Velo output
from Configurables import TrackListRefiner
filterVelo = TrackListRefiner('VeloFilter',
                              inputLocation = HltSharedTrackLoc["Velo"],
                              outputLocation = HltSharedTrackLoc["VeloSelection"])
from Configurables import  LoKi__Hybrid__TrackSelector as LoKiTrackSelector
filterVelo.addTool(LoKiTrackSelector,name="VeloSelector")
filterVelo.Selector = LoKiTrackSelector(name="VeloSelector")
veloSelector = filterVelo.Selector
veloSelector.Code = HltRecoConf().getProp("VeloSelectionCut")
veloSelector.StatPrint = True

def fittedVelo(inputTracks, outputTracks, name='VeloOnlyFitterAlg'):
    from Configurables import TrackEventFitter, TrackInitFit
    from Configurables import TrackStateInitTool, TrackMasterFitter
    if HltRecoConf().getProp("FastFitVelo"):
        # For now use the save option where Velo tracks are copied and the original ones are not changed
        from Configurables import TrackStateInitAlg, TrackStateInitTool,FastVeloFitLHCbIDs
        from Configurables import TrackContainerCopy
        copy = TrackContainerCopy(name+"CopyVelo")
        copy.inputLocations = [ inputTracks ]
        copy.outputLocation = outputTracks
        init = TrackStateInitAlg(name)
        init.TrackLocation = copy.outputLocation
        init.addTool(TrackStateInitTool, name="StateInitTool")
        init.StateInitTool.VeloFitterName = "FastVeloFitLHCbIDs"
        init.StateInitTool.addTool(TrackMasterExtrapolator, "Extrapolator")
        init.StateInitTool.Extrapolator.addTool(SimplifiedMaterialLocator,
                                                name = "MaterialLocator")
        init.StateInitTool.addTool(FastVeloFitLHCbIDs,name="FastVeloFitLHCbIDs")
        init.StateInitTool.FastVeloFitLHCbIDs.UseKalmanFit = True
        return [copy,init]
    else:
        fa = TrackEventFitter(name)
        fa.TracksInContainer = inputTracks
        fa.TracksOutContainer = outputTracks
        fa.Fitter = "TrackInitFit/Fit"
        fa.addTool(TrackInitFit, "Fit")
        fa.Fit.Init = "TrackStateInitTool/VeloOnlyStateInit"
        fa.Fit.addTool(TrackStateInitTool, "VeloOnlyStateInit")
        fa.Fit.VeloOnlyStateInit.VeloFitterName = "FastVeloFitLHCbIDs"
        fa.Fit.VeloOnlyStateInit.addTool(TrackMasterExtrapolator, "Extrapolator")
        fa.Fit.VeloOnlyStateInit.Extrapolator.addTool(SimplifiedMaterialLocator,
                                                      name = "MaterialLocator")
        fa.Fit.Fit = "TrackMasterFitter/Fit"
        fa.Fit.addTool(TrackMasterFitter, name = "Fit")
        from TrackFitter.ConfiguredFitters import ConfiguredForwardFitter
        fitter = ConfiguredForwardFitter(fa.Fit.Fit)
        return [ fa ]

#############################################################################################
# HLT2 tracking codes
#############################################################################################
def ConfiguredForwardComplement(name
                                , InputTracksName #= HltSharedTrackLoc["Velo"]
                                , OutputTracksName #= Hlt2TrackLoc["ForwardComp"]
                                , VetoSeedLocations #= [ HltSharedTrackLoc["ForwardHPT"] ]
                                , MinMomentum #= HltRecoConf().getProp("Forward_LPT_MinP")
                                , MinPt #= HltRecoConf().getProp("Forward_LPT_MinPt")
                                ):
    if name == None:
        log.fatal( '##################################################################################')
        log.fatal( '## FATAL You did not specify the name of the PatForward instance.' )
        log.fatal( '## FATAL I will now die, you need to make Hlt2Tracking aware of the name!  ')
        log.fatal( '##################################################################################')
    forward = PatForward( name
                          , InputTracksName  = InputTracksName
                          , OutputTracksName = OutputTracksName
                          )

    #Sascha Stahl: We should get rid of GECs in the pattern reco, do it centrally
    from HltTracking.HltRecoConf import CommonForwardOptions
    forward.maxOTHits = HltRecoConf().getProp("Forward_MaxOTHits")
    forward.maxITHits = CommonForwardOptions["MaxITHits"]
    forward.MaxNVelo = CommonForwardOptions["MaxNVelo"]

    from HltTracking.HltRecoConf import CommonForwardTrackingOptions, ComplementForwardToolOptions
    opts = CommonForwardTrackingOptions.copy()
    opts.update(ComplementForwardToolOptions)
    opts.update({"MinMomentum" : MinMomentum
                 ,"MinPt" : MinPt })
    forward.addTool(PatForwardTool(**opts), name='PatForwardTool')

    from Configurables import TrackUsedLHCbID
    if VetoSeedLocations != None and  len(VetoSeedLocations) != 0:
        forward.PatForwardTool.addTool(TrackUsedLHCbID, name='TrackUsedLHCbID')
        forward.PatForwardTool.UsedLHCbIDToolName="TrackUsedLHCbID"
        forward.PatForwardTool.TrackUsedLHCbID.inputContainers = map(str, VetoSeedLocations)
        forward.PatForwardTool.VeloVetoTracksNames = map(str, VetoSeedLocations)
    # make them a bit more verbose
    forward.StatPrint = True
    forward.PatForwardTool.StatPrint = True
    return forward

def ConfiguredPatSeeding(name
                         , OutputTracksName
                         , VetoTrackLocations = None):
    if name == None:
        log.fatal( '##################################################################################')
        log.fatal( '## FATAL You did not specify the name of the PatSeeding instance.' )
        log.fatal( '## FATAL I will now die, you need to make Hlt2Tracking aware of the name!  ')
        log.fatal( '##################################################################################')
    from Configurables import PatSeeding
    from Configurables import PatSeedingTool
    recoSeeding = PatSeeding(name, OutputTracksName = OutputTracksName )
    recoSeeding.addTool(PatSeedingTool, name="PatSeedingTool")
    # New tool
    if VetoTrackLocations != None and  len(VetoTrackLocations) != 0:
        from Configurables import TrackUsedLHCbID
        recoSeeding.PatSeedingTool.UsedLHCbIDToolName = "TrackUsedLHCbID"
        recoSeeding.PatSeedingTool.addTool( TrackUsedLHCbID, name="TrackUsedLHCbID" )
        recoSeeding.PatSeedingTool.TrackUsedLHCbID.inputContainers = VetoTrackLocations

    from HltTracking.HltRecoConf import OnlineSeedingToolOptions, OfflineSeedingToolOptions
    if HltRecoConf().getProp("OfflineSeeding"):
        recoSeeding.PatSeedingTool.NDblOTHitsInXSearch = OfflineSeedingToolOptions ["NDblOTHitsInXSearch"]
        recoSeeding.PatSeedingTool.MinMomentum = OfflineSeedingToolOptions["MinMomentum"]
    else:
        recoSeeding.PatSeedingTool.NDblOTHitsInXSearch = OnlineSeedingToolOptions ["NDblOTHitsInXSearch"]
        recoSeeding.PatSeedingTool.MinMomentum = OnlineSeedingToolOptions["MinMomentum"]
    from HltTracking.HltRecoConf import CommonForwardOptions
    recoSeeding.PatSeedingTool.MaxOTHits = HltRecoConf().getProp("Forward_MaxOTHits")
    recoSeeding.PatSeedingTool.MaxITHits = CommonForwardOptions["MaxITHits"]
    return recoSeeding

#TODO: Move this to TrackFitter package?
def ConfigureFitter( fitter ):
    from TrackFitter.ConfiguredFitters import ConfiguredHltFitter, ConfiguredMasterFitter
    # configure the fitter
    if not HltRecoConf().getProp("MoreOfflineLikeFit"):
        ConfiguredHltFitter( fitter )
        fitter.NumberFitIterations = HltRecoConf().getProp("FitIterationsInHltFit")
    else:
        ConfiguredMasterFitter( fitter , SimplifiedGeometry = True, LiteClusters = True)#, MSRossiAndGreisen = HltRecoConf().getProp("NewMSinFit"))
    # Ignore Muon hits
    fitter.MeasProvider.IgnoreMuon = True
    return fitter

def ConfiguredHltInitFitter( parent ):
    from Configurables import ( TrackEventFitter, TrackMasterFitter, TrackInitFit, TrackStateInitTool )
    if not HltRecoConf().getProp("InitFits"):
        parent.addTool( TrackMasterFitter, name="Fitter")
        fitter = parent.Fitter
    else:
        parent.addTool( TrackInitFit, name = "Fitter")
        parent.Fitter.Init = "TrackStateInitTool/StateInit"
        parent.Fitter.addTool(TrackStateInitTool, name="StateInit")
        parent.Fitter.StateInit.VeloFitterName = "FastVeloFitLHCbIDs"
        parent.Fitter.StateInit.UseFastMomentumEstimate = True
        parent.Fitter.StateInit.addTool(TrackMasterExtrapolator, "Extrapolator")
        parent.Fitter.StateInit.Extrapolator.MaterialLocator = "SimplifiedMaterialLocator"
        parent.Fitter.Fit = "TrackMasterFitter/Fitter"
        parent.Fitter.addTool(TrackMasterFitter, name="Fitter")
        fitter = parent.Fitter.Fitter
    ConfigureFitter( fitter )
    return fitter

def ConfiguredHltEventFitter( name,
                              TracksInContainer,
                              TracksOutContainer = None):
    # create the event fitter
    from Configurables import ( TrackEventFitter, TrackMasterFitter, TrackInitFit, TrackStateInitTool )

    eventfitter = TrackEventFitter(name)
    eventfitter.TracksInContainer = TracksInContainer
    if TracksOutContainer != None:
        eventfitter.TracksOutContainer = TracksOutContainer
    ConfiguredHltInitFitter(eventfitter)
    return eventfitter

def ConfiguredGoodTrackFilter (name,
                               InputLocation,
                               OutputLocation = None):
    if name == None:
        log.fatal( '##################################################################################')
        log.fatal( '## FATAL You did not specify the name of the Filter instance.' )
        log.fatal( '## FATAL I will now die, you need to make Hlt2Tracking aware of the name!  ')
        log.fatal( '##################################################################################')
    if OutputLocation == None: OutputLocation = InputLocation + "Filtered"
    from Configurables import TrackListRefiner
    filterTracks = TrackListRefiner( name,
                                     inputLocation = InputLocation,
                                     outputLocation = OutputLocation)
    from Configurables import  LoKi__Hybrid__TrackSelector as LoKiTrackSelector
    filterTracks.addTool(LoKiTrackSelector,name="LokiTracksSelector")
    filterTracks.Selector = LoKiTrackSelector(name="LokiTracksSelector")
    filterTracks.Selector.Code = "(~TrINVALID) & ( TrCHI2PDOF < %(TrChi2)s )" % {"TrChi2":  HltRecoConf().getProp("GoodTrCHI2PDOF")}
    filterTracks.Selector.StatPrint = True
    return filterTracks

#############################################################################################
# Define modules for the reconstruction sequence
#############################################################################################
from HltLine.HltDecodeRaw import DecodeVELO, DecodeTRACK, DecodeTT, DecodeIT

### define exported symbols (i.e. these are externally visible, the rest is NOT)
#This is the part which is shared between Hlt1 and Hlt2
MinimalVelo = bindMembers( None, [DecodeVELO, recoVelo( OutputTracksName=HltSharedTrackLoc["Velo"] ) ] ).setOutputSelection( HltSharedTrackLoc["Velo"] )
# We have to remove the decoder from the sequence if disabled otherwise the PV unit complains and does not run.
veloAlgs = [DecodeVELO, recoVelo( OutputTracksName=HltSharedTrackLoc["Velo"] )]
RevivedVelo = bindMembers( None, veloAlgs ).setOutputSelection( HltSharedTrackLoc["Velo"] )
FittedVelo  = bindMembers( None, RevivedVelo.members() + fittedVelo(RevivedVelo.outputSelection(), Hlt1TrackLoc["FittedVelo"])).setOutputSelection(Hlt1TrackLoc["FittedVelo"])

# Revamped tracking configuration for the upgrade HLT
from Configurables import TrackSys, TrackBestTrackSplitter
from TrackSys.RecoUpgradeTracking import RecoFastTrackingStage, \
    RecoBestTrackingStage, RecoBestTrackCreator
HltTrackLocations = TrackSys().DefaultTrackLocations
HltConvertedTrackLocations = TrackSys().DefaultConvertedTrackLocations

# Upgrade velo tracking
track_locs, reco_algs = RecoFastTrackingStage(defTracks = HltTrackLocations,
    trackTypes = [ 'Velo' ], includePVs = False)
FittedVelo = bindMembers(None, reco_algs).setOutputSelection(
    track_locs['VeloFitted']['Location'])

# Upgrade forward tracking -- without fit
track_locs, reco_algs = RecoFastTrackingStage(defTracks = HltTrackLocations,
    trackTypes = [ 'Velo', 'Upstream', 'Forward' ], fitForward = False,
    includePVs = False)
ForwardFast = bindMembers(None, reco_algs).setOutputSelection(
  track_locs['ForwardFast']['Location'])
HltHPTTracking = ForwardFast # backwards compatible name

# Upgrade forward tracking -- with fit
track_locs, reco_algs = RecoFastTrackingStage(defTracks = HltTrackLocations,
    trackTypes = [ 'Velo', 'Upstream', 'Forward' ], includePVs = False)
ForwardFastFitted = bindMembers(None, reco_algs).setOutputSelection(
  track_locs['ForwardFastFitted']['Location'])

# Upgrade upstream tracking
track_locs, reco_algs = RecoFastTrackingStage(defTracks = HltTrackLocations,
    trackTypes = [ 'Velo', 'Upstream'], includePVs = False)
Upstream = bindMembers(None, reco_algs).setOutputSelection(
    track_locs['Upstream']['Location'])
VeloTTTracking = Upstream

# Upgrade best sequence, make several versions of this with different output
# locations
track_locs, best_reco_algs = RecoBestTrackingStage(
    defTracks = HltTrackLocations,
    trackTypes = [ 'Velo', 'Forward', 'Seeding', 'Match', 'Downstream' ],
    includePVs = False)

# Upgrade best track creator -- merge the forward/match/downstream tracks and
# remove clones
best_tracks_output_location = "Rec/Track/Best"
best_tracks_algs = RecoBestTrackCreator(defTracks = track_locs,
                                        defTracksConverted = HltConvertedTrackLocations,
                                        out_container = best_tracks_output_location)

# Calculate GhostProb in the Hlt2 reconstruction
bestTrackCreator = best_tracks_algs[-1] # Eww
bestTrackCreator.AddGhostProb = True
if HltRecoConf().getProp("ApplyGHOSTPROBCutInTBTC"):
  bestTrackCreator.MaxGhostProb = HltRecoConf().getProp("MaxTrGHOSTPROB")

# Split the clone-killed best track container into long/downstream for use in
# the HLT
from Gaudi.Configuration import DEBUG
best_long_tracks = "Rec/Track/Long"
best_down_tracks = "Rec/Track/DownstreamBest"
best_tracks_splitter = TrackBestTrackSplitter("Hlt2TrackBestTrackSplitter",
    TracksInContainerLoc = best_tracks_output_location,
    TracksOutContainerLocs = [ best_long_tracks, best_down_tracks ],
    TracksOutContainerTypes = [ "Long", "Downstream" ])

all_best_algs = best_reco_algs + best_tracks_algs + [ best_tracks_splitter ]
AllBest =bindMembers(None,all_best_algs).setOutputSelection([
  best_long_tracks, best_down_tracks ])
LongBest=bindMembers("HltLongBest", all_best_algs).setOutputSelection(
    best_long_tracks)
DownBest=bindMembers("HltDownBest", all_best_algs).setOutputSelection(
    best_down_tracks)
