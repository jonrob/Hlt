/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files 

//from Event
#include "Event/Track.h"

// local
#include "HltTrackFit.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltTrackFit
//
// 2008-10-31 : Johannes Albrecht
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( HltTrackFit )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
HltTrackFit::HltTrackFit( const std::string& type,
                          const std::string& name,
                          const IInterface* parent )
  : base_class ( type, name , parent )
{
  declareInterface<ITracksFromTrack>(this);
  declareProperty("FitterName", m_fitName = "TrackMasterFitter/Fit");
  declareProperty("FailureWarnThreshold", m_failureWarnThreshold = 0.0005);
}

//=============================================================================
StatusCode HltTrackFit::initialize() 
{
  StatusCode sc = GaudiTool::initialize();
  if (sc.isSuccess()) m_fit = tool<ITrackFitter>(m_fitName, this);
  m_fitFailureCounter = &counter("FitFailed");
  return sc;
}

//=============================================================================
StatusCode HltTrackFit::tracksFromTrack( const LHCb::Track& seed,
		                                 std::vector<LHCb::Track*>& tracks ) const
{
  auto tr = std::make_unique<LHCb::Track>(seed);
  StatusCode sc = m_fit->operator()( *tr );
  *m_fitFailureCounter += sc.isFailure();
  if( sc.isFailure() ) {
    if ( m_failureWarnThreshold * m_fitFailureCounter->nEntries() > 1. && m_fitFailureCounter->mean() > m_failureWarnThreshold )
      warning() << "Fit failure rate high. " << endmsg;
  } else {
    tracks.push_back( tr.release() );
  }
  return sc;
}
