/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: HltTrackFit.h,v 1.1 2008-11-03 08:10:27 albrecht Exp $
#ifndef HLTTRACKFIT_H 
#define HLTTRACKFIT_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "TrackInterfaces/ITracksFromTrack.h"            // Interface
#include "TrackInterfaces/ITrackFitter.h"
// Forward declarations
class StatEntity;

/** @class HltTrackFit HltTrackFit.h
 *
 *  Class to do a track fit for the HLT. To be called via HltTrackUpgrade  
 *  with RecoName = "FitTrack"
 *
 *  The idea is to use the fast track fit, to be configured via options. 
 *  Subdetectors can be switched off via options.
 *  (if you just run this tool, you get the offline fit)
 *
 *
 *  @author Johannes Albrecht
 *  @date   2008-10-31
 */
class HltTrackFit : public extends<GaudiTool, ITracksFromTrack> {
public: 
  /// Standard constructor
  HltTrackFit( const std::string& type, 
               const std::string& name,
               const IInterface* parent);

  /// Initialize method
  StatusCode initialize() override;

  StatusCode tracksFromTrack( const LHCb::Track& seed,
                              std::vector<LHCb::Track*>& tracks ) const override;
  
private:

  std::string m_fitName;
  ITrackFitter* m_fit = nullptr;
  double m_failureWarnThreshold;
  StatEntity* m_fitFailureCounter{nullptr};
  
};
#endif // HLTTRACKFIT_H
