###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Hlt2Lines.Utilities.Hlt2Filter   import Hlt2ParticleFilter
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2VoidFilter
from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner

from Inputs import Hlt2NoPIDsPions
from Inputs import Hlt2NoPIDsKaons


# The GEC
class TrackGEC(Hlt2VoidFilter):
    def __init__(self, name):
        from Configurables import LoKi__Hybrid__CoreFactory as Factory
        modules =  Factory().Modules
        for i in [ 'LoKiTrigger.decorators' ] :
            if i not in modules : modules.append(i)

        from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking as Hlt2LongTracking
        tracks = Hlt2LongTracking().hlt2PrepareTracks()
        code = ("CONTAINS('%s')" % tracks.outputSelection()) + " < %(NTRACK_MAX)s"
        Hlt2VoidFilter.__init__(self, 'TrackGEC', code, [tracks], shared = True)


class InputTrackFilter(Hlt2ParticleFilter) : # {

    def __init__(self, name, inputs): # {

        cut = ("  (TRCHI2DOF < %(TRCHI2DOF_MAX)s )" +
               "& (TRGHOSTPROB < %(GHOSTPROB_MAX)s)" +
               "& (PT > %(PT_MIN)s)" +
               "& (P > %(P_MIN)s)" +
               "& (MIPCHI2DV(PRIMARY) > %(MIPCHI2_MIN)s)")

        from HltTracking.HltPVs import PV3D
        Hlt2ParticleFilter.__init__(self, name, cut, inputs, shared = True,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    UseP2PVRelations = True)


class BachHadFilter(Hlt2ParticleFilter) : # {

    def __init__(self, name, inputs): # {

        cut = ("HASTRACK  "
               " & (PT > %(PT_MIN)s)" 
               " & (P > %(P_MIN)s)"
               )

        from HltTracking.HltPVs import PV3D
        Hlt2ParticleFilter.__init__(self, name, cut, inputs, shared = True,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    UseP2PVRelations = True)



InputNoPIDPions = InputTrackFilter("InputNoPIDPions",[Hlt2NoPIDsPions])
InputNoPIDKaons = InputTrackFilter("InputNoPIDKaons",[Hlt2NoPIDsKaons])
BachNoPIDPions = BachHadFilter("BachNoPIDPion",[InputNoPIDPions])
BachNoPIDKaons = BachHadFilter("BachNoPIDKaon",[InputNoPIDKaons])

class D2HHBuilder(Hlt2Combiner):
    def __init__(self,name,nickname,decay,inputs):


        DauCuts  = ( "%(DauCuts)s" )
        ComCuts  = ( "%(ComCuts)s" )
        MomCuts  = ( "%(MomCuts)s" )         

        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name,decay , inputs,                             
                              dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                              shared = True,
                              nickname = nickname,
                              DaughtersCuts  = {},
                              CombinationCut = ComCuts,
                              MotherCut      = MomCuts,
                              UseP2PVRelations = True,
                              Preambulo = [])


CombD2KPi = D2HHBuilder("CombD2KPi", "D2HH","[ D0 -> K- pi+ ]cc",[InputNoPIDKaons,InputNoPIDPions])
CombD2PiK = D2HHBuilder("CombD2PiK", "D2HH","[ D0 -> K+ pi- ]cc",[InputNoPIDKaons,InputNoPIDPions])
CombD2PiPi = D2HHBuilder("CombD2PiPi","D2HH","[ D0 -> pi- pi+ ]cc",[InputNoPIDPions])
CombD2KK = D2HHBuilder("CombD2KK","D2HH","[ D0 -> K- K+ ]cc",[InputNoPIDKaons])


class D2HHMerger(Hlt2ParticleFilter) : # {

    def __init__(self, name, nickname,inputs): # {

        cut = "%(PIDCuts)s"
        from HltTracking.HltPVs import PV3D
        Hlt2ParticleFilter.__init__(self, name, cut, inputs, shared = True,
                                    nickname = nickname,
                                    dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                                    UseP2PVRelations = True)

SelD2HH = D2HHMerger("D2HH","D2HH",[CombD2KPi,CombD2PiK,CombD2KK,CombD2PiPi])

class B2DHBuilder(Hlt2Combiner):
    def __init__(self,name,nickname,decay, inputs):

        DauCuts  = ( "%(DauCuts)s" )
        ComCuts  = ( "%(ComCuts)s" )
        MomCuts  = ( "%(MomCuts)s" ) 


        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name,decay, inputs,                             
                              dependencies = [TrackGEC('TrackGEC'),PV3D('Hlt2')],
                              nickname = nickname,
                              shared = True,
                              DaughtersCuts  = {},
                              CombinationCut = ComCuts,
                              MotherCut      = MomCuts,
                              UseP2PVRelations = True,
                              Preambulo = [])



## ------------------------------------------------------------------------- ##
class BDTFilter( Hlt2ParticleFilter ) : # {
    """
    Filter with a BBDecTreeTool.

    The part of the configuration dictionary that configures the lookup
    table and filter threshold must be passed as constructor argument 'props'
    and must contain the following keys:
        'ParamFile' : the name of the file that contains the lookup
                                table.  Assumed to be in $PARAMFILESROOT/data/.
        'VarMap'   : the map of nickname:functor strings to define
                                the input variables of the lookup table.
        'Threshold'       : Minimum response value accepted by the filter.
    """
    def __init__( self, name,nickname, inputs, props ) : # {

        ## Configure the DictOfFunctors variable handler
        from HltLine.HltLine import Hlt1Tool
        from Configurables import LoKi__Hybrid__DictOfFunctors
        varHandler = Hlt1Tool( type = LoKi__Hybrid__DictOfFunctors
                               , name = 'VarHandler'
                               , Variables = props['VarMap']
                             )
        from Configurables import BBDecTreeTool
        bdtTool = Hlt1Tool( type = BBDecTreeTool
                            , name = 'B2CBBDT'
                            , Threshold = props['Threshold']
                            , ParamFile = props['ParamFile']
                            , ParticleDictTool = "%s/%s" % (varHandler.Type.getType(), varHandler.Name )
                            , tools = [varHandler]
                          )
        filtCut = "FILTER('%s/%s')" % (bdtTool.Type.getType(), bdtTool.Name )
        from HltTracking.HltPVs import PV3D
        Hlt2ParticleFilter.__init__( self, name, filtCut, inputs
                                     , nickname = nickname
                                     , dependencies = [ PV3D('Hlt2') ]
                                     , UseP2PVRelations = True
                                     , shared = True
                                     , tools = [bdtTool ] )
    # }
# }



