###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser


class HeavyIonsLines(Hlt2LinesConfigurableUser):
    __slots__ = {
        'Prescale': {},
        'Postscale': {},
        'PersistReco': {"HighVeloMultTurbo": True},
        'NPVs'       : {"nPVs" :  1},
        'MBMicroBiasVelo': {
            'HLT1': "HLT_PASS('Hlt1BBMicroBiasVeloDecision')",
            'VoidFilter': '',
        },
        'MBHighMult': {
            'HLT1': "HLT_PASS('Hlt1BBHighMultDecision')",
            'VoidFilter': '',
        },
        'HighVeloMultTurbo': {
            'HLT1': "HLT_PASS_RE('^Hlt1HighVeloMult.*Decision$')",
            'VoidFilter': '',
        },
        'SMOGDiMuon': {
            'HLT1': "HLT_PASS_RE('^Hlt1SMOGDiMuon.*Decision$')",
            'VoidFilter': '',
        },
        'SMOGPassThrough': {
            'HLT1': "HLT_PASS_RE('Hlt1(BE|EB).*Decision')",
            'VoidFilter': '',
        },
        'BBSMOGPassThrough': {
            'HLT1': "HLT_PASS_RE('Hlt1BB(?!SMOG).*Decision')",
            'VoidFilter': '',
        },
    }

    def __apply_configuration__(self):
        from Stages import CreateReco,FilterOnPVs
        stages = {
            'MBMicroBiasVelo': [],
            'MBHighMult': [],
            'HighVeloMultTurbo': [CreateReco("Hlt2HighVeloMultTurbo"),FilterOnPVs("Decision")],
            'SMOGDiMuon': [],
            'SMOGPassThrough': [],
            'BBSMOGPassThrough': [],
        }
        from HltLine.HltLine import Hlt2Line
        for name, algos in self.algorithms(stages):
            localProps = self.getProps().get(name, {})
            linename = name
            doturbo = linename.endswith('Turbo')
            Hlt2Line(linename,
                     prescale=self.prescale,
                     postscale=self.postscale,
                     algos=algos,
                     HLT1=localProps.get('HLT1', None),
                     VoidFilter=localProps.get('VoidFilter', None),
                     priority=localProps.get('Priority', None),
                     Turbo=doturbo,
                     PersistReco=self.getProps()['PersistReco'].get(linename, False),
                     )
