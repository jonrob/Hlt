###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Hlt2Lines.Utilities.Hlt2MergedStage import Hlt2MergedStage
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2VoidFilter
from Inputs import Pions, DownPions, Photons
from HltTracking.HltPVs import PV3D

class CreateReco(Hlt2MergedStage):
    def __init__(self,name):
        inputs = [ Pions, DownPions, Photons ]
        Hlt2MergedStage.__init__(self, name, inputs, dependencies = [PV3D('Hlt2')])

class FilterOnPVs(Hlt2VoidFilter):
    def __init__(self,name):
        code = ("CONTAINS('%s')" % PV3D('Hlt2').output) + " == %(nPVs)s"
        Hlt2VoidFilter.__init__(self, name, code, [PV3D('Hlt2')],nickname = 'NPVs')

