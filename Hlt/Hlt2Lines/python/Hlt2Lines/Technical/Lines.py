###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser

class TechnicalLines(Hlt2LinesConfigurableUser):
    __slots__ = {'Prescale'    : {'Hlt2PassThrough' : 0.0001,
                                  'Hlt2Forward'     : 0.0001,
                                   # target 10 Hz output asuming 400 Hz NoBias after Hlt1
                                  'Hlt2MBNoBias'    : 0.025,
                                  'Hlt2DebugEvent'  : 0.0001},
                 'Postscale'   : {'Hlt2ErrorEvent'  : 'RATE(1)'},
                 # do not want debug events on lumi-exclusive Hlt1 events...
                 'DebugEvent'  : {'HLT1' : "HLT_PASS_RE('^Hlt1(?!Lumi).*Decision$')"},
                 'ErrorEvent'  : {'Priority' : 254,
                                  'VoidFilter' : '',
                                  'HLT2' : "HLT_COUNT_ERRORBITS_RE('^Hlt2.*',0xffff) > 0"},
                 'PassThrough' : {'HLT1' : "HLT_PASS_RE('^Hlt1(?!Lumi).*Decision$')",
                                  'VoidFilter' : ''},
                 'MBNoBias'    : {'HLT1' : "HLT_PASS('Hlt1MBNoBiasDecision')",
                                  'VoidFilter' : ''},
                 'Transparent' : {'HLT1' : "HLT_PASS_RE('^Hlt1(ODIN.*|L0.*|Velo.*|NZS.*|Incident|ErrorEvent)Decision$')",
                                  'VoidFilter' : ''},
                 'Lumi'        : {'HLT1' : "HLT_PASS_SUBSTR('Hlt1Lumi')",
                                  'VoidFilter' : ''},
                 'FullLumi'    : {'HLT1' : "HLT_PASS_SUBSTR('Hlt1Lumi')",
                                  'VoidFilter' : ''},
                 'BeamGas'     : {'HLT1' : "HLT_PASS_SUBSTR('Hlt1BeamGas')",
                                  'VoidFilter' : ''},
                 'BeamGasLumi' : {'HLT1' : "HLT_PASS_SUBSTR('Hlt1Lumi')",
                                  'VoidFilter' : ''},
                 'BeamGasCrossingFullZ' : {'HLT1' : "HLT_PASS_RE('Hlt1BeamGasCrossing.*FullZ.*Decision')",
                                       'VoidFilter' : ''},
                 'BeamGasCrossingBG' : {'HLT1' : "HLT_PASS('Hlt1BeamGasCrossingForcedRecoDecision')",
                                       'VoidFilter' : ''},
                 'BeamGasBeam' : {'HLT1' : "HLT_PASS_SUBSTR('Hlt1BeamGasBeam')",
                                  'VoidFilter' : ''},
                 'BeamGasNoBeam' : {'HLT1' : "HLT_PASS_SUBSTR('Hlt1BeamGasNoBeam')",
                                    'VoidFilter' : ''},
                 'KS0_DD'      : {'HLT1' : "HLT_PASS_RE('^Hlt1(?!Lumi).*Decision$')",
                                  'VoidFilter' : ''},
                 'KS0_LL'      : {'HLT1' : "HLT_PASS_RE('^Hlt1(?!Lumi).*Decision$')",
                                  'VoidFilter' : ''},
                 'Turbo'       : ['KS0_DD', 'KS0_LL']
                }

    def __apply_configuration__(self):
        from Stages import CopyTracks, IncidentGenerator
        from Inputs import KS0_DD, KS0_LL
        stages = {'Forward'     : [CopyTracks()],
                  'DebugEvent'  : [IncidentGenerator()],
                  'ErrorEvent'  : [],
                  'PassThrough' : [],
                  'MBNoBias'    : [],
                  'Transparent' : [],
                  'Lumi'        : [],
                  'FullLumi'    : [],
                  'BeamGas'     : [],
                  'BeamGasLumi' : [],
                  'BeamGasCrossingFullZ' : [],
                  'BeamGasCrossingBG' : [],
                  'BeamGasBeam' : [],
                  'BeamGasNoBeam' : [],
                  'KS0_DD'      : [KS0_DD],
                  'KS0_LL'      : [KS0_LL]}

        from HltLine.HltLine import Hlt2Line
        for name, algos in self.algorithms(stages):
            localProps = self.getProps().get(name, {})
            doturbo = False
            linename = name
            if name in self.getProps()['Turbo']:
                doturbo = True
                linename += 'Turbo'
            Hlt2Line(linename, prescale = self.prescale, postscale = self.postscale,
                     algos      = algos,
                     HLT1       = localProps.get('HLT1', None),
                     HLT2       = localProps.get('HLT2', None),
                     VoidFilter = localProps.get('VoidFilter', None),
                     priority   = localProps.get('Priority', None),
                     Turbo = doturbo)
