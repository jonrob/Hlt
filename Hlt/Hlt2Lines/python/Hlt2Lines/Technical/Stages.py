###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Each stage must specify its own inputs
from Hlt2Lines.Utilities.Hlt2Stage import Hlt2Stage

# The GEC
class CopyTracks(Hlt2Stage):
    def __init__(self):
        from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking
        self.__tracks = Hlt2BiKalmanFittedForwardTracking().hlt2PrepareTracks()
        super(CopyTracks, self).__init__("CopyTracks", [], [self.__tracks])

    def stage(self, stages, cuts):
        stages += self.dependencies(cuts)
        from Configurables import HltCopyTrackSelection
        return HltCopyTrackSelection('Hlt2ForwardDecision',
                                     InputSelection = 'TES:' + self.__tracks.outputSelection())

class IncidentGenerator(Hlt2Stage):
    def __init__(self):
        super(IncidentGenerator, self).__init__("Generator", [])

    def stage(self, stages, cuts):
        from Configurables import HltIncidentGenerator, Hlt2SelReportsMaker
        Hlt2SelReportsMaker().DebugIncident = 'RequestDebugEvent'
        return HltIncidentGenerator('Hlt2DebugEventDecision',
                                    Incident = Hlt2SelReportsMaker().DebugIncident)
