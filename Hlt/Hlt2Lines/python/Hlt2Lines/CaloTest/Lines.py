#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# -*- coding: utf-8 -*-
# =============================================================================
# @file   Hlt2CaloTest.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   19.12.2014
# =============================================================================
"""Test for Calo reconstruction for HLT2."""
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser

class CaloTestLines(Hlt2LinesConfigurableUser) :
    __slots__ = {# Postscale
                  'Postscale'   : {'Hlt2CaloTest' : 0.0,
                                   'Hlt2PFTest' : 1.0}
                }

    def __apply_configuration__(self) :
        from HltLine.HltLine import Hlt2Line
        from Stages import Photons, ParticleFlow
        from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter

        # Instantiate filters to make the warnings about TisTos go away
        photons = Photons()
        photon_filter = Hlt2ParticleFilter("Filter", "ALL", [photons])

        pf_filter = Hlt2ParticleFilter("Filter", "ALL", [ParticleFlow()])

        # Final stages of the lines
        stages = {'CaloTest' : [photon_filter],
                  'PFTest' : [pf_filter]}

        # Build the lines
        for name, algos in self.algorithms(stages):
            Hlt2Line(name,
                     prescale  = self.prescale,
                     algos     = algos,
                     postscale = self.postscale)
