###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# Each stage must specify its own inputs
from Hlt2Lines.Utilities.Hlt2Stage import Hlt2Stage

# The GEC
class Photons(Hlt2Stage):
    def __init__(self):
        name = "Photons"
        from HltLine.HltDecodeRaw import DecodeL0CALO
        from HltTracking.HltPVs   import PV3D
        super(Photons, self).__init__(name, [], [PV3D('Hlt2'), DecodeL0CALO])

    def stage(self, stages, cuts):
        from Inputs import Photons
        return Photons

class ParticleFlow(Hlt2Stage):
    def __init__(self):
        from HltTracking.HltPVs import PV3D
        super(ParticleFlow, self).__init__("ParticleFlow", [], [PV3D('Hlt2')])

    def stage(self, stages, cuts):
        import Reco
        return Reco.ParticleFlow
