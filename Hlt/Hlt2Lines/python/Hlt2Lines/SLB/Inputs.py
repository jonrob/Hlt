###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedMuons   as Hlt2LooseMuons
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedRichPions   as Hlt2LoosePions
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedPions   as Hlt2NoPIDPions
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedRichKaons   as Hlt2LooseKaons
