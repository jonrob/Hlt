###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# =============================================================================
# @file:   Definition of the lines devoted to the study of MuonElectron decays
# @author: Miguel Ramos Pernas miguel.ramos.pernas@cern.ch
# @date:   2016-29-04
# =============================================================================

__author__ = "Miguel Ramos Pernas miguel.ramos.pernas@cern.ch"
__date__   = "2016-29-04"

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser
class StrangeLFVLines(Hlt2LinesConfigurableUser) :
    __slots__ = {'Prescale' : {},

                 'Common'   : {},

                 'MuonElectronSoft'   : {'VDZ'         :        0,
                                         'CosAngle'    : 0.999997,
                                         'IpDzRatio'   :     0.01,
                                         'SumGP'       :      0.5,
                                         'Rho2'        :       64 * mm * mm,
                                         'VertexChi2'  :       25,
                                         'DOCA'        :      0.2 * mm,
                                         'MaxMuGP'     :      0.4,
                                         'MinProbNNe'  :      0.1,
                                         'MaxElGP'     :     0.15,
                                         'MinMuIp'     :      0.3 * mm,
                                         'MinElIp'     :      0.4 * mm,
                                         'MinMuIpChi2' :      1.5,
                                         'MinElIpChi2' :        6,
                                         'MinProbNNmu' :      0.1,
                                         'MinPT'       :       80 * MeV,
                                         'SVZ'         :      600 * mm,
                                         'Mass'        :     1000 * MeV,
                                         'Dira'        :        0
                                 }
                 }

    def stages(self, nickname = ""):
        from Stages import MuonElectronFromKS0
        self._stages = { 'MuonElectronSoft'  : [MuonElectronFromKS0] }
        if nickname:
            return self._stages[nickname]
        else:
            return self._stages

    def __apply_configuration__(self) :
        from HltLine.HltLine import Hlt2Line
        for (nickname, algos) in self.algorithms(self.stages()):
            linename = 'StrangeLFV' + nickname if nickname != 'MuonElectron' else nickname
            Hlt2Line(linename, prescale = self.prescale,
                     algos = algos, postscale = self.postscale)
