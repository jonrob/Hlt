###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## @file
#  HLT2 lines for Hb -> V0 V0 like decays
#
#  @author Timon Schmelzer <timon.schmelzer@cern.ch>, Moritz Demmer <moritz.demmer@cern.ch>
#=============================================================================
""" HLT2 lines for Hb -> V0 V0 like decays

"""
#=============================================================================
__author__  = "Timon Schmelzer, Moritz Demmer"

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser

class Hb2V0V0Lines(Hlt2LinesConfigurableUser) :
  __slots__ = {
    '_stages' : {},
    'Prescale' : {},
    'PromptPrescale' : {},
    'Postscale' : {},
    'KS_DD' : {
      'KS_DD_MASS_WINDOW' : 80 * MeV,
      'KS_DD_BPVVDCHI2()'   : 5,
      'KS_DD_TRCHI2DOF'   : 4
    },
    'KS_LL' : {
      'KS_LL_MASS_WINDOW' : 50 * MeV,
      'KS_LL_BPVVDCHI2()'   : 5,
      'KS_LL_TRCHI2DOF'   : 4,
      'KS_LL_TRGHOSTPROB' : 0.5
    },
    'Hb2V0V0_DD' : {
      # Combination Cuts
      'Hb_MASS_MIN'   : 4000 * MeV ,
      'Hb_MASS_MAX'   : 6500 * MeV ,
      'Hb_DOCA'       : 4 * mm,
      # Mother Cuts
      'Hb_VTXCHI2'      : 40 ,
      'Hb_DIRA'         : 0.999
     },
     'Hb2V0V0_LD' : {
      # Combination Cuts
      'Hb_MASS_MIN'   : 4000 * MeV ,
      'Hb_MASS_MAX'   : 6500 * MeV ,
      'Hb_DOCA'       : 4 * mm,
      # Mother Cuts
      'Hb_VTXCHI2'     : 30 ,
      'Hb_DIRA'         : 0.999
     },
     'Hb2V0V0_LL' : {
      # Combination Cuts
      'Hb_MASS_MIN'   : 4000 * MeV ,
      'Hb_MASS_MAX'   : 6500 * MeV ,
      'Hb_DOCA'       : 1 * mm,
      # Mother Cuts
      'Hb_VTXCHI2'     : 20 ,
      'Hb_DIRA'         : 0.999
     },

  }

  def stages(self, nickname = ""):
    if hasattr(self, '_stages') and self._stages:
      if nickname:
        return self._stages[nickname]
      else:
        return self._stages

    from Stages import (Hb2KSKSDD, Hb2KSKSLD, Hb2KSKSLL)


    self._stages = {
      'Hb2KSKSDD'             : [Hb2KSKSDD],
      'Hb2KSKSLD'             : [Hb2KSKSLD],
      'Hb2KSKSLL'             : [Hb2KSKSLL]
    }

    if nickname:
      return self._stages[nickname]
    else:
      return self._stages

  def __apply_configuration__(self) :
    from HltLine.HltLine import Hlt2Line
    prefix = 'Hb2V0V0'
    stages = self.stages()
    for (nickname, algos) in self.algorithms(stages):
      Hlt2Line(prefix + nickname, algos = algos)
