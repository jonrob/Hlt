###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import MeV
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser

class CEPElectronLines() :
    def localcuts(self) :
        return {'DiElectron'      :   {'nVeloTracksmax'  :     8,
                                       'nBackTracksmax'  :     1,
                                       'e_PTmin'         :     250 * MeV},
                'DiElectron_noTrFilt':{'nVeloTracksmax'  :     10000,
                                       'nBackTracksmax'  :     10000,
                                       'e_PTmin'         :     250 * MeV}
               }

    def locallines(self):
      from Stages import LowMultDiElectronFilter
      stages = {'LowMultDiElectron'         : [LowMultDiElectronFilter('DiElectron')],
                'LowMultDiElectron_noTrFilt': [LowMultDiElectronFilter('DiElectron_noTrFilt')]}
      return stages
