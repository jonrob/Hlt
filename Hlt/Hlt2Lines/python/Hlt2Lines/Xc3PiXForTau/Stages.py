###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
###
#  @author A. Romero Vidal antonio.romero@usc.es
#  @date 2018-02-23
#
###

from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner
from Inputs import Hlt2Muons, Hlt2Pions, Hlt2Kaons, Hlt2Protons
from Hlt2Lines.Utilities.Hlt2MergedStage import Hlt2MergedStage
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter

class DTrackFilter(Hlt2ParticleFilter):

    def __init__(self, name, nickname, inputs, hadron):

        dc = "(PT > %(D_Trk_ALL_PT_MIN)s)" \
             "& (P > %(D_Trk_ALL_P_MIN)s)"

        if (hadron=='pi'):
            dc = dc + "& (PROBNNpi > %(D_Pi_PROBNNpi)s)" \
               " & (MIPCHI2DV(PRIMARY) > %(D_Trk_ALL_MIPCHI2DV_MIN)s)"
        if (hadron=='K'):
            dc = dc + "& (PIDK > %(D_K_PIDK)s)" \
               " & (MIPCHI2DV(PRIMARY) > %(D_Trk_ALL_MIPCHI2DV_MIN)s)"
        if (hadron=='p'):
            dc = dc + "& (PIDp > %(D_P_PIDp)s)" \
               " & (MIPCHI2DV(PRIMARY) > %(D_Trk_ALL_MIPCHI2DV_MIN)s)"
        if (hadron=='mu'):
            dc = dc + "& (PIDmu > %(D_Mu_PIDmu)s)" \
               " & (MIPCHI2DV(PRIMARY) > %(D_Trk_ALL_MIPCHI2DV_MIN)s)"
        if (hadron=='pi_D02K3pi'):
            dc = dc + "& (PROBNNpi > %(D02K3pi_Pi_PROBNNpi)s)" \
               " & (MIPCHI2DV(PRIMARY) > %(D02K3pi_Trk_ALL_MIPCHI2DV_MIN)s)"
        if (hadron=='K_D02K3pi'):
            dc = dc + "& (PIDK > %(D02K3pi_K_PIDK)s)" \
               " & (MIPCHI2DV(PRIMARY) > %(D02K3pi_Trk_ALL_MIPCHI2DV_MIN)s)"

        from HltTracking.HltPVs import PV3D
        Hlt2ParticleFilter.__init__(self, name, dc, inputs,nickname = nickname, dependencies=[PV3D('Hlt2')], shared=True)


filteredDPions = DTrackFilter('Xc3PiXForTauFilteredDPions','XcDaughter', [Hlt2Pions],'pi')
filteredDKaons = DTrackFilter('Xc3PiXForTauFilteredDKaons','XcDaughter', [Hlt2Kaons],'K')
filteredD02K3piPions = DTrackFilter('Xc3PiXForTauFilteredD02K3piPions','XcDaughter', [Hlt2Pions],'pi_D02K3pi')
filteredD02K3piKaons = DTrackFilter('Xc3PiXForTauFilteredD02K3piKaons','XcDaughter', [Hlt2Kaons],'K_D02K3pi')
filteredDProtons = DTrackFilter('Xc3PiXForTauFilteredDProtons','XcDaughter', [Hlt2Protons],'p')
filteredDMuons = DTrackFilter('Xc3PiXForTauFilteredDHlt2Muons','XcDaughter', [Hlt2Muons],'mu')

class TauTrackFilter(Hlt2ParticleFilter):

    def __init__(self, name, nickname, inputs, hadron):

        dc = ("(MIPCHI2DV(PRIMARY) > %(Tau_Trk_ALL_MIPCHI2DV_MIN)s)"
              "& (PT > %(Tau_Trk_ALL_PT_MIN)s)"
              "& (P > %(Tau_Trk_ALL_P_MIN)s)"
              )

        dc = dc + "& (PROBNNpi > %(Tau_Pi_PROBNNpi)s)"

        from HltTracking.HltPVs import PV3D
        Hlt2ParticleFilter.__init__(self, name, dc, inputs, nickname = nickname, dependencies=[PV3D('Hlt2')], shared=True)


filteredTauPions = TauTrackFilter('Xc3PiXForTauFilteredTauPions','TauDaughter', [Hlt2Pions],'pi')
                                         
class D2XCombiner(Hlt2Combiner):
    
    def __init__(self, name, nickname, decayDesc, inputs, masswindow):

        from HltTracking.HltPVs import PV3D

        masscut = ''
        if masswindow == 'D':
            masscut = "in_range(%(D_AM_MIN)s,  AM, %(D_AM_MAX)s)"
        if masswindow == 'Ds':
            masscut = "in_range(%(Ds_AM_MIN)s,  AM, %(Ds_AM_MAX)s)"
        if masswindow == 'Lc':
            masscut = "(in_range(%(Lc_AM_MIN)s,  AM, %(Lc_AM_MAX)s))"
        if masswindow == 'Jpsi':
            masscut = "(in_range(%(Jpsi_AM_MIN)s,  AM, %(Jpsi_AM_MAX)s))"

        combcuts = masscut + " & (APT > %(Xc_PT_MIN)s)" \
                             " & (AMAXCHILD(PT) > %(D_Trk_ALL_MAXPT_MIN)s)" \
                             " & (AMAXDOCA('',0) < %(D_AMAXDOCA_MAX)s )"

        parentcuts =   "(VFASPF(VCHI2PDOF) < %(Xc_VCHI2PDOF_MAX)s)" \
                     "& (BPVVDCHI2()> %(Xc_BPVVDCHI2_MIN)s )" \
                     "& (BPVDIRA() > %(Xc_BPVDIRA_MIN)s )" \
                     "& (MIPCHI2DV(PRIMARY) > %(Xc_IPCHI2_MIN)s)"

        preambulo = [
                    #  "Xc_DOCA_Histo = Gaudi.Histo1DDef('h_Xc_DOCA', 0., 1.0*mm, 100)"
                    # ,"Xc_trk_PT_Histo = Gaudi.Histo1DDef('h_Xc_trk_PT', 0., 4. * GeV, 100)"
                    # ,"Xc_trk_P_Histo = Gaudi.Histo1DDef('h_Xc_trk_P', 0., 40. * GeV, 100)"
                    ]

        #motherMonitor = ("process(monitor(DOCA(1,2),Xc_DOCA_Histo,'h_Xc_DOCA'))"
        #                 ">>process(monitor(CHILD(PT,1),Xc_trk_PT_Histo,'h_Xc_trk_PT'))"
        #                 ">>process(monitor(CHILD(P,1),Xc_trk_P_Histo,'h_Xc_trk_P'))"
        #                 ">> ~EMPTY")

        Hlt2Combiner.__init__(self, name,  decayDesc,
                              inputs = inputs,
                              dependencies = [ PV3D('Hlt2') ],
                              nickname = nickname,
                              CombinationCut = combcuts,
                              MotherCut = parentcuts,
                              Preambulo = preambulo,
                              #MotherMonitor = motherMonitor,
                              shared=True)
    
class Tau2XCombiner(Hlt2Combiner):
    
    def __init__(self, name, nickname, decayDesc, inputs, masswindow):

        from HltTracking.HltPVs import PV3D

        masscut = "in_range(%(Tau_AM_MIN)s,  AM, %(Tau_AM_MAX)s)"

        combcuts = masscut + " & (APT > %(Tau_PT_MIN)s)" \
                             " & (AMAXDOCA('',0) < %(Tau_AMAXDOCA_MAX)s )"

        parentcuts = "(VFASPF(VCHI2PDOF) < %(Tau_VCHI2PDOF_MAX)s)" \
                     "& (BPVDIRA() > %(Tau_BPVDIRA_MIN)s )" \
                     "& (BPVVDR > %(Tau_FDT_MIN)s )"

        preambulo = [
                    # "Tau_pions_PT_Histo = Gaudi.Histo1DDef('h_Tau_pions_PT', 0. * GeV, 2.5 * GeV, 100)"
                    #,"Tau_pions_P_Histo = Gaudi.Histo1DDef('h_Tau_pions_P', 0., 10. * GeV, 100)"
                    ]

        #motherMonitor = ( 
        #                   "process(monitor(CHILD(PT,2),Tau_pions_PT_Histo,'h_Tau_pions_PT'))"
        #                 ">>process(monitor(CHILD(P,2),Tau_pions_P_Histo,'h_Tau_pions_P'))"
        #                 ">> ~EMPTY"
        #                )

        Hlt2Combiner.__init__(self, name,  decayDesc,
                              inputs = inputs,
                              dependencies = [ PV3D('Hlt2') ],
                              nickname = nickname,
                              CombinationCut = combcuts,
                              MotherCut = parentcuts,
                              Preambulo = preambulo,
                              #MotherMonitor = motherMonitor,
                              shared=True)
 
# Combiners
D02kpiComb   = D2XCombiner("Xc3PiXForTau_D02Kpi","Xc","[D0 -> K- pi+]cc",[filteredDPions, filteredDKaons],"D")
D02k3piComb   = D2XCombiner("Xc3PiXForTauK3Pi","Xc","[D0 -> K- pi+ pi- pi+]cc",[filteredD02K3piPions, filteredD02K3piKaons],"D")
Dplus2kpipiComb   = D2XCombiner("Xc3PiXForTauKPiPi","Xc","[D+ -> K- pi+ pi+]cc",[filteredDPions, filteredDKaons],"D")
Ds2kkpiComb   = D2XCombiner("Xc3PiXForTauKKPi","Xc","[D_s+ -> K- K+ pi+]cc",[filteredDPions, filteredDKaons],"Ds")
Lc2pkpiComb   = D2XCombiner("Xc3PiXForTauPKPi","Xc","[Lambda_c+ -> p+ K- pi+]cc",[filteredDPions, filteredDKaons,filteredDProtons],"Lc")
Jpsi2mumuComb = D2XCombiner("Xc3PiXForTauMuMu","Xc","J/psi(1S) -> mu- mu+",[filteredDMuons], "Jpsi")

Tau23piComb   = Tau2XCombiner("Xc3PiXForTau_tau23pi","Tau","[tau+ -> pi+ pi- pi+]cc",[filteredTauPions],"tau")
Tau23piNonPhysComb   = Tau2XCombiner("Xc3PiXForTau_tau23piNonPhys","Tau","[tau+ -> pi+ pi+ pi+]cc",[filteredTauPions],"tau")

MergedD0 = Hlt2MergedStage('MergedD0', inputs = [D02kpiComb,D02k3piComb],shared=True)

class B2XcTauCombiner(Hlt2Combiner):
    def __init__(self, name, nickname, decayDesc, inputSeq):

        from HltTracking.HltPVs import PV3D

        combcuts = "(AM>%(XcTau_M_MIN)s) & (AM<%(XcTau_M_MAX)s) & (AMAXDOCA('',0) < %(XcTau_DOCA_MAX)s)" 

        mothercuts = "(BPVDIRA() > %(XcTau_DIRA)s ) " 

        preambulo = [
                      "B_mass_Histo = Gaudi.Histo1DDef('h_B_mass', 2000 * MeV, 10000 * MeV, 200)"
                     ,"Xc_mass_Histo = Gaudi.Histo1DDef('h_Xc_mass', 1600. * MeV, 3400 * MeV, 200)"
                     ,"Tau_mass_Histo = Gaudi.Histo1DDef('h_Tau_mass', 300. * MeV, 5400 * MeV, 200)"
                     ,"Xc_mass_B_Histo = Gaudi.Histo1DDef('h_Xc_mass_B', 1600. * MeV, 3400 * MeV, 200)"
                     ,"Tau_mass_B_Histo = Gaudi.Histo1DDef('h_Tau_mass_B', 400. * MeV, 5400 * MeV, 200)"
                     #,"B_DOCA_Histo = Gaudi.Histo1DDef('h_B_DOCA', 0., 0.2, 40)"
                     #,"Xc_PT_B_Histo = Gaudi.Histo1DDef('h_Xc_PT_B', 0., 20000.*MeV, 100)"
                     #,"Xc_P_B_Histo = Gaudi.Histo1DDef('h_Xc_P_B', 0., 80000.*MeV, 100)"
                     #,"Xc_trk_PT_B_Histo = Gaudi.Histo1DDef('h_Xc_trk_PT_B', 0., 4000.*MeV, 100)"
                     #,"Xc_trk_P_B_Histo = Gaudi.Histo1DDef('h_Xc_trk_P_B', 0., 40000.*MeV, 100)"
                     #,"Xc_IPCHI2PV_B_Histo = Gaudi.Histo1DDef('h_Xc_IPCHI2PV_B', 0., 100., 100)"
                     #,"Xc_trk_IPCHI2PV_B_Histo = Gaudi.Histo1DDef('h_Xc_trk_IPCHI2PV_B', 0., 100., 100)"
                     #,"Tau_trk_IPCHI2PV_B_Histo = Gaudi.Histo1DDef('h_Tau_trk_IPCHI2PV_B', 0., 100., 100)"
                     #,"B_FDT_Histo = Gaudi.Histo1DDef('h_B_FDT', 0., 10.*mm, 100)"
                     #,"Tau_FDT_B_Histo = Gaudi.Histo1DDef('h_Tau_FDT_B', 0., 10.*mm, 100)"
                     #,"Tau_PT_B_Histo = Gaudi.Histo1DDef('h_Tau_PT_B', 0., 20000.*MeV, 100)"
                     #,"Tau_trk_PT_B_Histo = Gaudi.Histo1DDef('h_Tau_trk_PT_B', 0., 4000.*MeV, 100)"
                     #,"Tau_trk_P_B_Histo = Gaudi.Histo1DDef('h_Tau_trk_P_B', 0., 40000.*MeV, 100)"
                     #,"Tau_P_B_Histo = Gaudi.Histo1DDef('h_Tau_P_B', 0., 80000.*MeV, 100)"
                    ]
        motherMonitor = (   "process(monitor(M,B_mass_Histo,'h_B_mass'))"
                         ">> process(monitor(CHILD(M,1),Xc_mass_B_Histo,'h_Xc_mass_B'))"
                         ">> process(monitor(CHILD(M,2),Tau_mass_B_Histo,'h_Tau_mass_B'))"
                         #">> process(monitor(DOCA(1,2),B_DOCA_Histo,'h_B_DOCA'))"
                         #">> process(monitor(BPVVDR,B_FDT_Histo,'h_B_FDT'))"
                         #">> process(monitor(CHILD(BPVVDR,2),Tau_FDT_B_Histo,'h_Tau_FDT_B'))"
                         #">> process(monitor(CHILD(PT,1),Xc_PT_B_Histo,'h_Xc_PT_B'))"
                         #">> process(monitor(CHILD(P,1),Xc_P_B_Histo,'h_Xc_P_B'))"
                         #">> process(monitor(CHILD(CHILD(PT,1),1),Xc_trk_PT_B_Histo,'h_Xc_trk_PT_B'))"
                         #">> process(monitor(CHILD(CHILD(P,1),1),Xc_trk_P_B_Histo,'h_Xc_trk_P_B'))"
                         #">> process(monitor(CHILD(MIPCHI2DV(PRIMARY),1),Xc_IPCHI2PV_B_Histo,'h_Xc_IPCHI2PV_B'))"
                         #">> process(monitor(CHILD(CHILD(MIPCHI2DV(PRIMARY),1),1),Xc_trk_IPCHI2PV_B_Histo,'h_Xc_trk_IPCHI2PV_B'))"
                         #">> process(monitor(CHILD(CHILD(MIPCHI2DV(PRIMARY),1),2),Tau_trk_IPCHI2PV_B_Histo,'h_Tau_trk_IPCHI2PV_B'))"
                         #">> process(monitor(CHILD(PT,2),Tau_PT_B_Histo,'h_Tau_PT_B'))"
                         #">> process(monitor(CHILD(CHILD(PT,1),2),Tau_trk_PT_B_Histo,'h_Tau_trk_PT_B'))"
                         #">> process(monitor(CHILD(CHILD(P,1),2),Tau_trk_P_B_Histo,'h_Tau_trk_P_B'))"
                         #">> process(monitor(CHILD(P,2),Tau_P_B_Histo,'h_Tau_P_B'))"
                         ">> ~EMPTY")

        daughterMonitor = { "D0"  : (  "process(monitor(M, Xc_mass_Histo, 'h_Xc_mass'))"
                                    ">> ~EMPTY"
                                    ),
                            "tau+" : ( "process(monitor(M, Tau_mass_Histo, 'h_Tau_mass'))"
                                    ">> ~EMPTY"
                                    )
                          }

        Hlt2Combiner.__init__(self, name, decayDesc, inputSeq,
                              dependencies = [PV3D('Hlt2')],
                              nickname = nickname,
                              CombinationCut = combcuts,
                              MotherCut = mothercuts,
                              Preambulo = preambulo,
                              MotherMonitor = motherMonitor,
                              DaughtersMonitors = daughterMonitor,
                              shared=True)

B2Xc3PiXCombD02Kpi   = B2XcTauCombiner("B2Xc3PiXCombD02Kpi","B","[B+ -> D0 tau-]cc",[D02kpiComb, Tau23piComb])
B2Xc3PiXWSCombD02Kpi   = B2XcTauCombiner("B2Xc3PiXWSCombD02Kpi","B","[B+ -> D0 tau+]cc",[D02kpiComb, Tau23piComb])
B2Xc3PiXNonPhysCombD02Kpi   = B2XcTauCombiner("B2Xc3PiXNonPhysCombD02Kpi","B","[B+ -> D0 tau-]cc",[D02kpiComb, Tau23piNonPhysComb])
B2Xc3PiXCombD02K3pi   = B2XcTauCombiner("B2Xc3PiXCombD02K3pi","B","[B+ -> D0 tau-]cc",[D02k3piComb, Tau23piComb])
B2Xc3PiXWSCombD02K3pi   = B2XcTauCombiner("B2Xc3PiXWSCombD02K3pi","B","[B+ -> D0 tau+]cc",[D02k3piComb, Tau23piComb])
B2Xc3PiXNonPhysCombD02K3pi   = B2XcTauCombiner("B2Xc3PiXNonPhysCombD02K3pi","B","[B+ -> D0 tau-]cc",[D02k3piComb, Tau23piNonPhysComb])
B2Xc3PiXCombDp2Kpipi   = B2XcTauCombiner("B2Xc3PiXCombDp2Kpipi","B","[B+ -> D+ tau-]cc",[Dplus2kpipiComb, Tau23piComb])
B2Xc3PiXWSCombDp2Kpipi   = B2XcTauCombiner("B2Xc3PiXWSCombDp2Kpipi","B","[B+ -> D+ tau+]cc",[Dplus2kpipiComb, Tau23piComb])
B2Xc3PiXNonPhysCombDp2Kpipi   = B2XcTauCombiner("B2Xc3PiXNonPhysCombDp2Kpipi","B","[B+ -> D+ tau-]cc",[Dplus2kpipiComb, Tau23piNonPhysComb])
B2Xc3PiXCombDs2kkpi   = B2XcTauCombiner("B2Xc3PiXCombDs2kkpi","B","[B+ -> D_s+ tau-]cc",[Ds2kkpiComb, Tau23piComb])
B2Xc3PiXWSCombDs2kkpi   = B2XcTauCombiner("B2Xc3PiXWSCombDs2kkpi","B","[B+ -> D_s+ tau+]cc",[Ds2kkpiComb, Tau23piComb])
B2Xc3PiXNonPhysCombDs2kkpi   = B2XcTauCombiner("B2Xc3PiXNonPhysCombDs2kkpi","B","[B+ -> D_s+ tau-]cc",[Ds2kkpiComb, Tau23piNonPhysComb])
B2Xc3PiXCombLc2pkpi   = B2XcTauCombiner("B2Xc3PiXCombLc2pkpi","B","[B+ -> Lambda_c+ tau-]cc",[Lc2pkpiComb, Tau23piComb])
B2Xc3PiXWSCombLc2pkpi   = B2XcTauCombiner("B2Xc3PiXWSCombLc2pkpi","B","[B+ -> Lambda_c+ tau+]cc",[Lc2pkpiComb, Tau23piComb])
B2Xc3PiXNonPhysCombLc2pkpi   = B2XcTauCombiner("B2Xc3PiXNonPhysCombLc2pkpi","B","[B+ -> Lambda_c+ tau-]cc",[Lc2pkpiComb, Tau23piNonPhysComb])
B2Xc3PiXCombJpsi2mumu   = B2XcTauCombiner("B2Xc3PiXCombJpsi2mumu","B","[B+ -> J/psi(1S) tau+]cc",[Jpsi2mumuComb, Tau23piComb])
B2Xc3PiXNonPhysCombJpsi2mumu   = B2XcTauCombiner("B2Xc3PiXNonPhysCombJpsi2mumu","B","[B+ -> J/psi(1S) tau+]cc",[Jpsi2mumuComb, Tau23piNonPhysComb])
