###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file:   Set of "stages" for lines devoted to the study of DiElectron decays
# @author: Miguel Ramos Pernas miguel.ramos.pernas@cern.ch
# @author: Jessica Prisciandaro jessica.prisciandaro@cern.ch
# @date:   2016-02-12
# =============================================================================

__all__    = ( 'SoftDiElectronCombiner' )

from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner
from HltTracking.HltPVs import PV3D
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter

from Inputs import TrackFittedSoftDiElectron

class SoftDiElectronCombiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs):

        dc = { 'e+': ("(MIPCHI2DV(PRIMARY) < %(MaxIpChi2)s)"+ 
                      " & (PROBNNe > %(MinProbNNe)s)" )}

        cc = ( "(ACHILD(MIPDV(PRIMARY),1) * ACHILD(MIPDV(PRIMARY),2) > %(IpProd)s)" +
               " & (ACHILD(TRGHOSTPROB,1) + ACHILD(TRGHOSTPROB,2) < %(SumGP)s)" +
               " & ((ACHILD(MIPCHI2DV(PRIMARY),1) - %(MinIpChi2)s) * (ACHILD(MIPCHI2DV(PRIMARY),2) - %(MinIpChi2)s) > %(IpChi2Prod)s)" )

        mc = ( "(PCUTA(ALV(1,2) < %(CosAngle)s))" +
               " & (BPVVDZ() > %(VDZ)s)" +
               " & ((MIPDV(PRIMARY)/BPVVDZ()) < %(IpDzRatio)s)" +
               " & (VFASPF(VX*VX + VY*VY) > %(Rho2)s)" +
               " & (DOCAMAX < %(DOCA)s)" +
               " & (VFASPF(VZ) < %(SVZ)s)" +
               " & (MM < %(Mass)s)" +
               " & (BPVDIRA() > %(Dira)s)" )

        Hlt2Combiner.__init__(self,
                              name,
                              decay,
                              inputs,
                              DaughtersCuts = dc,
                              CombinationCut = cc,
                              MotherCut = mc,
                              shared = True,
                              dependencies = [PV3D('Hlt2')],
                              UseP2PVRelations = False)

from Inputs import TrackFittedDetachedDiElectron
DiElectronFromKS0 = SoftDiElectronCombiner("ElSoft", "KS0 -> e+ e-", [TrackFittedDetachedDiElectron])

class JpsiPIDFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(ADMASS('J/psi(1S)') < %(MassWindow)s) " +
                "& (PT > %(Pt)s) " +
                "& (MAXTREE('e-' == ABSID,TRCHI2DOF) < %(TrChi2Tight)s )" +
                "& (MINTREE('e-' == ABSID,PT) > %(ePt)s) " +
                " & (CHILDCUT((TRGHOSTPROB < %(TRACK_TRGHOSTPROB_MAX)s),1))" +
                " & (CHILDCUT((TRGHOSTPROB < %(TRACK_TRGHOSTPROB_MAX)s),2))" +
                "& (VFASPF(VCHI2PDOF) < %(VertexChi2)s )" +
                "& ( %(PIDCut)s ) ")

        from HltLine.Hlt2Monitoring import Hlt2Monitor, Hlt2MonitorMinMax
        args = {'PostMonitor' : Hlt2Monitor("M", "M(#e#e)", 3097, 1100, 'Jpsi_mass', nbins = 100)
                                + " & " + Hlt2MonitorMinMax("PT","PT(#e#e)", 0, 10000, 'Jpsi_pt', nbins = 100)}
        inputs = [TrackFittedSoftDiElectron]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, shared = True, **args)
