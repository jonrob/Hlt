###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Hlt2Stage import Hlt2Stage
class Hlt2Tracks(Hlt2Stage):
    def __init__(self, name = "Hlt2Tracks"):
        Hlt2Stage.__init__(self, name, inputs = [])

    def clone(self, name):
        return Hlt2Tracks(name)

    def stage(self, cuts):
        from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking as Hlt2LongTracking
        return Hlt2LongTracking().hlt2PrepareTracks()
