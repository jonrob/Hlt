###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# -*- coding: utf-8 -*-
## @file
#  Set of Hlt2-lines suitable for the study of charm decay channels with
#  two K0s and one hadron in the final state.
#
#  @author Maurizio MARTINELLI maurizio.martinelli@cern.ch
#=============================================================================
""" Set of Hlt2-lines suitable for the study of charm decay channels with
    four particles in the final state.
"""
#=============================================================================
__author__  = "Maurizio MARTINELLI maurizio.martinelli@cern.ch"
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm, mrad
import copy

class CharmHadD2KsKsHLines(object):
    def localcuts(self) :
        ConeVariables = ['CONEANGLE', 'CONEMULT', 'CONEPTASYM']
        CommonKsKsH = {
        'KS0LL_ALL_PT_MIN'        : 500.0 * MeV,
        'KS0LL_ALL_MIPCHI2DV_MIN' : 9.0,
        'KS0DD_ALL_PT_MIN'        : 500.0 * MeV,
        'KS0DD_ALL_MIPCHI2DV_MIN' : 9.0,
        'Trk_ALL_PT_MIN'          : 250 * MeV,
        'Trk_ALL_MIPCHI2DV_MIN'   : 3,
        'AM_MIN'                  : 1740. * MeV,
        'AM_MAX'                  : 1990. * MeV,
        'AM_3'                    : 139.57018 * MeV,
        'PT12_MIN'                : 1500 * MeV,
        'ASUMPT_MIN'              : 1500 * MeV,
        'VCHI2PDOF_MAX'           : 8,
        'acosBPVDIRA_MAX'         : 34.6 * mrad,
        'BPVLTIME_MIN'            : 0.4 * picosecond,
        'DPT_MIN'                 : 3000 * MeV,
        'DMOM_MIN'                : 20000 * MeV,
        'TisTosSpec'              : ["Hlt1.*Track.*Decision%TOS"],#"Hlt1.*Track.*Decision%TOS",
        }
        # Isolation Variables
        IsoVars = {
            'DConeVar05' : { "ConeAngle" : 0.5, "Variables" : ConeVariables, "Location"  : 'DConeVar05' },
            'DConeVar10' : { "ConeAngle" : 1.0, "Variables" : ConeVariables, "Location"  : 'DConeVar10' },
            'DConeVar15' : { "ConeAngle" : 1.5, "Variables" : ConeVariables, "Location"  : 'DConeVar15' },
            'DpConeVar10KS0' : { "ConeAngle" : 1.0, "Variables" : ConeVariables,
                                "DaughterLocations" : {"[D+ -> ^KS0 KS0 X+]CC" : "KS0ConeVar10"} },
            'DpConeVar10KS1' : { "ConeAngle" : 1.0, "Variables" : ConeVariables,
                                "DaughterLocations" : {"[D+ -> KS0 ^KS0 X+]CC" : "KS1ConeVar10"} },
            'DpConeVar10H' : { "ConeAngle" : 1.0, "Variables" : ConeVariables,
                              "DaughterLocations" : {"[D+ -> KS0 KS0 ^X+]CC" : "HConeVar10"} },
            'VertexIsolation' : { "Location"  : "VertexIsoInfo" },
            }

        Dp2KsKsH = copy.deepcopy(CommonKsKsH)
        Dp2KsKsHMass = { 'Mass_M_MIN' : 1765 * MeV,
                         'Mass_M_MAX' : 1965 * MeV }

        Ds2KsKsH = copy.deepcopy(CommonKsKsH)
        Ds2KsKsH.update({   'AM_MIN'                : 1840. * MeV,
                            'AM_MAX'                : 2090. * MeV,
                            'BPVLTIME_MIN'          : 0.2 * picosecond} )
        Ds2KsKsHMass = {    'Mass_M_MIN' : 1865. * MeV,
                            'Mass_M_MAX' : 2065. * MeV }
        for ivar, dauLoc in {'ConeVar10KS0':"[D_s+ -> ^KS0 KS0 X+]CC",
            'ConeVar10KS1':"[D_s+ -> KS0 ^KS0 X+]CC",
            'ConeVar10H':"[D_s+ -> KS0 KS0 ^X+]CC"}.iteritems():
            IsoVars['Ds'+ivar] = copy.deepcopy(IsoVars['Dp'+ivar])
            IsoVars['Ds'+ivar]['DaughterLocations'] = { dauLoc: ivar[9:]+ivar[:9] }

        Lc2KsKsH = copy.deepcopy(CommonKsKsH)
        Lc2KsKsH.update({   'AM_MIN'                : 2181. * MeV,
                            'AM_MAX'                : 2391. * MeV,
                            'BPVLTIME_MIN'          : 0.1 * picosecond} )
        Lc2KsKsHMass = {    'Mass_M_MIN' : 2156. * MeV,
                            'Mass_M_MAX' : 2366. * MeV }
        for ivar, dauLoc in {'ConeVar10KS0':"[Lambda_c+ -> ^KS0 KS0 p+]CC",
            'ConeVar10KS1':"[Lambda_c+ -> KS0 ^KS0 p+]CC",
            'ConeVar10H':"[Lambda_c+ -> KS0 KS0 ^p+]CC"}.iteritems():
            IsoVars['Lc'+ivar] = copy.deepcopy(IsoVars['Dp'+ivar])
            IsoVars['Lc'+ivar]['DaughterLocations'] = { dauLoc: ivar[9:]+ivar[:9] }

        cuts = { 'Dp2KsKsH'       : Dp2KsKsH,
                 'Ds2KsKsH'       : Ds2KsKsH,
                 'Lc2KsKsH'       : Lc2KsKsH,
                 'Dp2KsKsHMass'   : Dp2KsKsHMass,
                 'Ds2KsKsHMass'   : Ds2KsKsHMass,
                 'Lc2KsKsHMass'   : Lc2KsKsHMass }
        for cname, cut in IsoVars.iteritems():
            cuts[cname] = cut

        return cuts

    def locallines(self):
        from Stages import HKsKsCombiner, MassFilter
        from Stages import CharmHadSharedKsLL, CharmHadSharedKsDD
        from Stages import SharedDetachedDpmChild_pi, SharedDetachedDpmChild_K, SharedDetachedLcChild_p

        Dp2KsKsPip_LL =   HKsKsCombiner('Dp2KsKsPip_LL', decay="[D+ -> KS0 KS0 pi+]cc",
                           inputs=[CharmHadSharedKsLL, CharmHadSharedKsDD, SharedDetachedDpmChild_pi],
                           nickname='Dp2KsKsH')
        Dp2KsKsKp_LL =    HKsKsCombiner('Dp2KsKsKp_LL', decay="[D+ -> KS0 KS0 K+]cc",
                           inputs=[CharmHadSharedKsLL, CharmHadSharedKsDD, SharedDetachedDpmChild_K],
                           nickname='Dp2KsKsH')
        Dsp2KsKsPip_LL =   HKsKsCombiner('Dsp2KsKsPip_LL', decay="[D_s+ -> KS0 KS0 pi+]cc",
                           inputs=[CharmHadSharedKsLL, CharmHadSharedKsDD, SharedDetachedDpmChild_pi],
                           nickname='Ds2KsKsH')
        Dsp2KsKsKp_LL =    HKsKsCombiner('Dsp2KsKsKp_LL', decay="[D_s+ -> KS0 KS0 K+]cc",
                           inputs=[CharmHadSharedKsLL, CharmHadSharedKsDD, SharedDetachedDpmChild_K],
                           nickname='Ds2KsKsH')
        Lcp2KsKsPp_LL =   HKsKsCombiner('Lcp2KsKsPp_LL', decay="[Lambda_c+ -> KS0 KS0 p+]cc",
                           inputs=[CharmHadSharedKsLL, CharmHadSharedKsDD, SharedDetachedLcChild_p],
                           nickname='Lc2KsKsH')


        Dp2KS0KS0Pip = MassFilter('Dp2KsKsHMass',inputs=[Dp2KsKsPip_LL])
        Dp2KS0KS0Kp = MassFilter('Dp2KsKsHMass',inputs=[Dp2KsKsKp_LL])
        Dsp2KS0KS0Pip = MassFilter('Ds2KsKsHMass',inputs=[Dsp2KsKsPip_LL])
        Dsp2KS0KS0Kp = MassFilter('Ds2KsKsHMass',inputs=[Dsp2KsKsKp_LL])
        Lcp2KS0KS0Pp = MassFilter('Lc2KsKsHMass',inputs=[Lcp2KsKsPp_LL])

        stages = {
                  'Dp2KS0KS0PipTurbo'   : [Dp2KS0KS0Pip],
                  'Dp2KS0KS0KpTurbo'    : [Dp2KS0KS0Kp],

                  'Dsp2KS0KS0PipTurbo'  : [Dsp2KS0KS0Pip],
                  'Dsp2KS0KS0KpTurbo'   : [Dsp2KS0KS0Kp],

                  'Lcp2KS0KS0PpTurbo'   : [Lcp2KS0KS0Pp],
                 }

        return stages

    def relatedInfo(self):
        from Hlt2Lines.Utilities.Hlt2RelatedInfo import Hlt2RelInfoConeVariables, Hlt2RelInfoVertexIsolation
        DConeVar05 = Hlt2RelInfoConeVariables('DConeVar05')
        DConeVar10 = Hlt2RelInfoConeVariables('DConeVar10')
        DConeVar15 = Hlt2RelInfoConeVariables('DConeVar15')
        DpConeVar10KS0 = Hlt2RelInfoConeVariables('DpConeVar10KS0')
        DpConeVar10KS1 = Hlt2RelInfoConeVariables('DpConeVar10KS1')
        DpConeVar10H   = Hlt2RelInfoConeVariables('DpConeVar10H')
        DsConeVar10KS0 = Hlt2RelInfoConeVariables('DsConeVar10KS0')
        DsConeVar10KS1 = Hlt2RelInfoConeVariables('DsConeVar10KS1')
        DsConeVar10H   = Hlt2RelInfoConeVariables('DsConeVar10H')
        LcConeVar10KS0 = Hlt2RelInfoConeVariables('LcConeVar10KS0')
        LcConeVar10KS1 = Hlt2RelInfoConeVariables('LcConeVar10KS1')
        LcConeVar10H   = Hlt2RelInfoConeVariables('LcConeVar10H')
        VertexIso = Hlt2RelInfoVertexIsolation("VertexIsolation")

        IsoVarsBase = [DConeVar05, DConeVar10, DConeVar15, VertexIso]
        IsoVarsDp = IsoVarsBase+[DpConeVar10KS0, DpConeVar10KS1, DpConeVar10H]
        IsoVarsDs = IsoVarsBase+[DsConeVar10KS0, DsConeVar10KS1, DsConeVar10H]
        IsoVarsLc = IsoVarsBase+[LcConeVar10KS0, LcConeVar10KS1, LcConeVar10H]
        IsoVars = {
            'Dp2KS0KS0PipTurbo'  : IsoVarsDp,
            'Dp2KS0KS0KpTurbo'   : IsoVarsDp,
            'Dsp2KS0KS0PipTurbo' : IsoVarsDs,
            'Dsp2KS0KS0KpTurbo'  : IsoVarsDs,
            'Lcp2KS0KS0PpTurbo'  : IsoVarsLc
            }

        relInfo = {}
        for lname in ['Dp2KS0KS0PipTurbo','Dp2KS0KS0KpTurbo',
            'Dsp2KS0KS0PipTurbo','Dsp2KS0KS0KpTurbo',
            'Lcp2KS0KS0PpTurbo']: relInfo[lname] = IsoVars[lname]
        return relInfo
