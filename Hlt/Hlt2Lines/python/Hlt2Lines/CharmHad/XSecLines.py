###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm, mrad
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser

# Lines for XSection measurements, specifically for the 2015 Early Measurement campaign
# These are tuned to be as close as possible to the stripping lines

## Temporary local definition of particle masses.  Used to adapt TagDecay cut
## variables from from Delta M to Q.
_local_m_pip = 139.57018 * MeV
_local_m_piz = 134.97660 * MeV

class CharmHadXSecLines() :
    def localcuts(self) : 
        return {
                 'CharmHadSharedDetachedDpmChild_pi_XSec': {'PID_LIM': 3,
                                                            'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                                                            'Trk_ALL_PT_MIN': 200 * MeV},
                 'CharmHadSharedDetachedDpmChild_K_XSec': {'PID_LIM': 5,
                                                           'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                                                           'Trk_ALL_PT_MIN': 200 * MeV},
                 'CharmHadSharedSoftTagChild_pi_XSec': {'Trk_ALL_PT_MIN': 100.0 * MeV},
                 'CharmHadSharedNeutralLowPtChild_pi0_XSec': {'Neut_ALL_ADMASS_MAX': 60.0,
                                                              'Neut_ALL_PT_MIN': 350.0 * MeV},
                 'CharmHadSharedNeutralLowPtChild_gamma_XSec': {'Neut_ALL_PT_MIN': 350.0 * MeV},
                 'CharmHadSharedDetachedLcChild_pi_XSec': {'PID_LIM': 3,
                                                           'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                                                           'Trk_ALL_PT_MIN': 200 * MeV},
                 'CharmHadSharedDetachedLcChild_K_XSec': {'PID_LIM': 5,
                                                          'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                                                          'Trk_ALL_PT_MIN': 200 * MeV},
                 'CharmHadSharedDetachedLcChild_p_XSec': {'PID_LIM': 5,
                                                          'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                                                          'Trk_ALL_PT_MIN': 200 * MeV},
                 'CharmHadSharedPromptChild_pi_XSec': {'PID_LIM': 3.0, 'Trk_ALL_PT_MIN': 250.0 * MeV},
                 'CharmHadSharedPromptChild_K_XSec': {'PID_LIM': 5.0, 'Trk_ALL_PT_MIN': 250.0 * MeV},
                 'CharmHadSharedPromptChild_p_XSec': {'PID_LIM': 10, 'Trk_ALL_PT_MIN': 250.0 * MeV},
                 'TighterProtons_XSec': {'DeltaPID_MIN': 5.0,
                     'PIDp_MIN': 5.0,
                     'P_MIN': 10000 * MeV},
                 # Now the combiner for the CPV lines 
                 'D02HH_XSec'   : {
                                  'TisTosSpec'               : "Hlt1CharmXSecDecision%TOS",
                                  'Pair_AMINDOCA_MAX'        : 0.10 * mm,
                                  'Trk_Max_APT_MIN'          : 500.0 * MeV, 
                                  'D0_BPVVDCHI2_MIN'         : 49.0,       # neuter
                                  'D0_acosBPVDIRA_MAX'       : 17.3 * mrad,
                                  'D0_VCHI2PDOF_MAX'         : 10.0,       # neuter
                                  'D0_PT_MIN'                : 0.0 * MeV, 
                                  'Comb_AM_MIN'              : 1775.0 * MeV, 
                                  'Comb_AM_MAX'              : 1955.0 * MeV, 
                                  ## These should be removed to input particle filtering
                                  'Trk_ALL_PT_MIN'           : 250.0 * MeV, 
                                  'Trk_ALL_P_MIN'            : 2.0  * GeV, 
                                  'Trk_ALL_MIPCHI2DV_MIN'    : 16.0, 
                                  ## Add mass filter after combination cut
                                  'Mass_M_MIN'               :  1784.0 * MeV,
                                  'Mass_M_MAX'               :  1944.0 * MeV,
                                },
                 'D02HHHH_XSec' : {
                                 'TisTosSpec'               : "Hlt1CharmXSecDecision%TOS",
                                 'Trk_ALL_PT_MIN'           :  200 * MeV,
                                 'Trk_ALL_MIPCHI2DV_MIN'    :  4,
                                 'AM_34'                    :  (139.5 + 139.5) * MeV,
                                 'AM_4'                     :  (139.5) * MeV,
                                 'AM_MIN'                   :  1774 * MeV,
                                 'AM_MAX'                   :  1954 * MeV,
                                 'ASUMPT_MIN'               :  0.0 * MeV,
                                 'ADOCA_MAX'                :  0.1 * mm,
                                 'ACHI2DOCA_MAX'            :  99999999.0,
                                 'VCHI2PDOF_MAX'            :  10.0,
                                 'acosBPVDIRA_MAX'          :  17.3 * mrad,
                                 'BPVLTIME_MIN'             :  0.1*picosecond,
                                 'Mass_M_MIN'               :  1784.0 * MeV,
                                 'Mass_M_MAX'               :  1944.0 * MeV,
                                 'AMOM_MIN'                 :  0 * MeV,
                                 'DPT_MIN'                  :  0 * MeV,
                                 'DMOM_MIN'                 :  0 * MeV,
                                 'BPVVDCHI2_MIN'            :  9,
                                },
                 'Dpm2HHH_XSec' : {
                                 'TisTosSpec'               : "Hlt1CharmXSecDecision%TOS",
                                 'Trk_ALL_MIPCHI2DV_MIN'    :  4.0,
                                 'Trk_2OF3_MIPCHI2DV_MIN'   :  10.0,
                                 'Trk_1OF3_MIPCHI2DV_MIN'   :  50.0,
                                 'Trk_ALL_PT_MIN'           :  200.0 * MeV,
                                 'Trk_2OF3_PT_MIN'          :  400.0 * MeV,
                                 'Trk_1OF3_PT_MIN'          :  1000.0 * MeV,
                                 'VCHI2PDOF_MAX'            :  25.0,
                                 'BPVVDCHI2_MIN'            :  16.0,
                                 'BPVLTIME_MIN'             :  0.15 * picosecond,
                                 'acosBPVDIRA_MAX'          :  34.6 * mrad,
                                 'ASUMPT_MIN'               :  0 * MeV,
                                 'AM_MIN'                   :  1779 * MeV,
                                 'AM_MAX'                   :  1959 * MeV,
                                 'Mass_M_MIN'               :  1789.0 * MeV,
                                 'Mass_M_MAX'               :  1949.0 * MeV,
                                },
                 'Ds2HHH_XSec' : { 
                                 'TisTosSpec'               : "Hlt1CharmXSecDecision%TOS",
                                 'Trk_ALL_MIPCHI2DV_MIN'    :  4.0,
                                 'Trk_2OF3_MIPCHI2DV_MIN'   :  10.0,
                                 'Trk_1OF3_MIPCHI2DV_MIN'   :  50.0,
                                 'Trk_ALL_PT_MIN'           :  200.0 * MeV,
                                 'Trk_2OF3_PT_MIN'          :  400.0 * MeV,
                                 'Trk_1OF3_PT_MIN'          :  1000.0 * MeV,
                                 'VCHI2PDOF_MAX'            :  25.0,
                                 'BPVVDCHI2_MIN'            :  16.0,
                                 'BPVLTIME_MIN'             :  0.15 * picosecond,
                                 'acosBPVDIRA_MAX'          :  34.6 * mrad,
                                 'ASUMPT_MIN'               :  0 * MeV,
                                 'AM_MIN'                   :  1879 * MeV,
                                 'AM_MAX'                   :  2059 * MeV,
                                 'Mass_M_MIN'               :  1889.0 * MeV,
                                 'Mass_M_MAX'               :  2049.0 * MeV,
                                },
                 'Lc2HHH_XSec' : {
                                 'TisTosSpec'               : "Hlt1CharmXSecDecision%TOS",
                                 'Trk_ALL_MIPCHI2DV_MIN'    :  4.0, 
                                 'Trk_2OF3_MIPCHI2DV_MIN'   :  10.0,
                                 'Trk_1OF3_MIPCHI2DV_MIN'   :  50.0,
                                 'Trk_ALL_PT_MIN'           :  200.0 * MeV,
                                 'Trk_2OF3_PT_MIN'          :  400.0 * MeV,
                                 'Trk_1OF3_PT_MIN'          :  1000.0 * MeV,
                                 'VCHI2PDOF_MAX'            :  25.0,
                                 'BPVVDCHI2_MIN'            :  4.0,
                                 'BPVLTIME_MIN'             :  0.075 * picosecond,
                                 'acosBPVDIRA_MAX'          :  34.6 * mrad,
                                 'ASUMPT_MIN'               :  0 * MeV,
                                 'AM_MIN'                   :  2201 * MeV,
                                 'AM_MAX'                   :  2553 * MeV,
                                 'Mass_M_MIN'               :  2211.0 * MeV,
                                 'Mass_M_MAX'               :  2543.0 * MeV,
                                },
                 'D0_TAG_XSec' : {
                                 'Q_AM_MIN'                 :  130.0 * MeV - _local_m_pip,
                                 'Q_M_MIN'                  :  130.0 * MeV - _local_m_pip,
                                 'Q_AM_MAX'                 :  165.0 * MeV - _local_m_pip,
                                 'Q_M_MAX'                  :  160.0 * MeV - _local_m_pip,
                                 'TagVCHI2PDOF_MAX'         :  25.0
                                },
                 'D_TAG_Pi0_XSec': {
                                 'Q_AM_MIN' : -5.0 * MeV,
                                 'Q_AM_MAX' : 30.0 * MeV,
                                 'Q_M_MIN' : -5.0 * MeV,
                                 'Q_M_MAX' :  25.0 * MeV
                                },
                 'D_TAG_Gamma_XSec': {
                                 'Q_AM_MIN' : -5.0 * MeV,
                                 'Q_AM_MAX' : 350.0 * MeV,
                                 'Q_M_MIN' : -5.0 * MeV,
                                 'Q_M_MAX' :  300.0 * MeV
                                },
                 'Sigmac_TAG_XSec' : { 
                                 'Q_AM_MIN'                 :  150.0 * MeV - _local_m_pip,
                                 'Q_M_MIN'                  :  155.0 * MeV - _local_m_pip,
                                 'Q_AM_MAX'                 :  205.0 * MeV - _local_m_pip,
                                 'Q_M_MAX'                  :  200.0 * MeV - _local_m_pip,
                                 'TagVCHI2PDOF_MAX'         :  25.0
                                },
                 'Xic02HHHH_XSec': {'AM_MAX': 2780.0,
                                    'AM_MIN': 2386.0,
                                    'ASUMPT_MIN': 0.0 * MeV,
                                    'acosBPVDIRA_MAX' : 35. * mrad,
                                    'BPVLTIME_MIN': 0.075 * picosecond,
                                    'BPVVDCHI2_MIN': 4.0,
                                    'Mass_M_MAX': 2770.0,
                                    'Mass_M_MIN': 2396.0,
                                    'Trk_1OF4_MIPCHI2DV_MIN': 4.0,
                                    'Trk_1OF4_PT_MIN': 800.0 * MeV,
                                    'Trk_2OF4_MIPCHI2DV_MIN': 4.0,
                                    'Trk_2OF4_PT_MIN': 400.0 * MeV,
                                    'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                                    'Trk_ALL_PT_MIN': 250.0 * MeV,
                                    'VCHI2PDOF_MAX': 10.0,
                                    'TisTosSpec': 'Hlt1CharmXSecDecision%TOS'},
                }
    
    def locallines(self):
        from Stages import MassFilter,TagDecay,TagDecayWithNeutral
        from Stages import XSec_SharedSoftTagChild_pi,XSec_SharedNeutralLowPtChild_pi0,XSec_SharedNeutralLowPtChild_gamma
        from Stages import XSec_D02KPi, XSec_D02K3Pi
        from Stages import XSec_DpToKmPipPip, XSec_DpToKmKpPim
        from Stages import XSec_DspToKmKpPim, XSec_DspToPimPipPip
        from Stages import XSec_LcpToKmPpPip, XSec_LcpToPimPpPip, XSec_LcpToKmPpKp
        from Stages import XSec_Xic0ToPpKmKmPip
        
        stages = {# First the D2HHH lines
                  'Dpm2KPiPi_XSecTurbo'       : [MassFilter('Dpm2KPiPi_XSec',nickname='Dpm2HHH_XSec', 
                                                      inputs=[XSec_DpToKmPipPip],shared=True, monitor='D+')],
                  'Dpm2KKPi_XSecTurbo'        : [MassFilter('Dpm2KKPi_XSec', nickname = 'Dpm2HHH_XSec',
                                                      inputs=[XSec_DpToKmKpPim],shared=True, monitor='D+')],
                  'Ds2KKPi_XSecTurbo'         : [MassFilter('Ds2KKPi_XSec', nickname = 'Ds2HHH_XSec', 
                                                      inputs=[XSec_DspToKmKpPim],shared=True, monitor='D_s+')],
                  'Ds2PiPiPi_XSecTurbo'       : [MassFilter('Ds2PiPiPi_XSec', nickname = 'Ds2HHH_XSec',
                                                      inputs=[XSec_DspToPimPipPip],shared=True, monitor='D_s+')],
                  # Now the Lc2HHH lines, untagged, CF shared to reuse in tagged lines
                  # Because of the mass window these also catch neutral Xi_c baryons
                  'Lc2KPPi_XSecTurbo'        : [MassFilter('Lc2KPPi_XSec', nickname = 'Lc2HHH_XSec',
                                                      inputs=[XSec_LcpToKmPpPip],shared=True,
                                                      monitor='Lambda_c+')],
                  'Lc2KPK_XSecTurbo'         : [MassFilter('Lc2KPK_XSec', nickname = 'Lc2HHH_XSec',
                                                      inputs=[XSec_LcpToKmPpKp],shared=True, monitor='Lambda_c+')],
                  'Lc2PiPPi_XSecTurbo'       : [MassFilter('Lc2PiPPi_XSec', nickname = 'Lc2HHH_XSec',
                                                      inputs=[XSec_LcpToPimPpPip],shared=True, monitor='Lambda_c+')],
                  # Now the Xic02HHHH
                  'Xic02PKKPi_XSecTurbo'     : [MassFilter('Xic02PKKPi_XSec', nickname = 'Xic02HHHH_XSec',
                                                      inputs=[XSec_Xic0ToPpKmKmPip],shared=True,
                                                           monitor='Xi_c0')],
                  # The untagged D->KPi line
                  'D02KPi_XSecTurbo'         : [MassFilter('D02KPi_XSec',nickname='D02HH_XSec',
                                                      inputs=[XSec_D02KPi],shared=True, monitor='D0')]
                 }
        # Now the Sigma_c0,++->Lambda_c(pKpi)pi line
        # Because of the mass window this also catches some excited Xi_c
        stages['Sigmac_2LcPi_XSecTurbo']      = [TagDecay('Sigmac_2LcPi_XSec',
                                                     ["[Sigma_c0 -> Lambda_c+ pi-]cc",
                                                      "[Sigma_c++ -> Lambda_c+ pi+]cc"],
                                                     inputs = [ stages["Lc2KPPi_XSecTurbo"][0],
                                                                XSec_SharedSoftTagChild_pi ], 
                                                     nickname = 'Sigmac_TAG_XSec', shared=True,
                                                     Preambulo = [
                            "massHisto_Sc    = Gaudi.Histo1DDef('Sigma_c_mass', 2356, 2753, 198)",
                            "massHisto_delta = Gaudi.Histo1DDef('Sc_Lcp_delta_mass', 150, 205, 109)",
                            "massHisto_Lcp   = Gaudi.Histo1DDef('Lambda_c+_mass', 2206, 2548, 170)"],
                                                     MotherMonitor = (
                            "process(monitor(CHILD(M, ABSID == 'Lambda_c+'), massHisto_Lcp, 'Lambda_c+_mass')) >> "
                            "process(monitor(M, massHisto_Sc, 'Sigma_c_mass')) >> "
                            "process(monitor((M - CHILD(M, ABSID == 'Lambda_c+')), massHisto_delta, 'Sc_Lcp_delta_mass')) >> "
                            "~EMPTY")

                                                     ) ]
        # Now the D*->D0pi line for the D0->KPi case
        stages['Dst_2D0Pi_D02KPi_XSecTurbo']  = [TagDecay('Dst_2D0Pi_D02KPi_XSec',
                                                     ["[D*(2010)+ -> D0 pi+]cc"],
                                                     inputs = [ stages["D02KPi_XSecTurbo"][0],
                                                                XSec_SharedSoftTagChild_pi ], 
                                                     nickname = 'D0_TAG_XSec', shared=True,
                                                     Preambulo = [
                            "massHisto_Dst = Gaudi.Histo1DDef('D*(2010)+_mass', 1897, 2123, 112)",
                            "massHisto_delta = Gaudi.Histo1DDef('Dst_D0_delta_mass', 125, 165, 159)",
                            "massHisto_D0 = Gaudi.Histo1DDef('D0_mass', 1772, 1958, 93)"],
                                                     MotherMonitor = (
                            "process(monitor(CHILD(M, ABSID == 'D0'), massHisto_D0, 'D0_mass')) >> "
                            "process(monitor(M, massHisto_Dst, 'D*(2010)+_mass')) >> "
                            "process(monitor((M - CHILD(M, ABSID == 'D0')), massHisto_delta, 'Dst_D0_delta_mass')) >> "
                            "~EMPTY")
) ]

        # Now the D*->D0pi line for the D0->K3Pi case
        stages['Dst_2D0Pi_D02K3Pi_XSecTurbo'] = [TagDecay('Dst_2D0Pi_D02K3Pi_XSec',
                                                     ["[D*(2010)+ -> D0 pi+]cc"],
                                                     inputs = [ MassFilter('D02K3Pi_XSec',nickname='D02HHHH_XSec',
                                                                           inputs=[XSec_D02K3Pi],shared=True),
                                                                XSec_SharedSoftTagChild_pi ], 
                                                     nickname='D0_TAG_XSec', shared=True,
                                                     Preambulo = [
                            "massHisto_Dst = Gaudi.Histo1DDef('D*(2010)+_mass', 1897, 2123, 112)",
                            "massHisto_delta = Gaudi.Histo1DDef('Dst_D0_delta_mass', 125, 165, 159)",
                            "massHisto_D0 = Gaudi.Histo1DDef('D0_mass', 1772, 1958, 93)"],
                                                     MotherMonitor = (
                            "process(monitor(CHILD(M, ABSID == 'D0'), massHisto_D0, 'D0_mass')) >> "
                            "process(monitor(M, massHisto_Dst, 'D*(2010)+_mass')) >> "
                            "process(monitor((M - CHILD(M, ABSID == 'D0')), massHisto_delta, 'Dst_D0_delta_mass')) >> "
                            "~EMPTY")
) ]

        # Now the lines tagged with a photon or pi0
        stages['Dst_2D0Pi0_D02KPi_XSecTurbo']    = [TagDecayWithNeutral('Dst_2D0Pi0_D02KPi_XSec',
                                                                   ["D*(2007)0 -> D0 pi0","D*(2007)0 -> D~0 pi0"],
                                                                   inputs = [ stages["D02KPi_XSecTurbo"][0],
                                                                   XSec_SharedNeutralLowPtChild_pi0 ], 
                                                                   nickname='D_TAG_Pi0_XSec', shared=True,
                                                     Preambulo = [
                            "massHisto_Dst = Gaudi.Histo1DDef('D*(2007)0_mass', 1897, 2123, 112)",
                            "massHisto_delta = Gaudi.Histo1DDef('Dst_D0_delta_mass', 125, 165, 159)",
                            "massHisto_D0 = Gaudi.Histo1DDef('D0_mass', 1772, 1958, 93)"],
                                                     MotherMonitor = (
                            "process(monitor(CHILD(M, ABSID == 'D0'), massHisto_D0, 'D0_mass')) >> "
                            "process(monitor(M, massHisto_Dst, 'D*(2007)0_mass')) >> "
                            "process(monitor((M - CHILD(M, ABSID == 'D0')), massHisto_delta, 'Dst_D0_delta_mass')) >> "
                            "~EMPTY")
) ]
        stages['Dst_2D0Gamma_D02KPi_XSecTurbo']  = [TagDecayWithNeutral('Dst_2D0Gamma_D02KPi_XSec',
                                                                   ["D*(2007)0 -> D0 gamma","D*(2007)0 -> D~0 gamma"],
                                                                   inputs = [ stages["D02KPi_XSecTurbo"][0],
                                                                   XSec_SharedNeutralLowPtChild_gamma ],  
                                                                   nickname='D_TAG_Gamma_XSec', shared=True,
                                                     Preambulo = [
                            "massHisto_Dst = Gaudi.Histo1DDef('D*(2007)0_mass', 1897, 2123, 112)",
                            "massHisto_delta = Gaudi.Histo1DDef('Dst_D0_delta_mass', 125, 165, 159)",
                            "massHisto_D0 = Gaudi.Histo1DDef('D0_mass', 1772, 1958, 93)"],
                                                     MotherMonitor = (
                            "process(monitor(CHILD(M, ABSID == 'D0'), massHisto_D0, 'D0_mass')) >> "
                            "process(monitor(M, massHisto_Dst, 'D*(2007)0_mass')) >> "
                            "process(monitor((M - CHILD(M, ABSID == 'D0')), massHisto_delta, 'Dst_D0_delta_mass')) >> "
                            "~EMPTY")
) ]
        stages['Dst_2DsGamma_Ds2KKPi_XSecTurbo'] = [TagDecayWithNeutral('Dst_2DsGamma_Ds2KKPi_XSec',
                                                                   ["[D*_s+ -> D_s+ gamma]cc"],
                                                                   inputs = [ stages["Ds2KKPi_XSecTurbo"][0],
                                                                   XSec_SharedNeutralLowPtChild_gamma ], 
                                                                   nickname='D_TAG_Gamma_XSec',shared=True,
                                                     Preambulo = [
                            "massHisto_Dsst = Gaudi.Histo1DDef('D*_s+_mass', 2009, 2219, 104)",
                            "massHisto_delta = Gaudi.Histo1DDef('Dsst_Dsp_delta_mass', 125, 165, 159)",
                            "massHisto_Dsp = Gaudi.Histo1DDef('D_s+_mass', 1884, 2054, 84)"],
                                                     MotherMonitor = (
                            "process(monitor(CHILD(M, ABSID == 'D_s+'), massHisto_Dsp, 'D_s+_mass')) >> "
                            "process(monitor(M, massHisto_Dsst, 'D*_s+_mass')) >> "
                            "process(monitor((M - CHILD(M, ABSID == 'D_s+')), massHisto_delta, 'Dsst_Dsp_delta_mass')) >> "
                            "~EMPTY")
) ]  
 

        return stages
