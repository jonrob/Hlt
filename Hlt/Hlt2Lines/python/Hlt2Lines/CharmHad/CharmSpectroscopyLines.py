###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser

## Temporary local definition of particle masses.  Used to adapt TagDecay cut
## variables from from Delta M to Q.
_local_m_pip =  139.57018 * MeV
_local_m_KS0 =  497.61400 * MeV
_local_m_pp  =  938.27205 * MeV
_local_m_Lam = 1115.68300 * MeV
_local_m_eta =  547.86200 * MeV

class CharmSpectroscopyLines :
    def localcuts(self) :
        return {
##  D0,pi is considered separately from the others because the inclusive D0 rate is
##  expected to be very high due to the large number of real D0 signal events
                'Spec_D0Pi' : {
                                        'Q_AM_MIN'         : 0. * MeV,
                                        'Q_AM_MAX'         : 885. * MeV,
                                        'Q_M_MIN'          : -1. * MeV,
                                        'Q_M_MAX'          : 855. * MeV,
                                        'TagVCHI2PDOF_MAX' : 10.0,
                                        'TisTosSpec'       : "Hlt1.*Track.*Decision%TOS",
                                       }
##  Dpm,pi is considered separately from the others because the inclusive Dpm rate is
##  expected to be very high due to the large number of real D+ signal events
               ,'Spec_DpmPi' : {
                                        'Q_AM_MIN'         : 0. * MeV,
                                        'Q_AM_MAX'         : 885. * MeV,
                                        'Q_M_MIN'          : -1. * MeV,
                                        'Q_M_MAX'          : 855. * MeV,
                                        'TagVCHI2PDOF_MAX' : 10.0,
                                       }
               ,'Spec_DPi' : {
                                        'Q_AM_MIN'         : 0. * MeV,
                                        'Q_AM_MAX'         : 885. * MeV,
                                        'Q_M_MIN'          : -1. * MeV,
                                        'Q_M_MAX'          : 855. * MeV,
                                        'TagVCHI2PDOF_MAX' : 10.0,
                                       }
               ,'Spec_DK' : {
                                        'Q_AM_MIN'         : 0. * MeV,
                                        'Q_AM_MAX'         : 885. * MeV,
                                        'Q_M_MIN'          : -1. * MeV,
                                        'Q_M_MAX'          : 855. * MeV,
                                        'TagVCHI2PDOF_MAX' : 10.0,
                                       }
               ,'Spec_DPr' : {
                                        'Q_AM_MIN'         : 0. * MeV,
                                        'Q_AM_MAX'         : 885.* MeV,
                                        'Q_M_MIN'          : -1. * MeV,
                                        'Q_M_MAX'          : 855. * MeV,
                                        'TagVCHI2PDOF_MAX' : 10.0,
                                       }
               ,'Spec_DLambda' : {
                                        'Q_AM_MIN'         : 0. * MeV,
                                        'Q_AM_MAX'         : 885. * MeV,
                                        'Q_M_MIN'          : -1. * MeV,
                                        'Q_M_MAX'          : 855. * MeV,
                                        'TagVCHI2PDOF_MAX' : 10.0,
                                       }
               ,'Spec_DEta' : {
                                        'Q_AM_MIN'         :  0. * MeV,
                                        'Q_AM_MAX'         : 885. * MeV,
                                        'Q_M_MIN'          :  -1. * MeV,
                                        'Q_M_MAX'          : 855. * MeV,
                                       }
               ,'Spec_DGamma' : {
                                        'Q_AM_MIN'         :  0.  * MeV,
                                        'Q_AM_MAX'         : 885. * MeV,
                                        'Q_M_MIN'          :  0.  * MeV,
                                        'Q_M_MAX'          : 855. * MeV,
                                        }
               ,'Spec_DMu' : {
                                        'Q_AM_MIN'         :  0.  * MeV,
                                        'Q_AM_MAX'         : 885. * MeV,
                                        'Q_M_MIN'          :  -1. * MeV,
                                        'Q_M_MAX'          : 855. * MeV,
                                        'TagVCHI2PDOF_MAX' : 10.0,
                                        }
                ,'Spec_DE' : {
                                        'Q_AM_MIN'         :  0.  * MeV,
                                        'Q_AM_MAX'         : 885. * MeV,
                                        'Q_M_MIN'          :  -1. * MeV,
                                        'Q_M_MAX'          : 855. * MeV,
                                        'TagVCHI2PDOF_MAX' : 10.0,
                                        }
                ,'Dst2D0pi_D02KPiPiPi' : {
                                        'Q_AM_MIN'         : 0. * MeV,
                                        'Q_AM_MAX'         : 35.5 * MeV,
                                        'Q_M_MIN'          : -1. * MeV,
                                        'Q_M_MAX'          : 25.5 * MeV,
                                        'TagVCHI2PDOF_MAX' : 10.0,
                                       }
                 ,'ChargedSpectroscopyBachelors' : {
                                        'IPCHI2_MAX'       :  15,
                                       }

                 # these are cuts for the PromptSpectroscopyFilter
                 # the default mass window, 2211 - 2362 correspond to the values in D2HHHLines.py
                 ,'LcSpec' : {
                                'IPCHI2_MAX'       :  15,
                                'D_BPVLTIME_MIN'   :  0.3 * picosecond,
                                'DMASS_MIN'        :  2211 * MeV,
                                'DMASS_MAX'        :  2362 * MeV,
                                'DPT_MIN'          :  2000 * MeV,
                                'DMom_MIN'         :  30000 * MeV
                               }

                 , 'D02KPiPiPiSpec' : {
                                'IPCHI2_MAX'       :  15,
                                'D_BPVLTIME_MIN'   :  0.3 * picosecond,
                                'DMASS_MIN'        :  1790 * MeV,
                                'DMASS_MAX'        :  1940 * MeV,
                                'DPT_MIN'          :  2000 * MeV,
                                'DMom_MIN'         :  30000 * MeV
                               }
        }

    def locallines(self):

##  these are  combiners instantiated in Stages.py
##  as indicated below, there is a different grammar to access
##  candidates passing trigger selections specified in  other files
        from Stages import D02HH_D0ToKmPip
        from Stages import TagDecay, TagDecayWithNeutral
        from Stages import TagExtraDecay, TagExtraDecayWithNeutral
        from Stages import SharedSoftTagChild_pi, SharedPromptChild_K, SharedPromptChild_p, SharedPromptChild_mu, SharedPromptChild_e
        from Stages import CharmHadSharedKsLL, CharmHadSharedKsDD
        from Stages import CharmHadSharedSecondaryPIDLambdaLL, CharmHadSharedSecondaryPIDLambdaDD
        from Stages import D02KsKK_LL, D02KsKK_DD, D02KsPiPi_LL, D02KsPiPi_DD
        from Stages import D02KsKPi_LL, D02KsKPi_DD
        from Stages import D2HHH_DspToKmKpPip, D2HHH_DpToKmPipPip
        from Stages import D0_TAG_CPV_Dstp2D0Pip_D02KmPip
        from Stages import DstToD02HHHH_D02CFKPiPiPi
        from Stages import D02HHHHMass_D02KPiPiPi
        from Stages import SharedNeutralLowPtChild_pi0, SharedNeutralLowPtChild_eta
        from Stages import SharedNeutralLowPtChild_gamma
        from Stages import PromptBachelorFilter, PromptSpectroscopyFilter
        from Stages import SharedTighterPromptChild_p
        from Stages import SharedSoftTagChild_pi
##


## these come from D02HHHHLines.py
        D02KPiPiPiForSpectroscopy = PromptSpectroscopyFilter('D02KPiPiPiSpec'
                , inputs=[D02HHHHMass_D02KPiPiPi]
                , shared = True)

        from Stages import Lc2HHH_LcpToKmPpPip




        from Hlt2Lines.Utilities.Hlt2Stage import Hlt2ExternalStage
        from Hlt2Lines.DPS.Stages import MergeCharm

##  change from MergedD0 to separate trigger lines as D0 --> K,pi and
##  D0 --> K,pi,pi,pi have very different S:B circa July 15, 2015
##  mds        d0 = MergeCharm('MergedD0', inputs = [D02HH_D0ToKmPip, D02KsKK_LL, D02KsKK_DD,
##  mds                D02KsPiPi_LL, D02KsPiPi_DD,
##  mds                D02KsKPi_LL, D02KsKPi_DD,
##  mds                D02KPiPiPiForSpectroscopy
##  mds                ])

##        dstar = MergeCharm('MergedDstar', inputs = [D0_TAG_CPV_Dstp2D0Pip_D02KmPip, DstToD02HHHH_D02CFKPiPiPi])
##        dstar = MergeCharm('MergedDstar', inputs = [D0_TAG_CPV_Dstp2D0Pip_D02KmPip])
        dstarKPi  = D0_TAG_CPV_Dstp2D0Pip_D02KmPip
        dstarK3Pi = TagDecay('Dst2D0pi_D02KPiPiPi'
                , decay = ["[D*(2010)+ -> D0 pi+]cc"]
                , inputs = [D02KPiPiPiForSpectroscopy, SharedSoftTagChild_pi])


##
##
##  start by filtering the standard "prompt" track lists to add IPChi2 cuts
##
        ## shared instances should usually be defined in Stages.py.
        ## Consider moving them there.
        BachelorPi = PromptBachelorFilter('CharmHadBachelorPi',
                                       inputs = [SharedSoftTagChild_pi],
                                       nickname = 'ChargedSpectroscopyBachelors',
                                       shared = True)

        BachelorK = PromptBachelorFilter('CharmHadBachelorK',
                                       inputs = [SharedPromptChild_K],
                                       nickname = 'ChargedSpectroscopyBachelors',
                                       shared = True)

        BachelorPr = PromptBachelorFilter('CharmHadBachelorPr',
                                       inputs = [SharedTighterPromptChild_p],
                                       nickname = 'ChargedSpectroscopyBachelors',
                                       shared = True)
        
        BachelorMu = PromptBachelorFilter('CharmHadBachelorMu',
                                       inputs = [SharedPromptChild_mu],
                                       nickname = 'ChargedSpectroscopyBachelors',
                                       shared = True)
        BachelorE = PromptBachelorFilter('CharmHadBachelorE',
                                       inputs = [SharedPromptChild_e],
                                       nickname = 'ChargedSpectroscopyBachelors',
                                       shared = True)


## LcpForSpectroscopy is meant to be used as input for spectroscopy studies. It has
## a tighter set of selections on  decay time, on IPChi2 (nonexistent for
## default CPV lines), and (potentially) Lambda_c mass to create candidates for spectroscopy studies
## It should be sent to *neither* Full or Turbo.
        ## shared instances should usually be defined in Stages.py.
        ## Consider moving them there.
        LcpForSpectroscopy = PromptSpectroscopyFilter('LcForSpec', nickname = 'LcSpec',
                                            inputs=[Lc2HHH_LcpToKmPpPip], shared = True)



        stages = {}
        extra_selector = 'X -> Charm ^X'
        return {
            'D02KmPipTurbo': {
                'D02KmPip_Add_Pi': [
                    TagExtraDecay(
                        'D02KmPip_Add_Pi',
                        decay=["[D*(2010)+ -> D0 pi+]cc", "[D*(2010)- -> D0 pi-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_D0Pi',
                        inputs=[D02HH_D0ToKmPip, BachelorPi])],
                'D02KmPip_Add_Ks': [
                    TagExtraDecay(
                        'D02KmPip_Add_Ks',
                        decay=["D*(2007)0 -> D0 KS0"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DK',
                        inputs=[D02HH_D0ToKmPip, CharmHadSharedKsLL, CharmHadSharedKsDD])],
                'D02KmPip_Add_K': [
                    TagExtraDecay(
                        'D02KmPip_Add_K',
                        decay=["[D*(2010)+ -> D0 K+]cc", "[D*(2010)- -> D0 K-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DK',
                        inputs=[D02HH_D0ToKmPip, BachelorK])],
                'D02KmPip_Add_Pr': [
                    TagExtraDecay(
                        'D02KmPip_Add_Pr',
                        decay=["[D*(2010)+ -> D0 p+]cc", "[D*(2010)- -> D0 p~-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DPr',
                        inputs=[D02HH_D0ToKmPip, BachelorPr])],
                'D02KmPip_Add_Lambda': [
                    TagExtraDecay(
                        'D02KmPip_Add_Lambda',
                        decay=["[D*(2007)0 -> D0 Lambda0]cc", "[D*(2007)0 -> D~0 Lambda0]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DLambda',
                        inputs=[D02HH_D0ToKmPip, CharmHadSharedSecondaryPIDLambdaLL,
                                CharmHadSharedSecondaryPIDLambdaDD])],
                'D02KmPip_Add_Pi0': [
                    TagExtraDecayWithNeutral(
                        'D02KmPip_Add_Pi0',
                        decay=["D*(2007)0 -> D0 pi0"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DPi',
                        inputs=[D02HH_D0ToKmPip, SharedNeutralLowPtChild_pi0])],
                'D02KmPip_Add_Eta': [
                    TagExtraDecayWithNeutral(
                        'D02KmPip_Add_Eta',
                        decay=["D*(2007)0 -> D0 eta"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DEta',
                        inputs=[D02HH_D0ToKmPip, SharedNeutralLowPtChild_eta])],
                'D02KmPip_Add_Gamma': [
                    TagExtraDecayWithNeutral(
                        'D02KmPip_Add_Gamma',
                        decay=["D*(2007)0 -> D0 gamma"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DGamma',
                        inputs=[D02HH_D0ToKmPip, SharedNeutralLowPtChild_gamma])],
                'D02KmPip_Add_Mu': [
                    TagExtraDecay(
                        'D02KmPip_Add_Mu',
                        decay=["[D*(2010)+ -> D0 mu+]cc", "[D*(2010)- -> D0 mu-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DMu',
                        inputs=[D02HH_D0ToKmPip, BachelorMu])],
                'D02KmPip_Add_E': [
                    TagExtraDecay(
                        'D02KmPip_Add_E',
                        decay=["[D*(2010)+ -> D0 e+]cc", "[D*(2010)- -> D0 e-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DE',
                        inputs=[D02HH_D0ToKmPip, BachelorE])],
            },
            'DspToKmKpPipTurbo': {
                'DspToKmKpPip_Add_Pi': [
                    TagExtraDecay(
                        'DspToKmKpPip_Add_Pi',
                        decay=["[Delta(1905)++ -> D_s+ pi+]cc", "[D*(2007)0 -> D_s+ pi-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DPi',
                        inputs=[D2HHH_DspToKmKpPip, BachelorPi])],
                'DspToKmKpPip_Add_Ks': [
                    TagExtraDecay(
                        'DspToKmKpPip_Add_Ks',
                        decay=["[D*(2010)+ -> D_s+ KS0]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DK',
                        inputs=[D2HHH_DspToKmKpPip, CharmHadSharedKsLL, CharmHadSharedKsDD])],
                'DspToKmKpPip_Add_K': [
                    TagExtraDecay(
                        'DspToKmKpPip_Add_K',
                        decay=["[Delta(1905)++ -> D_s+ K+]cc", "[D*(2007)0 -> D_s+ K-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DK',
                        inputs=[D2HHH_DspToKmKpPip, BachelorK])],
                'DspToKmKpPip_Add_Pr': [
                    TagExtraDecay(
                        'DspToKmKpPip_Add_Pr',
                        decay=["[Delta(1905)++ -> D_s+ p+]cc", "[D*(2007)0 -> D_s+ p~-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DPr',
                        inputs=[D2HHH_DspToKmKpPip, BachelorPr])],
                'DspToKmKpPip_Add_Lambda': [
                    TagExtraDecay(
                        'DspToKmKpPip_Add_Lambda',
                        decay=["[D*(2010)+ -> D_s+ Lambda0]cc", "[D*(2010)- -> D_s- Lambda0]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DLambda',
                        inputs=[D2HHH_DspToKmKpPip, CharmHadSharedSecondaryPIDLambdaLL,
                                CharmHadSharedSecondaryPIDLambdaDD])],
                'DspToKmKpPip_Add_Pi0': [
                    TagExtraDecayWithNeutral(
                        'DspToKmKpPip_Add_Pi0',
                        decay=["[D*(2010)+ -> D_s+ pi0]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DPi',
                        inputs=[D2HHH_DspToKmKpPip, SharedNeutralLowPtChild_pi0])],
                'DspToKmKpPip_Add_Eta': [
                    TagExtraDecayWithNeutral(
                        'DspToKmKpPip_Add_Eta',
                        decay=["[D*(2010)+ -> D_s+ eta]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DEta',
                        inputs=[D2HHH_DspToKmKpPip, SharedNeutralLowPtChild_eta])],
                'DspToKmKpPip_Add_Gamma': [
                    TagExtraDecayWithNeutral(
                        'DspToKmKpPip_Add_Gamma',
                        decay=["[D*(2010)+ -> D_s+ gamma]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DGamma',
                        inputs=[D2HHH_DspToKmKpPip, SharedNeutralLowPtChild_gamma])],
                'DspToKmKpPip_Add_Mu': [
                    TagExtraDecay(
                        'DspToKmKpPip_Add_Mu',
                        decay=["[Delta(1905)++ -> D_s+ mu+]cc", "[D*(2007)0 -> D_s+ mu-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DMu',
                        inputs=[D2HHH_DspToKmKpPip, BachelorMu])],
                'DspToKmKpPip_Add_E': [
                    TagExtraDecay(
                        'DspToKmKpPip_Add_E',
                        decay=["[Delta(1905)++ -> D_s+ e+]cc", "[D*(2007)0 -> D_s+ e-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DE',
                        inputs=[D2HHH_DspToKmKpPip, BachelorE])],
            },
            'DpToKmPipPipTurbo': {
                'DpToKmPipPip_Add_Pi': [
                    TagExtraDecay(
                        'DpToKmPipPip_Add_Pi',
                        decay=["[Delta(1905)++ -> D+ pi+]cc", "[D*(2007)0 -> D+ pi-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DpmPi',
                        inputs=[D2HHH_DpToKmPipPip, BachelorPi])],
                'DpToKmPipPip_Add_Ks': [
                    TagExtraDecay(
                        'DpToKmPipPip_Add_Ks',
                        decay=["[D*(2010)+ -> D+ KS0]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DK',
                        inputs=[D2HHH_DpToKmPipPip, CharmHadSharedKsLL, CharmHadSharedKsDD])],
                'DpToKmPipPip_Add_K': [
                    TagExtraDecay(
                        'DpToKmPipPip_Add_K',
                        decay=["[Delta(1905)++ -> D+ K+]cc", "[D*(2007)0 -> D+ K-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DK',
                        inputs=[D2HHH_DpToKmPipPip, BachelorK])],
                'DpToKmPipPip_Add_Pr': [
                    TagExtraDecay(
                        'DpToKmPipPip_Add_Pr',
                        decay=["[Delta(1905)++ -> D+ p+]cc", "[D*(2007)0 -> D+ p~-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DPr',
                        inputs=[D2HHH_DpToKmPipPip, BachelorPr])],
                'DpToKmPipPip_Add_Lambda': [
                    TagExtraDecay(
                        'DpToKmPipPip_Add_Lambda',
                        decay=["[D*(2010)+ -> D+ Lambda0]cc", "[D*(2010)- -> D- Lambda0]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DLambda',
                        inputs=[D2HHH_DpToKmPipPip, CharmHadSharedSecondaryPIDLambdaLL,
                                CharmHadSharedSecondaryPIDLambdaDD])],
                'DpToKmPipPip_Add_Pi0': [
                    TagExtraDecayWithNeutral(
                        'DpToKmPipPip_Add_Pi0',
                        decay=["[D*(2010)+ -> D+ pi0]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DPi',
                        inputs=[D2HHH_DpToKmPipPip, SharedNeutralLowPtChild_pi0])],
                'DpToKmPipPip_Add_Eta': [
                    TagExtraDecayWithNeutral(
                        'DpToKmPipPip_Add_Eta',
                        decay=["[D*(2010)+ -> D+ eta]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DEta',
                        inputs=[D2HHH_DpToKmPipPip, SharedNeutralLowPtChild_eta])],
                'DpToKmPipPip_Add_Gamma': [
                    TagExtraDecayWithNeutral(
                        'DpToKmPipPip_Add_Gamma',
                        decay=["[D*(2010)+ -> D+ gamma]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DGamma',
                        inputs=[D2HHH_DpToKmPipPip, SharedNeutralLowPtChild_gamma])],
                'DpToKmPipPip_Add_Mu': [
                    TagExtraDecay(
                        'DpToKmPipPip_Add_Mu',
                        decay=["[Delta(1905)++ -> D+ mu+]cc", "[D*(2007)0 -> D+ mu-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DMu',
                        inputs=[D2HHH_DpToKmPipPip, BachelorMu])],
                'DpToKmPipPip_Add_E': [
                    TagExtraDecay(
                        'DpToKmPipPip_Add_E',
                        decay=["[Delta(1905)++ -> D+ e+]cc", "[D*(2007)0 -> D+ e-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DE',
                        inputs=[D2HHH_DpToKmPipPip, BachelorE])],
            },
            'LcpToPpKmPipTurbo': {
                'LcpToPpKmPip_Add_Pi': [
                    TagExtraDecay(
                        'LcpToPpKmPip_Add_Pi',
                        decay=["[Delta(1905)++ -> Lambda_c+ pi+]cc", "[D*(2007)0 -> Lambda_c+ pi-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DPi',
                        inputs=[LcpForSpectroscopy, BachelorPi])],
                'LcpToPpKmPip_Add_Ks': [
                    TagExtraDecay(
                        'LcpToPpKmPip_Add_Ks',
                        decay=["[D*(2010)+ -> Lambda_c+ KS0]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DK',
                        inputs=[LcpForSpectroscopy, CharmHadSharedKsLL,
                                CharmHadSharedKsDD])],
                'LcpToPpKmPip_Add_K': [
                    TagExtraDecay(
                        'LcpToPpKmPip_Add_K',
                        decay=["[Delta(1905)++ -> Lambda_c+ K+]cc",
                               "[D*(2007)0 -> Lambda_c+ K-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DK',
                        inputs=[LcpForSpectroscopy, BachelorK])],
                'LcpToPpKmPip_Add_Pr': [
                    TagExtraDecay(
                        'LcpToPpKmPip_Add_Pr',
                        decay=[
                            "[Delta(1905)++ -> Lambda_c+ p+]cc",
                            "[D*(2007)0 -> Lambda_c+ p~-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DPr',
                        inputs=[LcpForSpectroscopy,
                                BachelorPr])],
                'LcpToPpKmPip_Add_Lambda': [
                    TagExtraDecay(
                        'LcpToPpKmPip_Add_Lambda',
                        decay=[
                            "[D*(2010)+ -> Lambda_c+ Lambda0]cc",
                            "[D*(2010)- -> Lambda_c~- Lambda0]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DLambda',
                        inputs=[
                            LcpForSpectroscopy,
                            CharmHadSharedSecondaryPIDLambdaLL,
                            CharmHadSharedSecondaryPIDLambdaDD])],
                'LcpToPpKmPip_Add_Pi0': [
                    TagExtraDecayWithNeutral(
                        'LcpToPpKmPip_Add_Pi0',
                        decay=["[D*(2010)+ -> Lambda_c+ pi0]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DPi',
                        inputs=[
                            LcpForSpectroscopy,
                            SharedNeutralLowPtChild_pi0])],
                'LcpToPpKmPip_Add_Eta': [
                    TagExtraDecayWithNeutral(
                        'LcpToPpKmPip_Add_Eta',
                        decay=["[D*(2010)+ -> Lambda_c+ eta]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DEta',
                        inputs=[
                            LcpForSpectroscopy,
                            SharedNeutralLowPtChild_eta])],
                'LcpToPpKmPip_Add_Gamma': [
                    TagExtraDecayWithNeutral(
                        'LcpToPpKmPip_Add_Gamma',
                        decay=["[D*(2010)+ -> Lambda_c+ gamma]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DGamma',
                        inputs=[LcpForSpectroscopy, SharedNeutralLowPtChild_gamma])],
                'LcpToPpKmPip_Add_Mu': [
                    TagExtraDecay(
                        'LcpToPpKmPip_Add_Mu',
                        decay=["[Delta(1905)++ -> Lambda_c+ mu+]cc", "[D*(2007)0 -> Lambda_c+ mu-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DMu',
                        inputs=[LcpForSpectroscopy, BachelorMu])],
                'LcpToPpKmPip_Add_E': [
                    TagExtraDecay(
                        'LcpToPpKmPip_Add_E',
                        decay=["[Delta(1905)++ -> Lambda_c+ e+]cc", "[D*(2007)0 -> Lambda_c+ e-]cc"],
                        ParticlesToSave=extra_selector,
                        nickname='Spec_DE',
                        inputs=[LcpForSpectroscopy, BachelorE])],
                }
        }
##  recall that the "nicknake" in TagDecay is the "name" used in the dictionary of cuts.
##
##


        return stages
