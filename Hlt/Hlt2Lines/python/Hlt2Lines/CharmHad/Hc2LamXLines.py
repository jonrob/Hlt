###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Exclusive reconstruction of Xi_c/Lambda_c+ -> Lambda0 h (h') (h'') decays
##
## @author Louis Henry
## @author Joan Ruiz Vidal

from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm, mrad
#Helper function to make proper DD/LL lines

## LHENRY

def makeLambdaDDLL(combType, myDecay, myInputs, myNickname,combiner):
    #    from Stages import MassFilter, DetachedV0HCombiner
    from Inputs import Lambda_LL, Lambda_DD
    decayLL = combiner( combType
                        , decay = myDecay
                        , inputs = myInputs+[ Lambda_LL ]
                        , nickname = myNickname
                        , extraSeparationCut = True )
    decayDD = combiner( combType
                        , decay = myDecay
                        , inputs = myInputs+[ Lambda_DD ]
                        , nickname = myNickname )
    return {"LL":[decayLL],"DD":[decayDD]}

#Helper function to make proper DDD/LLL lines
def makeXimDDDLLL(combType, myDecay, myInputs, myNickname,combiner):    
    #    from Stages import MassFilter, DetachedV0HCombiner
    #    from Stages import Xi2LambdaPi_LLL, Xi2LambdaPi_DDL, Xi2LambdaPi_DDD
    from Stages import CharmHadSharedSecondaryPIDLambdaLL,SharedDetachedDpmChild_pi, CharmHadSharedSecondaryPIDLambdaDD,Hlt2DownPions
    from Stages import ChargedHyperonLambdaHCombiner
    Hc2LamX_Xi2LambdaPi_LLL =   ChargedHyperonLambdaHCombiner('Hc2LamX_Ximinus2LambdaPi_LLL',
                                                              decay="[Xi- -> Lambda0 pi-]cc",
                                                              inputs=[CharmHadSharedSecondaryPIDLambdaLL,SharedDetachedDpmChild_pi])
    Hc2LamX_Xi2LambdaPi_DDL =   ChargedHyperonLambdaHCombiner('Hc2LamX_Ximinus2LambdaPi_DDL',
                                                              decay="[Xi- -> Lambda0 pi-]cc",
                                                              inputs=[CharmHadSharedSecondaryPIDLambdaDD,SharedDetachedDpmChild_pi])
    Hc2LamX_Xi2LambdaPi_DDD =   ChargedHyperonLambdaHCombiner('Hc2LamX_Ximinus2LambdaPi_DDD',
                                                              decay="[Xi- -> Lambda0 pi-]cc",
                                                              inputs=[CharmHadSharedSecondaryPIDLambdaDD,Hlt2DownPions])

    #    from Stages import Hc2LamX_Xi2LambdaPi_LLL,  Hc2LamX_Xi2LambdaPi_DDL,  Hc2LamX_Xi2LambdaPi_DDD
    decayLLL = combiner( combType
                         , decay = myDecay
                         , inputs = myInputs+[  Hc2LamX_Xi2LambdaPi_LLL ]
                         , nickname = myNickname
                         , extraSeparationCut = True )
    decayDDL = combiner( combType
                         , decay = myDecay
                         , inputs = myInputs+[  Hc2LamX_Xi2LambdaPi_DDL ]
                         , nickname = myNickname
                         ,extraSeparationCut = True )
    decayDDD = combiner( combType
                         , decay = myDecay
                         , inputs = myInputs+[  Hc2LamX_Xi2LambdaPi_DDD ]
                         , nickname = myNickname
                         , extraSeparationCut = True )

    return {"LLL":[decayLLL],"DDL":[decayDDL],"DDD":[decayDDD]}


class CharmHadHc2LamXLines() :
    def localcuts(self) :
        massCuts = { 'Xi_c0':{'AM_MIN'    :2386.*MeV,
                              'AM_MAX'    :2556.*MeV,
                              'Mass_M_MIN':2396.*MeV,
                              'Mass_M_MAX':2546.*MeV,
                              'TisTosSpec':"Hlt1.*Track.*Decision%TOS"
                              },
                     'Xi_c+':{'AM_MIN':2382.*MeV,
                              'AM_MAX':2552.*MeV,
                              'Mass_M_MIN':2392.*MeV,
                              'Mass_M_MAX':2542.*MeV,
                              'TisTosSpec':"Hlt1.*Track.*Decision%TOS"
                              },
                     'Lambda_c+':{'AM_MIN':2201.*MeV,
                                  'AM_MAX':2371.*MeV,
                                  'Mass_M_MIN':2211.*MeV,
                                  'Mass_M_MAX':2361.*MeV,
                                  'TisTosSpec':"Hlt1.*Track.*Decision%TOS"
                                  },
                     }
        #Defining the basic values that are shared between most modes.        
        baseCuts   = {}
        for mode in ["Hc2LamH","Hc2LamHH","Hc2LamHHH","Hc2XimH","Hc2XimHH","Hc2XimHHH"]:
            if (mode == "Hc2LamH" or mode == "Hc2XimH"):
                baseCuts[mode] = dict(
                    Trk_ALL_MIPCHI2DV_MIN    =  2,
                    BPVVDCHI2_MIN            =  6,
                    BPVLTIME_MIN             =  0.1 * picosecond,
                    acosBPVDIRA_MAX          =  77.5 * mrad,
                    ASUMPT_MIN               =  1900 * MeV,
                    D2DVVDCHI2_MIN           =  50.0,
                    )
            else:
                baseCuts[mode] = dict(
                    Trk_ALL_MIPCHI2DV_MIN    =  2,
                    BPVVDCHI2_MIN            =  6,
                    BPVLTIME_MIN             =  0.1 * picosecond,
                    acosBPVDIRA_MAX          =  77.5 * mrad,
                    ASUMPT_MIN               =  1900 * MeV,
                    D2DVVDCHI2_MIN           =  50.0,
                    ADOCA_MAX                =  3.0 * mm,
                    ACHI2DOCA_MAX            =  10.0
                    )
            
        cuts = {}
        #Xi_c0 -> Lam hh
        cuts["Xic2LambdaKmPip"] = dict(
            Trk_ALL_MIPCHI2DV_MIN    =  5,
            BPVVDCHI2_MIN            =  8,
            BPVLTIME_MIN             =  0.12 * picosecond,
            acosBPVDIRA_MAX          =  36 * mrad,
            ASUMPT_MIN               =  1900 * MeV,
            D2DVVDCHI2_MIN           =  50.0,
            ADOCA_MAX                =  2.0 * mm,
            ACHI2DOCA_MAX            =  10.0
            )
        
        #        cuts['Xic2LambdaKmPip'] = dict(**(baseCuts["Hc2LamHH"]))
        cuts['Xic2LambdaKmPip'].update(massCuts['Xi_c0'])

        cuts["Xic2LambdaKpKm"] = dict(
            Trk_ALL_MIPCHI2DV_MIN    =  2,
            BPVVDCHI2_MIN            =  8,
            BPVLTIME_MIN             =  0.1 * picosecond,
            acosBPVDIRA_MAX          =  77.5 * mrad,
            ASUMPT_MIN               =  1900 * MeV,
            D2DVVDCHI2_MIN           =  50.0,
            ADOCA_MAX                =  2.0 * mm,
            ACHI2DOCA_MAX            =  10.0
            )
        cuts['Xic2LambdaKpKm'   ].update(massCuts['Xi_c0'])
        
        #Xi_c+ -> Lam Kpipi and L_c+ -> Lam pi pi pi
        cuts['Xic2LambdaKmPipPip'] = dict(**(baseCuts["Hc2LamHHH"]))
        cuts['Xic2LambdaKmPipPip'].update(massCuts['Xi_c+'])
        #        cuts['Lc2LambdaPipPimPip'   ] = dict(**(baseCuts["Hc2LamHHH"]))
        cuts["Lc2LambdaPipPimPip"] = dict(
            Trk_ALL_MIPCHI2DV_MIN    =  5,
            BPVVDCHI2_MIN            =  10,
            BPVLTIME_MIN             =  0.1 * picosecond,
            acosBPVDIRA_MAX          =  45 * mrad,
            ASUMPT_MIN               =  1800 * MeV,
            D2DVVDCHI2_MIN           =  60.0,
            ADOCA_MAX                =  2.0 * mm,
            ACHI2DOCA_MAX            =  7.0
            )
        
        cuts['Lc2LambdaPipPimPip'   ].update(massCuts['Lambda_c+'])

        #Xi_c0 -> Xim h
        cuts['Xic2XimPip'] = dict(**(baseCuts["Hc2XimH"]))
        cuts['Xic2XimPip'].update(massCuts["Xi_c0"])
        cuts['Xic2XimKp' ] = dict(**(baseCuts["Hc2XimH"]))
        cuts['Xic2XimKp' ].update(massCuts["Xi_c0"])

        #Xi_c+ -> Xim pi pi and Lc+ -> Xim K pi
        cuts['Xic2XimPipPip'] = dict(**(baseCuts["Hc2XimHH"]))
        cuts['Xic2XimPipPip'].update(massCuts["Xi_c+"])
        cuts['Lc2XimKpPip'  ] = dict(**(baseCuts["Hc2XimHH"]))
        cuts['Lc2XimKpPip'  ].update(massCuts["Lambda_c+"])

        #Xi_c0 -> Xim pi pi pi
        cuts['Xic2XimPipPimPip'] = dict(**(baseCuts["Hc2XimHHH"]))
        cuts['Xic2XimPipPimPip'].update(massCuts["Xi_c0"])        

        #New Xim cuts
        cuts['Hc2LamX_Ximinus2LambdaPi_LLL']={'VCHI2PDOF_MAX'            :  50.0,
                                              'Trk_ALL_MIPCHI2DV_MIN'    :   4.0,
                                              'BPVVDCHI2_MIN'            :  10.0,
                                              'BPVLTIME_MIN'             :  0.0 * picosecond,
                                              'acosBPVDIRA_MAX'          :  50. * mrad,
                                              'ASUMPT_MIN'               :  100 * MeV,
                                              'AM_MIN'                   :  1200 * MeV,
                                              'AM_MAX'                   :  1455 * MeV,
                                              'Mass_M_MIN'               :  1200.0 * MeV,
                                              'Mass_M_MAX'               :  1455.0 * MeV,
                                              }
        cuts['Hc2LamX_Ximinus2LambdaPi_DDL']={'VCHI2PDOF_MAX'            :  50.0,
                                              'Trk_ALL_MIPCHI2DV_MIN'    :  4.0,
                                              'BPVVDCHI2_MIN'            :  10.0,
                                              'BPVLTIME_MIN'             :  2.0 * picosecond,
                                              'acosBPVDIRA_MAX'          :  50. * mrad,
                                              'ASUMPT_MIN'               :  100 * MeV,
                                              'AM_MIN'                   :  1200 * MeV,
                                              'AM_MAX'                   :  1455 * MeV,
                                              'Mass_M_MIN'               :  1200.0 * MeV,
                                              'Mass_M_MAX'               :  1455.0 * MeV,
                                              }
        cuts['Hc2LamX_Ximinus2LambdaPi_DDD']={'VCHI2PDOF_MAX'            :  50.0,
                                              'Trk_ALL_MIPCHI2DV_MIN'    :  4.0,
                                              'BPVVDCHI2_MIN'            :  10.0,
                                              'BPVLTIME_MIN'             :  0.0 * picosecond,
                                              'acosBPVDIRA_MAX'          :  50. * mrad,
                                              'ASUMPT_MIN'               :  100 * MeV,
                                              'AM_MIN'                   :  1200 * MeV,
                                              'AM_MAX'                   :  1455 * MeV,
                                              'Mass_M_MIN'               :  1200.0 * MeV,
                                              'Mass_M_MAX'               :  1455.0 * MeV,
                                              }
        
        return cuts

    def locallines(self) :
        from Stages import MassFilter, DetachedV0HCombiner, DetachedV0HHCombiner, DetachedV0HHHCombiner
        from Stages import SharedDetachedLcChild_pi, SharedDetachedLcChild_K
        from Inputs import Lambda_LL, Lambda_DD
#        from Stages import Xi2LambdaPi_LLL, Xi2LambdaPi_DDL, Xi2LambdaPi_DDD
#        from Stages import Hc2LamX_Xi2LambdaPi_LLL, Hc2LamX_Xi2LambdaPi_DDL, Hc2LamX_Xi2LambdaPi_DDD
        #Combiners
        combiners = {}
        
        #        # Lambda modes
        combiners["Xic2LambdaKmPip"] = makeLambdaDDLL('Comb',
                                                      "[Xi_c0 -> K- Lambda0 pi+]cc",
                                                      [ SharedDetachedLcChild_K, SharedDetachedLcChild_pi],
                                                      'Xic2LambdaKmPip',
                                                      DetachedV0HHCombiner
                                                      )

        combiners["Xic2LambdaKpKm"   ] = makeLambdaDDLL('Comb',
                                                     "[Xi_c0 -> K- Lambda0 K+]cc",
                                                     [ SharedDetachedLcChild_K],
                                                     'Xic2LambdaKpKm',
                                                      DetachedV0HHCombiner
                                                        )

        combiners["Xic2LambdaKmPipPip"] = makeLambdaDDLL('Comb',
                                                         "[Xi_c+ -> K- Lambda0 pi+ pi+]cc",
                                                         [ SharedDetachedLcChild_K, SharedDetachedLcChild_pi],
                                                         'Xic2LambdaKmPipPip',
                                                         DetachedV0HHHCombiner                                                         
                                                         )

        combiners["Lc2LambdaPipPimPip"   ] = makeLambdaDDLL('Comb',
                                                            "[Lambda_c+ -> pi+ Lambda0 pi- pi+]cc",
                                                            [ SharedDetachedLcChild_pi ],
                                                            'Lc2LambdaPipPimPip',
                                                            DetachedV0HHHCombiner                                                            
                                                            )        

        #        # Xi modes
        #Xi_c0 -> Xim pi pi pi
        combiners["Xic2XimPip"] = makeXimDDDLLL('Comb',
                                                "[Xi_c0 ->  pi+ Xi-]cc",
                                                [SharedDetachedLcChild_pi],
                                                'Xic2XimPip',
                                                DetachedV0HCombiner
                                                )
        
        combiners["Xic2XimKp"]  = makeXimDDDLLL('Comb',
                                                "[Xi_c0 -> K+ Xi-]cc",
                                                [SharedDetachedLcChild_K],
                                                'Xic2XimKp',
                                                DetachedV0HCombiner
                                                )

        combiners["Xic2XimPipPip"] = makeXimDDDLLL('Comb',
                                                   "[Xi_c+ -> pi+ Xi- pi+]cc",
                                                   [SharedDetachedLcChild_pi],
                                                   'Xic2XimPipPip',
                                                   DetachedV0HHCombiner
                                                 )

        combiners["Lc2XimKpPip"] = makeXimDDDLLL('Comb',
                                                 "[Lambda_c+ ->  K+ Xi- pi+]cc",
                                                 [SharedDetachedLcChild_pi,SharedDetachedLcChild_K],
                                                 'Lc2XimKpPip',
                                                 DetachedV0HHCombiner
                                                 )

        combiners["Xic2XimPipPimPip"] = makeXimDDDLLL('Comb',
                                                      "[Xi_c0 -> pi+  Xi- pi+ pi-]cc",
                                                      [SharedDetachedLcChild_pi],
                                                      'Xic2XimPipPimPip',
                                                      DetachedV0HHHCombiner
                                                   )
        #Stages
        stages = {}
        lambdaModes = ["Xic2LambdaKmPip","Xic2LambdaKpKm","Xic2LambdaKmPipPip","Lc2LambdaPipPimPip"]
        ximModes    = ["Xic2XimPip","Xic2XimKp","Xic2XimPipPip","Lc2XimKpPip","Xic2XimPipPimPip"]

        for lambdaRec in ["DD","LL"]:
            for lambdaMode in lambdaModes:
                stages[lambdaMode+"_Lam"+lambdaRec] = [MassFilter(lambdaMode,inputs = combiners[lambdaMode][lambdaRec])]

        for ximRec in ["DDD","DDL","LLL"]:
            for ximMode in ximModes:
                stages[ximMode+"_Xim"+ximRec] = [MassFilter(ximMode,inputs = combiners[ximMode][ximRec])]

        return stages
