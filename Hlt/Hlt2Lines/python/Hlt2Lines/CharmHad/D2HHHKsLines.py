###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# -*- coding: utf-8 -*-
## @file
#  Set of Hlt2-lines suitable for the study of four-body charm decay channels
#  with KS (DD,LL) in the final state.
#
#  @author Maurizio MARTINELLI maurizio.martinelli@cern.ch
#  @author Simone STRACKA simone.stracka@cern.ch
#=============================================================================
""" Set of Hlt2-lines suitable for the study of four-body charm decay channels
    with KS (DD,LL) in the final state.
"""
#=============================================================================
__author__  = "Maurizio MARTINELLI maurizio.martinelli@cern.ch ; Simone STRACKA simone.stracka@cern.ch"

import copy

from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm, mrad
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser

class CharmHadD2HHHKsLines() :
    def localcuts(self) :
        ## The configuration dictionaries for the lines used to be generated
        ## in a loop in which keys/line names were assembled from components.
        ## This led to very compact code, but it also made it difficult to
        ## find things with simple search tools.
        ## Further, there seem to be only two variants of the dictionaries.
        ## Having one configuration dictionary per combiner does make it
        ## possible to apply different configurations for each of the lines
        ## in HltSettings.  However, it is not clear that it would be desirable
        ## to do so.  Until it is clear that we need different configurations
        ## for the combiners of the lines, use just two configuration
        ## dictionaries to configure all of the lines.  (P.S.)

        cutsForDp2HHHKs = {
            'Trk_ALL_PT_MIN'        :  300 * MeV,
            'Trk_ALL_MIPCHI2DV_MIN' :  3,
            'AM_34'                 : (139.5 + 139.5) * MeV,
            'AM_4'                  : (139.5) * MeV,
            'AM_MIN'                :  1755.0 * MeV,
            'AM_MAX'                :  2055.0 * MeV,
            'ADOCA_MAX'             :  100.0 * mm,
            'ACHI2DOCA_MAX'         :  10.0,
            'ASUMPT_MIN'            :  2.0 * GeV,
            'VCHI2PDOF_MAX'         :  12.0,
            'BPVLTIME_MIN'          :  0.2 * picosecond,
            'acosBPVDIRA_MAX'       :  10.0 * mrad,
            'BPVIPCHI2_MAX'         :  20.0,
            'Mass_M_MIN'            :  1795.0 * MeV,
            'Mass_M_MAX'            :  1935.0 * MeV,
            'TisTosSpec'            :  "Hlt1.*Track.*Decision%TOS",
            'AMOM_MIN'              :  25000 * MeV,
            'DPT_MIN'               :  2000 * MeV,
            'DMOM_MIN'              :  30000 * MeV,
            'BPVVDCHI2_MIN'         :  25,
            }
        # Use the previously defined selection cuts as a baseline and update some of them
        # For the Ds, adapt the mass window
        cutsForDsp2HHHKs = copy.deepcopy(cutsForDp2HHHKs)
        cutsForDsp2HHHKs.update({
            'Mass_M_MIN'            : 1895.0 * MeV,
            'Mass_M_MAX'            : 2035.0 * MeV,
            })
        # For the Hc, adapt the mass window, lifetime
        cutsForHc2HHHLam = copy.deepcopy(cutsForDp2HHHKs)
        cutsForHc2HHHLam.update({
            'AM_MIN'                : 2186.0 * MeV,
            'AM_MAX'                : 2586.0 * MeV,
            'Mass_M_MIN'            : 2206.0 * MeV,
            'Mass_M_MAX'            : 2348.0 * MeV,
            'BPVLTIME_MIN'          : 0.1*picosecond,
            'acosBPVDIRA_MAX'       : 30.0 * mrad,
            'ASUMPT_MIN'            : 1800.0 * MeV,
            })
        # For the Hc, adapt the mass window, lifetime
        cutsForHc2HHpKs = copy.deepcopy(cutsForDp2HHHKs)
        cutsForHc2HHpKs.update({
            'AM_MIN'                : 2186.0 * MeV,
            'AM_MAX'                : 2586.0 * MeV,
            'Mass_M_MIN'            : 2206.0 * MeV,
            'Mass_M_MAX'            : 2348.0 * MeV,
            'BPVLTIME_MIN'          : 0.1*picosecond,
            'acosBPVDIRA_MAX'       : 30.0 * mrad,
            'ASUMPT_MIN'            : 1800.0 * MeV,
            })

        cuts = {   'Dp2HHHKs'  : cutsForDp2HHHKs
                 , 'Dsp2HHHKs' : cutsForDsp2HHHKs
                 , 'Hc2HHHLam' : cutsForHc2HHHLam
                 , 'Hc2HHpKs'  : cutsForHc2HHpKs
               }

        return cuts

    def locallines(self):
        from Stages import MassFilter
        from Stages import CharmHadSharedKsLL, CharmHadSharedKsDD
        from Stages import CharmHadSharedSecondaryPIDLambdaLL, CharmHadSharedSecondaryPIDLambdaDD
        from Stages import SharedDetachedDpmChild_K, SharedDetachedDpmChild_pi
        from Stages import SharedDetachedLcChild_K, SharedDetachedLcChild_pi, SharedDetachedLcChild_p
        #from Stages import LcpToLambda0KmKpPip, LcpToLambda0KmPipPip
        from Stages import DV4BCombiner

        ## For tracibility, i want the combiners explictly made.
        ## Not made as shared instances, so line names will be inserted
        ## into the combiner names automatically.
        DpToKS0KmPipPip_KS0LL = DV4BCombiner( 'Comb'
                , decay = [ '[D+ -> K- pi+ pi+ KS0]cc' ]
                , inputs = [ SharedDetachedDpmChild_pi
                             , SharedDetachedDpmChild_K
                             , CharmHadSharedKsLL ]
                , shared = False
                , nickname = 'Dp2HHHKs' )

        DpToKS0KpPimPip_KS0LL = DpToKS0KmPipPip_KS0LL.clone( 'Comb'
                , decay = [ '[D+ -> K+ pi+ pi- KS0]cc' ] )

        DpToKS0KmKpPip_KS0LL = DpToKS0KmPipPip_KS0LL.clone( 'Comb'
                , decay =  [ '[D+ -> K- K+ pi+ KS0]cc' ] )

        DpToKS0KpKpPim_KS0LL = DpToKS0KmPipPip_KS0LL.clone( 'Comb'
                , decay =  [ '[D+ -> K+ K+ pi- KS0]cc' ] )

        DpToKS0PimPipPip_KS0LL = DpToKS0KmPipPip_KS0LL.clone( 'Comb'
                , decay = [ '[D+ -> pi+ pi+ pi- KS0]cc' ]
                , inputs = [ SharedDetachedDpmChild_pi, CharmHadSharedKsLL ])

        ## Combiners with DD KS rather than LL KS.
        DpToKS0KmPipPip_KS0DD = DpToKS0KmPipPip_KS0LL.clone( 'Comb'
                , inputs = [ SharedDetachedDpmChild_pi
                             ,  SharedDetachedDpmChild_K
                             , CharmHadSharedKsDD ] )

        DpToKS0KpPimPip_KS0DD = DpToKS0KmPipPip_KS0DD.clone( 'Comb'
                , decay = [ '[D+ -> K+ pi+ pi- KS0]cc' ] )

        DpToKS0KmKpPip_KS0DD = DpToKS0KmPipPip_KS0DD.clone( 'Comb'
                , decay =  [ '[D+ -> K- K+ pi+ KS0]cc' ] )

        DpToKS0KpKpPim_KS0DD = DpToKS0KmPipPip_KS0DD.clone( 'Comb'
                , decay =  [ '[D+ -> K+ K+ pi- KS0]cc' ] )

        DpToKS0PimPipPip_KS0DD = DpToKS0KmPipPip_KS0DD.clone( 'Comb'
                , decay = [ '[D+ -> pi+ pi+ pi- KS0]cc' ]
                , inputs = [ SharedDetachedDpmChild_pi, CharmHadSharedKsDD ])


        ## Combiners for D_s+ with LL KS.
        DspToKS0KmPipPip_KS0LL = DV4BCombiner( 'Comb'
                , decay = [ '[D_s+ -> K- pi+ pi+ KS0]cc' ]
                , inputs = [ SharedDetachedDpmChild_pi
                             ,  SharedDetachedDpmChild_K
                             , CharmHadSharedKsLL ]
                , shared = False
                , nickname = 'Dsp2HHHKs' )

        DspToKS0KpPimPip_KS0LL = DspToKS0KmPipPip_KS0LL.clone( 'Comb'
                , decay = [ '[D_s+ -> K+ pi+ pi- KS0]cc' ] )

        DspToKS0KmKpPip_KS0LL = DspToKS0KmPipPip_KS0LL.clone( 'Comb'
                , decay =  [ '[D_s+ -> K- K+ pi+ KS0]cc' ] )

        DspToKS0KpKpPim_KS0LL = DspToKS0KmPipPip_KS0LL.clone( 'Comb'
                , decay =  [ '[D_s+ -> K+ K+ pi- KS0]cc' ] )

        DspToKS0PimPipPip_KS0LL = DspToKS0KmPipPip_KS0LL.clone( 'Comb'
                , decay = [ '[D_s+ -> pi+ pi+ pi- KS0]cc' ]
                , inputs = [ SharedDetachedDpmChild_pi, CharmHadSharedKsLL ])

        ## Combiners for D_s+ with DD KS.
        DspToKS0KmPipPip_KS0DD = DspToKS0KmPipPip_KS0LL.clone( 'Comb'
                , inputs = [ SharedDetachedDpmChild_pi
                             ,  SharedDetachedDpmChild_K
                             , CharmHadSharedKsDD ] )

        DspToKS0KpPimPip_KS0DD = DspToKS0KmPipPip_KS0DD.clone( 'Comb'
                , decay = [ '[D_s+ -> K+ pi+ pi- KS0]cc' ] )

        DspToKS0KmKpPip_KS0DD = DspToKS0KmPipPip_KS0DD.clone( 'Comb'
                , decay =  [ '[D_s+ -> K- K+ pi+ KS0]cc' ] )

        DspToKS0KpKpPim_KS0DD = DspToKS0KmPipPip_KS0DD.clone( 'Comb'
                , decay =  [ '[D_s+ -> K+ K+ pi- KS0]cc' ] )

        DspToKS0PimPipPip_KS0DD = DspToKS0KmPipPip_KS0DD.clone( 'Comb'
                , decay = [ '[D_s+ -> pi+ pi+ pi- KS0]cc' ]
                , inputs = [ SharedDetachedDpmChild_pi, CharmHadSharedKsDD ])


        ## Combiners for H_c+ with LL Lam.
        HcToLamKmPipPip_LamLL = DV4BCombiner( 'Comb'
                , decay = [ '[Lambda_c+ -> Lambda0 K- pi+ pi+]cc' ]
                , inputs = [   SharedDetachedLcChild_K
                             , SharedDetachedLcChild_pi
                             , CharmHadSharedSecondaryPIDLambdaLL ]
                , shared = False
                , nickname = 'Hc2HHHLam' )

        HcToLamKpPimPip_LamLL = HcToLamKmPipPip_LamLL.clone( 'Comb'
                , decay = [ '[Lambda_c+ -> Lambda0 K+ pi- pi+]cc' ] )

        HcToLamKmKpPip_LamLL = HcToLamKmPipPip_LamLL.clone( 'Comb'
                , decay =  [ '[Lambda_c+ -> Lambda0 K- K+ pi+]cc' ] )

        HcToLamKpKpPim_LamLL = HcToLamKmPipPip_LamLL.clone( 'Comb'
                , decay =  [ '[Lambda_c+ -> Lambda0 K+ K+ pi-]cc' ] )

        HcToLamPimPipPip_LamLL = HcToLamKmPipPip_LamLL.clone( 'Comb'
                , decay = [ '[Lambda_c+ -> Lambda0 pi+ pi+ pi-]cc' ]
                , inputs = [ SharedDetachedLcChild_pi, CharmHadSharedSecondaryPIDLambdaLL ])

        ## Combiners for H_c+ with DD Lam.
        HcToLamKmPipPip_LamDD = DV4BCombiner( 'Comb'
                , decay = [ '[Lambda_c+ -> Lambda0 K- pi+ pi+]cc' ]
                , inputs = [   SharedDetachedLcChild_K
                             , SharedDetachedLcChild_pi
                             , CharmHadSharedSecondaryPIDLambdaDD ]
                , shared = False
                , nickname = 'Hc2HHHLam' )

        HcToLamKpPimPip_LamDD = HcToLamKmPipPip_LamDD.clone( 'Comb'
                , decay = [ '[Lambda_c+ -> Lambda0 K+ pi- pi+]cc' ] )

        HcToLamKmKpPip_LamDD = HcToLamKmPipPip_LamDD.clone( 'Comb'
                , decay =  [ '[Lambda_c+ -> Lambda0 K- K+ pi+]cc' ] )

        HcToLamKpKpPim_LamDD = HcToLamKmPipPip_LamDD.clone( 'Comb'
                , decay =  [ '[Lambda_c+ -> Lambda0 K+ K+ pi-]cc' ] )

        HcToLamPimPipPip_LamDD = HcToLamKmPipPip_LamDD.clone( 'Comb'
                , decay = [ '[Lambda_c+ -> Lambda0 pi+ pi+ pi-]cc' ]
                , inputs = [ SharedDetachedLcChild_pi, CharmHadSharedSecondaryPIDLambdaDD ])


        ## Combiners for H_c+ with LL Lam.
        HcToKS0KmPipP_KS0LL = DV4BCombiner( 'Comb'
                , decay = [ '[Lambda_c+ -> KS0 K- pi+ p+]cc' ]
                , inputs = [   SharedDetachedLcChild_K
                             , SharedDetachedLcChild_pi
                             , SharedDetachedLcChild_p
                             , CharmHadSharedKsLL ]
                , shared = False
                , nickname = 'Hc2HHpKs' )

        HcToKS0KpPimP_KS0LL = HcToKS0KmPipP_KS0LL.clone( 'Comb'
                , decay = [ '[Lambda_c+ -> KS0 K+ pi- p+]cc' ] )

        HcToKS0KmKpP_KS0LL = HcToKS0KmPipP_KS0LL.clone( 'Comb'
                , decay =  [ '[Lambda_c+ -> KS0 K- K+ p+]cc' ] )

        HcToKS0PimPipP_KS0LL = HcToKS0KmPipP_KS0LL.clone( 'Comb'
                , decay = [ '[Lambda_c+ -> KS0 pi- pi+ p+]cc' ]
                , inputs = [ SharedDetachedLcChild_pi,
                             SharedDetachedLcChild_p,
                             CharmHadSharedKsLL ])

        ## Combiners for H_c+ with DD Lam.
        HcToKS0KmPipP_KS0DD = DV4BCombiner( 'Comb'
                , decay = [ '[Lambda_c+ -> KS0 K- pi+ p+]cc' ]
                , inputs = [   SharedDetachedLcChild_K
                             , SharedDetachedLcChild_pi
                             , SharedDetachedLcChild_p
                             , CharmHadSharedKsDD ]
                , shared = False
                , nickname = 'Hc2HHpKs' )

        HcToKS0KpPimP_KS0DD = HcToKS0KmPipP_KS0DD.clone( 'Comb'
                , decay = [ '[Lambda_c+ -> KS0 K+ pi- p+]cc' ] )

        HcToKS0KmKpP_KS0DD = HcToKS0KmPipP_KS0DD.clone( 'Comb'
                , decay =  [ '[Lambda_c+ -> KS0 K- K+ p+]cc' ] )

        HcToKS0PimPipP_KS0DD = HcToKS0KmPipP_KS0DD.clone( 'Comb'
                , decay = [ '[Lambda_c+ -> KS0 pi- pi+ p+]cc' ]
                , inputs = [ SharedDetachedLcChild_pi,
                             SharedDetachedLcChild_p,
                             CharmHadSharedKsDD ])


        ## The stages dictionary should be a clear two-column list from
        ##   which the lines defined in this module can be directly read.
        stages = {
                  'Dp2KS0KmPipPip_KS0LLTurbo'   : [ DpToKS0KmPipPip_KS0LL ]
                , 'Dp2KS0KpPimPip_KS0LLTurbo'   : [ DpToKS0KpPimPip_KS0LL ]
                , 'Dp2KS0KmKpPip_KS0LLTurbo'    : [ DpToKS0KmKpPip_KS0LL ]
                , 'Dp2KS0KpKpPim_KS0LLTurbo'    : [ DpToKS0KpKpPim_KS0LL ]
                , 'Dp2KS0PimPipPip_KS0LLTurbo'  : [ DpToKS0PimPipPip_KS0LL ]

                , 'Dp2KS0KmPipPip_KS0DDTurbo'   : [ DpToKS0KmPipPip_KS0DD ]
                , 'Dp2KS0KpPimPip_KS0DDTurbo'   : [ DpToKS0KpPimPip_KS0DD ]
                , 'Dp2KS0KmKpPip_KS0DDTurbo'    : [ DpToKS0KmKpPip_KS0DD ]
                , 'Dp2KS0KpKpPim_KS0DDTurbo'    : [ DpToKS0KpKpPim_KS0DD ]
                , 'Dp2KS0PimPipPip_KS0DDTurbo'  : [ DpToKS0PimPipPip_KS0DD ]

                , 'Dsp2KS0KmPipPip_KS0LLTurbo'  : [ DspToKS0KmPipPip_KS0LL ]
                , 'Dsp2KS0KpPimPip_KS0LLTurbo'  : [ DspToKS0KpPimPip_KS0LL ]
                , 'Dsp2KS0KmKpPip_KS0LLTurbo'   : [ DspToKS0KmKpPip_KS0LL ]
                , 'Dsp2KS0KpKpPim_KS0LLTurbo'   : [ DspToKS0KpKpPim_KS0LL ]
                , 'Dsp2KS0PimPipPip_KS0LLTurbo' : [ DspToKS0PimPipPip_KS0LL ]

                , 'Dsp2KS0KmPipPip_KS0DDTurbo'  : [ DspToKS0KmPipPip_KS0DD ]
                , 'Dsp2KS0KpPimPip_KS0DDTurbo'  : [ DspToKS0KpPimPip_KS0DD ]
                , 'Dsp2KS0KmKpPip_KS0DDTurbo'   : [ DspToKS0KmKpPip_KS0DD ]
                , 'Dsp2KS0KpKpPim_KS0DDTurbo'   : [ DspToKS0KpKpPim_KS0DD ]
                , 'Dsp2KS0PimPipPip_KS0DDTurbo' : [ DspToKS0PimPipPip_KS0DD ]

                , 'HcToLamKmPipPip_LamLLTurbo'  : [ HcToLamKmPipPip_LamLL ]
                , 'HcToLamKpPimPip_LamLLTurbo'  : [ HcToLamKpPimPip_LamLL ]
                , 'HcToLamKmKpPip_LamLLTurbo'   : [ HcToLamKmKpPip_LamLL ]
                , 'HcToLamKpKpPim_LamLLTurbo'   : [ HcToLamKpKpPim_LamLL ]
                , 'HcToLamPimPipPip_LamLLTurbo' : [ HcToLamPimPipPip_LamLL ]

                , 'HcToLamKmPipPip_LamDDTurbo'  : [ HcToLamKmPipPip_LamDD ]
                , 'HcToLamKpPimPip_LamDDTurbo'  : [ HcToLamKpPimPip_LamDD ]
                , 'HcToLamKmKpPip_LamDDTurbo'   : [ HcToLamKmKpPip_LamDD ]
                , 'HcToLamKpKpPim_LamDDTurbo'   : [ HcToLamKpKpPim_LamDD ]
                , 'HcToLamPimPipPip_LamDDTurbo' : [ HcToLamPimPipPip_LamDD ]

                , 'HcToKS0KmPipP_KS0LLTurbo'    : [ HcToKS0KmPipP_KS0LL ]
                , 'HcToKS0KpPimP_KS0LLTurbo'    : [ HcToKS0KpPimP_KS0LL ]
                , 'HcToKS0KmKpP_KS0LLTurbo'     : [ HcToKS0KmKpP_KS0LL ]
                , 'HcToKS0PimPipP_KS0LLTurbo'  : [ HcToKS0PimPipP_KS0LL ]

                , 'HcToKS0KmPipP_KS0DDTurbo'    : [ HcToKS0KmPipP_KS0DD ]
                , 'HcToKS0KpPimP_KS0DDTurbo'    : [ HcToKS0KpPimP_KS0DD ]
                , 'HcToKS0KmKpP_KS0DDTurbo'     : [ HcToKS0KmKpP_KS0DD ]
                , 'HcToKS0PimPipP_KS0DDTurbo'  : [ HcToKS0PimPipP_KS0DD ]

        }

        return stages
