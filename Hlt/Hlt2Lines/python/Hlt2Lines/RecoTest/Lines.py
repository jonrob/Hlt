#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# -*- coding: utf-8 -*-
# =============================================================================
# @file   Hlt2RecoTest.py
# @author Sascha Stahl (sascha.stahl@cern.ch)
# @date   17.04.2015
# =============================================================================
"""Test for reconstruction for HLT2."""
import importlib
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser
from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Configurables import HltANNSvc

class RecoTestLines(Hlt2LinesConfigurableUser) :
    __slots__ = { 'Common'      : {'Pt'             : 0.0,
                                   'TrChi2Tight'    : 4,
                                   'MuPt'           : 500 * MeV,
                                   'VertexChi2'     : 25,
                                   'MassWindow'     : 70 * MeV},
                  'Postscale'   : {'Hlt2CreateReco' : 0.0},

                #   Define cut values for your Extra selections
                #   The name of the selections should match the one in extraAlgos()
                  'SelExtra'    : {'PT_daughter'    :   500 * MeV,
                                   'SumPT'          :   200 * MeV,
                                   'BPT'            :   200 * MeV},

                 'SelExtraTight': {'PT_daughter'    :     1 * GeV,
                                   'SumPT'          :   500 * MeV,
                                   'BPT'            :   500 * MeV},
                }



    """
    To add extra selections to your line:
    define extraAlgos() method, which returns dictionary
    with line nickname as a key and as a value:
    dictionary with extra selections and their nicknames,
    as shown below
    """
    def extraAlgos(self):
        from Stages import ExtraCombiner
        extraAlgos = { 'JPsiReFitPVsTurbo'  : { 'SelExtra'      : [ExtraCombiner('SelExtra')]
                                               ,'SelExtraTight' : [ExtraCombiner('SelExtraTight')]    } }
        return extraAlgos


    def __apply_configuration__(self) :
        from HltLine.HltLine import Hlt2Line
        from Stages import CreatePions, JpsiReFitPVsFilter
        stages = {
                'CreateReco'   : [ CreatePions("AllOfThem") ]
                ,'JPsiReFitPVsTurbo'   : [ JpsiReFitPVsFilter("JpsiReFitPVsFilter") ]
                }

        """
        Add extralgos in the loop:
        If extra selections are not defined for the line,
        then extralgos will hold a value of an empty dictionary.
        Otherwise, the extralgos will be a corresponding dictionary
        with extra selections, defined in extraAlgos().
        (no need to add explicit checks)

        In case RelatedInfo is defined for lines,
        extraAlgos should be added after it:
        for nickname, algos, relatedInfo, extralgos in self.algorithms(stages):
        """
        for nickname, algos, extralgos in self.algorithms(stages):
            if nickname is 'JPsiReFitPVsTurbo':
                HltANNSvc().Hlt2SelectionID.update( { "Hlt2JPsiReFitPVsTurboDecision":  57221 } )
                Hlt2Line(nickname, prescale = self.prescale,
                         algos = algos,
                         extraAlgos = extralgos, # don't forget to add extraAlgos to Hlt2Line
                         postscale = self.postscale, Turbo=True)
            else:
                Hlt2Line(nickname, priority = 1, prescale = self.prescale,
                         algos = algos, postscale = self.postscale)


        lines = {'Radiative': {'B2GammaGamma' : 50213}}
        for (module, lines) in lines.iteritems():
            # Import the other module that has the stages.
            mod = importlib.import_module('Hlt2Lines.' + module + '.Lines')
            # Instantiate the configurable
            conf = getattr(mod, module + 'Lines')()
            # Loop over the line, use the configurable to get the algorithms and
            # instantiate the turbo lines.
            for line, annId in lines.iteritems():
                stages = conf.stages(module + line)
                algos = dict(conf.algorithms({'RecoTest' : stages}))
                linename = module + line + 'Turbo'
                Hlt2Line(linename, prescale = self.prescale,
                         algos = algos['RecoTest'], postscale = self.postscale, Turbo = True)

                HltANNSvc().Hlt2SelectionID.update({'Hlt2'+ linename + 'Decision' : annId})
