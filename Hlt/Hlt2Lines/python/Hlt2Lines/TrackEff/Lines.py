###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__  = 'Mika Vesterinen'
__date__    = '$Date: 2015-02-23$'
__version__ = '$Revision: 0.0 $'

from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser

class TrackEffLines(Hlt2LinesConfigurableUser):
    __slots__ = {'Prescale' : {},
                 'TrackGEC' : {'NTRACK_MAX'           : 180},
                 'Common' : {'TisTosSpec'               : "Hlt1Track.*Decision%TOS"
                             , 'D0_MinAM' : 900.*MeV
                             , 'D0_MaxAM' : 2250.*MeV
                             
                             , 'D0_MinVDZ' : 3.5*mm
                             , 'D0_MinVDCHI2' : 25
                             
                             , 'D0_MaxLogVD': 3.5
                             
                             , 'D0_MaxOverlap': 0.9
                             , 'D0_MaxVCHI2NDF' : 3.5
                             , 'D0_MaxDeltaEta' : 1.25
                             
                             , 'D0_MaxDOCA' : 0.1 * mm
                             , 'D0_MaxDOCACHI2' : 5
                             
                             , 'D0_MinSimpleFitM' : 1865.-650. * MeV # Allow for the fit of the pi0 
                             , 'D0_MaxSimpleFitM' : 1865.+650. * MeV
                             
                             , 'Dst_MinAPT'  : 1750. *MeV
                             , 'Dst_MaxDTFM' : 2030. *MeV
                             , 'Dst_MaxDTFCHI2NDF' : 3.0
                             
                             , 'Dst_MaxSimpleFitDeltaMass' : 225. * MeV
                             
                             , 'TagK_MinPROBNNk' : 0.15
                             , 'TagPi_MinPROBNNpi' : 0.15
                             
                             , 'Slowpi_MinPt' : 110*MeV
                             
                             , 'D0_MCONST_MaxLogIPCHI2' : -1.0
                             , 'D0_MaxLogIPPerp' : -3
                             },
                 'D2KPi_TagPi': {'Tag_MinPT': 1300 * MeV
                                , 'Tag_MinP': 5 * GeV
                                , 'Tag_MinIPCHI2': 8
                                , 'Tag_MaxTrChi2': 3.5},
                 'D2KPi_TagK':{
                              'Tag_MinPT': 1300 * MeV
                             , 'Tag_MinP': 5 * GeV
                             , 'Tag_MinIPCHI2': 8
                             , 'Tag_MaxTrChi2': 3.5},
                 'D2K3Pi_TagK':{            
                               'Tag_MinPT' : 250. * MeV
                             , 'Tag_MinP' : 2. * GeV
                             , 'Tag_MinIPCHI2' : 6.
                             , 'Tag_MaxTrChi2' : 3.5},
                 'D2K3Pi_TagPi':{            
                               'Tag_MinPT' : 250. * MeV
                             , 'Tag_MinP' : 2. * GeV
                             , 'Tag_MinIPCHI2' : 6.
                             , 'Tag_MaxTrChi2' : 3.5},
                 'D2K3Pi_KStar': {'Kst_MinAPTTwoDaughters': 400 * MeV,
                                  'Kst_MinAPT': 2500. * MeV,
                                  'Kst_MaxVCHI2NDF': 4,
                                  'Kst_MAXCHILDMinIPCHI2': 8,
                                  'Kst_MAXCHILDMinPT': 1200, # HLT1
                                  'Kst_MinIPCHI2': 5,
                                  'Kst_MinAM': 900. * MeV,
                                  'Kst_MaxAM': 1800. * MeV
                            },
                 'D2K3Pi_D0ForFourBody': {
                               'D0_AMAXIPCHI2' : 7
                             , 'D0_MCONST_MaxLogIPCHI2' : -2.5
                             , 'D0_MaxLogIPPerp' : -3
                             , 'D0_MinVDZ' : 2.5*mm
                             },
                 'D2KPi_D0ForPionProbe': {
                               'D0_AMAXIPCHI2' : 12
                             , 'D0_MCONST_MaxLogIPCHI2' : -2.5
                             , 'D0_MaxLogIPPerp' : -3.0
                             , 'D0_MinVDCHI2' : 30
                             },
                 'D2KPi_D0ForKaonProbe': {
                               'D0_AMAXIPCHI2' : 12
                             , 'D0_MCONST_MaxLogIPCHI2' : -3.0
                             , 'D0_MaxLogIPPerp' : -3.0
                             , 'D0_MinVDCHI2' : 30
                             },
                 'Phi_TagK':{
                              'Tag_MinPT': 500 * MeV
                             , 'Tag_MinP': 5 * GeV
                             , 'Tag_MinIPCHI2': 8
                             , 'Tag_MaxTrChi2': 3},
                 'Ds_TagPi':{
                              'Tag_MinPT': 1500 * MeV
                             , 'Tag_MinP': 5 * GeV
                             , 'Tag_MinIPCHI2': 8
                             , 'Tag_MaxTrChi2': 3},
                 'Phi2KPi': {
                             'PHITAGS_AMAXIPCHI2': 14,
                             'Phi_MinVDZ': 2.5,
                             'VCHI2': 10,
                             'VDCHI2': 15,
                             'MAXCHILDMinPT': 1800*MeV,
                             'MAXCORRM': 2300,
                             'AMMIN': 700,
                             'AMMAX': 1600},
                 'Kstar2KK': {'KSTARTAGS_AMAXIPCHI2': 10,
                             'Kstar_MinVDZ': 2.5,
                             'VCHI2': 10,
                             'VDCHI2': 50,
                             'MAXCHILDMinPT': 1000*MeV},
                 'Ds2PhiPi_KaonProbe': {
                            'Ds_AMAXIPCHI2': 15,
                            'Ds_MaxDOCA': 0.15*mm,
                            'Ds_MaxVDZ': 300.0*mm,
                            'Ds_MaxVCHI2NDF': 6,
                            'Ds_MinVDCHI2': 40,
                            'Ds_MinVDZ': 3.5*mm,
                            'DIRA': 0.99,
                            'Ds_MinSimpleFitM': 1270,
                            'Ds_MaxSimpleFitM': 2570,
                            'Phi_M_PV_FitMin': 980,
                            'Phi_M_PV_FitMax': 1100,
                            'DELTA_MASS_MIN': 800,
                            'DELTA_MASS_MAX': 1100
                        },
                 "Ds2KStarK_PionProbe": {
                            'Ds_AMAXIPCHI2': 12,
                            'Ds_MaxDOCA': 0.1*mm,
                            'Ds_MaxVCHI2NDF': 10,
                            'Ds_MinVDCHI2': 50,
                            'Ds_MinVDZ': 3.0
                             },
                "D2KPi_PionProbe_LongCandidates":{
                            'MINOVERLAPPROBE': 0.2
                			},
                "Ds2PhiPi_KaonProbe_LongCandidates":{
                            'MINOVERLAPPROBE': 0.2,
                			}
                            }
    def __apply_configuration__(self):
        from HltLine.HltLine import Hlt2Line
        stages = self.stages();
        
        for (nickname, algos, extraAlgs) in self.algorithms(stages):
            Hlt2Line('TrackEff_' + nickname+'Turbo', 
                     prescale = self.prescale,
                     algos = algos, 
                     postscale = self.postscale,
                     extraAlgos = extraAlgs,
                     Turbo=True,
                     PersistReco=True)
            
    
    def extraAlgos(self):
        from Stages import ExtraProbeSelectionDstar
        
        from Stages import FilteredDstD0ToKpiPionProbe
        from Inputs import AllPions
        
        from Stages import ExtraProbeSelectionDs
        from Stages import FilteredDs2PhiPiKaonProbe
        from Inputs import AllKaons
        
        extraAlgos = { 
                  'D0ToKpiPionProbe' : 
                    { 'D0ToKpiPionProbe_LongProbeCandidates' : 
                     [ ExtraProbeSelectionDstar('D2KPi_PionProbe_LongCandidates', [ FilteredDstD0ToKpiPionProbe, AllPions ]) ]},
                      
                  'DsToPhiPiKaonProbe' : 
                    { 'DsToPhiPiKaonProbe_LongProbeCandidates' : 
                     [ ExtraProbeSelectionDs('Ds2PhiPi_KaonProbe_LongCandidates', [FilteredDs2PhiPiKaonProbe, AllKaons]) ]}
                      }

        return extraAlgos

    def stages(self, nickname = ""):
        if hasattr(self, '_stages') and self._stages:
          if nickname:
            return self._stages[nickname]
          else:
            return self._stages
        
        from Stages import FilteredDstD0ToKpiPionProbe
        from Stages import FilteredDstD0ToKpiKaonProbe
        from Stages import FilteredDstD0ToK3piPionProbe
        from Stages import FilteredDs2PhiPiKaonProbe

        self._stages = {'D0ToKpiPionProbe'  : [FilteredDstD0ToKpiPionProbe],
                  'D0ToKpiKaonProbe'  : [FilteredDstD0ToKpiKaonProbe],
                  'D0ToK3piPionProbe' : [FilteredDstD0ToK3piPionProbe],
                  'DsToPhiPiKaonProbe': [FilteredDs2PhiPiKaonProbe]}

    
        if nickname:
          return self._stages[nickname]
        else:
          return self._stages

            
