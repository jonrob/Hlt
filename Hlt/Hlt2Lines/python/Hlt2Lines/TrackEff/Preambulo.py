###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
__author__  = 'Mika Vesterinen'
__date__    = '$Date: 2015-02-23$'
__version__ = '$Revision: 0.0 $'

def D0Preambulo():
    return  [
        "from numpy import inner, cross",
        "InnerProduct = ((CHILD(PX,1)*CHILD(PX,2)+CHILD(PY,1)*CHILD(PY,2)+CHILD(PZ,1)*CHILD(PZ,2))/CHILD(P,2))",
        "MassConstraintMomentum = 0.5*((1864.8**2 - CHILD(M,1)**2 - CHILD(M,2)**2)/(CHILD(E,1)*math.sqrt(1+(CHILD(M,2)/20000.)**2)-InnerProduct))",
        "MassConstraintMomentum = 0.5*((1864.8**2 - CHILD(M,1)**2 - CHILD(M,2)**2)/(CHILD(E,1)*math.sqrt(1+(CHILD(M,2)/MassConstraintMomentum)**2)-InnerProduct))",
        "mass_constraint_d0_momentum_vector = [ CHILD(PX,1) + MassConstraintMomentum * CHILD(PX,2)/CHILD(P,2), CHILD(PY,1) + MassConstraintMomentum * CHILD(PY,2)/CHILD(P,2), CHILD(PZ,1)+ MassConstraintMomentum * CHILD(PZ,2)/CHILD(P,2)]",
        "mass_constraint_d0_momentum = math.sqrt( mass_constraint_d0_momentum_vector[0]**2+mass_constraint_d0_momentum_vector[1]**2+mass_constraint_d0_momentum_vector[2]**2)",
        "normalised_mass_constraint_d0_momentum_vector = [mass_constraint_d0_momentum_vector[i]/mass_constraint_d0_momentum for i in range (0,3)]",
        
        "D0_ENDVERTEX_POSITION = [VFASPF(VX), VFASPF(VY), VFASPF(VZ)]",
        "D0_PV_POSITION = [BPV(VX),BPV(VY),BPV(VZ)]",
        
        "LambdaFactor = - (inner([VFASPF(VX)-BPV(VX), VFASPF(VY)-BPV(VY), VFASPF(VZ)-BPV(VZ)], normalised_mass_constraint_d0_momentum_vector))",
        "D0_MASS_CONSTRAINT_IP_VECTOR = [(D0_ENDVERTEX_POSITION[i] + LambdaFactor * normalised_mass_constraint_d0_momentum_vector[i]) - D0_PV_POSITION[i] for i in range (0,3)]",
        "D0_MASS_CONSTRAINT_IP = math.sqrt(D0_MASS_CONSTRAINT_IP_VECTOR[0]**2+D0_MASS_CONSTRAINT_IP_VECTOR[1]**2+D0_MASS_CONSTRAINT_IP_VECTOR[2]**2)",
        
        "perp_vector = cross( [CHILD(PX,1), CHILD(PY,1), CHILD(PZ,1) ], [ CHILD(PX,2), CHILD(PY,2), CHILD(PZ,2) ] )",
        "perp_vector_norm = math.sqrt(perp_vector[0]**2+perp_vector[1]**2+perp_vector[2]**2)",
        "D0_IP_PERP = abs(inner( [VFASPF(VX)-BPV(VX), VFASPF(VY)-BPV(VY), VFASPF(VZ)-BPV(VZ)], perp_vector )/perp_vector_norm)"
        ]

def DstPreambulo():
    return  [
        'D0_M  = POINTINGMASS ( LoKi.Child.Selector(1), LoKi.Child.Selector(1,1), LoKi.Child.Selector(1,2) )',
        'Dst_M = POINTINGMASS ( LoKi.Child.Selector(1), LoKi.Child.Selector(1,1), LoKi.Child.Selector(1,2), LoKi.Child.Selector(2) )']

def DsPreambulo(TwoBodyMass=1019.4,
                ProbeM=493.67):
    return  [
        "from numpy import inner, cross",
        'Kaon_M = '+str(ProbeM), # in MeV
        
        'TagKaonMomentumVector    = [CHILD(CHILD(PX,1), 1), CHILD(CHILD(PY,1),1), CHILD(CHILD(PZ,1),1)]',
        'TagKaonEnergy            = CHILD(CHILD(E,1),1)',
        
        'TagPionMomentumVector    = [CHILD(CHILD(PX,2), 1), CHILD(CHILD(PY,2), 1), CHILD(CHILD(PZ,2), 1)]',
        'TagPionEnergy = CHILD(CHILD(E,2), 1)',
        
        'ProbeKaon    = [CHILD(PX,2), CHILD(PY,2), CHILD(PZ,2)]',
        'ProbeUnnormalised = ProbeKaon[:]',
        'ProbeKaon = [ProbeUnnormalised[i]/CHILD(P,2) for i in range (0,3)]',
        
        'TagPCosineTheta = inner(ProbeKaon, TagKaonMomentumVector)', # |p_tag| Cos(Theta) 
        "MassConstraintMomentum = 0.5*(("+str(TwoBodyMass)+"**2 - Kaon_M**2 - Kaon_M**2)/(CHILD(CHILD(E,1), 1)*math.sqrt(1+(Kaon_M/CHILD(CHILD(P,1),1))**2)-TagPCosineTheta))",
        "MassConstraintMomentum = 0.5*(("+str(TwoBodyMass)+"**2 - Kaon_M**2 - Kaon_M**2)/(CHILD(CHILD(E,1), 1)*math.sqrt(1+(Kaon_M/MassConstraintMomentum)**2)-TagPCosineTheta))",
        "MassConstraintMomentum = 0.5*(("+str(TwoBodyMass)+"**2 - Kaon_M**2 - Kaon_M**2)/(CHILD(CHILD(E,1), 1)*math.sqrt(1+(Kaon_M/MassConstraintMomentum)**2)-TagPCosineTheta))",
        
        'Phi_momentum = [ MassConstraintMomentum*ProbeKaon[i] + TagKaonMomentumVector[i] for i in range (0,3)]',
        'Phi_energy = TagKaonEnergy + math.sqrt(Kaon_M**2 + MassConstraintMomentum**2)',
        
        'DsMassFromConstraint = math.sqrt( (Phi_energy+TagPionEnergy)**2 - (Phi_momentum[0]+TagPionMomentumVector[0])**2 -(Phi_momentum[1]+TagPionMomentumVector[1])**2 - (Phi_momentum[2]+TagPionMomentumVector[2])**2)',
        
        # Now calculate the POINTING mass of the PHI from the Ds PV constraint
        'Flight = [VFASPF(VX)-BPV(VX),VFASPF(VY)-BPV(VY),VFASPF(VZ)-BPV(VZ)]',
        'TotalTagVector = [TagPionMomentumVector[i] + TagKaonMomentumVector[i] for i in range (3)]',
        'Tag_PERP = [TotalTagVector[1]*Flight[2]-TotalTagVector[2]*Flight[1],TotalTagVector[2]*Flight[0]-TotalTagVector[0]*Flight[2],TotalTagVector[0]*Flight[1]-TotalTagVector[1]*Flight[0]]',
        'Tag_P_PERP = math.sqrt(Tag_PERP[0]**2+Tag_PERP[1]**2+Tag_PERP[2]**2)',

        'Prb_PERP = [ProbeKaon[1]*Flight[2]-ProbeKaon[2]*Flight[1],ProbeKaon[2]*Flight[0]-ProbeKaon[0]*Flight[2],ProbeKaon[0]*Flight[1]-ProbeKaon[1]*Flight[0]]',
        'Prb_P_PERP = math.sqrt(Prb_PERP[0]**2+Prb_PERP[1]**2+Prb_PERP[2]**2)',
        'Prb_P_from_PV_constraint = [ProbeKaon[i]*(Tag_P_PERP/Prb_P_PERP) for i in range(3)]',
        'Prb_PT = CHILD(PT, 2)*(Tag_P_PERP/Prb_P_PERP)',
        'Prb_PABS_from_PV_constraint = math.sqrt(Prb_P_from_PV_constraint[0]**2+Prb_P_from_PV_constraint[1]**2+Prb_P_from_PV_constraint[2]**2)',

        'Phi_E_from_PV_constraint = math.sqrt(Kaon_M**2 + Prb_PABS_from_PV_constraint**2) + TagKaonEnergy',
        'Phi_PVector_from_PV_constraint = [Prb_P_from_PV_constraint[i] +  TagKaonMomentumVector[i] for i in range(3)]',
        'Phi_P2_from_PV_constraint = inner(Phi_PVector_from_PV_constraint,Phi_PVector_from_PV_constraint)',
        'Phi_M_from_PV_constraint = math.sqrt(Phi_E_from_PV_constraint**2 - Phi_P2_from_PV_constraint)',
        
        'D_E_from_PV_constraint = Phi_E_from_PV_constraint+TagPionEnergy',
        'D_P2_from_PV_constraint = (Phi_PVector_from_PV_constraint[0]+TagPionMomentumVector[0])**2 + (Phi_PVector_from_PV_constraint[1]+TagPionMomentumVector[1])**2 + (Phi_PVector_from_PV_constraint[2]+TagPionMomentumVector[2])**2',
        'Ds_M_from_PV_constraint = math.sqrt(D_E_from_PV_constraint**2 - D_P2_from_PV_constraint)',
        
]
