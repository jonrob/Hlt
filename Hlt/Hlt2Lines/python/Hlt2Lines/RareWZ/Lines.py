###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import math

from WZ2MesonLines import RareWZWZ2MesonLines

from Configurables import HltANNSvc
from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm, mrad
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser

WZ2MesonLines = RareWZWZ2MesonLines()

theseslots = {   
    'Prescale' : {}, 
    #'Hlt2RareWZVHighPTDspTurbo'                : 0.05},
    'PersistReco' : {
        'Hlt2RareWZKaonGammaTurbo'                 : True,
        'Hlt2RareWZPionGammaTurbo'                 : True,
        'Hlt2RareWZGammaGammaTurbo'                : True,
        'Hlt2RareWZPi0GammaTurbo'                  : True,
        'Hlt2RareWZPi0Pi0Turbo'                    : True,
        'Hlt2RareWZDiMuonGammaTurbo'               : True,
        'Hlt2RareWZDspGammaTurbo'                  : True,
        'Hlt2RareWZDpGammaTurbo'                   : True,
        'Hlt2RareWZD0GammaTurbo'                   : True,
        'Hlt2RareWZKstGammaTurbo'                  : True,
        'Hlt2RareWZKs0GammaTurbo'                  : True,
        'Hlt2RareWZRho0GammaTurbo'                 : True,
        'Hlt2RareWZPhiGammaTurbo'                  : True,
        'Hlt2RareWZJPsiDiJetsTurbo'                : True,
    },
    'Postscale' : {},
    'TrackGEC'  : { 'NTRACK_MAX'                   : 10000},
    'Common'    : {
        'TisTosSpec'                               : "Hlt1.*Track.*Decision%TOS",
        'Trk_ALL_TRCHI2DOF_MAX'                    : 3.0,
        'Trk_ALL_TRGHOSTPROB_MAX'                  : 0.4,
        'Trk_ALL_P_MIN'                            : 1000 * MeV,
        'VCHI2PDOF_MAX'                            : 10.0,
        ## jet configurations, copied from Jet package
        'D_TOS'                                    : 'Hlt1((Two)?Track(Muon)?MVA.*|TrackMuon)Decision%TOS',
        'D_MASS'                                   : 50*MeV,
        'GHOSTPROB'                                : 0.2,
        'DPHI'                                     : 0,
        'SV_VCHI2'                                 : 10,
        'SV_TRK_PT'                                : 500*MeV,
        'SV_TRK_IPCHI2'                            : 16,
        'SV_FDCHI2'                                : 25,
        'MU_PT'                                    : 1000*MeV,
        'MU_PROBNNMU'                              : 0.5,
        'JET_PT'                                   : 10*GeV,
    },
    # Particles for the "Detached" CPV lines
    'RareWZSharedChild_K' : {
        'Trk_ALL_PT_MIN'                           : 400 * MeV,
    },
    'RareWZSharedChild_Pi' : {
        'Trk_ALL_PT_MIN'                           : 400 * MeV,
    },
    'RareWZSharedChild_HighPTK' : {
        'Trk_ALL_PT_MIN'                           : 20000 * MeV,
    },
    'RareWZSharedChild_HighPTPi' : {
        'Trk_ALL_PT_MIN'                           : 20000 * MeV,
    },
    'RareWZSharedChild_Gamma' : {
        'Neut_ALL_PT_MIN'                          : 10000 * MeV,
},
    'RareWZSharedChild_HighPTGamma' : {
        'Neut_ALL_PT_MIN'                          : 15000 * MeV,
    },
    'RareWZSharedChild_Pi0' : {
        'Neut_ALL_PT_MIN'                          : 10000 * MeV,
        'Neut_ALL_ADMASS_MAX'                      : 60.0 * MeV
    },
    'RareWZSharedChild_HighPTPi0' : {
        'Neut_ALL_PT_MIN'                          : 15000 * MeV,
        'Neut_ALL_ADMASS_MAX'                      : 60.0 * MeV
    },
    'RareWZSharedChild_LoosePi0' : {
        'Neut_ALL_PT_MIN'                          : 500 * MeV,
        'Neut_ALL_ADMASS_MAX'                      : 60.0 * MeV
    },
    'RareWZSharedDiMuon' : {
        'MUONPT_MIN'                               : 700 * MeV,
        'VertexChi2'                               : 9.,
        'TRCHI2DOFTIGHT'                           : 3.,
        'AM_MIN1'                                  : 3030 * MeV,
        'AM_MAX1'                                  : 3150 * MeV,
        'AM_MIN2'                                  : 9000 * MeV,
        'AM_MAX2'                                  : 10800 * MeV,
        'PT_MIN'                                   : 15000 * MeV,
    },
    'RareWZSharedKs0': {
        'AM_MIN'                                   : 792 * MeV,
        'AM_MAX'                                   : 992 * MeV,
        'ASUMPT_MIN'                               : 1000. * MeV,
        'ADOCA_MAX'                                : 0.08 * mm,
        'VCHI2_MAX'                                : 10.0,
        'BPVVD_MIN'                                : -999.,
        'BPVCORRM_MAX'                             : 3500 * MeV,
        'BPVVDCHI2_MIN'                            : 25.0,
        'PT_MIN'                                   : 15000. * MeV,
    },
    'RareWZSharedRho0': {
        'AM_MIN'                                   : 670 * MeV,
        'AM_MAX'                                   : 870 * MeV,
        'ASUMPT_MIN'                               : 1000. * MeV,
        'ADOCA_MAX'                                : 0.08 * mm,
        'VCHI2_MAX'                                : 10.0,
        'BPVVD_MIN'                                : -999.,
        'BPVCORRM_MAX'                             : 3500 * MeV,
        'BPVVDCHI2_MIN'                            : 25.0,
        'PT_MIN'                                   : 15000. * MeV,
    },
    'RareWZSharedPhi': {
        'AM_MIN'                                   : 920 * MeV,
        'AM_MAX'                                   : 1120 * MeV,
        'ASUMPT_MIN'                               : 1000. * MeV,
        'ADOCA_MAX'                                : 0.08 * mm,
        'VCHI2_MAX'                                : 10.0,
        'BPVVD_MIN'                                : -999.,
        'BPVCORRM_MAX'                             : 3500. * MeV,
        'BPVVDCHI2_MIN'                            : 25.0,
        'PT_MIN'                                   : 15000. * MeV,
    },
    'RareWZSharedKst' : {
        'AM_MIN'                                   : 792 * MeV,
        'AM_MAX'                                   : 992 * MeV,
        'ASUMPT_MIN'                               : 1000. * MeV,
        'PT_MIN'                                   : 15000 * MeV,
        'Trk_ALL_PT_MIN'                           : 400. * MeV,
        'Comb_ACHI2DOCA_MAX'                       : 10.0,
        'VCHI2PDOF_MAX'                            : 10.0,
        'BPVCORRM_MAX'                             : 3500 * MeV,
        'acosBPVDIRA_MAX'                          : 100 * mrad,
        'BPVVDCHI2_MIN'                            : 20.0,
        'BPVLTIME_MIN'                             : 0.3 * picosecond,
        'TisTosSpec'                               : "Hlt1.*Track.*Decision%TOS"
    },
    'RareWZDiJets': {
        'Reco'                                     : 'TURBO',
        'JetPtMin'                                 : 5. * GeV,
        'JetInfo'                                  : False,
        'JetEcPath'                                : ''
    },
}

theseslots.update(WZ2MesonLines.localcuts())

class RareWZLines(Hlt2LinesConfigurableUser):
    from copy import deepcopy
    __slots__ = deepcopy(theseslots)
    __lines__ = {}

    def stages ( self , nickname = '' ) :

        if not self.__lines__ :
            self.__lines__.update(WZ2MesonLines.locallines())

        return self.__lines__[nickname] if nickname else self.__lines__

    def __apply_configuration__(self):

        _stages = self.stages()
        #_relatedInfo = self.relatedInfo()

        from HltLine.HltLine import Hlt2Line
        #>>><extraAlgs>for nickname, algos, relatedInfo, extraAlgs in self.algorithms( _stages ):
        for nickname, algos in self.algorithms( _stages ):
            doturbo = True if (nickname.find('Turbo') > -1) else False

            Hlt2Line('RareWZ' + nickname,
                     prescale = self.prescale,
                     algos = algos,
                     postscale = self.postscale,
                     Turbo = doturbo,
                     #extraAlgos = extraAlgs,
                     PersistReco = self.getProps()['PersistReco'].get('Hlt2RareWZ' + nickname, False))
                     #RelatedInfo = relatedInfo)

