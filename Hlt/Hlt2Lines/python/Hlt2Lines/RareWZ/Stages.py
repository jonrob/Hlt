###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## Each stage must specify its own inputs
from Hlt2Lines.Utilities.Hlt2Filter     import Hlt2VoidFilter
from Hlt2Lines.Utilities.Hlt2Combiner   import Hlt2Combiner
from Hlt2Lines.Utilities.Hlt2Filter     import Hlt2ParticleFilter
from Hlt2Lines.Utilities.Hlt2JetBuilder import Hlt2JetBuilder
from Configurables                      import DaVinci__N3BodyDecays
from Configurables                      import DaVinci__N4BodyDecays
from Configurables                      import DaVinci__N5BodyDecays

## ========================================================================= ##
## Global event cuts
## ========================================================================= ##
class TrackGEC(Hlt2VoidFilter):
    '''
    Cut on the number of reconstructed long tracks.
    Historically useful, may be obsolete.
    '''
    def __init__(self, name):
        from Configurables import LoKi__Hybrid__CoreFactory as Factory
        modules =  Factory().Modules
        for i in [ 'LoKiTrigger.decorators' ] :
            if i not in modules : modules.append(i)

        from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking as Hlt2LongTracking
        tracks = Hlt2LongTracking().hlt2PrepareTracks()
        code = ("CONTAINS('%s')" % tracks.outputSelection()) + " < %(NTRACK_MAX)s"
        Hlt2VoidFilter.__init__(self, 'RareWZ' + name, code, [tracks], shared = True, nickname = 'TrackGEC')


## ========================================================================= ##
## Filters for Input particles
## ========================================================================= ##
## ------------------------------------------------------------------------- ##
class InParticleFilter(Hlt2ParticleFilter) : # {
    '''
    Filter charged particles on TRCHI2DOF, TRGHOSTPROB, PT, P, and (optionally)
    a PID variable.  Does NOT filter on IP or IPCHI2.

    Always creates a shared instance of the filter.

    Configuration dictionaries must contain the following keys:
        'Trk_ALL_TRCHI2DOF_MAX'   :  upper limit on TRCHI2DOF
        'Trk_ALL_TRGHOSTPROB_MAX' :  upper limit on TRGHOSTPROB
        'Trk_ALL_PT_MIN'          :  lower limit on PT
        'Trk_ALL_P_MIN'           :  lower limit on P

    If filtering on a PID variable, the pidVar parameter must be set to the
    name of the PID functor on which to cut, and the configuration dictionary
    must contain the key 'PID_LIM' defining the one-sided limit for that
    variable.  By default the cut on the PID variable is a lower limit.
    It may be changed to an upper limit by setting the parameter
    pidULim = True.
    '''
    def __init__(self, name, inputs, pidVar = None, pidULim = False): # {
        cut = ("  (TRCHI2DOF < %(Trk_ALL_TRCHI2DOF_MAX)s )" +
               "& (TRGHOSTPROB < %(Trk_ALL_TRGHOSTPROB_MAX)s)" +
               "& (PT > %(Trk_ALL_PT_MIN)s)" +
               "& (P > %(Trk_ALL_P_MIN)s)" )

        Hlt2ParticleFilter.__init__(self, name, cut, inputs, shared = True)
    # }
# }


## Shared instances of InParticleFilter
## Names of shared particles  begin with RareWZ to avoid name conflicts
##   with other subdirectories.
## ------------------------------------------------------------------------- ##
from Inputs import Hlt2NoPIDsPions, Hlt2NoPIDsKaons, Hlt2Muons
SharedChild_k        = InParticleFilter( 'RareWZSharedChild_K',        [Hlt2NoPIDsKaons] )
SharedChild_pi       = InParticleFilter( 'RareWZSharedChild_Pi',       [Hlt2NoPIDsPions] )
SharedChild_HighPTk  = InParticleFilter( 'RareWZSharedChild_HighPTK',  [Hlt2NoPIDsKaons] )
SharedChild_HighPTpi = InParticleFilter( 'RareWZSharedChild_HighPTPi', [Hlt2NoPIDsPions] )

## ------------------------------------------------------------------------- ##
class NeutralInParticleFilter(Hlt2ParticleFilter) : # {
    '''
    Filter neutral particles on PT and (optionally) an ADMASS window centered
    on the database mass of a particle type identified in a constructor
    argument.
    class NeutralInParticleFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        cut = ("  (PT > %(Trk_ALL_PT_MIN)s)" )
    Configuration dictionaries must contain the following keys:
        'Neut_ALL_PT_MIN' :  lower limit on PT

    If filtering on a ADMASS window, the massPart parameter must be set to
    the name of the particle type (e.g. 'pi0'), and the configuration
    dictionary must contain the key 'Neut_ALL_ADMASS_MAX' defining the window
    half-width, which is the upper limit for ADMASS.
    '''
    def __init__(self, name, inputs, massPart = None) : # {
        cut = "(PT > %(Neut_ALL_PT_MIN)s)"

        if massPart : # {
            masscut = "(ADMASS('%s') < %%(Neut_ALL_ADMASS_MAX)s) & " % (massPart)
            cut = masscut + cut
        # }

        Hlt2ParticleFilter.__init__(self, name, cut, inputs, shared = True)
    # }
# }


## Shared instances of NeutralInParticleFilter
## Names of shared particles  begin with RareWZ to avoid name conflicts
##   with other subdirectories.
## ------------------------------------------------------------------------- ##
from Inputs import Hlt2NoPIDsPhotons, Hlt2AllPi0
SharedChild_Pi0         = NeutralInParticleFilter("RareWZSharedChild_Pi0",
                                                  [Hlt2AllPi0], 'pi0' )
SharedChild_HighPTPi0   = NeutralInParticleFilter("RareWZSharedChild_HighPTPi0",
                                                  [Hlt2AllPi0], 'pi0' )
SharedChild_LoosePi0    = NeutralInParticleFilter("RareWZSharedChild_LoosePi0",
                                                  [Hlt2AllPi0], 'pi0' )
SharedChild_Gamma       = NeutralInParticleFilter("RareWZSharedChild_Gamma",
                                                  [Hlt2NoPIDsPhotons] )
SharedChild_HighPTGamma = NeutralInParticleFilter("RareWZSharedChild_HighPTGamma",
                                                  [Hlt2NoPIDsPhotons] )

## ========================================================================= ##
## Filters for composite particles
## ========================================================================= ##

# Mass filter
## ------------------------------------------------------------------------- ##
## Ugh.  I do not think that i approve of this hack.  (P.S.)
def refit_pvs_kwargs(reFitPVs, kwargs) :
    if reFitPVs :
        kwargs.update({'ReFitPVs' : True,
                       'CloneFilteredParticles' : True})
    return kwargs

class MassFilter(Hlt2ParticleFilter):
    def __init__(self, name, inputs, nickname = None, shared = False, reFitPVs=False,
                 monitor = None, **kwargs):
        cut = "in_range( %(AM_MIN)s , M , %(AM_MAX)s )"
        nickname = name if nickname == None else nickname
        name     = name if not shared       else 'RareWZ%sMass' % name
        if monitor:
            args = (monitor, "%(AM_MIN)s", "%(AM_MAX)s")
            pre = ["massHisto = Gaudi.Histo1DDef('{0}_mass', {1}, {2}, int(({2} - {1})/2))".format(*args)]
            kwargs['Preambulo'] = kwargs.get('Preambulo', []) + pre
            kwargs['PostMonitor'] = "process(monitor(M, massHisto, '%s_mass')) >> ~EMPTY" % monitor
        Hlt2ParticleFilter.__init__(self, name, cut, inputs,
                                    nickname = nickname , shared = shared,
                                    **refit_pvs_kwargs(reFitPVs, kwargs))


## ========================================================================= ##
## 2-body Combiners
## ========================================================================= ##
class TagDecayWithNeutral(Hlt2Combiner) : # {
    """
    Generic 2-body combiner for adding a soft neutral particle to another particle
    candidate, e.g., adding a pi0 to a D0 to created a D*0 -> D0 pi0 candidate.
    It cuts on the Q-value (difference between the combination mass and
    the sum of the masses of the individal decay products) of the 2-body
    combination.

    Previous versions of this combiner cut on the difference between the mass
    of the combination and the mass of the first individual decay product.
    This is the usual Delta m cut for D* decays if the D is the first particle
    in the list of descendents.  This restriction meant that decay descriptors
    for TagDecay had to be written carefuilly.  It was a fragile system.
    The cut on the Q-value is symmetric with respect to the decay products
    and removes this restriction and its associated danger.

    Configuration dictionaries must contain the following keys:
        'AM_MIN'
        'AM_MAX'         : lower and upper limits of the mass difference
                             window at the CombinationCut level.
    """
    def __init__(self, name, decay, inputs, DaughtersCuts = { }, shared = False, nickname = None):
        cc =    ("in_range( %(AM_MIN)s, AM, %(AM_MAX)s )")
        mc =    ("in_range( %(AM_MIN)s,  M, %(AM_MAX)s )")

        ## Since this class allows freedom to externally specify DaughtersCuts,
        ##   we should add a dependence on the PV3D
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, decay, inputs,
                              dependencies = [TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              DaughtersCuts = DaughtersCuts,
                              CombinationCut = cc,
                              shared = shared,
                              nickname = nickname,
                              MotherCut = mc,
                              ParticleCombiners={'':'ParticleAdder'},
                              Preambulo = [])
# }


## Combiner class for D0 -> h h' decays with detachement cuts
## ------------------------------------------------------------------------- ##
class WZD02HHCombiner(Hlt2Combiner) : # {
    """
    Combiner for 3 basic track-based particles.  The 'WZ' in the class
    name indicates that it applies cuts to lifetime-biasing non-lifetime
    values like the MIPCHI2DV(PRIMARY) of the decay products and the
    BPVVDCHI2() of the fitted vertex.

    Configuration dictionaries must contain the following keys:
        ## P.S. -- If filtered input particles are used, i would prefer that
        ##         all filtering that applies to every charged input be done
        ##         in those filters to prevent inadvertent configuration
        ##         mismatches.
        'Trk_ALL_PT_MIN'        :  lower limit on PT for all inputs
        'Trk_ALL_P_MIN'         :  lower limit on P for all inputs

        'Comb_AM_MIN'
        'Comb_AM_MAX'       : lower and upper limits on AM in CombinationCut
        'Trk_Max_APT_MIN'   : lower limit on largest product PT (APT1 or APT2)
        'D0_PT_MIN'         : lower limit on APT in CombinationCut
        'Pair_AMINDOCA_MAX' : upper limit on AMINDOCA('') in CombinationCut
        'D0_VCHI2PDOF_MAX'  : upper limit on VFASPF(VCHI2PDOF) in MotherCut
        'D0_BPVVDCHI2_MIN'  : lower limit on BPVVDCHI2() in MotherCut
        'D0_acosBPVDIRA_MAX': upper limit on acos(BPVDIRA()) in MotherCut (rad)
        'TisTosSpec'        : configuration string of the Hlt1 TISTOS filter.
    """
    def __init__(self, name, decay, inputs, nickname = None, shared = False, **kwargs) : # {
        '''**kwargs can be anything accepted by the Hlt2Combiner constructor, eg, to enable PV refitting use
        ReFitPVs = True.'''
        dc = { }
        for child in ['pi+','K+','p+'] :
            dc[child] = "(PT > %(Trk_ALL_PT_MIN)s) & (P > %(Trk_ALL_P_MIN)s)"

        ## Assume that the wide mass range is wider than the narrow range.
        combcuts = "(in_range(%(AM_MIN)s,  AM, %(AM_MAX)s)) & (APT > %(PT_MIN)s) & (AMINDOCA('') < %(Pair_AMINDOCA_MAX)s )"

        parentcuts = "(VFASPF(VCHI2PDOF) < %(VCHI2PDOF_MAX)s) & BPVVALID() & (BPVVDCHI2()> %(BPVVDCHI2_MIN)s ) & (BPVDIRA() > lcldira )"

        pream = [
                "import math"
                , "lcldira = math.cos( %(acosBPVDIRA_MAX)s )"
        ] + kwargs.pop('Preambulo', [])

        from HltTracking.HltPVs import PV3D
        ## The name passed to the base class constructor determines the
        ##   configuration dictionary that is picked up.
        name = name if not shared else 'RareWZ' + name
        Hlt2Combiner.__init__( self, name, decay, inputs,
                               dependencies = [TrackGEC('TrackGEC'), PV3D('Hlt2')],
                               tistos = 'TisTosSpec',
                               nickname = nickname,
                               shared = shared,
                               DaughtersCuts = dc,
                               CombinationCut = combcuts,
                               MotherCut = parentcuts,
                               Preambulo = pream,
                               **kwargs)
    # }
# }

## Shared instances of WZD02HHCombiner
## ------------------------------------------------------------------------- ##
WZD02HH_D0ToKmPip = WZD02HHCombiner( 'WZD02HH_D0ToKmPip'
                                     , decay = "D0 -> K- pi+"       ## Only D0s to prevent duplication
                                     , inputs = [ SharedChild_k, SharedChild_pi ]
                                     , nickname = 'WZD02HH'            ## def in D02HHLines.py
                                     , shared = True
                                     , ReFitPVs = True
                                     , Preambulo = ["massHisto = Gaudi.Histo1DDef('D0_mass', 1543 * MeV, 2216 * MeV, 336)"]
                                     , MotherMonitor = "process(monitor(M, massHisto, 'D0_mass')) >> ~EMPTY")

## ========================================================================= ##
## 3-body Combiners
## ========================================================================= ##
# ------------------------------------------------------------------------- ##
class WZHHHCombiner(Hlt2Combiner) : # {
    """
    Combiner for 3 basic track-based particles.  The 'WZ' in the class
    name indicates that it applies cuts to lifetime-biasing non-lifetime
    values like the MIPCHI2DV(PRIMARY) of the decay products and the
    BPVVDCHI2() of the fitted vertex.

    Always creates a shared instance of the filter.

    Configuration dictionaries must contain the following keys:
        'Trk_1OF3_PT_MIN'
        'Trk_2OF3_PT_MIN'
        'Trk_ALL_PT_MIN'        : tiered lower limits on product PT.
                                  All 3 must pass the ALL threshold.
                                  At least 2 must pass the 2OF3 threshold.
                                  At least 1 must pass the 1OF3 threshold.
        'AM_MIN'
        'AM_MAX'                : lower and upper limits on AM in CombinationCut
        'ASUMPT_MIN'            : lower limit on APT1+APT2+APT3 in CombinationCut
        'VCHI2PDOF_MAX'         : upper limit on VFASPF(VCHI2PDOF) in MotherCut
        'acosBPVDIRA_MAX'       : upper limit on acos(BPVDIRA()) in MotherCut (rad)
        'BPVVDCHI2_MIN'         : lower limit on BPVVDCHI2() in MotherCut
        'BPVLTIME_MIN'          : lower limit on BPVLTIME() in MotherCut
        'TisTosSpec'            : configuration string of the Hlt1 TISTOS filter.
    """
    def __init__(self, name, decay, inputs, nickname = None) : # {
        dc =    {}
        for child in ['pi+','K+','p+','mu+'] :
            dc[child] = "(PT > %(Trk_ALL_PT_MIN)s)"
        cc =    ("(in_range( %(AM_MIN)s, AM, %(AM_MAX)s ))" +
                 " & ((APT1+APT2+APT3) > %(ASUMPT_MIN)s )" +
                 " & (AHASCHILD(PT > %(Trk_1OF3_PT_MIN)s))"+
                 " & (ANUM(PT > %(Trk_2OF3_PT_MIN)s) >= 2)"+
                 " & (APT > %(PT_MIN)s)")
                 
        ## P.S. -- Are cuts on both BPVVDCHI2() and BPVLTIME useful?
        mc =    ("(VFASPF(VCHI2PDOF) < %(VCHI2PDOF_MAX)s)" +
                 " & (BPVDIRA() > lcldira )" +
                 " & (BPVLTIME() > %(BPVLTIME_MIN)s )")

        pream = [
                "import math"
                , "lcldira = math.cos( %(acosBPVDIRA_MAX)s )"
        ]

        nickname = name if nickname == None else nickname
        from HltTracking.HltPVs import PV3D
        ## P.S. -- I want to remove the dependence on the GEC.
        ## This kind of automatic name-mangling breaks under cloning.
        Hlt2Combiner.__init__(self, "RareWZ" + name, decay, inputs,
                              nickname = nickname, dependencies = [TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              shared = True, tistos = 'TisTosSpec', DaughtersCuts = dc,
                              CombinationCut = cc, MotherCut = mc, Preambulo = pream)
    # }
# }

## Shared instances of WZHHHCombiner
## ------------------------------------------------------------------------- ##

## Main line D+ -> 3h combiners
WZD2HHH_DpToKmPipPip = WZHHHCombiner( 'WZD2HHH_DpToKmPipPip'
                                      , decay = "[D+ -> K- pi+ pi+]cc"
                                      , inputs = [ SharedChild_k, SharedChild_pi ]
                                      , nickname = 'WZDpm2HHH' )  ## 'D2HHH' defined in D2HHHLines.py
## Main line D_s+ -> 3h combiners
WZD2HHH_DspToKmKpPip = WZHHHCombiner( 'WZD2HHH_DspToKmKpPip'
                                      , decay = "[D_s+ -> K- K+ pi+]cc"
                                      , inputs = [ SharedChild_k, SharedChild_pi ]
                                    , nickname = 'WZDs2HHH' )  ## 'Ds2HHHLoose' defined in Lines.py

## ------------------------------------------------------------------------- ##
from Inputs import TrackFittedDiMuon
class DiMuonFilter(Hlt2ParticleFilter):
    def __init__(self, name):
        code = ("(((MM > %(AM_MIN1)s) & (MM <  %(AM_MAX1)s)) | ((MM > %(AM_MIN2)s) & (MM <  %(AM_MAX2)s)))"+
                " & (PT> %(PT_MIN)s)" +
                " & (MINTREE('mu-' == ABSID, PT) > %(MUONPT_MIN)s)" +
                " & (VFASPF(VCHI2PDOF)<%(VertexChi2)s )" +
                " & (MAXTREE('mu-' == ABSID, TRCHI2DOF) < %(TRCHI2DOFTIGHT)s)")
        inputs = [TrackFittedDiMuon]
        Hlt2ParticleFilter.__init__(self, name, code, inputs, shared = True)

## ------------------------------------------------------------------------- ##
##  Kst combiner
class WZHHChildCombiner(Hlt2Combiner):
    # combiner for 2-body displaced tracks to be used in multi-body D decays
    def __init__(self, name, decay, inputs, daucuts = {}, shared = False, nickname = None):
        dc = daucuts
        cc = ("(in_range( %(AM_MIN)s, AM, %(AM_MAX)s ))" +
              "&( (APT1+APT2) > %(ASUMPT_MIN)s )" +
              "&( ACUTDOCA( %(ADOCA_MAX)s,'' ) )" +
              "&( APT > %(PT_MIN)s)" +
              "&( AALLSAMEBPV(-1, -1, -1) )" )
        mc = (" ( VFASPF(VCHI2)<%(VCHI2_MAX)s ) " +
              "&( BPVVD > %(BPVVD_MIN)s )" +
              "&( BPVCORRM() < %(BPVCORRM_MAX)s )" +
              "&( BPVVDCHI2() > %(BPVVDCHI2_MIN)s )")

        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, decay, inputs, shared = shared,
                              dependencies = [TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              #tistos = 'TisTosSpec',
                              DaughtersCuts = dc, CombinationCut = cc,
                              nickname = nickname,
                              MotherCut = mc, Preambulo = [])

WZRare_Ks0 = WZHHChildCombiner('WZRare_Kst'
                                , decay=["[K*(892)0 -> pi+ K-]cc"]
                                , inputs = [SharedChild_pi,
                                            SharedChild_k]
                                , nickname = 'RareWZSharedKs0' ## def in D2HHPi0Lines.py
                                , shared = True)

WZRare_rho0 = WZHHChildCombiner('WZRare_rho'
                                , decay=["rho(770)0 -> pi+ pi-"]
                                , inputs = [SharedChild_pi]
                                , nickname = 'RareWZSharedRho0' ## def in D2HHPi0Lines.py
                                , shared = True)

WZRare_phi = WZHHChildCombiner('WZRare_phi'
                                , decay=["phi(1020) -> K+ K-"]
                                , inputs = [SharedChild_k]
                                , nickname = 'RareWZSharedPhi' ## def in D2HHPi0Lines.py
                                , shared = True)

## ------------------------------------------------------------------------- ##

class WZHHHChildCombiner(Hlt2Combiner):
    def __init__(self, name, decay, inputs, nickname = None) :
        dc =    {}
        for child in ['pi+','K+'] :
            dc[child] = "(PT > %(Trk_ALL_PT_MIN)s)"
        cc =    ("(in_range( %(AM_MIN)s, AM, %(AM_MAX)s ))" +
                 " & (APT > %(PT_MIN)s)" +
		 " & ( ACHI2DOCA(1,3) < %(Comb_ACHI2DOCA_MAX)s ) "+
		 " & ( ACHI2DOCA(2,3) < %(Comb_ACHI2DOCA_MAX)s ) "+
                 " & ((APT1+APT2+APT3) > %(ASUMPT_MIN)s )")
        #mc =    ("(VFASPF(VCHI2PDOF) < %(VCHI2PDOF_MAX)s)" +
        mc =    ("(CHI2VXNDOF < %(VCHI2PDOF_MAX)s)" +
                 " & (BPVDIRA() > lcldira )" +
                 " & (BPVVDCHI2() > %(BPVVDCHI2_MIN)s )" +
                 " & (BPVLTIME() > %(BPVLTIME_MIN)s )"+
                 " & (BPVCORRM() < %(BPVCORRM_MAX)s)")
        comb12 = "( ACHI2DOCA(1,2) < %(Comb_ACHI2DOCA_MAX)s ) "
        pream = [
                "import math"
                , "lcldira = math.cos( %(acosBPVDIRA_MAX)s )"
        ]
        from HltTracking.HltPVs import PV3D
        Hlt2Combiner.__init__(self, name, decay, inputs,
                              dependencies = [TrackGEC('TrackGEC'), PV3D('Hlt2')],
                              tistos = 'TisTosSpec', combiner = DaVinci__N3BodyDecays,
			      DaughtersCuts = dc, CombinationCut = cc,
                              MotherCut = mc, Combination12Cut = comb12, Preambulo = pream, nickname = nickname)


WZRare_KstHHH = WZHHHChildCombiner('HHHComb'
                                   , decay = ["[K*(892)+ -> K- pi+ pi+]cc"
                                              ,"[K*(892)+ -> K+ pi- pi+]cc"
                                              ,"[K*(892)+ -> K+ K- pi+]cc"
                                              ,"[K*(892)+ -> K+ K- K+]cc"
                                              ,"[K*(892)+ -> pi+ pi- pi+]cc"
                                              ,"[K*(892)+ -> K+ K+ pi-]cc"]
                                   , inputs = [SharedChild_pi, SharedChild_k]
                                   , nickname = 'RareWZSharedKst')

## ------------------------------------------------------------------------- ##
## -------------- DIjets 
from HltTracking.HltPVs import PV3D
class DiJetCombiner(Hlt2Combiner):
    def __init__(self, inputs, tag, pt):
        tags = {
            'SV':   (" & ((ACHILD(INFO(9600, -1), 1) != -1)"
                     " | (ACHILD(INFO(9600, -1), 2) != -1))"),
            'MV':   (" & (((ACHILD(INFO(9600, -1), 1) != -1)"
                     " & (ACHILD(INFO(9601, -1), 1) != -1))"
                     " | ((ACHILD(INFO(9601, -1), 2) != -1)"
                     " & (ACHILD(INFO(9600, -1), 2) != -1)))"),
            'SVSV': (" & (ACHILD(INFO(9600, -1), 1) != -1)"
                     " & (ACHILD(INFO(9600, -1), 2) != -1)"),
            'MuMu': (" & (ACHILD(INFO(9601, -1), 1) != -1)"
                     " & (ACHILD(INFO(9601, -1), 2) != -1)"),
            'SVMu': (" & (((ACHILD(INFO(9600, -1), 1) != -1)"
                     " & (ACHILD(INFO(9601, -1), 2) != -1))"
                     " | ((ACHILD(INFO(9601, -1), 1) != -1)"
                     " & (ACHILD(INFO(9600, -1), 2) != -1)))")}
        cc = ("(AMINCHILD(PT) > %(JET_PT)s)"
              " & (abs(ACHILD(PHI,1) - ACHILD(PHI,2)) > %(DPHI)s)")
        if tag in tags: cc += tags[tag]
        Hlt2Combiner.__init__(self, 'WZJetsDiJet' + tag + pt,
                              ["CLUSjet -> CELLjet CELLjet"], inputs,
                              dependencies = [PV3D('Hlt2')],
                              CombinationCut = cc, MotherCut = '(ALL)',
                              Preambulo = [],
                              ParticleCombiners = {'': 'ParticleAdder'})

class FilterMuon(Hlt2ParticleFilter):
    def __init__(self):
        inputs = [Hlt2Muons]
        code = ("(PT > %(MU_PT)s) &  (PROBNNmu > %(MU_PROBNNMU)s)"
                "& (TRGHOSTPROB < %(GHOSTPROB)s)")
        Hlt2ParticleFilter.__init__(self,'JetsMuon', code, inputs, 
                                    dependencies = [PV3D('Hlt2')])
class FilterSV(Hlt2ParticleFilter):
    def __init__(self, inputs):
        pid  = "((ABSID=='K+') | (ID=='KS0') | (ABSID=='Lambda0'))"
        code = ("(MINTREE(" + pid + ",PT) > %(SV_TRK_PT)s)"
                "& (MINTREE(ISBASIC,TRGHOSTPROB) < %(GHOSTPROB)s) "
                "& (MINTREE((ABSID=='K+'),MIPCHI2DV(PRIMARY)) > "
                "%(SV_TRK_IPCHI2)s)"
                "& (HASVERTEX) & (VFASPF(VCHI2PDOF) < %(SV_VCHI2)s)"
                "& (BPVVDCHI2() > %(SV_FDCHI2)s)")
        Hlt2ParticleFilter.__init__(self, 'JetsSV', code, inputs, 
                                    dependencies = [PV3D('Hlt2')])

## ------------------------------------------------------------------------- ##
