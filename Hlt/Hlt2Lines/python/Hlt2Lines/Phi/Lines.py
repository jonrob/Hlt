###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## @file
#  HLT2 lines for decays involving one or more phi mesons
#
#  @author Jon Harrison jonathan.harrison@manchester.ac.uk, Matthew Kenzie matthew.william.kenzie@cern.ch, stefanie.reichert@cern.ch and niklas.henrik.kitzmann@cern.ch for Phi2EE and Phi2MuMu lines
#=============================================================================
""" HLT2 lines for decays involving one or more phi mesons
	
"""
#=============================================================================
__author__  = "Jon Harrison jonathan.harrison@manchester.ac.uk, Matthew Kenzie matthew.william.kenzie@cern.ch"

from GaudiKernel.SystemOfUnits import GeV, MeV, mm

from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser
from HltLine.HltLine import Hlt2Line

class PhiLines(Hlt2LinesConfigurableUser) :

  m_phi = 1020

  __slots__ = {'_stages' : {},
               'Prescale' : {},
               'PromptPrescale' : 0.1,
               'Postscale' : {},
               'PhiIncPhi' : {'TisTosSpec' : "Hlt1IncPhi.*Decision%TOS" ,
                           'KaonPT'     : 800 * MeV ,
                           'KaonIPS'    : 6 ,
                           'KaonPID'    : 0 ,
                           'TrChi2DOF'  : 5,
                           'PhiPT'      : 1800 * MeV,
                           'PhiVCHI2'   : 20 ,
                           'PhiMassWin' : 20 * MeV},
               'PhiBs2PhiPhi': {'TisTosSpec' : "Hlt1B2PhiPhi_LTUNB.*Decision%TOS" ,
                            },
               'GoodKs' : {'KS_MASS_WINDOW'   : 20 * MeV,
                           'KS_PT_MIN'        : 400 * MeV,
                           'KS_VCHI2NDOF_MAX' : 4,
                           'GHOSTPROB_MAX'    :       0.35,
                           'KS_FD_MIN'        :      10.0 * mm},
               'PhiPhi2KsKs' : {'Phi_MASS_WINDOW'   : 70 * MeV,
                             'Phi_PT_MIN'        : 800 * MeV,
                             'Phi_DOCACHI2_MAX'  : 20,
                             'Phi_VCHI2NDOF_MAX' : 6},
               'PhiPhi2KsKsD0Ctrl' : {'D0_MASS_WINDOW'   : 70 * MeV,
                                   'D0_PT_MIN'        : 800 * MeV,
                                   'D0_DOCACHI2_MAX'  : 20,
                                   'D0_VCHI2NDOF_MAX' : 6},
               'PhiPhi2EETurbo' : {'CombMassHigh': (m_phi + 550) * MeV,
                 'CombMassLow': (m_phi - 850) * MeV,
                 'ElectronProbNn': 0.87,
                 'ElectronTrChi2DoF': 3,
                 'ElectronTrGhostProb': 0.15, 
                 'MassHigh': (m_phi + 500) * MeV,
                 'MassLow': (m_phi - 800) * MeV,
                 'VertexChi2DoF': 3,
                 'IPCHI2_Min': 16,
                 'BPVVDCHI2_Min': 144,
                 'NSPD': 340},
               'PhiPhi2MuMuTurbo' : {'CombMassHigh': (m_phi + 550) * MeV,
                 'CombMassLow': (m_phi - 850) * MeV,
                 'MuonProbNn': 0.87,
                 'MuonTrChi2DoF': 3,
                 'MuonTrGhostProb': 0.15, 
                 'MassHigh': (m_phi + 500) * MeV,
                 'MassLow': (m_phi - 800) * MeV,
                 'VertexChi2DoF': 3,
                 'IPCHI2_Min': 16,
                 'BPVVDCHI2_Min': 144,
                 'NSPD': 340},
               'PhiPromptPhi2EETurbo' : {'CombMassHigh': (m_phi + 550) * MeV,
                 'CombMassLow': (m_phi - 850) * MeV,
                 'ElectronProbNn': 0.97,
                 'ElectronTrChi2DoF': 3,
                 'ElectronTrGhostProb': 0.2, 
                 'MassHigh': (m_phi + 500) * MeV,
                 'MassLow': (m_phi - 800) * MeV,
                 'VertexChi2DoF': 3},
               'PromptSpdCut': {'NSPD': 250},
              }

  def stages(self, nickname = ""):
    if hasattr(self, '_stages') and self._stages:
      if nickname:
        return self._stages[nickname]
      else:
        return self._stages

    from Stages import IncPhiFilter, Bs2PhiPhiCombiner, Phi2KsKsCombiner, D02KsKsCombiner, Phi2EECombiner, Phi2MuMuCombiner, PromptPhi2EECombiner
    self._stages = {'PhiIncPhi'            : [IncPhiFilter('PhiIncPhi')],
                    'PhiBs2PhiPhi'         : [Bs2PhiPhiCombiner('PhiBs2PhiPhi')],
                    'PhiPhi2KsKs'          : [Phi2KsKsCombiner('PhiPhi2KsKs')],
                    'PhiPhi2KsKsD0Ctrl'    : [D02KsKsCombiner('PhiPhi2KsKsD0Ctrl')],
                    'PhiPhi2EETurbo'       : [Phi2EECombiner('PhiPhi2EETurbo')],
                    'PhiPhi2MuMuTurbo'     : [Phi2MuMuCombiner('PhiPhi2MuMuTurbo')],
                    'PhiPromptPhi2EETurbo' : [PromptPhi2EECombiner('PhiPromptPhi2EETurbo')]}

    if nickname:
      return self._stages[nickname]
    else:
      return self._stages

  def __apply_configuration__(self) :
    from HltLine.HltLine import Hlt2Line
    from Configurables import HltANNSvc
    stages = self.stages()
    for (nickname, algos) in self.algorithms(stages):
      linename = nickname
      if 'Phi2EE' in linename:
        if 'Prompt' in linename:
          Hlt2Line(linename,
                   L0DU="(L0_DATA('Spd(Mult)') < {})".format(self.getProp('PromptSpdCut')['NSPD']),
                   prescale = self.getProp('PromptPrescale'),
                   algos = algos,
                   postscale = self.postscale,
                   Turbo=True)
        else:
          Hlt2Line(linename,
                   L0DU="(L0_DATA('Spd(Mult)') < {})".format(self.getProp('PhiPhi2EETurbo')['NSPD']),
                   prescale = self.prescale,
                   algos = algos,
                   postscale = self.postscale,
                   Turbo=True)
      elif 'Phi2MuMu' in linename:
          Hlt2Line(linename,
                   L0DU="(L0_DATA('Spd(Mult)') < {})".format(self.getProp('PhiPhi2MuMuTurbo')['NSPD']),
                   prescale = self.prescale,
                   algos = algos,
                   postscale = self.postscale,
                   Turbo=True)
      else:
          Hlt2Line(linename, prescale = self.prescale, algos = algos, postscale = self.postscale)