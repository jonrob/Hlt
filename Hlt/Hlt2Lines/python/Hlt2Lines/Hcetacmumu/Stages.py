###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
###
#  @author R. Cardinale Roberta.Cardinale@cern.ch
#          M. Bartolini   Matteo.Bartolini@cern.ch
#  @date 2018-02-20
#
#  Please contact the abovementioned responsibles before editing this file
#
##

#Particle filters
from Hlt2Lines.Utilities.Hlt2Filter import Hlt2ParticleFilter
from Inputs import Hlt2Muons, Hlt2Protons



class MuonFilter(Hlt2ParticleFilter):
    def __init__(self, name):
                cut = "(TRCHI2DOF < %(TRACK_TRCHI2DOF_MAX)s)"
                inputs = [Hlt2Muons]
                Hlt2ParticleFilter.__init__(self,
                                            name,
                                            cut,
                                            inputs,
                                            shared= True)

MuonForHc = MuonFilter('MuonForHc')


# Combining tracks for hc-> (etac-> p pbar) mu mu 
from Hlt2Lines.Utilities.Hlt2Combiner import Hlt2Combiner


class Etac2PPCombiner(Hlt2Combiner):
    def __init__(self, name):

        DauCuts = ("%(DauCuts)s")
        ComCuts = ("%(ComCuts)s")
        MomCuts = ("%(MomCuts)s")
        
        from Inputs import Hlt2Protons
        inputs = [Hlt2Protons]
    
    
        # Combiner
        Hlt2Combiner.__init__(self, name, "eta_c(1S) -> p+ p~-",
                              inputs,
                              DaughtersCuts = {'p+': DauCuts,
                                               'p~-': DauCuts},
                              
                              CombinationCut = ComCuts, 
                              MotherCut = MomCuts,
                              Preambulo = ["massHisto = Gaudi.Histo1DDef('etacpp_mass', 2.8 * GeV, 3.2 * GeV, 100)"],
                              MotherMonitor = "process(monitor(M, massHisto, 'etacpp_mass')) >> ~EMPTY"
                              )


Etac2PP = Etac2PPCombiner('Etac2PP');


#####Etac2PhiPhi
class Etac2PhiPhiCombiner(Hlt2Combiner):
    def __init__(self, name):

        DauCuts = ("%(DauCuts)s")
        ComCuts = ("%(ComCuts)s")
        MomCuts = ("%(MomCuts)s")
        
        from Inputs import Hlt2WidePhi
        inputs = [Hlt2WidePhi]
    
    
        # Combiner
        Hlt2Combiner.__init__(self, name, "eta_c(1S) -> phi(1020) phi(1020)",
                              inputs,
                              DaughtersCuts = {'phi(1020)': DauCuts},
                              
                              CombinationCut = ComCuts, 
                              MotherCut = MomCuts,
                              Preambulo = ["massHisto = Gaudi.Histo1DDef('etacphiphi_mass', 2.8 * GeV, 3.2 * GeV, 100)"],
                              MotherMonitor = "process(monitor(M, massHisto, 'etacphiphi_mass')) >> ~EMPTY"
                              )
                   


Etac2PhiPhi = Etac2PhiPhiCombiner('Etac2PhiPhi');

class HcetacPPmumuCombiner(Hlt2Combiner):
    def __init__(self, name):

        HcDauCuts = ("%(HcDauCuts)s")
        HcComCuts = ("%(HcComCuts)s")
        HcMomCuts = ("%(HcMomCuts)s")
        
        
        inputs = [MuonForHc, Etac2PP ]
    
    
        # Combiner
        Hlt2Combiner.__init__(self, name, "h_c(1P) -> eta_c(1S) mu+ mu-",
                              inputs,
                              DaughtersCuts = {'mu+': HcDauCuts,
                                               'mu-': HcDauCuts},
                              
                              CombinationCut = HcComCuts, 
                              MotherCut = HcMomCuts,
                              Preambulo = ["massHisto = Gaudi.Histo1DDef('hcetacppmumu_mass', 3.3 * GeV, 3.7 * GeV, 100)"],
                              MotherMonitor = "process(monitor(M, massHisto, 'hcetacppmumu_mass')) >> ~EMPTY"
                              )


class HcetacPhiPhimumuCombiner(Hlt2Combiner):
    def __init__(self, name):

        HcPhiPhiDauCuts = ("%(HcPhiPhiDauCuts)s")
        HcPhiPhiComCuts = ("%(HcPhiPhiComCuts)s")
        HcPhiPhiMomCuts = ("%(HcPhiPhiMomCuts)s")
        
        
        inputs = [MuonForHc, Etac2PhiPhi ]
    
    
        # Combiner
        Hlt2Combiner.__init__(self, name, "h_c(1P) -> eta_c(1S) mu+ mu-",
                              inputs,
                              DaughtersCuts = {'mu+': HcPhiPhiDauCuts,
                                               'mu-': HcPhiPhiDauCuts},
                              
                              CombinationCut = HcPhiPhiComCuts, 
                              MotherCut = HcPhiPhiMomCuts,
                              Preambulo = ["massHisto = Gaudi.Histo1DDef('hcetacphiphimumu_mass', 3.3 * GeV, 3.7 * GeV, 100)"],
                              MotherMonitor = "process(monitor(M, massHisto, 'hcetacphiphimumu_mass')) >> ~EMPTY"
                              )

#Hcetacmumu = HcetacmumuCombiner('Hcetacmumu')

