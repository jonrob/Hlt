###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##
#  @author R. Cardinale Roberta.Cardinale@cern.ch
#          M. Bartolini  Matteo.Bartolini@cern.ch
#  @date 2018-02-28
#
#
#  Please contact the abovementioned responsibles before editing this file
#
##


from GaudiKernel.SystemOfUnits import GeV, MeV, picosecond, mm
from Hlt2Lines.Utilities.Hlt2LinesConfigurableUser import Hlt2LinesConfigurableUser

cuts= {'MuonForHc' : { 
    #'M_MIN'          :  200.0 * MeV
    #, 'M_MAX'          :  600.0 * MeV
    #, 'VCHI2PDOF_MAX'  :  25.0
    #,
    'TRACK_TRCHI2DOF_MAX' : 5
    , 'PT_MIN' : 0*MeV
    },  
       'Etac2PP' : {
    'DauCuts':  """
    (TRCHI2DOF < 3.0) & (PROBNNp > 0.6) &  (PT > 0.75*GeV) & (TRGHOSTPROB<0.2)
    """,
    'ComCuts': """
    in_range(2.7*GeV, AM, 3.3*GeV)
    & (APT > 0.8*GeV)
    """,
    'MomCuts': """
    (VFASPF(VCHI2PDOF) < 9)
    & (in_range(2.8*GeV, MM, 3.2*GeV))
    & (PT > 1.0*GeV)
    """
    
    },
       'Etac2PhiPhi' : {
    'DauCuts':  """
    (INTREE( ('K+'==ID) & (PT > 0.65*GeV)
    & (P > 1.*GeV) & (TRCHI2DOF < 3.) & (PIDK > 0.) & (TRGHOSTPROB<0.5)))
    & (INTREE( ('K-'==ID) & (PT > 0.65*GeV)
    & (P > 1.*GeV) & (TRCHI2DOF < 3.) & (PIDK > 0.) & (TRGHOSTPROB<0.5)))
    & (PT > 0.8*GeV)
    & (VFASPF(VCHI2PDOF) < 9)
    & (ADMASS('phi(1020)') < 30.*MeV)
    
    """,
    'ComCuts': """
    in_range(2.7*GeV, AM, 3.3*GeV)
    & (APT>0.0*GeV)
    """,
    'MomCuts': """
    (VFASPF(VCHI2PDOF) < 9)
    & (in_range(2.8*GeV, MM, 3.2*GeV))
    & (PT > 0.0*GeV)
    """
    
    },
       'HcetacPPmumu' : {
    'HcDauCuts'                :  """
    (INTREE( ('mu+'==ABSID) & (TRCHI2DOF < 5.0) & (TRGHOSTPROB<0.5) & ISMUON & (PIDmu>0.0)))
    
    """,
    'HcComCuts': """
    in_range(3.2*GeV, AM, 3.8*GeV)
    """,
    'HcMomCuts': """
    (VFASPF(VCHI2PDOF) < 9)
    & (in_range(3.3*GeV, MM, 3.7*GeV))
    """
    
    },
       'HcetacPhiPhimumu' : {
    'HcPhiPhiDauCuts'                :  """
    (INTREE( ('mu+'==ABSID) & (TRCHI2DOF < 5.0) & (TRGHOSTPROB<0.5)))
    """,
    'HcPhiPhiComCuts': """
    in_range(3.2*GeV, AM, 3.8*GeV)
    """,
    'HcPhiPhiMomCuts': """
    (VFASPF(VCHI2PDOF) < 9)
    & (in_range(3.3*GeV, MM, 3.7*GeV))
    """
    
    }
       }

class HcetacmumuLines(Hlt2LinesConfigurableUser):
    __slots__ = cuts
                 #'Prescale' : {
        #"Hlt2Hcetacmumu"    : 1.0
        #},
                 

    #def stages(self):
    #    if hasattr(self, '_stages') and self._stages:
    #        if nickname:
    #            return self._stages[nickname]
    #        else:
    #            return self._stages

            
        
        
    #    self._stages = { #'MuonForHc'      : ['MuonForHc'],
    #                     #'Etac2PP' : ['Etac2PP'],
    #                     'Hcetacmumu' : [HcetacmumuCombiner('Hcetacmumu')]
    #                     }
        #if nickname:
        #    return self._stages[nickname]
        #else:
        #    return self._stages

    def __apply_configuration__(self):

        from Stages import (MuonFilter, Etac2PPCombiner, Etac2PhiPhiCombiner, HcetacPPmumuCombiner, HcetacPhiPhimumuCombiner)
        HcetacPPmumu = HcetacPPmumuCombiner('HcetacPPmumu')
        HcetacPhiPhimumu = HcetacPhiPhimumuCombiner('HcetacPhiPhimumu')

        stages = {'HcetacPPmumu' : [HcetacPPmumu],
                  'HcetacPhiPhimumu' : [HcetacPhiPhimumu]
            }
        
        from HltLine.HltLine import Hlt2Line

        #stages = self.stages()

        for (linename, algos) in self.algorithms(stages):
            #linename = 'HcetacmumuA' 
            Hlt2Line(linename+'Turbo',
                     prescale = self.prescale,
                     algos = algos,
                     postscale = self.postscale,
                     Turbo = True)
