#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# -*- coding: utf-8 -*-
# =============================================================================
# @file   Inputs.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   05.03.2015
# =============================================================================
"""Import the needed inputs for the HLT2 radiative lines."""

from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedPhotons     as Hlt2Photons
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedRichPions   as Hlt2LoosePions
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedRichKaons   as Hlt2LooseKaons
from Hlt2SharedParticles.ConvPhoton import ConvPhotonLL as Hlt2DiElectron_LL
from Hlt2SharedParticles.ConvPhoton import ConvPhotonDD as Hlt2DiElectron_DD
from Hlt2SharedParticles.Lambda import LambdaLLTrackFitted as LambdaLL
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedDownKaons as Hlt2DownKaons
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedDownPions as Hlt2DownPions
from Hlt2SharedParticles.Lambda import LambdaDDTrackFitted as LambdaDD

# No PIDs
# from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedRichProtons as Hlt2LooseProtons
# from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedPions       as Hlt2NoPIDsPions
# from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedKaons       as Hlt2NoPIDsKaons
# from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedProtons     as Hlt2NoPIDsProtons

# EOF
