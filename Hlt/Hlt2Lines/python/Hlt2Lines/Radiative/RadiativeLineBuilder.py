#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# -*- coding: utf-8 -*-
# =============================================================================
# @file   RadiativeLineBuilder.py
# @author Albert Puig (albert.puig@cern.ch)
# @date   09.04.2015
# =============================================================================
"""Base class for radiative lines."""


class RadiativeLineBuilder(object):
    @staticmethod
    def get_stages(_):
        raise NotImplementedError()

    @staticmethod
    def get_cuts():
        raise NotImplementedError()

    @staticmethod
    def get_l0():
        return {}

    @staticmethod
    def get_hlt1():
        return {}


# EOF
