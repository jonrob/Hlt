#!/usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# -*- coding: utf-8 -*-
# =============================================================================
# @file   GammaGamma.py
# @author Sean Benson (sean.benson@cern.ch)
# @date   12.03.2015
# =============================================================================
"""Bs -> Gamma Gamma lines."""

from RadiativeLineBuilder import RadiativeLineBuilder


class B2GammaGammaLines(RadiativeLineBuilder):
    @staticmethod
    def get_stages(props):
        from Stages import TrackGEC
        from Stages import PhotonFilter
        from Stages import ConvPhotonLL, ConvPhotonDD, ConvPhotonAll
        from Stages import B2GammaGammaCombiner, FilterMVGammaGamma
        from HltTracking.HltPVs import PV3D

        # Make hard non-converted photons
        from Inputs import Hlt2Photons
        HardCALOGamma = PhotonFilter('HardCalo')

        # Build Bs -> gamma gamma (double conversion)
        bs2gammagammaDouble = B2GammaGammaCombiner('B2GammaGammaDouble',
                                                   'B_s0 -> gamma gamma',
                                                   [ConvPhotonAll()])
        MVFilter_Double = FilterMVGammaGamma('Double', [bs2gammagammaDouble], props['B2GammaGammaDouble'])

        # Build Bs -> gamma gamma (single conversion)
        bs2gammagammaLL = B2GammaGammaCombiner('B2GammaGammaLL',
                                               'B_s0 -> gamma gamma',
                                               [HardCALOGamma, ConvPhotonLL()])
        MVFilter_LL = FilterMVGammaGamma('LL', [bs2gammagammaLL], props['B2GammaGammaLL'])

        bs2gammagammaDD = B2GammaGammaCombiner('B2GammaGammaDD',
                                               'B_s0 -> gamma gamma',
                                               [HardCALOGamma, ConvPhotonDD()])
        MVFilter_DD = FilterMVGammaGamma('DD', [bs2gammagammaDD], props['B2GammaGammaDD'])

        # Build Bs -> gamma gamma (all calo)
        bs2gammagamma = B2GammaGammaCombiner('B2GammaGamma',
                                             'B_s0 -> gamma gamma',
                                             [HardCALOGamma])
        MVFilter_None = FilterMVGammaGamma('None', [bs2gammagamma], props['B2GammaGamma'])

        return {'RadiativeB2GammaGammaLL'     : [TrackGEC(), PV3D('Hlt2'), MVFilter_LL],
                'RadiativeB2GammaGammaDD'     : [TrackGEC(), PV3D('Hlt2'), MVFilter_DD],
                'RadiativeB2GammaGammaDouble' : [TrackGEC(), PV3D('Hlt2'), MVFilter_Double],
                'RadiativeB2GammaGamma'       : [TrackGEC(), PV3D('Hlt2'), MVFilter_None]}

        
    @staticmethod
    def get_cuts():
        from GaudiKernel.SystemOfUnits import MeV
        cuts = {}
        # Hard CALO photons
        cuts['HardCalo'] = {
                'P_MIN'    : 5000.0*MeV,
                'PT_MIN'   : 3000.0*MeV
                }
        # Bs
        cuts['B2GammaGamma'] = {
                'BsMin'     : 4300.0*MeV,
                'BsMax'     : 6800.0*MeV,
                'SUM_PT'    : 6000.0*MeV,
                'B_PT'      : 3000.0*MeV,
                'B_P'       : 5000.0*MeV,
                'MV_PARAMS': '$PARAMFILESROOT/data/keras_gammagammaNone.json',
                'MV_VARMAP': {'B_PT'         : "PT/MeV",
                    'minIsNotH'    : "MINTREE( (ABSID == 22) & ISBASIC, PPINFO(LHCb.ProtoParticle.IsNotH,1000))",
                    'maxIsNotH'    : "MAXTREE( (ABSID == 22) & ISBASIC, PPINFO(LHCb.ProtoParticle.IsNotH,1000))",
                    'minShowerShape': "MINTREE( (ABSID == 22) & ISBASIC, PPINFO(LHCb.ProtoParticle.ShowerShape,1000))",
                    "maxShowerShape": "MAXTREE( (ABSID == 22) & ISBASIC, PPINFO(LHCb.ProtoParticle.ShowerShape,1000))"},
                'MV_MIN'   : 1.0
                }
        cuts['B2GammaGammaLL'] = {
                'BsMin'     : 4000.0*MeV,
                'BsMax'     : 7000.0*MeV,
                'SUM_PT'    : 6000.0*MeV,
                'B_PT'      : 2500.0*MeV,
                'B_P'       : 4000.0*MeV,
                'MV_PARAMS': '$PARAMFILESROOT/data/keras_gammagammaLL.json',
                'MV_VARMAP': {'B_IPCHI2_wrtBPV'   : "BPVIPCHI2()",
                    'B_PT'  : "PT/MeV",
                    "Gamma1_CaloNeutralE49": "MAXTREE((ABSID==22) & ISBASIC, PPINFO(LHCb.ProtoParticle.CaloNeutralE49,1000))",
                    "Gamma1_IsNotH": "MAXTREE((ABSID == 22) & ISBASIC, PPINFO(LHCb.ProtoParticle.IsNotH,1000))",
                    "Gammaconv0_M"      : "MAXTREE((ABSID==22) & (NDAUGHTERS>0), M)",
                    "ptasym"   : "(MAXTREE((ABSID == 22),PT/MeV)-MINTREE((ABSID == 22),PT/MeV)) / (MAXTREE((ABSID == 22), PT/MeV)+MINTREE((ABSID == 22), PT/MeV))"},
                'MV_MIN'   : 1.0
                }
        cuts['B2GammaGammaDD'] = {
                'BsMin'     : 4200.0*MeV,
                'BsMax'     : 7000.0*MeV,
                'SUM_PT'    : 4000.0*MeV,
                'B_PT'      : 2500.0*MeV,
                'B_P'       : 4000.0*MeV,
                'MV_PARAMS': '$PARAMFILESROOT/data/keras_gammagammaDD.json',
                'MV_VARMAP': {'B_PT'  : "PT/MeV",
                    "Gamma1_CaloNeutralE49": "MAXTREE((ABSID==22) & ISBASIC, PPINFO(LHCb.ProtoParticle.CaloNeutralE49,1000))",
                    "Gamma1_IsNotH": "MAXTREE((ABSID == 22) & ISBASIC, PPINFO(LHCb.ProtoParticle.IsNotH,1000))",
                    "Gammaconv0_M"      : "MAXTREE((ABSID==22) & (NDAUGHTERS>0), M)",
                    "ptasym"   : "(MAXTREE((ABSID == 22),PT/MeV)-MINTREE((ABSID == 22),PT/MeV)) / (MAXTREE((ABSID == 22), PT/MeV)+MINTREE((ABSID == 22), PT/MeV))"},
                'MV_MIN'   : 1.0
                }
        cuts['B2GammaGammaDouble'] = {
                'BsMin'     : 4000.0*MeV,
                'BsMax'     : 7000.0*MeV,
                'SUM_PT'    : 2000.0*MeV,
                'B_PT'      : 1000.0*MeV,
                'B_P'       : 5000.0*MeV,
                'B_VTX'     : 25.0,
                'MV_PARAMS': '$PARAMFILESROOT/data/Hlt2B2GammaGamma_Double_v2.bbdt',
                'MV_VARMAP': {'BPT'    : "log( PT/MeV )",
                               'SUMPT'  : "log( SUMTREE(PT, (ABSID == 22), 0.0)/MeV)",
                               'PTASYM' : "(MAXTREE((ABSID == 22),PT/MeV)-MINTREE((ABSID == 22),PT/MeV)) / (MAXTREE((ABSID == 22), PT/MeV)+MINTREE((ABSID == 22), PT/MeV))",
                               'CONVM'  : "MAXTREE((ABSID==22) & (NDAUGHTERS>0), M)",
                               'CONVVTX': "MAXTREE((ABSID==22) & (NDAUGHTERS>0), VFASPF(VCHI2))",
                               'DLLE'   : "MINTREE((ABSID == 11),PPINFO(LHCb.ProtoParticle.CombDLLe,1000))"},
                'MV_MIN'   : 1.0
                }
        return cuts

    @staticmethod
    def get_hlt1():
        return {'RadiativeB2GammaGamma': "HLT_PASS_RE('Hlt1B2GammaGamma.*')"}

# EOF
