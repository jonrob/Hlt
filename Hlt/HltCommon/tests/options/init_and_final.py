###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# ============ Auto-generated init/finalize options file ======
from Gaudi.Configuration import *
import Configurables
from Configurables import LHCbApp
LHCbApp()

ApplicationMgr().EvtSel='NONE'
ApplicationMgr().EvtMax=0

from GaudiKernel.ConfigurableDb import cfgDb, loadConfigurableDb
loadConfigurableDb()
for name,conf in cfgDb.iteritems():
  if conf['package']=='HltCommon':
    if hasattr(Configurables,name):
      aConf=getattr(Configurables,name)
      if issubclass(aConf, ConfigurableAlgorithm):
        ApplicationMgr().TopAlg+=[aConf()]

