/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include <string>

// from Gaudi
#include "GaudiKernel/IIncidentSvc.h"
#include "GaudiKernel/Incident.h"
#include "GaudiAlg/GaudiAlgorithm.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltIncidentGenerator
//
//-----------------------------------------------------------------------------

class HltIncidentGenerator : public GaudiAlgorithm
{
  public:
    HltIncidentGenerator( const std::string& name, ISvcLocator* pSvcLocator );
    StatusCode initialize() override;
    StatusCode execute() override;

  private:
    std::string m_incident;
    SmartIF<IIncidentSvc> m_incidentSvc;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( HltIncidentGenerator )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
HltIncidentGenerator::HltIncidentGenerator( const std::string& name,
                                            ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator )
{
    declareProperty( "Incident", m_incident );
}

//=============================================================================
// Initialization
//=============================================================================
StatusCode HltIncidentGenerator::initialize()
{
    StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
    if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm

    m_incidentSvc = service( "IncidentSvc" );

    return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode HltIncidentGenerator::execute()
{
  if ( !m_incident.empty() )
    m_incidentSvc->fireIncident( Incident( name(), m_incident ) );
  return StatusCode::SUCCESS;
}
