/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HLTCOMMON_HLTL0MUONPREPARE_H
#define HLTCOMMON_HLTL0MUONPREPARE_H 1

// Include files
// from Gaudi
#include "HltBase/HltSelectionContainer.h"
#include "HltBase/HltAlgorithm.h"
#include "HltBase/IMuonSeedTool.h"
#include "Event/L0MuonCandidate.h"

/** @class HltHadAlleyPreTrigger HltHadAlleyPreTrigger.h
 *
 *
 *  @author Jose Angel Hernando Morata
 *  @date   2006-07-28
 */
class HltL0MuonCandidates : public HltAlgorithm
{
  public:
    /// Standard constructor
    HltL0MuonCandidates( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution

  private:
    // returns the ID of the extraInfo by name
    std::string hltInfoName( int id ) const;
    // print info of this container
    template <class CONT>
    void printInfo( const std::string& title, const CONT& cont ) const
    {
        info() << title << cont.size() << endmsg;
        for ( const auto& i : cont ) printInfo( title, *i );
    }

    // print info from track
    void printInfo( const std::string& title, const LHCb::Track& track ) const;

    // print info from vertex
    void printInfo( const std::string& title, const LHCb::RecVertex& ver ) const;

    // print info from extraInfo
    void printInfo( const std::string& title,
                    const GaudiUtils::VectorMap<int, double>& info ) const;


    std::vector<int> generateCutList( const LHCb::L0DUChannel& channel );

    Hlt::SelectionContainer<LHCb::Track, LHCb::L0MuonCandidate> m_selection { *this, { {1, "TES:" + LHCb::L0MuonCandidateLocation::Default}} };

    std::string m_l0Location;
    std::string m_l0Channel;
    IMuonSeedTool* m_maker = nullptr;
    AIDA::IHistogram1D* m_pt = nullptr;
    AIDA::IHistogram1D* m_ptMax = nullptr;

    bool checkClone( const LHCb::L0MuonCandidate* muon ) const;
};
#endif // HLTHADALLEYPRETRIGGER_H
