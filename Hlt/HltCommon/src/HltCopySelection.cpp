/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include <algorithm>
#include <iterator>
// from Gaudi
#include "HltCopySelection.h"

#include "Event/Particle.h"
#include "Event/Track.h"
#include "Event/RecVertex.h"
//-----------------------------------------------------------------------------
// Implementation file for class : HltCopySelection
//
// 2009-03-31 : Gerhard Raven
//-----------------------------------------------------------------------------

// explicit instantiation of class, and the corresponding
// declaration of the Algorithm Factory
template class HltCopySelection<LHCb::Particle>;
typedef HltCopySelection<LHCb::Particle> HltCopyParticleSelection;
DECLARE_COMPONENT_WITH_ID( HltCopyParticleSelection, "HltCopyParticleSelection" )

template class HltCopySelection<LHCb::Track>;
typedef HltCopySelection<LHCb::Track> HltCopyTrackSelection;
DECLARE_COMPONENT_WITH_ID( HltCopyTrackSelection, "HltCopyTrackSelection" )

template class HltCopySelection<LHCb::RecVertex>;
typedef HltCopySelection<LHCb::RecVertex> HltCopyRecVertexSelection;
DECLARE_COMPONENT_WITH_ID( HltCopyRecVertexSelection, "HltCopyRecVertexSelection" )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
template <typename T>
HltCopySelection<T>::HltCopySelection( const std::string& name,
                                       ISvcLocator* pSvcLocator )
    : HltAlgorithm( name, pSvcLocator ), m_selection( *this )
{
}

//=============================================================================
// Initialization
//=============================================================================
template <typename T>
StatusCode HltCopySelection<T>::initialize()
{
    StatusCode sc = HltAlgorithm::initialize(); // must be executed first
    if ( sc.isFailure() ) return sc; // error printed already by GaudiAlgorithm
    m_selection.retrieveSelections();
    m_selection.registerSelection();
    counter( "#input" );
    return sc;
}

//=============================================================================
// Main execution
//=============================================================================

template <typename T>
StatusCode HltCopySelection<T>::execute()
{
    counter( "#input" ) += m_selection.template input<1>()->size();
    m_selection.output()->insert( m_selection.output()->end(),
                                  m_selection.template input<1>()->begin(),
                                  m_selection.template input<1>()->end() );
    setFilterPassed( !m_selection.output()->empty() );
    return StatusCode::SUCCESS;
}
