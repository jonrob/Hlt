/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HLTOUTPUTSEQUENCE_H
#define HLTOUTPUTSEQUENCE_H 1

// Include files
// from Gaudi
#include <GaudiKernel/IIncidentListener.h>
#include <Gaudi/Sequence.h>
#include <GaudiAlg/GaudiCommon.h>
#include <GaudiAlg/GaudiHistos.h>
#include <GaudiAlg/ISequencerTimerTool.h>

/** @class HltOutputSequence HltOutputSequence.h
 *
 *
 *  @author Roel Aaij
 *  @date   2016-03-26
 */
class HltOutputSequence : public GaudiHistos<GaudiCommon<Gaudi::Sequence>> {
public:
   using base_class = GaudiHistos<GaudiCommon<Gaudi::Sequence>>;

   /// Standard constructor
   HltOutputSequence(const std::string& name, ISvcLocator* svcLocator);

   StatusCode initialize() override;
   StatusCode execute( const EventContext& ctx ) const override {
     return const_cast<HltOutputSequence*>( this )->execute( ctx );
   }
   StatusCode execute( const EventContext& ctx );

private:

   std::string m_writerType;
   bool m_outputPerGroup;
   bool m_ignoreFilters;
   std::map<std::string, std::vector<std::string>> m_forcedStreams;

   using PropMap = std::map<std::string, std::string>;
   PropMap m_writerProperties;
   PropMap m_streamProperties;
   PropMap m_groupProperties;

   // Get the member algorithms
   StatusCode decodeNames();

   class AlgorithmEntry final {
   public:

      AlgorithmEntry(Gaudi::Algorithm* alg, std::vector<SmartIF<IAlgorithm>>&& filters)
         : m_algorithm{alg}, m_filters{std::move(filters)} { }

      const std::vector<SmartIF<IAlgorithm>>& filters() const {
         return m_filters;
      }

      std::string type() const { return m_algorithm->type(); }
      Gaudi::Algorithm* algorithm() const { return m_algorithm; }

   private:

      Gaudi::Algorithm* m_algorithm = nullptr;
      std::vector<SmartIF<IAlgorithm>> m_filters;

   };

   std::vector<AlgorithmEntry> m_entries;

   std::unique_ptr<IIncidentListener> m_abortListener;
   bool m_abortEvent;

};
#endif // HLTOUTPUTSEQUENCE_H
