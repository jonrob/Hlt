/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HLTCOMMON_HLTCOPYSELECTION_H
#define HLTCOMMON_HLTCOPYSELECTION_H 1

// Include files
// from Gaudi
#include "HltBase/HltSelectionContainer.h"
#include "HltBase/HltAlgorithm.h"

/** @class HltCopySelection HltCopySelection.h
 *
 *  @author Gerhard Raven
 *  @date   2009-03-31
 */
template <typename T>
class HltCopySelection : public HltAlgorithm
{
  public:
    HltCopySelection( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution

  private:
    Hlt::SelectionContainer<T, T> m_selection;
};

#endif // HLTCOPYSELECTION_H
