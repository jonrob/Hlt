/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DICT_HLT1MUONSDICT_H 
#define DICT_HLT1MUONSDICT_H 1

#include <vector>

#include "Hlt1Muons/Candidate.h"
// #include "Hlt1Muons/Hlt1MuonHit.h"
#include "Hlt1Muons/IMatchVeloMuon.h"

namespace 
{
  struct _Instantiations 
  {
    // begin instantiations
    std::vector<Candidate*>         _i1;
    std::vector<const Candidate*>   _i2;
    // std::vector<Hlt1MuonHit*>       _i3;
    // std::vector<const Hlt1MuonHit*> _i4;
    // end instantiations
  };
}


#endif // DICT_HLT1MUONSDICT_H
