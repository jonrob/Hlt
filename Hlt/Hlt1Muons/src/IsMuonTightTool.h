/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef ISMUONTIGHTTOOL_H
#define ISMUONTIGHTTOOL_H

#include <string>
#include <vector>

#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/AnyDataHandle.h"
#include "MuonID/ICommonMuonTool.h"
#include "TrackInterfaces/ITracksFromTrack.h"

/** Tool for identifying muons in the HLT. Makes use of the CommonMuonTool.
 *
 * @author Francesco Dettori, Kevin Dungs
 * @date 2016-02-23
 */
class IsMuonTightTool : public GaudiTool, virtual public ITracksFromTrack {
 public:
  IsMuonTightTool(const std::string&, const std::string&, const IInterface*);
  ~IsMuonTightTool() override = default;
  StatusCode initialize() override;
  StatusCode tracksFromTrack(const LHCb::Track&,
                             std::vector<LHCb::Track*>&) const override;

 private:
  ICommonMuonTool* m_muonTool = nullptr;
  AnyDataHandle<MuonHitHandler> hitsHandlerGetter{
      MuonHitHandlerLocation::Default, Gaudi::DataHandle::Reader, this};
};

#endif  // ISMUONTIGHTTOOL_H
