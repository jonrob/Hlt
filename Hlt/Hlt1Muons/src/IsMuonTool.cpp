/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** Implementation of IsMuonTool.
 *
 * @author Kevin Dungs
 * @date 2015-01-06
 */
#include "IsMuonTool.h"

#include <string>
#include <vector>

#include "Event/Track.h"
#include "GaudiAlg/GaudiTool.h"
#include "MuonID/ICommonMuonTool.h"
#include "MuonID/ICommonMuonTool.h"
#include "TrackInterfaces/ITracksFromTrack.h"

DECLARE_COMPONENT( IsMuonTool )

/** C'tor
 * Also declare interface so tool can be obtained in Gaudi via tool<>().
 */
IsMuonTool::IsMuonTool(const std::string& type, const std::string& name,
                       const IInterface* parent)
    : GaudiTool(type, name, parent) {
  declareInterface<ITracksFromTrack>(this);
}

/** Load the CommonMuonTool.
 */
StatusCode IsMuonTool::initialize() {
  auto sc = GaudiTool::initialize();
  if (sc.isFailure()) {
    return sc;
  }
  m_muonTool = tool<ICommonMuonTool>("CommonMuonTool");
  return sc;
}

/** Implement signature specified by the ITracksFromTrack interface.
 * For a given track perform all necessary steps for muon id on it. Save an
 * output track if isMuon is true.
 */
StatusCode IsMuonTool::tracksFromTrack(const LHCb::Track& track,
                                       std::vector<LHCb::Track*>& tracks) const {
  if (!m_muonTool->preSelection(track.p())) {
    return StatusCode::SUCCESS;
  }
  const auto extrapolation = m_muonTool->extrapolateTrack(track.closestState(9450.0));
  if (!m_muonTool->inAcceptance(extrapolation)) {
    return StatusCode::SUCCESS;
  }
  CommonConstMuonHits hits;
  //std::array<unsigned, ICommonMuonTool::nStations> occupancies;
  ICommonMuonTool::MuonTrackOccupancies occupancies(0);
  const MuonHitHandler* hitHandler = hitsHandlerGetter.get();
  if (!hitHandler) {
      error() << "Can't load MuonHitHandler from TES" << endmsg;
      return StatusCode::FAILURE;
  }
  std::tie(hits, occupancies) =
      m_muonTool->hitsAndOccupancies(track.p(), extrapolation, *hitHandler);
  if (m_muonTool->isMuon(occupancies, track.p())) {
    // Add found hits to track
    LHCb::Track* output = new LHCb::Track(track);
    tracks.push_back(output);
    for (const auto& hit : hits) {
      output->addToLhcbIDs(LHCb::LHCbID{hit->tile()});
    }
  }
  return StatusCode::SUCCESS;
}
