/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HLT1MUONS_IMATCHVELOMUON_H 
#define HLT1MUONS_IMATCHVELOMUON_H 1

// Include files
// from STL
#include <string>
#include <vector>

// Interface
#include <TrackInterfaces/ITracksFromTrack.h>

class Candidate;

/** @class IMatchVeloMuon IMatchVeloMuon.h Hlt1Muons/IMatchVeloMuon.h
 *  
 *
 *  @author Roel Aaij
 *  @date   2014-06-04
 */
class GAUDI_API IMatchVeloMuon : virtual public extend_interfaces1<ITracksFromTrack> {
public: 
   // interface ID
   DeclareInterfaceID( IMatchVeloMuon, 2, 0 );

   virtual void findSeeds( const LHCb::Track& seed, const unsigned int seedStation ) = 0;

   virtual void addHits( Candidate& seed ) = 0;

   virtual void fitCandidate( Candidate& seed ) const = 0;

   virtual void clean() = 0;

   virtual const std::vector<Candidate>& seeds() const = 0;

};
#endif // HLT1MUONS_IMATCHVELOMUON_H
