###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
def Hlt1GECUnit( gec = 'Loose', accept = True ):
    gecs = {
        None    : { 'NumberFTUTClusters' : -1 },
       'Loose'  : { 'NumberFTUTClusters' : 11500 }
    }

    cuts = gecs.get( gec, gecs[None] )
    mode = { True : 'Accept', False : 'Reject' }[ accept ]
    name = "GEC%s%sUnit"%( mode, gec )

    from Configurables import PrGECFilter
    from TrackSys.RecoUpgradeTracking import DecodeTracking, GetSubDets
    decoders = DecodeTracking( GetSubDets() )
    gec_filter = PrGECFilter( name, **cuts )

    from HltLine.HltLine import bindMembers
    return bindMembers( None, decoders + [ gec_filter ] )
