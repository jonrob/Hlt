###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Copy and rename threshold settings.

Run this, preferably with a full project checkout, like so
    ./run python Hlt/HltSettings/scripts/copy_settings.py

"""
import os
import importlib
import inspect

# Main settings name
old_name = 'Physics_pp_2017'
new_name = 'Physics_pp_2018'
# Subdir suffix
old_suffix = '_pp_2017'
new_suffix = '_pp_2018'

# Main settings name
old_name = 'Physics_pp_2017'
new_name = 'Physics_pp_2018'
# Subdir suffix
old_suffix = '_pp_2017'
new_suffix = '_pp_2018'


def copy_settings(module, new_name):
    path = inspect.getsourcefile(module)
    dn, bn = os.path.split(path)
    name = os.path.splitext(bn)[0]
    new_path = os.path.join(dn, new_name + '.py')
    cmd = 'sed s/{}/{}/g <{} >{}'.format(name, new_name, path, new_path)
    print cmd
    os.system(cmd)


old_module = importlib.import_module('HltSettings.{}'.format(old_name))
copy_settings(old_module, new_name)

old_settings = getattr(old_module, old_name)()
for monthyear, subdirs in old_settings.SubDirs().iteritems():
    for subdir in subdirs:
        sub_module = importlib.import_module("HltSettings.{}.{}".format(subdir, subdir + old_suffix))
        copy_settings(sub_module, subdir + new_suffix)
