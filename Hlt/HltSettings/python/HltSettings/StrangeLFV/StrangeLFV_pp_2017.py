###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file   StrangeLFV_pp_2017.py
# @author Jessica Prisciandaro (jessica.prisciandaro@cern.ch)
# @date   22.04.2016
# =============================================================================
"""Threshold settings for Hlt2 DiElectron lines for 2016.
WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS 
"""

from GaudiKernel.SystemOfUnits import GeV, mm, MeV 

class StrangeLFV_pp_2017(object) :
    
    __all__ = ( 'ActiveHlt2Lines' )
    
    
    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        lines = [
            
            'Hlt2StrangeLFVMuonElectronSoft'
            ]
            
        return lines

   
    def Thresholds(self) :
        """
        Returns a dictionary of cuts
        """
        
        # keep pass through thresholds
        d = { }

        from Hlt2Lines.StrangeLFV.Lines     import StrangeLFVLines            
        d.update({StrangeLFVLines :
                      { 'MuonElectronSoft'   : { 'VDZ'         :        0,
                                                 'CosAngle'    : 0.999997,
                                                 'IpDzRatio'   :    0.015,
                                                 'SumGP'       :      0.4,
                                                 'Rho2'        :       36 * mm * mm,
                                                 'VertexChi2'  :       25,
                                                 'DOCA'        :      0.3 * mm,
                                                 'MaxMuGP'     :      0.3,
                                                 'MaxElGP'     :      0.3,
                                                 'MinMuIp'     :      0.2 * mm,
                                                 'MinElIp'     :      0.2 * mm,
                                                 'MinMuIpChi2' :        6,
                                                 'MinElIpChi2' :        6,
                                                 'MinProbNNe'  :      0.1,
                                                 'MinProbNNmu' :      0.1,
                                                 'MinPT'       :       80 * MeV,
                                                 'SVZ'         :      650 * mm,
                                                 'Mass'        :     1000 * MeV,
                                                 'Dira'        :        0
                                                 }
                        
                        }
                  })
        
        return d
    


