###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from VanDerMeerScan_2016 import VanDerMeerScan_2016
from Utilities.Utilities import update_thresholds


class VanDerMeerScan_pPb_2016(VanDerMeerScan_2016):
    """Settings for the pPb run in 2016.

    @author R. Matev
    @date 2016-11-03
    """

    def L0TCK(self):
        return '0x1622'

    def HltType(self):
        self.verifyType(VanDerMeerScan_pPb_2016)
        return 'VanDerMeerScan_pPb_2016'

    def ActiveHlt1Lines(self):
        """Return a list of active Hlt1 lines."""
        lines = super(VanDerMeerScan_pPb_2016, self).ActiveHlt1Lines()
        lines += [
            'Hlt1LumiSequencer',
        ]
        return lines

    def Thresholds(self):
        """Return a dictionary of cuts."""
        thresholds = super(VanDerMeerScan_pPb_2016, self).Thresholds()

        from Hlt1Lines.Hlt1BeamGasLines import Hlt1BeamGasLinesConf
        update_thresholds(thresholds, {
            Hlt1BeamGasLinesConf: {
                # Protect from busy events
                'UseGEC': 'HeavyIons',
                # Loosen bb vertex selection due to very low mu (0.01)
                'FullZVertexMinNTracks': 9,  # strictly greater than
            },
        })
        return thresholds
