###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Utilities.Utilities import update_thresholds


def __get_conf__(folder, suffix):
    import importlib
    conf = folder + suffix
    module = importlib.import_module("HltSettings.{0}.{1}".format(folder, conf))
    return getattr(module, conf)()


class XenonXenon_2017( object ):
    """Settings for Xenon-Xenon 2017 data taking, sqrt(s) = 5TeV, beam-beam physics.
    XeXe/PbPb: 24/24 colliding bunches, 5x bunch intensity


    @author P. Robbe, R. Matev, S. Stahl, Y. Zhang
    @date 2017-09-26
    """

    def __init__(self):
        # No TURBO => no aferburner
        from Configurables import HltConf
        HltConf().EnableHltAfterburner = False

        # Tighter GEC for the Hlt1TrackMonitor
        from Configurables import HltMonitoringConf
        HltMonitoringConf().Hlt1TrackMonitorGEC = 'HeavyIonsTight'

        from HltLine.HltDecodeRaw import DecodeVELO
        DecodeVELO.members()[0].MaxVeloClusters = 99999
        from DAQSys.Decoders import DecoderDB
        DecoderDB["DecodeVeloRawBuffer/createVeloClusters"].Properties["MaxVeloClusters"] = 99999

        from HltTracking.HltRecoConf import HltRecoConf
        HltRecoConf().Forward_MaxOTHits = 99999

        #from Hlt1Lines.Hlt1SharedParticles import Hlt1SharedParticles
        #Hlt1SharedParticles().PT = 500
        #Hlt1SharedParticles().PreFitPT = 475

        from HltConf.Hlt2 import Hlt2Conf
        Hlt2Conf().DefaultHlt1Filter = "HLT_PASS_RE('Hlt1(?!Lumi)(?!Velo)(?!BeamGas)(?!NoPV)(?!MB)(?!BBHighMult).*Decision')"

        # Set the lines for the data quality
        from HltConf.HltOutput import HltOutputConf
        HltOutputConf().Hlt2LinesForDQ = ['Hlt2PassThrough']

    def verifyType(self,ref) :
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if ( self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self)  or self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self) ) :
            raise RuntimeError( 'Must update HltType when modifying ActiveHlt.Lines()' )

    def L0TCK(self) :
        return '0x1723'

    def HltType(self) :
        self.verifyType( XenonXenon_2017 )
        return          'XenonXenon_2017'

    def SubDirs(self):
        #Load configurations
        return {'XeXe2017': ['Technical']}

    def Thresholds(self) :
        """
        Returns a dictionary of cuts
        """

        from Hlt1Lines.Hlt1MuonLines            import Hlt1MuonLinesConf  #for muons
        from Hlt1Lines.Hlt1MVALines             import Hlt1MVALinesConf   #for open charm?
        from Hlt1Lines.Hlt1IFTLines             import Hlt1IFTLinesConf   #MB and MB high multiplicity
        from Hlt1Lines.Hlt1MBLines              import Hlt1MBLinesConf    #Nobias
        from Hlt1Lines.Hlt1L0Lines              import Hlt1L0LinesConf  #

        from Hlt1Lines.Hlt1LumiLines            import Hlt1LumiLinesConf
        from Hlt1Lines.Hlt1CommissioningLines   import Hlt1CommissioningLinesConf
        from Hlt1Lines.Hlt1BeamGasLines         import Hlt1BeamGasLinesConf
        from Hlt1Lines.Hlt1CalibTrackingLines   import Hlt1CalibTrackingLinesConf
        from Hlt1Lines.Hlt1CalibRICHMirrorLines import Hlt1CalibRICHMirrorLinesConf

        thresholds = {
            Hlt1IFTLinesConf: {
                'ODIN': {
                    'BBNoBias': 'ODIN_PASS(LHCb.ODIN.NoBias)',
                },
                'MinVeloTracks': {
                    'BBMicroBiasVelo': 1,
                },
                'GEC': {
                    'BBMicroBiasVelo': 'HeavyIonsTight',
                    'BBHighMult':  'HeavyIonsTight',
                },
                'Prescale': {
                    'Hlt1BBMicroBiasVelo': 1.,   #TODO, apply a prescale if we have to
                    'Hlt1BBHighMult': 1,
                },
            },
            Hlt1L0LinesConf: {
                'L0Channels': ['Photon'],
                'Prescale': {
                    'Hlt1L0Any': 0.1,
                    'Hlt1L0Photon': 1,
                },
            },
            Hlt1LumiLinesConf : {
                'RecoGEC': 'HeavyIonsTight',
                'L0Channel': ['B1gas', 'B2gas'],
            },
            Hlt1BeamGasLinesConf: {
                # Global behaviour settings
                'TrackingConf'          : 'FastVelo',
                'FitTracks'             : True,
                'PVFitter'              : 'LSAdaptPV3DFitter',
                'PVSeeding'             : 'PVSeed3DTool',
                'SplitVertices'         : True,
                'CreateGlobalSelection' : False,
                'Turbo'                 : False,
                'UseGEC'                : 'Loose',

                # Minimum number of tracks for the produced vertices (#tr/vtx > X)
                'VertexMinNTracks'          : 9,  # strictly greater than
                'FullZVertexMinNTracks'     : 9,  # strictly greater than
                'Beam1VtxMaxBwdTracks'      : -1,  # less or equal than, negative to switch off
                'Beam2VtxMaxFwdTracks'      : -1,  # less or equal than, negative to switch off

                # z-ranges for Vertexing
                'Beam1VtxRangeLow'        : -2000.,
                'Beam1VtxRangeUp'         :  2000.,
                'Beam2VtxRangeLow'        : -2000.,
                'Beam2VtxRangeUp'         :  2000.,
                # Luminous region exclusion range
                'BGVtxExclRangeMin'       :  -250.,
                'BGVtxExclRangeMax'       :   250.,

                'L0Filter' : {
                    'BeamGasNoBeamBeam1' : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasNoBeamBeam2' : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasBeam1'       : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasBeam2'       : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasCrossingForcedReco': "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasCrossingForcedRecoFullZ': "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                },

                'Prescale' : {
                    'Hlt1BeamGasNoBeamBeam1'             : 1.,
                    'Hlt1BeamGasNoBeamBeam2'             : 1.,
                    'Hlt1BeamGasBeam1'                   : 1.,
                    'Hlt1BeamGasBeam2'                   : 1.,
                    'Hlt1BeamGasCrossingForcedReco'      : 1.,
                    'Hlt1BeamGasCrossingForcedRecoFullZ' : 1.,
                },
                'Postscale' : {
                    'Hlt1BeamGasNoBeamBeam1'             : 1.,
                    'Hlt1BeamGasNoBeamBeam2'             : 1.,
                    'Hlt1BeamGasBeam1'                   : 1.,
                    'Hlt1BeamGasBeam2'                   : 1.,
                    'Hlt1BeamGasCrossingForcedReco'      : 1.,
                    'Hlt1BeamGasCrossingForcedRecoFullZ' : 1.,
                },
            }

                , Hlt1MuonLinesConf :     { 
                        'SingleMuonHighPT_P'         : 3000
                        , 'SingleMuonHighPT_PT'      : 500
                        , 'SingleMuonHighPT_TrChi2'  : 9.
                        , 'SingleMuonHighPT_TrGP'    : 999.
                        , 'SingleMuonHighPT_GEC'     : 'HeavyIonsTight'

                        , 'DiMuonHighMass_VxDOCA'    : 1.
                        , 'DiMuonHighMass_VxChi2'    : 25
                        , 'DiMuonHighMass_P'         : 3000
                        , 'DiMuonHighMass_PT'        : 300
                        , 'DiMuonHighMass_TrGP'      : 999.  
                        , 'DiMuonHighMass_TrChi2'    : 9.
                        , 'DiMuonHighMass_M'         : 0   #take everything?
                        , 'DiMuonHighMass_GEC'       : 'HeavyIonsTight'
                        , 'DiMuonHighMass_CFTracking': False 

                        , 'CalibMuonAlignJpsi_ParticlePT'             : 800     # MeV
                        , 'CalibMuonAlignJpsi_ParticleP'              : 6000    # MeV
                        , 'CalibMuonAlignJpsi_TrackCHI2DOF'           : 2       # dimensionless
                        , 'CalibMuonAlignJpsi_CombMaxDaughtPT'        : 800     # MeV
                        , 'CalibMuonAlignJpsi_CombAPT'                : 1500    # MeV
                        , 'CalibMuonAlignJpsi_CombDOCA'               : 0.2     # mm
                        , 'CalibMuonAlignJpsi_CombVCHI2DOF'           : 10      # dimensionless
                        , 'CalibMuonAlignJpsi_CombVCHI2DOFLoose'      : 10      # dimensionless
                        , 'CalibMuonAlignJpsi_CombDIRA'               : 0.9     # dimensionless
                        , 'CalibMuonAlignJpsi_CombTAU'                : 0.     # ps
                        , 'CalibMuonAlignJpsi_JpsiMassWinLoose'         : 150     # MeV
                        , 'CalibMuonAlignJpsi_JpsiMassWin'              : 100     # MeV
                        , 'CalibMuonAlignJpsi_GEC'                     : 'Loose'     # TODO: Do we want a different one?
                        ,'L0Channels'               : {
                                'SingleMuonHighPT' : ( 'Muon',),
                                'DiMuonHighMass'   : ( 'Muon',),
                                'CalibMuonAlignJpsi'    : ( 'Muon',),
                                }
                        , 'Prescale'                 : { 
                                'Hlt1SingleMuonHighPT': 1.0,
                                'Hlt1DiMuonHighMass' : 1.0,
                                }
                        , 'Priorities'               : { 
                                'SingleMuonHighPT' : 8,
                                'DiMuonHighMass'   : 6,
                                }
                        }
                       , Hlt1CalibTrackingLinesConf :  { 'ParticlePT'            : 600     # MeV
                                                        ,'ParticleP'             : 4000    # MeV
                                                        ,'ParticlePT_LTUNB'      : 800     # MeV
                                                        ,'ParticleP_LTUNB'       : 4000    # MeV
                                                        ,'L0_DHH_LTUNB'          : ['Photon', 'Electron','Hadron','Muon']
                                                        ,'L0_DHH_LTUNB_Lines'    : ['CalibTrackingPiPi','CalibTrackingKPi','CalibTrackingKK']
                                                        ,'TrackCHI2DOF'          : 2       # dimensionless
                                                        ,'CombMaxDaughtPT'       : 900     # MeV 900
                                                        ,'CombAPT'               : 1800    # MeV 1200
                                                        ,'CombDOCA'              : 0.1     # mm
                                                        ,'CombVCHI2DOF'          : 10      # dimensionless
                                                        ,'CombVCHI2DOFLoose'     : 15      # dimensionless
                                                        ,'CombDIRA'              : 0.99    # dimensionless
                                                        ,'CombTAU'               : 0.25    # ps
                                                        ,'D0MassWinLoose'        : 100     # MeV
                                                        ,'D0MassWin'             : 60      # MeV
                                                        ,'B0MassWinLoose'        : 200     # MeV
                                                        ,'B0MassWin'             : 150     # MeV
                                                        ,'D0DetachedDaughtsIPCHI2': 9      # dimensionless
                                                        ,'D0DetachedIPCHI2'       : 9      # dimensionless
                                                        ,'BsPhiGammaMassMinLoose': 3350    # MeV
                                                        ,'BsPhiGammaMassMaxLoose': 6900    # MeV
                                                        ,'BsPhiGammaMassMin'     : 3850    # MeV
                                                        ,'BsPhiGammaMassMax'     : 6400    # MeV
                                                        ,'PhiMassWinLoose'       : 50      # MeV
                                                        ,'PhiMassWin'            : 30      # MeV
                                                        ,'PhiMassWinTight'       : 20      # MeV
                                                        ,'PhiPT'                 : 1800    # MeV
                                                        ,'PhiPTLoose'            : 800     # MeV
                                                        ,'PhiSumPT'              : 3000    # MeV
                                                        ,'PhiIPCHI2'             : 16      # dimensionless
                                                        ,'B0SUMPT'               : 4000    # MeV
                                                        ,'B0PT'                  : 1000    # MeV
                                                        ,'GAMMA_PT_MIN'          : 2000    # MeV
                                                        ,'Velo_Qcut'             : 999     # OFF
                                                        ,'TrNTHits'              : 0       # OFF
                                                        ,'ValidateTT'            : False
                                                        ,'GEC'            : "HeavyIons"
                                                        ,'Prescale'  : { 'Hlt1CalibTrackingKPi': 0.2 }
                                                       },
            Hlt1CommissioningLinesConf: {
                'Prescale': {
                    'Hlt1VeloClosingMicroBias': 1,
                    'Hlt1VeloClosingPV': 1,
                },
                'Postscale': {
                    'Hlt1VeloClosingMicroBias': 'RATE(500)',
                    'Hlt1VeloClosingPV': 'RATE(500)',
                },
                'ODINVeloClosing': '(ODIN_BXTYP == LHCb.ODIN.BeamCrossing) & ODIN_PASS(LHCb.ODIN.VeloOpen)',
                'ODIN': {
                    'VeloClosingPV': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
                },
                'L0': {
                    'VeloClosingPV': "L0_DECISION(LHCb.L0DUDecision.Any)",
                },
                'VeloClosingPV': {
                    'ZMin': -150 * mm,
                    'ZMax': 150 * mm,
                    'MinBackwardTracks': 1,
                    'MinForwardTracks': 1,
                },
                'GEC': 'HeavyIonsTight',
            },
                         Hlt1MBLinesConf :     { 
                               'Prescale' : { 
                                   'Hlt1MBNoBias'                       : 1
                                   }
                               }
                       , Hlt1CalibRICHMirrorLinesConf : { 
                               'Prescale' : { 
                                   'Hlt1CalibHighPTLowMultTrks'     : 0.0001,
                                   'Hlt1CalibRICHMirrorRICH1'       : 0.281,
                                   'Hlt1CalibRICHMirrorRICH2'       : 1.0
                                   }
                               , 'DoTiming' : False
                               , 'R2L_PreFitPT'    : 475. * MeV
                               , 'R2L_PreFitP'     : 38000. * MeV 
                               , 'R2L_PT'       : 500. * MeV
                               , 'R2L_P'        : 40000. * MeV
                               , 'R2L_MinETA'   : 2.65
                               , 'R2L_MaxETA'   : 2.80
                               , 'R2L_Phis'     : [(-2.59, -2.49), (-0.65, -0.55), (0.55, 0.65), (2.49, 2.59)]
                               , 'R2L_TrChi2'   : 2.
                               , 'R2L_MinTr'    : 0.5
                               , 'R2L_GEC'      : 'Loose'
                               , 'R1L_PreFitPT' : 475. * MeV
                               , 'R1L_PreFitP'  : 19000. * MeV 
                               , 'R1L_PT'       : 500. * MeV
                               , 'R1L_P'        : 20000. * MeV
                               , 'R1L_MinETA'   : 1.6
                               , 'R1L_MaxETA'   : 2.04
                               , 'R1L_Phis'     : [(-2.65, -2.30 ), (-0.80, -0.50), (0.50, 0.80), (2.30, 2.65)]
                               , 'R1L_TrChi2'   : 2.
                               , 'R1L_MinTr'    : 0.5
                               , 'R1L_GEC'      : 'Loose'
                               , 'LM_PreFitPT' : 475. * MeV
                               , 'LM_PreFitP'  : 950. * MeV 
                               , 'LM_PT'    : 500. * MeV
                               , 'LM_P'     : 1000. * MeV
                               , 'LM_TrChi2': 2.
                               , 'LM_MinTr' : 1
                               , 'LM_MaxTr' : 40
                               , 'LM_GEC'   : 'Loose'
                               }
                } # Thresholds

        # HLT2 thresholds from individual files
        sds = self.SubDirs()
        for monthyear, subdirs in self.SubDirs().iteritems():
            for subdir in subdirs:
                conf = __get_conf__(subdir, "_%s" %(monthyear))
                update_thresholds(thresholds, conf.Thresholds())

        return thresholds

    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        hlt2 = []
        sds = self.SubDirs()
        for monthyear, subdirs in self.SubDirs().iteritems():
            for subdir in subdirs:
                conf = __get_conf__(subdir, "_%s" %(monthyear))
                hlt2.extend(conf.ActiveHlt2Lines())

        return hlt2

    def ActiveHlt1Lines(self) :
        """
        Returns a list of active lines
        """
        lines =  [
            # MB lines
            'Hlt1BBHighMult', 'Hlt1BBMicroBiasVelo',
            'Hlt1L0Any', 'Hlt1MBNoBias',
            'Hlt1L0Photon',
            # Muon lines
            'Hlt1SingleMuonHighPT' , 'Hlt1DiMuonHighMass',
            # Calibration lines
            'Hlt1CalibTrackingKPi' , 'Hlt1CalibHighPTLowMultTrks',
            'Hlt1CalibTrackingKPiDetached' , 'Hlt1CalibMuonAlignJpsi',
            # RICH calibration
            'Hlt1IncPhi',
            'Hlt1CalibRICHMirrorRICH1',
            'Hlt1CalibRICHMirrorRICH2',
            # Lumi calbiration lines
            'Hlt1Lumi',
            'Hlt1LumiSequencer',
            'Hlt1LumiLowBeam1', 'Hlt1LumiLowBeam2', 'Hlt1LumiLowBeamCrossing', 'Hlt1LumiLowNoBeam',
            'Hlt1BeamGasBeam1', 'Hlt1BeamGasBeam2',
            'Hlt1BeamGasNoBeamBeam1', 'Hlt1BeamGasNoBeamBeam2',
            'Hlt1BeamGasCrossingForcedReco', 'Hlt1BeamGasCrossingForcedRecoFullZ',
            # Velo closing
            'Hlt1VeloClosingMicroBias',  # disk buffer only
            'Hlt1VeloClosingPV',  # disk buffer only
            # Technical lines
            'Hlt1ODINTechnical', 'Hlt1Tell1Error', 'Hlt1ErrorEvent',
        ]

        return lines

    def Streams(self):
        #FULL            everything about physics 
        return {
                # None means use default
                'FULL': "HLT_NONTURBOPASS_RE('Hlt2(?!BeamGas)(?!Lumi).*Decision')",
                'BEAMGAS': None,
                'HLT1NOBIAS' : None,
                'VELOCLOSING': None,
                }

    def StreamsWithBanks(self):
        return [
            (["FULL"], 'KILL', []),
            (["BEAMGAS"], 'KILL', []),
        ]

    def StreamsWithLumi(self):
        return ['FULL', 'BEAMGAS']

    def NanoBanks(self):
        nanobanks = {'LUMI': ['ODIN', 'HltLumiSummary', 'HltRoutingBits', 'DAQ']}
        nanobanks['BEAMGAS'] = nanobanks['LUMI'] + ['Velo', 'L0DU', 'HltDecReports', 'HC']
        return nanobanks
