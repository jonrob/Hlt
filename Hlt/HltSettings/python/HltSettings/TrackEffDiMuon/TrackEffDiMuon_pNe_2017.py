###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, mm, MeV

class TrackEffDiMuon_pNe_2017 :
    """
    Threshold settings for Hlt2 TrackEffDiMuon lines:
    2016 proton-Helium run

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

    @author M. Kolpin
    @date 2016-02-12
    """

    __all__ = ( 'ActiveHlt2Lines' )


    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        # TODO Default for pp is TurboCalib, protonArgon_2015 used Fullstream,
        #      what do we want here?
        lines = [
            'Hlt2TrackEffDiMuonMuonTTPlusLowStatTaggedFullstream',
            'Hlt2TrackEffDiMuonMuonTTMinusLowStatTaggedFullstream',
            'Hlt2TrackEffDiMuonVeloMuonPlusLowStatTaggedFullstream',
            'Hlt2TrackEffDiMuonVeloMuonMinusLowStatTaggedFullstream',
            'Hlt2TrackEffDiMuonDownstreamPlusLowStatTaggedFullstream',
            'Hlt2TrackEffDiMuonDownstreamMinusLowStatTaggedFullstream',
            'Hlt2TrackEffDiMuonMuonTTPlusHighStatTaggedFullstream',
            'Hlt2TrackEffDiMuonMuonTTMinusHighStatTaggedFullstream',
            'Hlt2TrackEffDiMuonVeloMuonPlusHighStatTaggedFullstream',
            'Hlt2TrackEffDiMuonVeloMuonMinusHighStatTaggedFullstream',
            'Hlt2TrackEffDiMuonDownstreamPlusHighStatTaggedFullstream',
            'Hlt2TrackEffDiMuonDownstreamMinusHighStatTaggedFullstream',
            'Hlt2TrackEffDiMuonMuonTTPlusLowStatMatchedFullstream',
            'Hlt2TrackEffDiMuonMuonTTMinusLowStatMatchedFullstream',
            'Hlt2TrackEffDiMuonVeloMuonPlusLowStatMatchedFullstream',
            'Hlt2TrackEffDiMuonVeloMuonMinusLowStatMatchedFullstream',
            'Hlt2TrackEffDiMuonDownstreamPlusLowStatMatchedFullstream',
            'Hlt2TrackEffDiMuonDownstreamMinusLowStatMatchedFullstream',
            'Hlt2TrackEffDiMuonMuonTTPlusHighStatMatchedFullstream',
            'Hlt2TrackEffDiMuonMuonTTMinusHighStatMatchedFullstream',
            'Hlt2TrackEffDiMuonVeloMuonPlusHighStatMatchedFullstream',
            'Hlt2TrackEffDiMuonVeloMuonMinusHighStatMatchedFullstream',
            'Hlt2TrackEffDiMuonDownstreamPlusHighStatMatchedFullstream',
            'Hlt2TrackEffDiMuonDownstreamMinusHighStatMatchedFullstream',
        ]


        return lines

    def Thresholds(self) :

        d = {}

        from Hlt2Lines.TrackEffDiMuon.Lines     import TrackEffDiMuonLines
        d.update ({TrackEffDiMuonLines : {
                 'Prescale' :  {},
                 'Common'   :  {
                                # TODO the TisTosSpec and the Hlt1Filter are from protonArgon_2015
                                #      Do they make sense?
                                'Hlt1Filter'    : "HLT_PASS_RE('Hlt1(?!Lumi)(?!Velo)(?!BeamGas)(?!NoPV)(?!MB).*Decision')",
                                'TisTosSpec'    : [],
                                #'TisTosSpec'    : "Hlt1TrackMuonDecision%TOS",
                                'L0Filter'      : "L0_CHANNEL('Muon')",
                                'OverlapTT'     : 0.4,
                                'OverlapIT'     : 0.4,
                                'OverlapOT'     : 0.4,
                                'OverlapMuon'   : 0.4,
                                'OverlapVelo'   : 0.5},
                 'MuonTT'   :  {'TagMuonID'     : 2,
                                'TagP'          : 10 * GeV,
                                'TagPt'         : 1300 * MeV,
                                'TagTrChi2'     : 5,
                                'TagMinIP'      : 0 * mm,
                                'TagMinIPchi2'  : 0, #dummy
                                'ProbeP'        : 5 * GeV,
                                'ProbePt'       : 500 * MeV,
                                'ProbeTrChi2'   : 9999, #dummy
                                'JPsiPt'        : 1000 * MeV,
                                'JPsiDOCA'      : 9999 * mm, #dummy
                                'JPsiMaxIP'     : 0.8 * mm,
                                'JPsiMaxIPchi2' : 9999, #dummy
                                'JPsiVtxChi2'   : 2,
                                'JPsiMassWin'   : 500 * MeV,
                                'StatHighPT'	: 2600 * MeV,
                                'StatLowETA'	: 2.7,
                                'StatHighETA'	: 4.2
                                },
                 'VeloMuon' :  {'TagMuonID'     : 1,
                                'TagP'          : 7 * GeV,
                                'TagPt'         : 0 * MeV,
                                'TagTrChi2'     : 3,
                                'TagMinIP'      : 0.2 * mm,
                                'TagMinIPchi2'  : 0, #dummy
                                'ProbeP'        : 5 * GeV,
                                'ProbePt'       : 500 * MeV,
                                'ProbeTrChi2'   : 5,
                                'JPsiPt'        : 500 * MeV,
                                'JPsiDOCA'      : 9999 * mm, #dummy
                                'JPsiMaxIP'     : 9999 * mm, #dummy
                                'JPsiMaxIPchi2' : 9999, #dummy
                                'JPsiVtxChi2'   : 2,
                                'JPsiMassWin'   : 500 * MeV,
                                'StatHighPT'	: 2600 * MeV,
                                'StatLowETA'	: 2.7,
                                'StatHighETA'	: 4.2
                                },
                 'Downstream': {'TagMuonID'     : 2,
                                'TagP'          : 5 * GeV,
                                'TagPt'         : 700 * MeV,
                                'TagTrChi2'     : 10,
                                'TagMinIP'      : 0.5 * mm,
                                'TagMinIPchi2'  : 0, #dummy
                                'ProbeP'        : 5 * GeV,
                                'ProbePt'       : 500 * MeV,
                                'ProbeTrChi2'   : 10,
                                'JPsiPt'        : 0 * MeV,
                                'JPsiDOCA'      : 5 * mm,
                                'JPsiMaxIP'     : 9999 * mm, #dummy
                                'JPsiMaxIPchi2' : 9999, #dummy
                                'JPsiVtxChi2'   : 5,
                                'JPsiMassWin'   : 200 * MeV,
                                'StatHighPT'	: 2600 * MeV,
                                'StatLowETA'	: 2.7,
                                'StatHighETA'	: 4.2
                                }
               }
        })

        return d
