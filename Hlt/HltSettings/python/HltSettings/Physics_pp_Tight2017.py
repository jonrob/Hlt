###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Utilities.Utilities import update_thresholds
from Physics_pp_2017 import Physics_pp_2017


class Physics_pp_Tight2017(Physics_pp_2017):
    """Settings for pp physics in 2017 (tight version)."""

    def HltType(self):
        self.verifyType(Physics_pp_Tight2017)
        return 'Physics_pp_Tight2017'

    def Thresholds(self):
        """Returns a dictionary of cuts"""
        from Hlt1Lines.Hlt1MVALines import Hlt1MVALinesConf

        thresholds = super(Physics_pp_Tight2017, self).Thresholds()
        new_thresholds = {
            Hlt1MVALinesConf: {
                'Prescale': {
                    'Hlt1TrackMVA': 0.01,
                    'Hlt1TwoTrackMVA': 0.01,
                },
            },
        }
        update_thresholds(thresholds, new_thresholds)

        return thresholds
