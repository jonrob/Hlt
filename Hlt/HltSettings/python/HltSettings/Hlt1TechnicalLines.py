###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
class Hlt1TechnicalLines:
    """Grouping of Hlt1 technical lines

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

    @author J. Albrecht
    @date 2012-04-18
    """

    def ActiveHlt1Lines(self):
        """Return a list of active lines."""
        lines = [
            'Hlt1BeamGasBeam1', 'Hlt1BeamGasBeam2',
            'Hlt1BeamGasBeam1VeloOpen', 'Hlt1BeamGasBeam2VeloOpen',
            'Hlt1BeamGasHighRhoVertices',
            'Hlt1Lumi',
            'Hlt1L0Any', 'Hlt1L0AnyNoSPD',
            'Hlt1MBNoBias',
            'Hlt1ODINTechnical', 'Hlt1Tell1Error', 'Hlt1ErrorEvent',  # 'Hlt1Incident'
            'Hlt1VeloClosingMicroBias',
        ]
        return lines
