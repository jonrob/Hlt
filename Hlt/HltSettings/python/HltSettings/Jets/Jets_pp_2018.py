###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import MeV, GeV, picosecond, mm

class Jets_pp_2018:
   """
   Threshold settings for Hlt2 jets lines for 2016 25 ns data-taking.
   
   WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS
   
   @author P. Ilten and M. Williams
   @date 2016-02-13
   """
   
   __all__ = ( 'ActiveHlt2Lines' )
   
   def ActiveHlt2Lines(self) :
      """Returns a list of active lines."""

      lines = [
         #'Hlt2JetsDiJetLowPt'    ,
         #'Hlt2JetsDiJetMVLowPt'  ,
         'Hlt2JetsDiJetMuMuLowPt',
         'Hlt2JetsDiJetSVLowPt'  ,
         'Hlt2JetsDiJetSVMuLowPt',
         'Hlt2JetsDiJetSVSVLowPt',
         'Hlt2JetsJetLowPt'      ,
         #'Hlt2JetsJetDHLowPt'    ,
         #'Hlt2JetsJetMuLowPt'    ,
         #'Hlt2JetsJetSVLowPt'    ,
         #'Hlt2JetsDiJet'    , #May
         #'Hlt2JetsDiJetMV'  , 
         'Hlt2JetsDiJetMuMu', #May
         'Hlt2JetsDiJetSV'  , #May
         'Hlt2JetsDiJetSVMu', #May
         'Hlt2JetsDiJetSVSV', #May
         #'Hlt2JetsJet'      ,
         #'Hlt2JetsJetDH'    ,
         #'Hlt2JetsJetMu'    ,
         'Hlt2JetsJetSV'    ,
         #'Hlt2JetsDiJetHighPt'  ,
         'Hlt2JetsDiJetSVHighPt',
         #'Hlt2JetsJetHighPt'    ,
         #'Hlt2JetsJetDHHighPt'  ,
         #'Hlt2JetsJetMuHighPt'  ,
         #'Hlt2JetsJetSVHighPt',  
         'Hlt2JetsJetDisplacedSV',
         'Hlt2JetsDiJetDisplacedSVSVLowPt',        
         'Hlt2JetsTriJet',
         'Hlt2JetsTriJetSV',
         'Hlt2JetsTriJetSVSV',
         'Hlt2JetsSixJet'
         ]
      
      return lines
   
   def Thresholds(self) :
      """Return the trigger thresholds."""
      
      d = {}
      
      from Hlt2Lines.Jets.Lines import JetsLines
      d.update({
            JetsLines : {
        'Prescale': {
            'Hlt2JetsDiJetLowPt'    : 1e-00,
            'Hlt2JetsDiJetMVLowPt'  : 1e-00,
            'Hlt2JetsDiJetMuMuLowPt': 1.7e-02, # 1 Hz
            'Hlt2JetsDiJetSVLowPt'  : 4.1e-04, # 1 Hz
            'Hlt2JetsDiJetSVMuLowPt': 1.7e-02, # 1 Hz
            'Hlt2JetsDiJetSVSVLowPt': 0.1, # 10 Hz
            'Hlt2JetsJetLowPt'      : 1.0, # 0.3Hz
            'Hlt2JetsJetDHLowPt'    : 1e-00,
            'Hlt2JetsJetMuLowPt'    : 1e-00,
            'Hlt2JetsJetSVLowPt'    : 1e-00,
            'Hlt2JetsDiJet'         : 1e-00, #May
            'Hlt2JetsDiJetMV'       : 1e-00,
            'Hlt2JetsDiJetMuMu'     : 1.0,  #May
            'Hlt2JetsDiJetSV'       : 2.2e-03, # 1 Hz
            'Hlt2JetsDiJetSVMu'     : 1.0,  #May
            'Hlt2JetsDiJetSVSV'     : 1.0,  #May
            'Hlt2JetsJet'           : 1.0, # 1 Hz, turned off
            'Hlt2JetsJetDH'         : 1e-00,
            'Hlt2JetsJetMu'         : 1e-00,
            'Hlt2JetsJetSV'         : 4.6e-03, # 10 Hz

            'Hlt2JetsDiJetHighPt'   : 1e-00,
            'Hlt2JetsDiJetSVHighPt' : 1e-01, # 1 Hz 
            'Hlt2JetsJetHighPt'     : 1.0, # 1 Hz, turned off
            'Hlt2JetsJetDHHighPt'   : 1e-00,
            'Hlt2JetsJetMuHighPt'   : 1e-00,
            'Hlt2JetsJetSVHighPt'   : 1e-00,
            'Hlt2JetsJetDisplacedSV': 1.0, 
            'Hlt2JetsDiJetDisplacedSVSVLowPt': 1.0,
            'Hlt2JetsTriJet'        : 1e-00,
            'Hlt2JetsTriJetSV'      : 1e-00,
            'Hlt2JetsTriJetSVSV'    : 1e-00,
            'Hlt2JetsSixJet'        : 1e-00            
            },
        'Postscale': {},
        'Common': {
            'D_TOS'        : 'Hlt1((Two)?Track(Muon)?MVA.*|TrackMuon)Decision%TOS',
            'D_MASS'       : 50*MeV,
            'GHOSTPROB'    : 0.2,
            'DPHI'         : 0,
            'SV_VCHI2'     : 10,
            'SV_TRK_PT'    : 500*MeV,
            'SV_TRK_IPCHI2': 16,
            'SV_FDCHI2'    : 25,
            'MU_PT'        : 1000*MeV,
            'MU_PROBNNMU'  : 0.5,
            'JET_PT'       : 17*GeV,
            },
        'JetBuilder': {
            'Reco'      : 'TURBO',
            'JetPtMin'  : 5*GeV,
            'JetInfo'   : False,
            'JetEcPath' : ''
            },
        'JetsDiJetLowPt'    : {'JET_PT': 10*GeV},
        'JetsDiJetMVLowPt'  : {'JET_PT': 10*GeV},
        'JetsDiJetMuMuLowPt': {'JET_PT': 10*GeV},
        'JetsDiJetSVLowPt'  : {'JET_PT': 10*GeV},
        'JetsDiJetSVMuLowPt': {'JET_PT': 10*GeV},
        'JetsDiJetSVSVLowPt': {'JET_PT': 10*GeV},
        'JetsJetLowPt'      : {'JET_PT': 10*GeV,
                               'Hlt1Req': "HLT_PASS_RE('Hlt1L0Any.*Decision')"
                              },
        'JetsJetDHLowPt'    : {'JET_PT': 10*GeV},
        'JetsJetMuLowPt'    : {'JET_PT': 10*GeV},
        'JetsJetSVLowPt'    : {'JET_PT': 10*GeV},
        'JetsJet'           : {'Hlt1Req': "HLT_PASS_RE('Hlt1L0Any.*Decision')"},
        #
        'JetsDiJetHighPt'   : {'JET_PT': 60*GeV},
        'JetsDiJetSVHighPt' : {'JET_PT': 50*GeV},
        'JetsJetHighPt'     : {'JET_PT': 40*GeV,
                               'Hlt1Req': "HLT_PASS_RE('Hlt1L0Any.*Decision')"
                              },
        'JetsJetDHHighPt'   : {'JET_PT': 40*GeV},
        'JetsJetMuHighPt'   : {'JET_PT': 60*GeV},
        'JetsJetSVHighPt'   : {'JET_PT': 70*GeV},
        'JetsJetDisplacedSV': {'JET_PT': 15*GeV},
        'JetsDiJetDisplacedSVSVLowPt': {'JET_PT': 10*GeV},         
        'JetsTriJet'                 : {'JET_PT': 30*GeV},
        'JetsTriJetSV'               : {'JET_PT': 20*GeV},
        'JetsTriJetSVSV'             : {'JET_PT': 10*GeV},
        'JetsSixJet'                 : {'JET_PT': 10*GeV}        
        }})
      
      return d
