###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, mm, MeV, picosecond

class Hb2V0V0_pp_2017:
    """
    Threshold settings for Hlt2 lines with Hb2V0V0 topology for 2017 25 ns data-taking.

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

    @author Timon Schmelzer, Moritz Demmer
    @date 2017-02-06
    """

    __all__ = ( 'ActiveHlt2Lines' )

    def ActiveHlt2Lines(self) :
        """Returns a list of active lines."""

        lines = ['Hlt2Hb2V0V0Hb2KSKSDD',
                 'Hlt2Hb2V0V0Hb2KSKSLD',
                 'Hlt2Hb2V0V0Hb2KSKSLL']

        return lines

    def Thresholds(self) :
        """Return the trigger thresholds."""

        d = {}
        from Hlt2Lines.Hb2V0V0.Lines import Hb2V0V0Lines
        d.update(
            {
                '_stages' : {},
                'Prescale' : {},
                'PromptPrescale' : {},
                'Postscale' : {},
                'KS_DD' : {
                  'KS_DD_MASS_WINDOW' : 80 * MeV,
                  'KS_DD_BPVVDCHI2()'   : 5,
                  'KS_DD_TRCHI2DOF'   : 4
                },
                'KS_LL' : {
                  'KS_LL_MASS_WINDOW' : 50 * MeV,
                  'KS_LL_BPVVDCHI2()'   : 5,
                  'KS_LL_TRCHI2DOF'   : 4,
                  'KS_LL_TRGHOSTPROB' : 0.5
                },
                'Hb2V0V0_DD' : {
                  # Combination Cuts
                  'Hb_MASS_MIN'   : 4000 * MeV ,
                  'Hb_MASS_MAX'   : 6500 * MeV ,
                  'Hb_DOCA'       : 4 * mm,
                  # Mother Cuts
                  'Hb_VTXCHI2'     : 40 ,
                  'Hb_DIRA'         : 0.999
                 },
                 'Hb2V0V0_LD' : {
                  # Combination Cuts
                  'Hb_MASS_MIN'   : 4000 * MeV ,
                  'Hb_MASS_MAX'   : 6500 * MeV ,
                  'Hb_DOCA'       : 4 * mm,
                  # Mother Cuts
                  'Hb_VTXCHI2'     : 30 ,
                  'Hb_DIRA'         : 0.999
                 },
                 'Hb2V0V0_LL' : {
                  # Combination Cuts
                  'Hb_MASS_MIN'   : 4000 * MeV ,
                  'Hb_MASS_MAX'   : 6500 * MeV ,
                  'Hb_DOCA'       : 1 * mm,
                  # Mother Cuts
                  'Hb_VTXCHI2'     : 20 ,
                  'Hb_DIRA'         : 0.999
                 },
            }
        )
        return d
