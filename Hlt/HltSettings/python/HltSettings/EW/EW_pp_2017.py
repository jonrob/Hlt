###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, MeV, mm

class EW_pp_2017( object ) :
	"""
	Threshold settings for Hlt2 Electroweak (EW) lines

	WARNING:: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

	@author K. Dreimanis, k.dreimanis@liverpool.ac.uk, karlis.dreimanis@cern.ch
	@date 2017-04-02
	"""

	__all__ = ( 'ActiveHlt2Lines' )
	
	def ActiveHlt2Lines( self ) :
		"""
		Returns the list of ACTIVE lines
		"""
		lines = [
			# DiMuon
			'Hlt2EWDiMuonZ',
			'Hlt2EWDiMuonDY1',
			'Hlt2EWDiMuonDY2',
			'Hlt2EWDiMuonDY3',
			'Hlt2EWDiMuonDY4',
			# Single Muon
			'Hlt2EWSingleMuonLowPt',
			'Hlt2EWSingleMuonHighPt',
			'Hlt2EWSingleMuonHighPtNoMUID',
			'Hlt2EWSingleMuonVHighPt',
			# DiElectron
			'Hlt2EWDiElectronDY',
			'Hlt2EWDiElectronHighMass',
			# Single Electron
			'Hlt2EWSingleElectronLowPt',
			'Hlt2EWSingleElectronHighPt',
			'Hlt2EWSingleElectronVHighPt',
         		# Converted Photon
        		'Hlt2EWConvPhotonLLHighPt',
        		'Hlt2EWConvPhotonLLVHighPt',
        		'Hlt2EWConvPhotonDDHighPt',
        		'Hlt2EWConvPhotonDDVHighPt',
			]
		return lines

	def Thresholds( self ) :
		"""
		Returns a dictionary of cuts
		"""
		d = {}
		from Hlt2Lines.EW.Lines import EWLines
		
		d.update( { EWLines : {
					'Prescale' : {'Hlt2EWDiMuonDY1'        	    :  0.02,
						      'Hlt2EWDiMuonDY2'             :   0.3,
						      'Hlt2EWSingleMuonLowPt'       : 0.002,
						      'Hlt2EWSingleMuonHighPt'      :   0.1,
						      'Hlt2EWSingleElectronLowPt'   : 0.001,
						      'Hlt2EWSingleElectronHighPt'  :  0.01,
                                                      'Hlt2EWConvPhotonLLHighPt'    :   0.05,
                                                      'Hlt2EWConvPhotonLLVHighPt'   :   0.1,
                                                      'Hlt2EWConvPhotonDDHighPt'    :   0.05,
                                                      'Hlt2EWConvPhotonDDVHighPt'   :   0.1},
					# DiMuon
					'DiMuonZ'               : {'MinMass'           : 40000 * MeV,
								   'Pt'                :     0 * MeV},
					
					'DiMuonDY1'             : {'MinMass'           : 2500 * MeV,
								   'MinExcMass'        : 3000 * MeV,
								   'MaxExcMass'        : 3200 * MeV,
								   'MuPt'              :  800 * MeV,
								   'Pt'                :    0 * MeV,
								   'TkChi2'            :   10},
					
					'DiMuonDY2'             : {'MinMass'           : 5000 * MeV,
								   'MuPt'              : 1000 * MeV,
								   'Pt'                :    0 * MeV,
								   'TkChi2'            :   10},
					
					'DiMuonDY3'             : {'MinMass'           : 10000 * MeV,
								   'MuPt'              :     0 * MeV,
								   'TkChi2'            :    10},
					
					'DiMuonDY4'             : {'MinMass'           : 20000 * MeV,
								   'MuPt'              :     0 * MeV,
								   'TkChi2'            :    10},
					
					# Single Muon
					'SingleMuonLowPt'       : {'Pt'                : 4800 * MeV,
								   'TkChi2'            :   10},
					
					'SingleMuonHighPt'      : {'Pt'                : 10000 * MeV},
					
					'SingleMuonHighPtNoMUID': {'Pt'                : 15000 * MeV},
					
					'SingleMuonVHighPt'     : {'Pt'                : 12500 * MeV},
					
					# DiElectron
					'DiElectronDY'          : {'L0Req'             : "L0_CHANNEL('Electron')",
								   'Hlt1Req'           : "HLT_PASS_RE('Hlt1(Track|.*Electron).*Decision')",
								   'MinMass'           : 10000 * MeV,
								   'MaxMass'           : 1e+10 * MeV,
								   'VtxChi2'           :    25,
								   'Pt'                :  -999 * MeV,
								   'ElecPt'            :  1000 * MeV,
								   'ElecPIDe'          :   1.5,
								   'ElecTkChi2'        :    10,
								   'PrsMin'            :    50,
								   'EcalMin'           :   0.1,
								   'HcalMax'           :  0.05},
					
					'DiElectronHighMass'    : {'L0Req'             : "L0_CHANNEL('Electron')",
								   'Hlt1Req'           : "HLT_PASS_RE('Hlt1(Track|.*Electron).*Decision')",
								   'MinMass'           : 20000 * MeV,
								   'VtxChi2'           :    25,
								   'TkChi2'            :    10,
								   'Pt'                :  -999 * MeV,
								   'ElecPt'            : 10000 * MeV,
								   'PrsMin'            :    50,
								   'EcalMin'           :   0.1,
								   'HcalMax'           :  0.05},
					
					# Single Electron
					'SingleElectronLowPt'   : {'L0Req'             : "L0_CHANNEL('Electron')",
								   'Hlt1Req'           : "HLT_PASS_RE('Hlt1(Track|.*Electron).*Decision')",
								   'Pt'                :  4800 * MeV,
								   'PIDe'              :     4,
								   'TkChi2'            :     5,
								   'PrsMin'            :    50,
								   'EcalMin'           :   0.1,
								   'HcalMax'           :  0.05},
					
					'SingleElectronHighPt'  : {'L0Req'             : "L0_CHANNEL('Electron')",
								   'Hlt1Req'           : "HLT_PASS_RE('Hlt1(Track|.*Electron).*Decision')",
								   'Pt'                : 10000 * MeV,
								   'PrsMin'            :    50,
								   'EcalMin'           :   0.1,
								   'HcalMax'           :  0.05,
								   'TkChi2'            :    20},
					
					'SingleElectronVHighPt' : {'L0Req'             : "L0_CHANNEL('Electron')",
								   'Hlt1Req'           : "HLT_PASS_RE('Hlt1(Track|.*Electron).*Decision')",
								   'Pt'                : 15000 * MeV,
								   'PrsMin'            :    50,
								   'EcalMin'           :   0.1,
								   'HcalMax'           :  0.05,
								   'TkChi2'            :    20},
              
               				# Converted Photon   
               							  'ConvPhotonLLHighPt' : { 'PT' :  5 * GeV },
               							  
								 'ConvPhotonLLVHighPt' : { 'PT' : 10 * GeV },
               
               							  'ConvPhotonDDHighPt' : { 'PT' :  5 * GeV },
               
               							 'ConvPhotonDDVHighPt' : { 'PT' : 10 * GeV },
					
					} } )
		
		return d

