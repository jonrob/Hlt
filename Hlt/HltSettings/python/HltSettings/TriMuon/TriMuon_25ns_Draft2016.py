###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file   TriMuon_25ns_Draft2016.py
# @author Jessica Prisciandaro (jessica.prisciandaro@cern.ch)
# @date   20.07.2015
# =============================================================================
"""Threshold settings for Hlt2 TriMuon lines for 2016."""

from GaudiKernel.SystemOfUnits import GeV, MeV, micrometer


class TriMuon_25ns_Draft2016(object):
    __all__ = ('ActiveHlt2Lines', 'Thresholds')

    def ActiveHlt2Lines(self):
        """Returns a list of active lines."""

        lines = ['Hlt2TriMuonTau23Mu',
                 'Hlt2TriMuonDetached']

        return lines

    def Thresholds(self):
        """Returns the line thresholds."""
        thresholds = {'Common': {},
                      'GoodMuonsForTriMuonLines': {'TrackChi2': 4,
                                                   'Chi2IP'   : 9},
                      'TriMuonTau23Mu': {'MassWin'   : 225 * MeV,
                                         'DiMuMass'  : 14 * MeV,
                                         'VertexChi2': 25,
                                         'ctau'      : 45 * micrometer},
                      'TriMuonDetached': {'Chi2IP_Tight': 36,
                                          'MuonPT'      : 1.4 * GeV}}
        # Noew build the final dictionary
        from Hlt2Lines.TriMuon.Lines import TriMuonLines

        return {TriMuonLines: thresholds}

# EOF

