###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import mm
from Configurables import HltConf


class VanDerMeerScan_2018(object):
    """Settings for VDM scans and BGI in 2018."""

    def __init__(self):
        # No TURBO => no aferburner
        from Configurables import HltConf
        HltConf().EnableHltAfterburner = False

    def verifyType(self, ref):
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if (self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self) or
            self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self)):
            raise RuntimeError('Must update HltType when modifying ActiveHlt.Lines()')

    def L0TCK(self):
        return '0x1620'

    def HltType(self):
        self.verifyType(VanDerMeerScan_2018)
        return 'VanDerMeerScan_2018'

    def ActiveHlt1Lines(self):
        """Return a list of active Hlt1 lines."""
        lines = [
            # General lines
            'Hlt1Lumi',
            'Hlt1LumiSequencer',
            'Hlt1VeloClosingMicroBias',
            'Hlt1VeloClosingPV',
            'Hlt1MBNoBias',  # for monitoring
            'Hlt1NoBiasEmptyEmpty',  # for monitoring
            # Beam-gas lines
            'Hlt1BeamGasNoBeamBeam1',
            'Hlt1BeamGasNoBeamBeam2',
            'Hlt1BeamGasBeam1',
            'Hlt1BeamGasBeam2',
            'Hlt1BeamGasCrossingForcedReco',
            'Hlt1BeamGasCrossingForcedRecoFullZ',
            'Hlt1BeamGasCrossingFullZLowNTracks',
            # Extra BeamGas lines
            'Hlt1BeamGasBeam1L0',
            'Hlt1BeamGasBeam1L0VeloOpen',
            'Hlt1BeamGasBeam1VeloOpen',
            'Hlt1BeamGasBeam2VeloOpen',
        ]
        return lines

    def ActiveHlt2Lines(self):
        """Return a list of active Hlt2 Lines."""
        lines = [
            # BEAMGAS
            'Hlt2BeamGas',
            'Hlt2BeamGasLumi',
            # FULL
            'Hlt2Lumi',
            'Hlt2FullLumi',
            'Hlt2BeamGasCrossingFullZ',
            'Hlt2BeamGasCrossingBG',
            'Hlt2BeamGasBeam',
            'Hlt2BeamGasNoBeam',
        ]
        return lines

    def Thresholds(self):
        """Return a dictionary of cuts."""

        from Hlt1Lines.Hlt1LumiLines import Hlt1LumiLinesConf
        from Hlt1Lines.Hlt1CommissioningLines import Hlt1CommissioningLinesConf
        from Hlt1Lines.Hlt1BeamGasLines import Hlt1BeamGasLinesConf
        from Hlt1Lines.Hlt1MBLines import Hlt1MBLinesConf
        from Hlt1Lines.Hlt1LowMultLines import Hlt1LowMultLinesConf

        thresholds = {
            Hlt1LumiLinesConf: {
                'Prescale': {'Hlt1Lumi': 1.},
                'Postscale': {'Hlt1Lumi': 1.},
            },
            Hlt1CommissioningLinesConf: {
                'Prescale': {
                    'Hlt1VeloClosingMicroBias': 1,
                    'Hlt1VeloClosingPV': 1,
                },
                'Postscale': {
                    'Hlt1VeloClosingMicroBias': 'RATE(500)',
                    'Hlt1VeloClosingPV': 'RATE(500)',
                },
                'ODINVeloClosing': '(ODIN_BXTYP == LHCb.ODIN.BeamCrossing) & ODIN_PASS(LHCb.ODIN.VeloOpen)',
                'ODIN': {
                    'VeloClosingPV': 'ODIN_BXTYP == LHCb.ODIN.BeamCrossing',
                },
                'L0': {
                    'VeloClosingPV': "scale(L0_DECISION(LHCb.L0DUDecision.Any), RATE(10000))",
                },
                'VeloClosingPV': {
                    'ZMin': -150 * mm,
                    'ZMax': 150 * mm,
                    'MinBackwardTracks': 1,
                    'MinForwardTracks': 1,
                },
            },
            Hlt1MBLinesConf: {
                'Prescale': {'Hlt1MBNoBias': 1},
            },
            Hlt1LowMultLinesConf: {
                'Prescale': {'Hlt1NoBiasEmptyEmpty': 0.1},
            },
            Hlt1BeamGasLinesConf: {
                # Global behaviour settings
                'TrackingConf'          : 'FastVelo',
                'FitTracks'             : True,
                'PVFitter'              : 'LSAdaptPV3DFitter',
                'PVSeeding'             : 'PVSeed3DTool',
                'SplitVertices'         : True,
                'CreateGlobalSelection' : False,
                'Turbo'                 : False,
                'UseGEC'                : 'None',

                # Minimum number of tracks for the produced vertices (#tr/vtx > X)
                'VertexMinNTracks'          : 9,  # strictly greater than
                'FullZVertexMinNTracks'     : 27,  # strictly greater than
                'Beam1VtxMaxBwdTracks'      : -1,  # less or equal than, negative to switch off
                'Beam2VtxMaxFwdTracks'      : -1,  # less or equal than, negative to switch off

                # z-ranges for Vertexing
                'Beam1VtxRangeLow'        : -2000.,
                'Beam1VtxRangeUp'         :  2000.,
                'Beam2VtxRangeLow'        : -2000.,
                'Beam2VtxRangeUp'         :  2000.,
                # Luminous region exclusion range
                'BGVtxExclRangeMin'       :  -250.,
                'BGVtxExclRangeMax'       :   250.,

                # Take any L0 channel
                'L0Filter' : {
                    'BeamGasNoBeamBeam1' : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasNoBeamBeam2' : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasBeam1'       : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasBeam2'       : "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasCrossingForcedReco': "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasCrossingForcedRecoFullZ': "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasCrossingFullZLowNTracks': "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasBeam1L0': "L0_CHANNEL('B1gas')",
                    'BeamGasBeam1L0VeloOpen': "L0_CHANNEL('B1gas')",
                    'BeamGasBeam1VeloOpen': "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                    'BeamGasBeam2VeloOpen': "L0_CHANNEL('B1gas') | L0_CHANNEL('B2gas')",
                },

                'Prescale' : {
                    'Hlt1BeamGasNoBeamBeam1'             : 1.,
                    'Hlt1BeamGasNoBeamBeam2'             : 1.,
                    'Hlt1BeamGasBeam1'                   : 1.,
                    'Hlt1BeamGasBeam2'                   : 1.,
                    'Hlt1BeamGasCrossingForcedReco'      : 1.,
                    'Hlt1BeamGasCrossingForcedRecoFullZ' : 1.,
                    'Hlt1BeamGasCrossingFullZLowNTracks' : 0.25,
                    'Hlt1BeamGasBeam1L0'                 : 0.01,
                    'Hlt1BeamGasBeam1L0VeloOpen'         : 1.,
                    'Hlt1BeamGasBeam1VeloOpen'           : 1.,
                    'Hlt1BeamGasBeam2VeloOpen'           : 1.,
                },
                'Postscale' : {
                    'Hlt1BeamGasNoBeamBeam1'             : 1.,
                    'Hlt1BeamGasNoBeamBeam2'             : 1.,
                    'Hlt1BeamGasBeam1'                   : 1.,
                    'Hlt1BeamGasBeam2'                   : 1.,
                    'Hlt1BeamGasCrossingForcedReco'      : 1.,
                    'Hlt1BeamGasCrossingForcedRecoFullZ' : 1.,
                    'Hlt1BeamGasCrossingFullZLowNTracks' : 1.,
                    'Hlt1BeamGasBeam1L0'                 : 1.,
                    'Hlt1BeamGasBeam1L0VeloOpen'         : 1.,
                    'Hlt1BeamGasBeam1VeloOpen'           : 1.,
                    'Hlt1BeamGasBeam2VeloOpen'           : 1.,
                },
            },
        }

        # Hlt2 pass through lines
        from Hlt2Lines.Technical.Lines import TechnicalLines
        thresholds.update({
            TechnicalLines: {
                'Prescale': {
                    # BEAMGAS stream
                    'Hlt2BeamGas': 1.,
                    'Hlt2BeamGasLumi': 1.,
                    # FULL stream
                    'Hlt2Lumi': 0.2,  # prescale the lumi events in FULL
                    'Hlt2FullLumi': 0.02,
                    'Hlt2BeamGasCrossingFullZ': 0.1,  # prescale the BB events
                    'Hlt2BeamGasCrossingBG': 1.,
                    'Hlt2BeamGasBeam': 0.2,  # prescale beam-gas in BE/EB
                    'Hlt2BeamGasNoBeam': 1.,
                },
            }
        })
        return thresholds

    def Streams(self):
        return {
            'FULL': ("HLT_PASS_RE('Hlt2BeamGas(Crossing.*|Beam|NoBeam)Decision') | "
                     "HLT_PASS('Hlt2FullLumiDecision')"),  # Hlt2Lumi is added by HltOutput
            'BEAMGAS': "HLT_PASS_SUBSTR('Hlt2BeamGas')",
            'EXPRESS': None,
            'VELOCLOSING': None,
            'HLT1NOBIAS': None,
        }

    def NanoBanks(self):
        # copy the default:
        nanobanks = {'LUMI': ['ODIN', 'HltLumiSummary', 'HltRoutingBits', 'DAQ']}
        # save more information for lumi events (but not all raw banks)
        nanobanks['FULL'] = nanobanks['LUMI'] + ['Velo', 'L0DU', 'HltDecReports', 'HC']
        return nanobanks

    def StreamsWithBanks(self):
        # In the BEAMGAS stream we keep the minimum for all events
        # In FULL we keep raw data for fewer events
        beamgas = ['ODIN', 'HltRoutingBits', 'DAQ', 'L0DU',
                   'Velo', 'HC', 'HltDecReports', 'HltSelReports']
        lumi = ['ODIN', 'HltLumiSummary', 'HltRoutingBits', 'DAQ']
        return [
            (["BEAMGAS"], 'KEEP', list(set(beamgas) | set(lumi))),
            (["FULL"],    'KILL', []),
            ]

    def StreamsWithLumi(self):
        return ['FULL']
