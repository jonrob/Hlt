###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, mm, MeV

class TrackEff_pp_2017 :
    """
    Threshold settings for Hlt2 Hadronic TrackEff lines: 25ns data taking, August 2015

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

    @author M. Kolpin
    @date 2015-07-23
    """

    __all__ = ( 'ActiveHlt2Lines' )


    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        lines = [
             'Hlt2TrackEff_D0ToKpiPionProbeTurbo',
             'Hlt2TrackEff_D0ToK3piPionProbeTurbo'
             ,'Hlt2TrackEff_D0ToKpiKaonProbeTurbo'
        ]


        return lines

    def Thresholds(self) :

        d = {}

        from Hlt2Lines.TrackEff.Lines     import TrackEffLines
        d.update ({TrackEffLines : { 
                                    'Prescale' : {},
                                    'TrackGEC' : {'NTRACK_MAX'           : 180},
                                    'Common' : {  'D0_MaxAM' : 2250.*MeV
												 , 'D0_MinVDZ' : 3.5*mm
												 , 'D0_MinVDCHI2' : 25
												 , 'D0_MaxLogVD': 3.5
												 , 'D0_MaxOverlap': 0.9
												 , 'D0_MaxVCHI2NDF' : 3.5
												 , 'D0_MaxDeltaEta' : 1.25
												 , 'D0_MaxDOCA' : 0.1 * mm
												 , 'D0_MaxDOCACHI2' : 5
												 , 'D0_MinSimpleFitM' : 1865.-450. * MeV
												 , 'D0_MaxSimpleFitM' : 1865.+650. * MeV # the fit more often overshoots the momentum...
												 , 'Dst_MinAPT'  : 1750. *MeV
												 , 'Dst_MaxDTFM' : 2030. *MeV
												 , 'Dst_MaxDTFCHI2NDF' : 3.0
												 , 'Dst_MaxSimpleFitDeltaMass' : 225. * MeV
												 , 'TagK_MinPIDK' : -5.
												 , 'TagPi_MaxPIDK' : 10.
												 , 'Slowpi_MinPt' : 110*MeV
												 , 'D0_MCONST_MaxLogIPCHI2' : -2.5
												 , 'D0_MaxLogIPPerp' : -3}
                               }
                 })
        return d

