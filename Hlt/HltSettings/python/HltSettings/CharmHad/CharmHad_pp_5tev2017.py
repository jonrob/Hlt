###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, mm, MeV, picosecond, mrad
import math

class CharmHad_pp_5tev2017(object):
    """
    Threshold settings for Hlt2 topological lines for 2017 5 TeV data-taking.
    """

    __all__ = ( 'ActiveHlt2Lines' )

    def ActiveHlt2Lines(self) :
        """Returns a list of active lines."""

        lines = ['Hlt2CharmHadDpm2KPiPi_XSecTurbo',
                 'Hlt2CharmHadDpm2KKPi_XSecTurbo',
                 'Hlt2CharmHadDs2KKPi_XSecTurbo',
                 'Hlt2CharmHadDs2PiPiPi_XSecTurbo',
                 'Hlt2CharmHadLc2KPPi_XSecTurbo',
                 'Hlt2CharmHadLc2KPK_XSecTurbo',
                 'Hlt2CharmHadLc2PiPPi_XSecTurbo',
                 'Hlt2CharmHadD02KPi_XSecTurbo',
                 'Hlt2CharmHadDst_2D0Pi_D02KPi_XSecTurbo',
                 'Hlt2CharmHadDst_2D0Pi0_D02KPi_XSecTurbo',
                 'Hlt2CharmHadDst_2DsGamma_Ds2KKPi_XSecTurbo',
                 'Hlt2CharmHadDst_2D0Pi_D02K3Pi_XSecTurbo',
                 'Hlt2CharmHadDst_2D0Gamma_D02KPi_XSecTurbo',
                 'Hlt2CharmHadSigmac_2LcPi_XSecTurbo',
                 'Hlt2CharmHadXic0ToPpKmKmPipTurbo']

        return lines

    def Thresholds(self) :
        """Return the trigger thresholds."""

        d = {}

        from Hlt2Lines.CharmHad.Lines import CharmHadLines
        d.update( { CharmHadLines : {
                    'Prescale': { },
 'Common': {'TisTosSpec': 'Hlt1.*Track.*Decision%TOS',
            'Trk_ALL_P_MIN': 1000 * MeV,
            'Trk_ALL_TRCHI2DOF_MAX': 3.0,
            'VCHI2PDOF_MAX': 10.0},
 'TrackGEC': {'NTRACK_MAX': 10000},
 'CharmHadSharedDetachedDpmChild_pi': {'PID_LIM': 3,
                                       'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                                       'Trk_ALL_PT_MIN': 200 * MeV},
 'CharmHadSharedDetachedDpmChild_K': {'PID_LIM': 5,
                                      'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                                      'Trk_ALL_PT_MIN': 200 * MeV},
 'CharmHadSharedSoftTagChild_pi': {'Trk_ALL_PT_MIN': 100.0 * MeV},
 'CharmHadSharedNeutralLowPtChild_pi0': {'Neut_ALL_ADMASS_MAX': 60.0,
                                         'Neut_ALL_PT_MIN': 350.0 * MeV},
 'CharmHadSharedDetachedLcChild_pi': {'PID_LIM': 3,
                                      'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                                      'Trk_ALL_PT_MIN': 200 * MeV},
 'CharmHadSharedDetachedLcChild_K': {'PID_LIM': 5,
                                     'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                                     'Trk_ALL_PT_MIN': 200 * MeV},
 'CharmHadSharedDetachedLcChild_p': {'PID_LIM': 5,
                                     'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                                     'Trk_ALL_PT_MIN': 200 * MeV},
 'CharmHadSharedPromptChild_pi': {'PID_LIM': 3.0, 'Trk_ALL_PT_MIN': 250.0 * MeV},
 'CharmHadSharedPromptChild_K': {'PID_LIM': 5.0, 'Trk_ALL_PT_MIN': 250.0 * MeV},
 'CharmHadSharedPromptChild_p': {'PID_LIM': 10, 'Trk_ALL_PT_MIN': 250.0 * MeV},
 'TighterProtons': {'DeltaPID_MIN': 5.0,
                    'PIDp_MIN': 5.0,
                    'P_MIN': 10000 * MeV},
 'D02HH_XSec': {'Comb_AM_MAX': 1955.0 * MeV,
                'Comb_AM_MIN': 1775.0 * MeV,
                'D0_acosBPVDIRA_MAX' : 17. * mrad,
                'D0_BPVVDCHI2_MIN': 49.0,
                'D0_PT_MIN': 0.0 * MeV,
                'D0_VCHI2PDOF_MAX': 10.0,
                'Mass_M_MAX': 1944.0 * MeV,
                'Mass_M_MIN': 1784.0 * MeV,
                'Pair_AMINDOCA_MAX': 0.1 * mm,
                'TisTosSpec': 'Hlt1.*Track.*Decision%TOS',
                'Trk_ALL_MIPCHI2DV_MIN': 16.0,
                'Trk_ALL_PT_MIN': 250.0 * MeV,
                'Trk_ALL_P_MIN': 2.0 * GeV,
                'Trk_Max_APT_MIN': 500.0 * MeV},
 'Dpm2HHH_XSec': {'AM_MAX': 1959 * MeV,
                  'AM_MIN': 1779 * MeV,
                  'ASUMPT_MIN': 0 * MeV,
                  'acosBPVDIRA_MAX' :  14. * mrad,
                  'BPVLTIME_MIN': 0.15 * picosecond,
                  'BPVVDCHI2_MIN': 16.0,
                  'Mass_M_MAX': 1949.0 * MeV,
                  'Mass_M_MIN': 1789.0 * MeV,
                  'TisTosSpec': 'Hlt1.*Track.*Decision%TOS',
                  'Trk_1OF3_MIPCHI2DV_MIN': 50.0,
                  'Trk_1OF3_PT_MIN': 1000.0 * MeV,
                  'Trk_2OF3_MIPCHI2DV_MIN': 10.0,
                  'Trk_2OF3_PT_MIN': 400.0 * MeV,
                  'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                  'Trk_ALL_PT_MIN': 200.0 * MeV,
                  'VCHI2PDOF_MAX': 6.0},
 'Ds2HHH_XSec': {'AM_MAX': 2059 * MeV,
                 'AM_MIN': 1879 * MeV,
                 'ASUMPT_MIN': 0 * MeV,
                 'acosBPVDIRA_MAX' :  14. * mrad,
                 'BPVLTIME_MIN': 0.15 * picosecond,
                 'BPVVDCHI2_MIN': 16.0,
                 'Mass_M_MAX': 2049.0 * MeV,
                 'Mass_M_MIN': 1889.0 * MeV,
                 'TisTosSpec': 'Hlt1.*Track.*Decision%TOS',
                 'Trk_1OF3_MIPCHI2DV_MIN': 50.0,
                 'Trk_1OF3_PT_MIN': 1000.0 * MeV,
                 'Trk_2OF3_MIPCHI2DV_MIN': 10.0,
                 'Trk_2OF3_PT_MIN': 400.0 * MeV,
                 'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                 'Trk_ALL_PT_MIN': 200.0 * MeV,
                 'VCHI2PDOF_MAX': 6.0},
 'D02HHHH_XSec': {'ACHI2DOCA_MAX': 99999999.0,
                  'ADOCA_MAX': 0.1 * mm,
                  'AMOM_MIN': 0 * MeV,
                  'AM_34': 279.0 * MeV,
                  'AM_4': 139.5 * MeV,
                  'AM_MAX': 1954 * MeV,
                  'AM_MIN': 1774 * MeV,
                  'ASUMPT_MIN': 0.0 * MeV,
                  'acosBPVDIRA_MAX' :  17. * mrad,
                  'BPVLTIME_MIN': 0.1 * picosecond,
                  'BPVVDCHI2_MIN': 9,
                  'DMOM_MIN': 0 * MeV,
                  'DPT_MIN': 0 * MeV,
                  'Mass_M_MAX': 1944.0 * MeV,
                  'Mass_M_MIN': 1784.0 * MeV,
                  'TisTosSpec': 'Hlt1.*Track.*Decision%TOS',
                  'Trk_ALL_MIPCHI2DV_MIN': 4,
                  'Trk_ALL_PT_MIN': 200 * MeV,
                  'VCHI2PDOF_MAX': 10.0},
 'D0_TAG_XSec': {'Q_AM_MIN' : -10.0 * MeV,
                 'Q_AM_MAX' : 25.5 * MeV,
                 'Q_M_MIN' : -10.0 * MeV,
                 'Q_M_MAX' :  20.5 * MeV,
                 'TagVCHI2PDOF_MAX': 25.0},
 'D_TAG_Pi0_XSec': {'Q_AM_MIN' : -5.0 * MeV,
                    'Q_AM_MAX' : 30.0 * MeV,
                    'Q_M_MIN' : -5.0 * MeV,
                    'Q_M_MAX' :  25.0 * MeV},
 'D_TAG_Gamma_XSec': {'Q_AM_MIN' : -5.0 * MeV,
                      'Q_AM_MAX' : 350.0 * MeV,
                      'Q_M_MIN' : -5.0 * MeV,
                      'Q_M_MAX' :  300.0 * MeV},
 'Lc2HHH_XSec': {'AM_MAX': 2553 * MeV,
                 'AM_MIN': 2201 * MeV,
                 'ASUMPT_MIN': 0 * MeV,
                 'acosBPVDIRA_MAX' :  14. * mrad,
                 'BPVLTIME_MIN': 0.075 * picosecond,
                 'BPVVDCHI2_MIN': 4.0,
                 'Mass_M_MAX': 2543.0 * MeV,
                 'Mass_M_MIN': 2211.0 * MeV,
                 'TisTosSpec': 'Hlt1.*Track.*Decision%TOS',
                 'Trk_1OF3_MIPCHI2DV_MIN': 50.0,
                 'Trk_1OF3_PT_MIN': 1000.0 * MeV,
                 'Trk_2OF3_MIPCHI2DV_MIN': 10.0,
                 'Trk_2OF3_PT_MIN': 400.0 * MeV,
                 'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                 'Trk_ALL_PT_MIN': 200.0 * MeV,
                 'VCHI2PDOF_MAX': 6.0},
 'Sigmac_TAG_XSec': {'Q_AM_MIN' : 0.0 * MeV,
                     'Q_AM_MAX' : 65.0 * MeV,
                     'Q_M_MIN' : 0.0 * MeV,
                     'Q_M_MAX' : 60.0 * MeV,
                     'TagVCHI2PDOF_MAX': 25.0},
 'Xic0ToPpKmKmPipTurbo': {'AM_MAX': 2780.0,
                          'AM_MIN': 2386.0,
                          'ASUMPT_MIN': 0.0 * MeV,
                          'acosBPVDIRA_MAX' : 35. * mrad,
                          'BPVLTIME_MIN': 0.075 * picosecond,
                          'BPVVDCHI2_MIN': 4.0,
                          'Mass_M_MAX': 2770.0,
                          'Mass_M_MIN': 2396.0,
                          'Trk_1OF4_MIPCHI2DV_MIN': 4.0,
                          'Trk_1OF4_PT_MIN': 800.0 * MeV,
                          'Trk_2OF4_MIPCHI2DV_MIN': 4.0,
                          'Trk_2OF4_PT_MIN': 400.0 * MeV,
                          'Trk_ALL_MIPCHI2DV_MIN': 4.0,
                          'Trk_ALL_PT_MIN': 250.0 * MeV,
                          'VCHI2PDOF_MAX': 10.0,
                          'TisTosSpec': 'Hlt1.*Track.*Decision%TOS'},
 'Postscale': { },

          }
        }   )

        return d
