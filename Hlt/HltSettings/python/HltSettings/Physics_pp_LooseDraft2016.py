###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Utilities.Utilities import update_thresholds
from Physics_pp_Draft2016 import Physics_pp_Draft2016


class Physics_pp_LooseDraft2016(Physics_pp_Draft2016):
    """Settings for pp physics in 2016 (loose version)."""

    def HltType(self):
        self.verifyType(Physics_pp_LooseDraft2016)
        return 'Physics_pp_LooseDraft2016'

    def Thresholds(self):
        """Returns a dictionary of cuts"""
        from Hlt1Lines.Hlt1MuonLines import Hlt1MuonLinesConf
        from Hlt1Lines.Hlt1MVALines import Hlt1MVALinesConf

        thresholds = super(Physics_pp_LooseDraft2016, self).Thresholds()
        new_thresholds = {
            Hlt1MVALinesConf: {
                'TrackMVA': {'Param3': 1.1},
                'TwoTrackMVA': {'Threshold': 0.95},
            },
            Hlt1MuonLinesConf: {
                'DiMuonNoL0_IP': 0.3,
            },
        }
        update_thresholds(thresholds, new_thresholds)

        return thresholds
