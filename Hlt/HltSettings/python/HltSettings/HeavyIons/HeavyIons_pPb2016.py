###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, mm, MeV


class HeavyIons_pPb2016(object):
    """
    Threshold settings for Hlt2 HeavyIons lines

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

    @author S. Stahl
    @date 2016-10-03
    """

    def ActiveHlt2Lines(self):
        lines = [
            'Hlt2MBMicroBiasVelo',
            'Hlt2MBHighMult',
            'Hlt2HighVeloMultTurbo',
        ]
        return lines

    def Thresholds(self):
        from Hlt2Lines.HeavyIons.Lines import HeavyIonsLines
        thresholds = {
            HeavyIonsLines: {
                'MBMicroBiasVelo': {
                    'HLT1': "HLT_PASS('Hlt1BBMicroBiasVeloDecision')",
                    'VoidFilter': '',
                },
                'MBHighMult': {
                    'HLT1': "HLT_PASS('Hlt1BBHighMultDecision')",
                    'VoidFilter': '',
                },
                'HighVeloMultTurbo': {
                    'HLT1': "HLT_PASS_RE('^Hlt1HighVeloMult.*Decision$')",
                    'VoidFilter': '',
                },
            },
        }
        return thresholds
