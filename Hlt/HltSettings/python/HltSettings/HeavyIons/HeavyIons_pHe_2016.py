###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
class HeavyIons_pHe_2016(object):
    def ActiveHlt2Lines(self):
        lines = [
            'Hlt2SMOGDiMuon',
            'Hlt2SMOGPassThrough',
        ]
        return lines

    def Thresholds(self):
        from Hlt2Lines.HeavyIons.Lines import HeavyIonsLines
        thresholds = {
            HeavyIonsLines: {
                'SMOGDiMuon': {
                    'HLT1': "HLT_PASS_RE('^Hlt1SMOGDiMuon.*Decision$')",
                    'VoidFilter': '',
                },
                'SMOGPassThrough': {
                    'HLT1': "HLT_PASS_RE('Hlt1(BE|EB).*Decision')",
                    'VoidFilter': '',
                },
            },
        }
        return thresholds
