###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from protonHelium_2016 import protonHelium_2016
from VanDerMeerScan_2016 import VanDerMeerScan_2016
from Utilities.Utilities import update_thresholds


class protonHelium_pPb_2016(object):
    """Settings for combined proton-helium physics and VDM/BGI in 2016.

    @author R. Matev
    @date 2016-10-05
    """

    def __init__(self):
        self.physics = protonHelium_2016()
        self.lumi = VanDerMeerScan_2016()

    def verifyType(self, ref):
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if (self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self) or
            self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self)):
            raise RuntimeError('Must update HltType when modifying ActiveHlt.Lines()')

    def L0TCK(self):
        return '0x1608'

    def HltType(self):
        self.verifyType(protonHelium_pPb_2016)
        return 'protonHelium_pPb_2016'

    def ActiveHlt1Lines(self):
        """Return a list of active Hlt1 lines."""
        lines = self.physics.ActiveHlt1Lines() + self.lumi.ActiveHlt1Lines()
        return list(set(lines))

    def ActiveHlt2Lines(self):
        """Return a list of active Hlt2 Lines."""
        lines = self.physics.ActiveHlt2Lines() + self.lumi.ActiveHlt2Lines()
        return list(set(lines))

    def Thresholds(self):
        """Return a dictionary of cuts."""
        thresholds = {}
        update_thresholds(thresholds, self.lumi.Thresholds())
        update_thresholds(thresholds, self.physics.Thresholds())

        from Hlt1Lines.Hlt1BeamGasLines import Hlt1BeamGasLinesConf
        update_thresholds(thresholds, {
            Hlt1BeamGasLinesConf: {
                # Protect from busy events
                'UseGEC': 'HeavyIons',
                # Loosen bb vertex selection due to very low mu (0.01)
                'FullZVertexMinNTracks': 9,  # strictly greater than
                # Reduce number of beam-gas in be/eb by 20/600 to roughly
                # match the statistics in bb crossings.
                # NOTE: from ee we might get about the same statistics if
                # ghost charge is about 3% per beam (f_GC * (R_be + R_eb))
                'Prescale': {
                    'Hlt1BeamGasBeam1': 0.03,
                    'Hlt1BeamGasBeam2': 0.03,
                },
            },
        })
        return thresholds

    def Streams(self):
        streams = {}
        streams.update(self.physics.Streams())
        streams.update(self.lumi.Streams())
        # TODO check that we're not overriding something
        return streams

    def NanoBanks(self):
        return self.lumi.NanoBanks()

    def StreamsWithLumi(self):
        # all streams except VELOCLOSING should have lumi events
        streams = set(self.Streams().keys())
        streams.discard('VELOCLOSING')
        return list(streams)

    def StreamsWithBanks(self):
        physics_banks = self.physics.StreamsWithBanks()
        lumi_banks = self.lumi.StreamsWithBanks()
        assert (not set(tuple(x[0]) for x in physics_banks)
                .intersection(tuple(x[0]) for x in lumi_banks))
        return physics_banks + lumi_banks
