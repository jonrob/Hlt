###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, MeV, mm
from Utilities.Utilities import update_thresholds


def _get_conf(folder, suffix):
    import importlib
    conf = folder + suffix
    module = importlib.import_module("HltSettings.{0}.{1}".format(
        folder, conf))
    return getattr(module, conf)()


class Physics_pp_Upgrade(object):
    def __init__(self):
        from Configurables import HltConf
        HltConf().EnableHltAfterburner = False

    def verifyType(self, ref):
        # verify self.ActiveLines is still consistent with
        # our types self.ActiveHlt2Lines and self.ActiveHlt1Lines...
        # so we can force that classes which inherit from us
        # and overrule either ActiveHlt.Lines also overrule
        # HltType...
        if (self.ActiveHlt1Lines() != ref.ActiveHlt1Lines(self)
                or self.ActiveHlt2Lines() != ref.ActiveHlt2Lines(self)):
            raise RuntimeError(
                'Must update HltType when modifying ActiveHlt.Lines()')

    def HltType(self):
        self.verifyType(Physics_pp_Upgrade)
        return 'Physics_pp_Upgrade'

    def SubDirs(self):
        return {'pp_Upgrade': ['Topo']}

    def Thresholds(self):
        """Return a dictionary of cuts."""

        from Hlt1Lines.Hlt1MuonLines import Hlt1MuonLinesConf
        from Hlt1Lines.Hlt1MVALines import Hlt1MVALinesConf

        thresholds = {
            Hlt1MVALinesConf: {
                'DoTiming': False,
                # 400 MeV / LL / noGEC
                'TrackMVAVLoose': {
                    'TrChi2': 2.5,
                    'TrGP': 0.2,
                    'MinPT': 2000. * MeV,  # shift of 1 GeV
                    'MaxPT': 26000. * MeV,  # shift of 1 GeV
                    'MinIPChi2': 7.4,
                    'Param1': 1.0,
                    'Param2': 2.0,  # shift of 1 GeV, add 1
                    'Param3': 1.248,  # 1.2 * ( 1 + 1/25 )
                    'GEC': 'All'
                },
                'TwoTrackMVAVLoose': {
                    'P': 5000. * MeV,
                    'PT': 400. * MeV,  # simulated tracking cut
                    'TrGP': 0.2,
                    'TrChi2': 2.5,
                    'IPChi2': 4.,
                    'MinMCOR': 1000. * MeV,
                    'MaxMCOR': 1e9 * MeV,
                    'MinETA': 2.,
                    'MaxETA': 5.,
                    'MinDirA': 0.,
                    'V0PT': 2000. * MeV,
                    'VxChi2': 10.,
                    'Threshold': 0.96,  # loose threshold
                    'MvaVars': {
                        'chi2': 'VFASPF(VCHI2)',
                        'fdchi2': 'BPVVDCHI2()',
                        'sumpt': 'SUMTREE(PT, ISBASIC, 0.0)',
                        'nlt16': 'NINTREE(ISBASIC & (BPVIPCHI2() < 16))'
                    },
                    'Classifier': {
                        'Type': 'MatrixNet',
                        'File': '$PARAMFILESROOT/data/Hlt1TwoTrackMVA.mx'
                    },
                    'GEC': 'All'
                },
                # 800 MeV / LL / GEC11500
                'TrackMVALoose': {
                    'TrChi2': 2.5,
                    'TrGP': 0.2,
                    'MinPT': 2000. * MeV,  # shift of 1 GeV
                    'MaxPT': 26000. * MeV,  # shift of 1 GeV
                    'MinIPChi2': 7.4,
                    'Param1': 1.0,
                    'Param2': 2.0,  # shift of 1 GeV, add 1
                    'Param3': 1.248,  # 1.2 * ( 1 + 1/25 )
                    'GEC': 'Loose'
                },
                'TwoTrackMVALoose': {
                    'P': 5000. * MeV,
                    'PT': 800. * MeV,  # simulated tracking cut
                    'TrGP': 0.2,
                    'TrChi2': 2.5,
                    'IPChi2': 4.,
                    'MinMCOR': 1000. * MeV,
                    'MaxMCOR': 1e9 * MeV,
                    'MinETA': 2.,
                    'MaxETA': 5.,
                    'MinDirA': 0.,
                    'V0PT': 2000. * MeV,
                    'VxChi2': 10.,
                    'Threshold': 0.96,  # loose threshold
                    'MvaVars': {
                        'chi2': 'VFASPF(VCHI2)',
                        'fdchi2': 'BPVVDCHI2()',
                        'sumpt': 'SUMTREE(PT, ISBASIC, 0.0)',
                        'nlt16': 'NINTREE(ISBASIC & (BPVIPCHI2() < 16))'
                    },
                    'Classifier': {
                        'Type': 'MatrixNet',
                        'File': '$PARAMFILESROOT/data/Hlt1TwoTrackMVA.mx'
                    },
                    'GEC': 'Loose'
                },
                # 800 MeV / TT / GEC11500
                'TrackMVATight': {
                    'TrChi2': 2.5,
                    'TrGP': 0.2,
                    'MinPT': 3000. * MeV,  # shift of 2 GeV
                    'MaxPT': 27000. * MeV,  # shift of 2 GeV
                    'MinIPChi2': 7.4,
                    'Param1': 1.0,
                    'Param2': 3.0,  # shift of 2 GeV, add 2
                    'Param3': 1.296,  # 1.2 * ( 1 + 2/25 )
                    'GEC': 'Loose'
                },
                'TwoTrackMVATight': {
                    'P': 5000. * MeV,
                    'PT': 800. * MeV,  # simulated tracking cut
                    'TrGP': 0.2,
                    'TrChi2': 2.5,
                    'IPChi2': 4.,
                    'MinMCOR': 1000. * MeV,
                    'MaxMCOR': 1e9 * MeV,
                    'MinETA': 2.,
                    'MaxETA': 5.,
                    'MinDirA': 0.,
                    'V0PT': 2000. * MeV,
                    'VxChi2': 10.,
                    'Threshold': 0.99,  # tight threshold
                    'MvaVars': {
                        'chi2': 'VFASPF(VCHI2)',
                        'fdchi2': 'BPVVDCHI2()',
                        'sumpt': 'SUMTREE(PT, ISBASIC, 0.0)',
                        'nlt16': 'NINTREE(ISBASIC & (BPVIPCHI2() < 16))'
                    },
                    'Classifier': {
                        'Type': 'MatrixNet',
                        'File': '$PARAMFILESROOT/data/Hlt1TwoTrackMVA.mx'
                    },
                    'GEC': 'Loose'
                },
                # 1400 MeV / TT / GEC11500
                'TrackMVAVTight': {
                    'TrChi2': 2.5,
                    'TrGP': 0.2,
                    'MinPT': 3000. * MeV,  # shift of 2 GeV
                    'MaxPT': 27000. * MeV,  # shift of 2 GeV
                    'MinIPChi2': 7.4,
                    'Param1': 1.0,
                    'Param2': 3.0,  # shift of 2 GeV, add 2
                    'Param3': 1.296,  # 1.2 * ( 1 + 2/25 )
                    'GEC': 'Loose'
                },
                'TwoTrackMVAVTight': {
                    'P': 5000. * MeV,
                    'PT': 1400. * MeV,  # simulated tracking cut
                    'TrGP': 0.2,
                    'TrChi2': 2.5,
                    'IPChi2': 4.,
                    'MinMCOR': 1000. * MeV,
                    'MaxMCOR': 1e9 * MeV,
                    'MinETA': 2.,
                    'MaxETA': 5.,
                    'MinDirA': 0.,
                    'V0PT': 2000. * MeV,
                    'VxChi2': 10.,
                    'Threshold': 0.99,  # tight threshold
                    'MvaVars': {
                        'chi2': 'VFASPF(VCHI2)',
                        'fdchi2': 'BPVVDCHI2()',
                        'sumpt': 'SUMTREE(PT, ISBASIC, 0.0)',
                        'nlt16': 'NINTREE(ISBASIC & (BPVIPCHI2() < 16))'
                    },
                    'Classifier': {
                        'Type': 'MatrixNet',
                        'File': '$PARAMFILESROOT/data/Hlt1TwoTrackMVA.mx'
                    },
                    'GEC': 'Loose'
                },
                # This corresponds to a shift of zero
                'TrackMuonMVA': {
                    'MinPT': 1000. * MeV,
                    'MaxPT': 25000. * MeV,
                    'MinIPChi2': 7.4,
                    'TrChi2': 2.5,
                    'TrGP': 0.2,
                    'Param1': 1.0,
                    'Param2': 1.0,
                    'Param3': 1.2,
                    'GEC': 'Loose'
                },
                'Priorities': {
                    'TrackMVA': 1,
                    'TwoTrackMVA': 2,
                    'TrackMVALoose': 3,
                    'TwoTrackMVALoose': 4,
                    'TrackMuonMVA': 5
                },
                'Prescale': {
                    'Hlt1TrackMuonMVA': 1.0,
                }
            },
            Hlt1MuonLinesConf: {
                'DiMuonLowMass_VxDOCA': 0.2,
                'DiMuonLowMass_VxChi2': 16,
                'DiMuonLowMass_P': 3000,
                'DiMuonLowMass_PT': 800.,
                'DiMuonLowMass_TrChi2': 2.5,
                'DiMuonLowMass_TrGP': 0.2,
                'DiMuonLowMass_M': 0.,
                'DiMuonLowMass_IPChi2': 9.,
                'DiMuonLowMass_GEC': 'All',
                'DiMuonLowMass_CFTracking': False,
                'DiMuonHighMass_VxDOCA': 0.2,
                'DiMuonHighMass_VxChi2': 16,
                'DiMuonHighMass_P': 6000,
                'DiMuonHighMass_PT': 800.,
                'DiMuonHighMass_TrChi2': 2.5,
                'DiMuonHighMass_TrGP': 0.2,
                'DiMuonHighMass_M': 2700,
                'DiMuonHighMass_GEC': 'All',
                'DiMuonHighMass_CFTracking': False,
                'DiMuonLowMassTight_VxDOCA': 0.2,
                'DiMuonLowMassTight_VxChi2': 16.,
                'DiMuonLowMassTight_P': 3000,
                'DiMuonLowMassTight_PT': 1400.,
                'DiMuonLowMassTight_TrChi2': 2.5,
                'DiMuonLowMassTight_TrGP': 0.2,
                'DiMuonLowMassTight_M': 0.,
                'DiMuonLowMassTight_IPChi2': 9.,
                'DiMuonLowMassTight_GEC': 'Loose',
                'DiMuonLowMassTight_CFTracking': False,
                'DiMuonHighMassTight_VxDOCA': 0.2,
                'DiMuonHighMassTight_VxChi2': 16.,
                'DiMuonHighMassTight_P': 6000.,
                'DiMuonHighMassTight_PT': 1400.,
                'DiMuonHighMassTight_TrChi2': 2.5,
                'DiMuonHighMassTight_TrGP': 0.2,
                'DiMuonHighMassTight_M': 2700,
                'DiMuonHighMassTight_GEC': 'Loose',
                'DiMuonHighMassTight_CFTracking': False,
                'Prescale': {
                    'Hlt1NoPVPassThrough': 1.0,
                    'Hlt1SingleMuonNoIP': 0.01,
                    'Hlt1DiMuonNoL0': 1.0,
                    'Hlt1DiMuonNoIPSS': 0.25,
                    'Hlt1MultiMuonNoL0': 0.01,
                    'Hlt1SingleMuonHighPTNoMUID': 0.0005
                },
                'Priorities': {
                    'SingleMuonHighPT': 8,
                    'DiMuonLowMass': 7,
                    'DiMuonHighMass': 6,
                    'DiMuonNoL0': 9
                }
            }
        }

        # HLT2 thresholds from individual files
        sds = self.SubDirs()
        for version, subdirs in self.SubDirs().iteritems():
            for subdir in subdirs:
                conf = _get_conf(subdir, "_" + version)
                update_thresholds(thresholds, conf.Thresholds())

        return thresholds

    def ActiveHlt2Lines(self):
        """Return a list of active lines."""
        hlt2 = []
        sds = self.SubDirs()
        for version, subdirs in self.SubDirs().iteritems():
            for subdir in subdirs:
                conf = _get_conf(subdir, "_" + version)
                hlt2.extend(conf.ActiveHlt2Lines())
        return hlt2

    def ActiveHlt1Lines(self):
        """Return a list of active lines."""
        lines = []
        for tight in ['VLoose', 'Loose', 'Tight', 'VTight']:
            lines += ['Hlt1TrackMVA' + tight, 'Hlt1TwoTrackMVA' + tight]
        lines += [
            'Hlt1TrackMuonMVA',
            'Hlt1DiMuonLowMass',
            'Hlt1DiMuonHighMass',
            'Hlt1DiMuonLowMassTight',
            'Hlt1DiMuonHighMassTight',
        ]
        return lines
