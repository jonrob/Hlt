###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from XenonXenon_2017 import XenonXenon_2017
from Utilities.Utilities import update_thresholds


class VanDerMeerScan_XeXe_2017(XenonXenon_2017):
    def HltType(self):
        self.verifyType(VanDerMeerScan_XeXe_2017)
        return 'VanDerMeerScan_XeXe_2017'

    def NanoBanks(self):
        nanobanks = {'LUMI': ['ODIN', 'HltLumiSummary', 'HltRoutingBits', 'DAQ']}
        nanobanks['BEAMGAS'] = nanobanks['LUMI'] + ['Velo', 'L0DU', 'HltDecReports', 'HC']
        return nanobanks

    def StreamsWithLumi(self):
        return ['BEAMGAS']
