###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, mm, MeV, picosecond

class DPS_pp_2018 :
    """
    Threshold settings for Hlt2 Double Parton Scattering lines: DPS_pp_2018
    
    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS
    
    @author S. Stracka < simone.stracka@unipi.it > 
    @date 2015-07-20
    """
    
    __all__ = ( 'ActiveHlt2Lines' )
    
    
    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        lines = [
            ## "double charm"
            'Hlt2DPS2x2mu'   ,
            'Hlt2DPS2xHc'    ,
            'Hlt2DPS2muHc'   ,
            ## "triple charm"
            'Hlt2DPS3x2mu'   ,
            'Hlt2DPS3xHc'    ,
            'Hlt2DPS2mu2Hc'  ,
            'Hlt2DPS2x2muHc'             
            ]
        
        return lines

    def Thresholds(self) :
        
        d = {}
        
        from Hlt2Lines.DPS.Lines import DPSLines
        
        d.update ( { DPSLines : {
            'Prescale'  : {
            ## double charm 
            'DPS2x2mu'   : 1.0 ,
            'DPS2xHc'    : 1.0 ,
            'DPS2muHc'   : 1.0 ,
            ## triple  charm
            'DPS3x2mu'   : 1.0 ,
            'DPS3xHc'    : 1.0 ,
            'DPS2mu2Hc'  : 1.0 ,
            'DPS2x2muHc' : 1.0 ,            
            },
            'Postscale' : {
            ## double charm 
            'DPS2x2mu'   : 1.0 ,
            'DPS2xHc'    : 1.0 ,
            'DPS2muHc'   : 1.0 ,
            ## triple  charm
            'DPS3x2mu'   : 1.0 ,
            'DPS3xHc'    : 1.0 ,
            'DPS2mu2Hc'  : 1.0 ,
            'DPS2x2muHc' : 1.0 }
           } } )
        
        return d

    
