###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from GaudiKernel.SystemOfUnits import GeV, mm, MeV, picosecond

class DiMuon_pp_5tev2017(object) :
    """
    Threshold settings for Hlt2 DiMuon lines for the 5 TeV run of 2017

    @author Alessio Piucci
    @date 20-10-2017
    """

    __all__ = ( 'ActiveHlt2Lines' )


    def ActiveHlt2Lines(self) :
        """
        Returns a list of active lines
        """

        lines = [

            # Turbo lines 
            'Hlt2DiMuonJPsiTurbo',
            'Hlt2DiMuonPsi2STurbo',
            'Hlt2DiMuonUpsilonTurbo',
            'Hlt2DiMuonChicXTurbo',
            'Hlt2DiMuonBJpsiKTurbo',
            
            ]

        return lines


    def Thresholds(self) :
        """
        Returns a dictionary of cuts
        """

        # keep pass through thresholds
        d = { }

        from Hlt2Lines.DiMuon.Lines     import DiMuonLines            
        d.update({DiMuonLines :
                  {'BaseDiMuon' :  {  'MuPt'       :     0 * MeV,
                                      'VertexChi2' :    25,
                                      'TRACK_TRGHOSTPROB_MAX': 0.4,
                                      'PIDCut'     : "(MINTREE('mu-' == ABSID, PROBNNmu)>0.1)"},
                   
                   'JPsi' :          {'MassWindow' :   150 * MeV,
                                      'Pt'         :     0 * MeV},

                   # do not set any pt cut here!
                   'JPsiNoPT' :      {'MassWindow' :    80 * MeV,
                                      'Pt'         :     0 * MeV},

                   'DetachedCommon' :  {'Pt'         :     0 * MeV,
                                        'MuPt'       :     0 * MeV,
                                        'VertexChi2' :    25,
                                        'TRACK_TRGHOSTPROB_MAX': 0.4,
                                        'DLS'        :     3},       

                   'DetachedJPsi' :        {'MassWindow'  :     150},

                   # Turbo lines
                   'JPsiTurbo' :          {'MassWindow' :   150 * MeV,
                                           'Pt'         :     0 * MeV},

                   'Psi2STurbo' :         {'MassWindow' :   150 * MeV,
                                           'Pt'         :     0 * MeV},

                   'UpsilonTurbo' :       {'MinMass'    :   7900 * MeV,
                                           'VertexChi2' :     25,
                                           'PIDCut'     : "MINTREE('mu-' == ABSID, PROBNNmu) > 0.2" },
                   
                   'ChicXTurbo':          {'MassWindow_comb' :  100 * MeV,
                                           'VertexChi2'      :  25},
                   
                   'ChicXExtraSelK' :        {'VertexChi2'  :  25},
                   'ChicXExtraSelP' :        {'VertexChi2'  :  25},
                   'ChicXExtraSelPi' :       {'VertexChi2'  :  25},
                   'ChicXExtraSelMu' :       {'VertexChi2'  :  25},
                   'ChicXExtraSelKsDD' :     {'VertexChi2'  :  25},
                   'ChicXExtraSelKsLL' :     {'VertexChi2'  :  25},
                   'ChicXExtraSelElectron' : {'VertexChi2'  :  25},
                   'ChicXExtraSelGamma' :    {'VertexChi2'  :  25},
                   'ChicXExtraSelLambdaDD' : {'VertexChi2'  :  25},
                   'ChicXExtraSelLambdaLL' : {'VertexChi2'  :  25},

                   'BJpsiKTurbo' :        {'MassWindow_comb'  :  200 * MeV,
                                           'Lifetime'         :  0.3 * picosecond,
                                           'VertexChi2'       :  25,
                                           },

                   # PersistReco
                   'PersistReco' : {'JPsiTurbo'         : False,
                                    'Psi2STurbo'        : False,
                                    'UpsilonTurbo'      : True,
                                    'BJpsiKTurbo'       : False,
                                    },
                   
                   }
                  })
        return d
    
    

