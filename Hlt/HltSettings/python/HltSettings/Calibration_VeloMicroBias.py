###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
class Calibration_VeloMicroBias :
    """
    Threshold settings for OT aging activity

    WARNING :: DO NOT EDIT WITHOUT PERMISSION OF THE AUTHORS

    @author J. Albrecht
    @date 2010-08-09
    """


    def __init__(self) :
        self.StripEndSequence = ['']

    def HltType(self) :
        return 'Calibration_VeloMicroBias'

    def L0TCK(self) :
        return '0x1715'

    def ActiveHlt2Lines(self) :
        """
        Returns a list of active Hlt2 lines
        """
        return  []

    def ActiveHlt1Lines(self) :
        """
        Returns a list of active Hlt1 lines
        """
        return [ 'Hlt1MBMicroBiasVelo', 'Hlt1Tell1Error' ]

    def Thresholds(self) :
        """
        Returns a dictionary of cuts
        """
        from Hlt1Lines.Hlt1MBLines import Hlt1MBLinesConf
        from Hlt1Lines.Hlt1CommissioningLines   import Hlt1CommissioningLinesConf

        d = {Hlt1CommissioningLinesConf : {'Prescale'  : {'Hlt1Tell1Error'   : 1},
                                           'Postscale' : {'Hlt1Tell1Error'   : 'RATE(10)'},
                                          },
                                          # TODO how to change TRGTYP below?
             Hlt1MBLinesConf : {'ODIN' : {'MicroBias'               : 'ODIN_PASS(LHCb.ODIN.Physics)',
                                          'MicroBiasLowMultVelo'    : 'ODIN_PASS(LHCb.ODIN.NoBias)',
                                          'NoBias'                  : 'ODIN_PASS(LHCb.ODIN.NoBias)',
                                          'CharmCalibrationNoBias'  : 'ODIN_PASS(LHCb.ODIN.NoBias)',
                                          'NoBiasLeadingCrossing'   : 'ODIN_PASS(LHCb.ODIN.SequencerTrigger)'},
                                'Prescale' : {'Hlt1MBMicroBias.*' : 1. }
                               }
            }

        return d

    def Streams(self):
        expr = "HLT_PASS('%sDecision')" % self.ActiveHlt1Lines()[0]
        return {'VELOCLOSING' : expr,
                'HLT1NOBIAS'  : expr}
