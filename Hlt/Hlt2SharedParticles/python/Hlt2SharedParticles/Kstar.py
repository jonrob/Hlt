###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
### @file
#
#  Standard K*
#
#  @author P. Koppenburg Patrick.Koppenburg@cern.ch
#  @date 2008-07-15
#
##
from Gaudi.Configuration import *
from Configurables import CombineParticles
from Hlt2SharedParticles.GoodParticles import GoodKaons, GoodPions
from Hlt2SharedParticles.TrackFittedBasicParticles import  BiKalmanFittedKaons, BiKalmanFittedPions
from Hlt2SharedParticles.TrackFittedBasicParticles import  BiKalmanFittedRichKaons
from HltLine.HltLine import bindMembers, Hlt2Member

__all__ = ( 'Kstar2KPi' , 'TightKstar2KPi' )

Hlt2SharedKstar2KPi = Hlt2Member( CombineParticles
                                  , "Hlt2SharedKstar2KPi"
                                  , Inputs = [ BiKalmanFittedKaons,
                                               BiKalmanFittedPions ]
                                  , DecayDescriptor = "[K*(892)0 -> K+ pi-]cc" 
                                  , DaughtersCuts = { "pi+" : "ALL", "K+" : "ALL" } 
                                  , CombinationCut = "(ADAMASS('K*(892)0')<300)"
                                  , MotherCut = "(VFASPF(VCHI2PDOF)<25)")

Kstar2KPi = bindMembers( 'Shared', [ BiKalmanFittedKaons, BiKalmanFittedPions, Hlt2SharedKstar2KPi ] )


# Tight K* with PID

Hlt2SharedTightKstar2KPi = Hlt2Member( CombineParticles
                                       , "Hlt2SharedTightKstar2KPi"
                                       , Inputs = [ BiKalmanFittedRichKaons,
                                                    BiKalmanFittedPions ]
                                       , DecayDescriptor = "[K*(892)0 -> K+ pi-]cc"
                                       , DaughtersCuts = { "pi+" : "(PT > 500*MeV)", "K+" : "(PT > 500*MeV) & (PIDK>0.)" }
                                       , CombinationCut = "(ADAMASS('K*(892)0')<100)"
                                       , MotherCut = "(VFASPF(VCHI2PDOF)<25)" )

TightKstar2KPi = bindMembers( 'Shared', [ BiKalmanFittedRichKaons, BiKalmanFittedPions, Hlt2SharedTightKstar2KPi ] )

