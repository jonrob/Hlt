###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
### @file
#
#  Standard Track Fitted Di-electron
#
#  @author P. Koppenburg Patrick.Koppenburg@cern.ch
#  @date 2008-07-15
#
##
from Gaudi.Configuration import *
from Configurables import CombineParticles
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedElectrons
from HltLine.HltLine import bindMembers, Hlt2Member

__all__ = ('TrackFittedDiElectron',
           'TrackFittedSoftDiElectron',
           'TrackFittedVerySoftDiElectron')

Hlt2SharedTrackFittedSoftDiElectron = Hlt2Member( CombineParticles
                                              , "TrackFittedSoftDiElectron"
                                              , Inputs = [ BiKalmanFittedElectrons ]
                                              , DecayDescriptor = "J/psi(1S) -> e+ e-"
                                              , DaughtersCuts = {"e+" : "(PT>500*MeV)"}
                                              , CombinationCut = "AALL"
                                              , MotherCut = "(VFASPF(VCHI2PDOF)<25)"
                                              )

Hlt2SharedTrackFittedDiElectron = Hlt2Member( CombineParticles
                                              , "TrackFittedDiElectron"
                                              , Inputs = [ BiKalmanFittedElectrons ]
                                              , DecayDescriptor = "J/psi(1S) -> e+ e-"
                                              , DaughtersCuts = {"e+" : "(PT>1000*MeV)"}
                                              , CombinationCut = "AALL"
                                              , MotherCut = "(VFASPF(VCHI2PDOF)<25)"
                                              )

Hlt2SharedTrackFittedVerySoftDiElectron = Hlt2Member( CombineParticles
                                                  , "TrackFittedVerySoftDiElectron"
                                                  , Inputs = [ BiKalmanFittedElectrons ]
                                                  , DecayDescriptor = "J/psi(1S) -> e+ e-"
                                                  , DaughtersCuts = {"e+" : "(PT>50*MeV) & (TRGHP < 0.2)",
                                                                     "e-" : "(PT>50*MeV) & (TRGHP < 0.2)"}
                                                  , CombinationCut = "(ADOCAMAX('')<1.0*mm)"
                                                  , MotherCut = "(VFASPF(VCHI2/VDOF) < 10)"
                                                  )


TrackFittedDiElectron = bindMembers( "Shared", [ BiKalmanFittedElectrons, Hlt2SharedTrackFittedDiElectron ] )
TrackFittedSoftDiElectron = bindMembers( "Shared", [ BiKalmanFittedElectrons, Hlt2SharedTrackFittedSoftDiElectron ] )
TrackFittedVerySoftDiElectron = bindMembers( "Shared", [ BiKalmanFittedElectrons, Hlt2SharedTrackFittedVerySoftDiElectron ] )
