###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
### @file
#
# Standard Lambda
#  @author F. Dettori Francesco.Dettori@cern.ch
#  Based on code by P. Koppenburg
#  @date 2010-03-18
#
##
from Gaudi.Configuration import *
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedPions, BiKalmanFittedDownPions, BiKalmanFittedProtons, BiKalmanFittedDownProtons
from Configurables import CombineParticles
from HltLine.HltLine import bindMembers, Hlt2Member
from HltTracking.HltPVs import PV3D
__all__ = ( 'LambdaLLTrackFitted', 'LambdaDDTrackFitted' )

##################################################
# Standard Lambda: Fitted Long Tracks
Hlt2SharedLambdaLLTrackFitted = Hlt2Member( CombineParticles, "LambdaLLTrackFitted"
                                            , DecayDescriptor = "[Lambda0 -> p+ pi-]cc"
                                            , DaughtersCuts = { "pi+" : "BPVVALID() & (TRCHI2DOF<4) & (MIPCHI2DV(PRIMARY)>36)",
                                                                "p+"  : "BPVVALID() & (TRCHI2DOF<4) & (MIPCHI2DV(PRIMARY)>36)"}
                                            , CombinationCut = "(ADAMASS('Lambda0')<50*MeV)"
                                            , MotherCut = "(ADMASS('Lambda0')<20*MeV)"
                                                          " & (CHI2VXNDOF<30)"
                                                          " & (BPVLTIME() > 2.0*ps) "
                                            , Inputs = [ BiKalmanFittedPions, BiKalmanFittedProtons ]
                                            )

##################################################
# Standard Lambda: Fitted Downstream Tracks
Hlt2SharedLambdaDDTrackFitted = Hlt2Member( CombineParticles, "LambdaDDTrackFitted"
                                            , DecayDescriptor = "[Lambda0 -> p+ pi-]cc"
                                            , DaughtersCuts = { "pi+" : "(TRCHI2DOF<4)& (P>3000*MeV)  & (PT>175.*MeV)",
                                                                "p+"  : "(TRCHI2DOF<4)& (P>3000*MeV)  & (PT>175.*MeV)"}
                                            , CombinationCut = "(ADAMASS('Lambda0')<80*MeV)"
                                            , MotherCut = "in_range(1095*MeV, M, 1140*MeV)"
                                                          " & (CHI2VXNDOF<30)"
                                                          " & (BPVVDZ() > 400.0*mm)"
                                            , Inputs = [ BiKalmanFittedDownPions, BiKalmanFittedDownProtons ]
                                            )

LambdaLLTrackFitted = bindMembers( "Shared", [ PV3D('Hlt2'), BiKalmanFittedPions, BiKalmanFittedProtons, Hlt2SharedLambdaLLTrackFitted ] )
LambdaDDTrackFitted = bindMembers( "Shared", [ PV3D('Hlt2'), BiKalmanFittedDownPions, BiKalmanFittedDownProtons, Hlt2SharedLambdaDDTrackFitted ] )


