###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
### @file
#
#  Basic particles for fitted tracks
#
#  @author O. Lupton oliver.lupton@cern.ch
#
#  Based on code by Patrick Koppenburg and Vladimir Gligorov
#
#  @date 2017-04-24
#
##

from Gaudi.Configuration import *
from GaudiKernel.SystemOfUnits import MeV
from HltLine.HltLine import bindMembers, Hlt2Member
from Configurables import NoPIDsParticleMaker, CombinedParticleMaker, ProtoParticleCALOFilter
from Configurables import PhotonMaker, PhotonMakerAlg, BremAdder

##########################################################################
#
# Forward tracking
#
from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking
Hlt2BiKalmanFittedForwardTracking     = Hlt2BiKalmanFittedForwardTracking()
#
# Downstream tracking
#
from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedDownstreamTracking
Hlt2BiKalmanFittedDownstreamTracking  = Hlt2BiKalmanFittedDownstreamTracking()

##########################################################################
#
# Long track protoparticles with all PID information
#
BiKalmanFittedChargedProtoMaker       = Hlt2BiKalmanFittedForwardTracking.hlt2ChargedAllPIDsProtos()
#
# Downstream track protoparticles with all PID information
#
BiKalmanFittedChargedDownProtoMaker   = Hlt2BiKalmanFittedDownstreamTracking.hlt2ChargedAllPIDsProtos()
#
# Neutral protoparticles
#
BiKalmanFittedNeutralProtoMaker       = Hlt2BiKalmanFittedForwardTracking.hlt2NeutralProtos()


##########################################################################
#
# Do we write P2PVRelations for basic charged particles?
#
writeP2PVforBasicCharged = True

##########################################################################
#
# Make muons without PID requirements (e.g. for PID calibration lines)
#
Hlt2BiKalmanFittedNoPIDsMuons = NoPIDsParticleMaker("Hlt2BiKalmanFittedNoPIDsMuons"
                                            , Particle                      = "muon"
                                            , Input                         = BiKalmanFittedChargedProtoMaker.outputSelection()
                                            , Output                        = "Hlt2/Hlt2BiKalmanFittedNoPIDsMuons/Particles"
                                            , WriteP2PVRelations            = writeP2PVforBasicCharged
                                            )

##########################################################################
#
# Make downstream muons without PID requirements (e.g. for PID calibration lines)
#
Hlt2BiKalmanFittedNoPIDsDownMuons = NoPIDsParticleMaker("Hlt2BiKalmanFittedNoPIDsDownMuons"
                                            , Particle                      = "muon"
                                            , Input                         = BiKalmanFittedChargedDownProtoMaker.outputSelection()
                                            , Output                        = "Hlt2/Hlt2BiKalmanFittedNoPIDsDownMuons/Particles"
                                            , WriteP2PVRelations            = writeP2PVforBasicCharged
                                            )

##########################################################################
#
# Make the pions
#
Hlt2BiKalmanFittedPions = NoPIDsParticleMaker("Hlt2BiKalmanFittedPions"
                                            , Particle                      = "pion"
                                            , Input                         = BiKalmanFittedChargedProtoMaker.outputSelection()
                                            , Output                        = 'Hlt2/Hlt2BiKalmanFittedPions/Particles'
                                            , WriteP2PVRelations            = writeP2PVforBasicCharged
                                            )
##########################################################################
#
# Make the downstream pions
#
Hlt2BiKalmanFittedDownPions = NoPIDsParticleMaker("Hlt2BiKalmanFittedDownPions"
                                            , Particle                      = "pion"
                                            , Input                         = BiKalmanFittedChargedDownProtoMaker.outputSelection()
                                            , Output                        = 'Hlt2/Hlt2BiKalmanFittedDownPions/Particles'
                                            , WriteP2PVRelations            = writeP2PVforBasicCharged
                                            )

##########################################################################
#
# Make the kaons
#
Hlt2BiKalmanFittedKaons = NoPIDsParticleMaker("Hlt2BiKalmanFittedKaons"
                                            , Particle                      = "kaon"
                                            , Input                         = BiKalmanFittedChargedProtoMaker.outputSelection()
                                            , Output                        = 'Hlt2/Hlt2BiKalmanFittedKaons/Particles'
                                            , WriteP2PVRelations            = writeP2PVforBasicCharged
                                            )

##########################################################################
#
# Make the downstream kaons
#
Hlt2BiKalmanFittedDownKaons = NoPIDsParticleMaker("Hlt2BiKalmanFittedDownKaons"
                                            , Particle                      = "kaon"
                                            , Input                         = BiKalmanFittedChargedDownProtoMaker.outputSelection()
                                            , Output                        = 'Hlt2/Hlt2BiKalmanFittedDownKaons/Particles'
                                            , WriteP2PVRelations            = writeP2PVforBasicCharged
                                            )

##########################################################################
#
# Make the protons
#
Hlt2BiKalmanFittedProtons = NoPIDsParticleMaker("Hlt2BiKalmanFittedProtons"
                                            , Particle                      = "proton"
                                            , Input                         = BiKalmanFittedChargedProtoMaker.outputSelection()
                                            , Output                        = 'Hlt2/Hlt2BiKalmanFittedProtons/Particles'
                                            , WriteP2PVRelations            = writeP2PVforBasicCharged
                                            )

##########################################################################
#
# Make the downstream protons
#
Hlt2BiKalmanFittedDownProtons = NoPIDsParticleMaker("Hlt2BiKalmanFittedDownProtons"
                                            , Particle                      = "proton"
                                            , Input                         = BiKalmanFittedChargedDownProtoMaker.outputSelection()
                                            , Output                        = 'Hlt2/Hlt2BiKalmanFittedDownProtons/Particles'
                                            , WriteP2PVRelations            = writeP2PVforBasicCharged
                                            )

##########################################################################
#
# Make electrons without PID requirements or bremsstrahlung correction
#
Hlt2BiKalmanFittedNoPIDsNoBremElectrons = NoPIDsParticleMaker("Hlt2BiKalmanFittedNoPIDsNoBremElectrons"
                                            , Particle                      = "electron"
                                            , Input                         = BiKalmanFittedChargedProtoMaker.outputSelection()
                                            , Output                        = "Hlt2/Hlt2BiKalmanFittedNoPIDsNoBremElectrons/Particles"
                                            , WriteP2PVRelations            = writeP2PVforBasicCharged
                                            , AddBremPhotonTo               = [ ]
                                            )

##########################################################################
#
# Make downstream electrons without PID requirements or bremsstrahlung correction
#
Hlt2BiKalmanFittedNoPIDsNoBremDownElectrons = NoPIDsParticleMaker("Hlt2BiKalmanFittedNoPIDsNoBremDownElectrons"
                                            , Particle                      = "electron"
                                            , Input                         = BiKalmanFittedChargedDownProtoMaker.outputSelection()
                                            , Output                        = "Hlt2/Hlt2BiKalmanFittedNoPIDsNoBremDownElectrons/Particles"
                                            , WriteP2PVRelations            = writeP2PVforBasicCharged
                                            , AddBremPhotonTo               = [ ]
                                            )

##########################################################################
#
# Make the photons
#
Hlt2BiKalmanFittedBremPhotons = PhotonMakerAlg("Hlt2BiKalmanFittedBremPhotons")
Hlt2BiKalmanFittedBremPhotons.Input = BiKalmanFittedNeutralProtoMaker.outputSelection()
Hlt2BiKalmanFittedBremPhotons.Output = 'Hlt2/Hlt2BiKalmanFittedBremPhotons/Particles'
Hlt2BiKalmanFittedBremPhotons.addTool(PhotonMaker)
Hlt2BiKalmanFittedBremPhotons.PhotonMaker.Input = BiKalmanFittedNeutralProtoMaker.outputSelection()
Hlt2BiKalmanFittedBremPhotons.PhotonMaker.ConvertedPhotons = True  
Hlt2BiKalmanFittedBremPhotons.PhotonMaker.UnconvertedPhotons = True  
Hlt2BiKalmanFittedBremPhotons.PhotonMaker.PtCut = 75.* MeV # cut value taken from offline
Hlt2BiKalmanFittedBremPhotons.WriteP2PVRelations = False # do P2PV relations make sense for neutrals?

##########################################################################
#
# Make electrons without PID requirements (e.g. for PID calibration lines)
#
# NOTE: these are *slow* to create because there is no PID requirement and the BremAdder is slow...
Hlt2BiKalmanFittedNoPIDsElectrons = NoPIDsParticleMaker("Hlt2BiKalmanFittedNoPIDsElectrons"
                                            , Particle                      = "electron"
                                            , Input                         = BiKalmanFittedChargedProtoMaker.outputSelection()
                                            , Output                        = "Hlt2/Hlt2BiKalmanFittedNoPIDsElectrons/Particles"
                                            , WriteP2PVRelations            = writeP2PVforBasicCharged
                                            )
Hlt2BiKalmanFittedNoPIDsElectrons.addTool(BremAdder)
Hlt2BiKalmanFittedNoPIDsElectrons.BremAdder.BremInput = Hlt2BiKalmanFittedBremPhotons.Output

##########################################################################
#
# Make downstream electrons without PID requirements (e.g. for PID calibration lines)
#
# NOTE: these are *slow* to create because there is no PID requirement and the BremAdder is slow...
Hlt2BiKalmanFittedNoPIDsDownElectrons = NoPIDsParticleMaker("Hlt2BiKalmanFittedNoPIDsDownElectrons"
                                            , Particle                      = "electron"
                                            , Input                         = BiKalmanFittedChargedDownProtoMaker.outputSelection()
                                            , Output                        = "Hlt2/Hlt2BiKalmanFittedNoPIDsDownElectrons/Particles"
                                            , WriteP2PVRelations            = writeP2PVforBasicCharged
                                            )
Hlt2BiKalmanFittedNoPIDsDownElectrons.addTool(BremAdder)
Hlt2BiKalmanFittedNoPIDsDownElectrons.BremAdder.BremInput = Hlt2BiKalmanFittedBremPhotons.Output

##########################################################################
#
# Make electrons with PID requirements *and* bremsstrahlung correction
#
# NOTE: these are *not* created by running FilterDesktop on Hlt2BiKalmanFittedNoPIDsElectrons because
#       those are slow to create
Hlt2BiKalmanFittedElectrons           = CombinedParticleMaker("Hlt2BiKalmanFittedElectrons"
                                            , Particle                      =  "electron"
                                            , Input                         =  BiKalmanFittedChargedProtoMaker.outputSelection()
                                            , Output                        = 'Hlt2/Hlt2BiKalmanFittedElectrons/Particles'
                                            , WriteP2PVRelations            = writeP2PVforBasicCharged
                                            )
Hlt2BiKalmanFittedElectrons.addTool(ProtoParticleCALOFilter('Electron'))
Hlt2BiKalmanFittedElectrons.Electron.Selection          = ["RequiresDet='CALO' CombDLL(e-pi)>'-2.0'"]
Hlt2BiKalmanFittedElectrons.addTool(BremAdder)
Hlt2BiKalmanFittedElectrons.BremAdder.BremInput = Hlt2BiKalmanFittedBremPhotons.Output

##########################################################################
#
# Make downstream electrons with PID requirements *and* bremsstrahlung correction
#
# NOTE: these are *not* created by running FilterDesktop on Hlt2BiKalmanFittedNoPIDsDownElectrons because
#       those are slow to create
Hlt2BiKalmanFittedDownElectrons       = CombinedParticleMaker("Hlt2BiKalmanFittedDownElectrons"
                                            , Particle                      =  "electron"
                                            , Input                         =  BiKalmanFittedChargedDownProtoMaker.outputSelection()
                                            , Output                        = 'Hlt2/Hlt2BiKalmanFittedDownElectrons/Particles'
                                            , WriteP2PVRelations            = writeP2PVforBasicCharged
                                            )
Hlt2BiKalmanFittedDownElectrons.addTool(ProtoParticleCALOFilter('Electron'))
Hlt2BiKalmanFittedDownElectrons.Electron.Selection          = ["RequiresDet='CALO' CombDLL(e-pi)>'-2.0'"]
Hlt2BiKalmanFittedDownElectrons.addTool(BremAdder)
Hlt2BiKalmanFittedDownElectrons.BremAdder.BremInput = Hlt2BiKalmanFittedBremPhotons.Output

#
# define exported symbols -- these are for available
# for use in Hlt2 by adding:
#
# from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedMuons
#
__all__ = (   'BiKalmanFittedMuons',
              'BiKalmanFittedDownMuons', 
              'BiKalmanFittedNoPIDsMuons',
              'BiKalmanFittedNoPIDsDownMuons',
              'BiKalmanFittedElectrons',
              'BiKalmanFittedNoBremElectrons',
              'BiKalmanFittedNoPIDsElectrons',
              'BiKalmanFittedNoPIDsNoBremElectrons',
              'BiKalmanFittedDownElectrons',
              'BiKalmanFittedNoBremDownElectrons',
              'BiKalmanFittedNoPIDsDownElectrons',
              'BiKalamnFittedNoPIDsNoBremDownElectrons',
              'BiKalmanFittedKaons',
              'BiKalmanFittedDownKaons',
              'BiKalmanFittedPions',
              'BiKalmanFittedDownPions',
              'BiKalmanFittedProtons',
              'BiKalmanFittedDownProtons',
              'BiKalmanFittedRichPions',
              'BiKalmanFittedRichKaons',
              'BiKalmanFittedRichProtons',
              'BiKalmanFittedPhotons',
              'BiKalmanFittedBremPhotons')

# Brem photons first, because these are needed for the with-brem electrons
BiKalmanFittedBremPhotons               = bindMembers(None, [ BiKalmanFittedNeutralProtoMaker,      Hlt2BiKalmanFittedBremPhotons                                                     ] )

# Now the long/downstream charged particles, all without PID cuts
BiKalmanFittedPions                     = bindMembers(None, [ BiKalmanFittedChargedProtoMaker,      Hlt2BiKalmanFittedPions                                                           ] )
BiKalmanFittedDownPions                 = bindMembers(None, [ BiKalmanFittedChargedDownProtoMaker,  Hlt2BiKalmanFittedDownPions                                                       ] )
BiKalmanFittedKaons                     = bindMembers(None, [ BiKalmanFittedChargedProtoMaker,      Hlt2BiKalmanFittedKaons                                                           ] )
BiKalmanFittedDownKaons                 = bindMembers(None, [ BiKalmanFittedChargedDownProtoMaker,  Hlt2BiKalmanFittedDownKaons                                                       ] )
BiKalmanFittedNoPIDsMuons               = bindMembers(None, [ BiKalmanFittedChargedProtoMaker,      Hlt2BiKalmanFittedNoPIDsMuons                                                     ] )
BiKalmanFittedNoPIDsDownMuons           = bindMembers(None, [ BiKalmanFittedChargedDownProtoMaker,  Hlt2BiKalmanFittedNoPIDsDownMuons                                                 ] )
BiKalmanFittedProtons                   = bindMembers(None, [ BiKalmanFittedChargedProtoMaker,      Hlt2BiKalmanFittedProtons                                                         ] )
BiKalmanFittedDownProtons               = bindMembers(None, [ BiKalmanFittedChargedDownProtoMaker,  Hlt2BiKalmanFittedDownProtons                                                     ] )
BiKalmanFittedNoPIDsElectrons           = bindMembers(None, [ BiKalmanFittedChargedProtoMaker,      BiKalmanFittedBremPhotons,                  Hlt2BiKalmanFittedNoPIDsElectrons     ] )
BiKalmanFittedNoPIDsDownElectrons       = bindMembers(None, [ BiKalmanFittedChargedDownProtoMaker,  BiKalmanFittedBremPhotons,                  Hlt2BiKalmanFittedNoPIDsDownElectrons ] )
BiKalmanFittedNoPIDsNoBremElectrons     = bindMembers(None, [ BiKalmanFittedChargedProtoMaker,      Hlt2BiKalmanFittedNoPIDsNoBremElectrons                                           ] )
BiKalmanFittedNoPIDsNoBremDownElectrons = bindMembers(None, [ BiKalmanFittedChargedDownProtoMaker,  Hlt2BiKalmanFittedNoPIDsNoBremDownElectrons                                       ] )

# Long/downstream electrons with PID cuts and bremsstrahlung -- special case because BremAdder is slow
BiKalmanFittedElectrons                 = bindMembers(None, [ BiKalmanFittedChargedProtoMaker,      BiKalmanFittedBremPhotons,                  Hlt2BiKalmanFittedElectrons           ] )
BiKalmanFittedDownElectrons             = bindMembers(None, [ BiKalmanFittedChargedDownProtoMaker,  BiKalmanFittedBremPhotons,                  Hlt2BiKalmanFittedDownElectrons       ] )

# now make the basic particles with PID cuts -- these used to use CombinedParticleMaker

from Configurables import FilterDesktop
# LoKi equivalent to ["RequiresDet='MUON' IsMuon=True"]
muon_cut = "(PPCUT(PP_HASINFO(LHCb.ProtoParticle.InAccMuon))) & (ISMUON)"
Hlt2BiKalmanFittedMuons                 = Hlt2Member(FilterDesktop
                                            , "BiKalmanFittedMuons"
                                            , Inputs = [ BiKalmanFittedNoPIDsMuons ]
                                            , Code = muon_cut
                                            , shared = True
                                            )

Hlt2BiKalmanFittedDownMuons             = Hlt2Member(FilterDesktop
                                            , "BiKalmanFittedDownMuons"
                                            , Inputs = [ BiKalmanFittedNoPIDsDownMuons ]
                                            , Code = muon_cut
                                            , shared = True
                                            )

# LoKi equivalent to ["RequiresDet='RICH'"]
rich_cut = "(PPCUT(PP_HASINFO(LHCb.ProtoParticle.RichPIDStatus)))"
Hlt2BiKalmanFittedRichPions             = Hlt2Member(FilterDesktop
                                            , "BiKalmanFittedRichPions"
                                            , Inputs = [ BiKalmanFittedPions ]
                                            , Code = rich_cut
                                            , shared = True
                                            )

Hlt2BiKalmanFittedRichKaons             = Hlt2Member(FilterDesktop
                                            , "BiKalmanFittedRichKaons"
                                            , Inputs = [ BiKalmanFittedKaons ]
                                            , Code = rich_cut
                                            , shared = True
                                            )

Hlt2BiKalmanFittedRichProtons           = Hlt2Member(FilterDesktop
                                            , "BiKalmanFittedRichProtons"
                                            , Inputs = [ BiKalmanFittedProtons ]
                                            , Code = rich_cut
                                            , shared = True
                                            )

Hlt2BiKalmanFittedPhotons               = Hlt2Member(FilterDesktop
                                            , "BiKalmanFittedPhotons"
                                            , Inputs = [ Hlt2BiKalmanFittedBremPhotons ]
                                            , Code = "(PT > 200*MeV)"
                                            , shared = True
                                            )

# LoKi equivalent to ["RequiresDet='CALO' CombDLL(e-pi)>'-2.0'"]
electron_cut = "((PPCUT(PP_HASCALOPRS) | PPCUT(PP_HASCALOECAL)) & (PIDe > -2.0))"
Hlt2BiKalmanFittedNoBremElectrons       = Hlt2Member(FilterDesktop
                                            , "BiKalmanFittedNoBremElectrons"
                                            , Inputs = [ BiKalmanFittedNoPIDsNoBremElectrons ]
                                            , Code = electron_cut
                                            , shared = True
                                            )

Hlt2BiKalmanFittedNoBremDownElectrons   = Hlt2Member(FilterDesktop
                                            , "BiKalmanFittedNoBremDownElectrons"
                                            , Inputs = [ BiKalmanFittedNoPIDsNoBremDownElectrons ]
                                            , Code = electron_cut
                                            , shared = True
                                            )

BiKalmanFittedMuons               = bindMembers("Shared", [ BiKalmanFittedNoPIDsMuons,              Hlt2BiKalmanFittedMuons               ])
BiKalmanFittedDownMuons           = bindMembers("Shared", [ BiKalmanFittedNoPIDsDownMuons,          Hlt2BiKalmanFittedDownMuons           ])
BiKalmanFittedRichPions           = bindMembers("Shared", [ BiKalmanFittedPions,                    Hlt2BiKalmanFittedRichPions           ])
BiKalmanFittedRichKaons           = bindMembers("Shared", [ BiKalmanFittedKaons,                    Hlt2BiKalmanFittedRichKaons           ])
BiKalmanFittedRichProtons         = bindMembers("Shared", [ BiKalmanFittedProtons,                  Hlt2BiKalmanFittedRichProtons         ])
BiKalmanFittedPhotons             = bindMembers("Shared", [ BiKalmanFittedBremPhotons,              Hlt2BiKalmanFittedPhotons             ])
BiKalmanFittedNoBremElectrons     = bindMembers("Shared", [ BiKalmanFittedNoPIDsNoBremElectrons,    Hlt2BiKalmanFittedNoBremElectrons     ])
BiKalmanFittedNoBremDownElectrons = bindMembers("Shared", [ BiKalmanFittedNoPIDsNoBremDownElectrons,Hlt2BiKalmanFittedNoBremDownElectrons ])
