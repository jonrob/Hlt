###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
### @file
#
#  Charged particles for the tracking efficiency lines, including
#  velo-muon particles, muon-TT particles, downstream particles
#  and, separately
#  just the particles based on velo track information only. 
#
#  @author V. Gligorov vladimir.gligorov@cern.ch
#  @date 2010-03-25
#
##
# =============================================================================
__author__  = "V. Gligorov vladimir.gligorov@cern.ch"
# =============================================================================
from Gaudi.Configuration import *
from HltLine.HltLine import bindMembers, Hlt2Member
from Configurables import FilterDesktop
from Configurables import CombinedParticleMaker, BestPIDParticleMaker, NoPIDsParticleMaker, TrackSelector
from Configurables import ProtoParticleMUONFilter
#
# These are all based on fitted tracks
#
from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking, Hlt2BiKalmanFittedDownstreamTracking
Hlt2BiKalmanFittedForwardTracking   			= Hlt2BiKalmanFittedForwardTracking()
Hlt2BiKalmanFittedDownstreamTracking   			= Hlt2BiKalmanFittedDownstreamTracking()

# Tag and probe tracking configurations
from HltTracking.Hlt2ProbeTrackingConfigurations import (Hlt2MuonTTTracking,
                                                         Hlt2VeloMuonTracking,
                                                         Hlt2FullDownstreamTracking)

Hlt2MuonTTTracking = Hlt2MuonTTTracking()
Hlt2VeloMuonTracking = Hlt2VeloMuonTracking()
Hlt2FullDownstreamTracking = Hlt2FullDownstreamTracking()

##########################################################################
#
# Charged protoparticles -> pulls all the pid
#
caloProtos 		= Hlt2BiKalmanFittedForwardTracking.hlt2ChargedAllPIDsProtos( )
muonWithCaloProtos 	= Hlt2BiKalmanFittedForwardTracking.hlt2ChargedAllPIDsProtos( )
muonTTProtos = Hlt2MuonTTTracking.hlt2ProbeMuonProtos()
velomuonProtos = Hlt2VeloMuonTracking.hlt2ProbeMuonProtos()
fulldownProtos = Hlt2FullDownstreamTracking.hlt2ProbeMuonProtos()
##########################################################################
# Make the pions 
#
Hlt2TagAndProbePions = CombinedParticleMaker("Hlt2TagAndProbePions")
Hlt2TagAndProbePions.Input =  caloProtos.outputSelection()
Hlt2TagAndProbePions.Output = 'Hlt2/Hlt2TagAndProbePions/Particles'
Hlt2TagAndProbePions.Particle =  "pion"
Hlt2TagAndProbePions.WriteP2PVRelations = False
##########################################################################
# Make the Muons
#
Hlt2TagAndProbeMuons = CombinedParticleMaker("Hlt2TagAndProbeMuons")
Hlt2TagAndProbeMuons.Particle = "muon"
Hlt2TagAndProbeMuons.addTool(ProtoParticleMUONFilter('Muon'))
Hlt2TagAndProbeMuons.Muon.Selection = ["RequiresDet='MUON' IsMuon=True" ]
Hlt2TagAndProbeMuons.Input =  muonWithCaloProtos.outputSelection()
Hlt2TagAndProbeMuons.Output = 'Hlt2/Hlt2TagAndProbeMuons/Particles'
Hlt2TagAndProbeMuons.WriteP2PVRelations = False
##########################################################################
#
# No PID muon particles, for association with probe track
#
##########################################################################
Hlt2LongAssocParts = NoPIDsParticleMaker("Hlt2LongAssocParts")
Hlt2LongAssocParts.Particle = 'pion' # call this a pion for CombinePart to distringuish from probe
Hlt2LongAssocParts.addTool( TrackSelector )
Hlt2LongAssocParts.TrackSelector.TrackTypes = [ "Long" ]
Hlt2LongAssocParts.Input =  muonWithCaloProtos.outputSelection()
Hlt2LongAssocParts.Output =  "Hlt2/Hlt2LongAssocMuons/Particles"

##########################################################################
#
# MuonTT particles
#
##########################################################################
Hlt2MuonTTParts = NoPIDsParticleMaker("Hlt2MuonTTParts")
Hlt2MuonTTParts.Particle = 'muon'
Hlt2MuonTTParts.addTool( TrackSelector )
Hlt2MuonTTParts.TrackSelector.TrackTypes = [ "Long" ]
Hlt2MuonTTParts.Input =  muonTTProtos.outputSelection()
Hlt2MuonTTParts.Output =  "Hlt2/Hlt2MuonTTMuons/Particles"



##########################################################################
#
# VeloMuon particles
#
##########################################################################
Hlt2VeloMuonParts = NoPIDsParticleMaker("Hlt2VeloMuonParts")
Hlt2VeloMuonParts.Particle = 'Muon'
Hlt2VeloMuonParts.Input =  velomuonProtos.outputSelection()
Hlt2VeloMuonParts.Output =  "Hlt2/Hlt2VeloMuons/Particles"

##########################################################################
#
# FullDownstream particles
#
##########################################################################
Hlt2FullDownParts = BestPIDParticleMaker("DownParts" , Particle = "muon")
Hlt2FullDownParts.addTool(ProtoParticleMUONFilter,name="muon")
Hlt2FullDownParts.muon.Selection = ["RequiresDet='MUON' IsMuonLoose=True"]
Hlt2FullDownParts.Particles = [ "muon" ]
Hlt2FullDownParts.Input = fulldownProtos.outputSelection()
Hlt2FullDownParts.Output = "Hlt2/Hlt2DownstreamMuons/Particles"

##########################################################################
#
# Velo particles
#
##########################################################################

from HltTracking.HltTrackNames import HltSharedTrackLoc, HltDefaultFitSuffix, _baseProtoPLocation, TrackName, Hlt2TrackEffRoot
from HltLine.HltLine import bindMembers
from Configurables import CombinedParticleMaker, ChargedProtoParticleMaker, BestPIDParticleMaker, NoPIDsParticleMaker
from Configurables import DelegatingTrackSelector
from HltTracking.HltSharedTracking import RevivedVelo, FittedVelo

Hlt2VeloProtos = ChargedProtoParticleMaker('Hlt2VeloOnlyProtosForTrackEff')
Hlt2VeloProtos.Inputs = [ FittedVelo.outputSelection() ]
Hlt2VeloProtos.Output = _baseProtoPLocation("Hlt2", Hlt2TrackEffRoot+"/"+TrackName["Velo"])
Hlt2VeloProtos.addTool(DelegatingTrackSelector, name='delTrackSelVelo')
Hlt2VeloProtos.delTrackSelVelo.TrackTypes = ['Velo']

Hlt2VeloOnlyElectronParts = NoPIDsParticleMaker("Hlt2VeloOnlyElectronParts")
Hlt2VeloOnlyElectronParts.Particle = 'electron'
Hlt2VeloOnlyElectronParts.Input =  Hlt2VeloProtos.Output
Hlt2VeloOnlyElectronParts.Output =  Hlt2TrackEffRoot+"/Hlt2VeloOnlyElectrons/Particles"

Hlt2VeloOnlyMuonParts = NoPIDsParticleMaker("Hlt2VeloOnlyMuons")
Hlt2VeloOnlyMuonParts.Particle = 'muon'
Hlt2VeloOnlyMuonParts.Input =  Hlt2VeloProtos.Output
Hlt2VeloOnlyMuonParts.Output =  Hlt2TrackEffRoot+"/Hlt2VeloOnlyMuons/Particles"

Hlt2VeloOnlyPionParts = NoPIDsParticleMaker("Hlt2VeloOnlyPionParts")
Hlt2VeloOnlyPionParts.Particle = 'pion'
Hlt2VeloOnlyPionParts.Input =  Hlt2VeloProtos.Output
Hlt2VeloOnlyPionParts.Output =  Hlt2TrackEffRoot+"/Hlt2VeloOnlyPions/Particles"

Hlt2VeloOnlyKaonParts = NoPIDsParticleMaker("Hlt2VeloOnlyKaonParts")
Hlt2VeloOnlyKaonParts.Particle = 'kaon'
Hlt2VeloOnlyKaonParts.Input =  Hlt2VeloProtos.Output
Hlt2VeloOnlyKaonParts.Output =  Hlt2TrackEffRoot+"/Hlt2VeloOnlyKaons/Particles"

Hlt2VeloOnlyProtonParts = NoPIDsParticleMaker("Hlt2VeloProtonParts")
Hlt2VeloOnlyProtonParts.Particle = "proton"
Hlt2VeloOnlyProtonParts.Input =  Hlt2VeloProtos.Output
Hlt2VeloOnlyProtonParts.Output =  Hlt2TrackEffRoot+"/Hlt2VeloOnlyProtons/Particles"

Hlt2GoodVeloKaons = Hlt2Member( FilterDesktop,"VeloOnlyKaons",
                                Inputs = [ Hlt2VeloOnlyKaonParts.Output ],
                                Code = "in_range(1.9, ETA, 5.0) & (MIPDV(PRIMARY) > 0.03) & (TRCHI2DOF<5)")

Hlt2GoodVeloPions = Hlt2Member( FilterDesktop,"VeloOnlyPions",
                                Inputs = [ Hlt2VeloOnlyPionParts.Output ],
                                Code = "in_range(1.9, ETA, 5.0) & (MIPDV(PRIMARY) > 0.03) & (TRCHI2DOF<5)")


# define exported symbols -- these are for available
# for use in Hlt2 by adding:
#
# from Hlt2SharedParticles.TagAndProbeParticles import TagAndProbeMuons
#

__all__ = ( 'TagAndProbePions', 
            'TagAndProbeMuons',
            'LongAssocMuons',
            'TagDownstreamMuons',
            'Hlt2ProbeVeloMuons',
            
            'Hlt2ProbeVeloElectrons',
            'Hlt2ProbeVeloPions',
            'Hlt2ProbeVeloKaons',
            'Hlt2GoodProbeVeloPions',
            'Hlt2GoodProbeVeloKaons')

Hlt2ProbeVeloOnlyMuons  = bindMembers( None, [ FittedVelo, Hlt2VeloProtos , Hlt2VeloOnlyMuonParts ] )
Hlt2ProbeVeloOnlyElectrons  = bindMembers( None, [ FittedVelo, Hlt2VeloProtos , Hlt2VeloOnlyElectronParts ] )
Hlt2ProbeVeloOnlyPions  = bindMembers( None, [ FittedVelo, Hlt2VeloProtos , Hlt2VeloOnlyPionParts ] )
Hlt2ProbeVeloOnlyKaons  = bindMembers( None, [ FittedVelo, Hlt2VeloProtos , Hlt2VeloOnlyKaonParts ] )

Hlt2ProbeVeloOnlyProtons  = bindMembers( None, [ FittedVelo, Hlt2VeloProtos , Hlt2VeloOnlyProtonParts ] )

Hlt2GoodProbeVeloPions  = bindMembers( 'Good', [ FittedVelo, Hlt2VeloProtos, Hlt2ProbeVeloOnlyPions, Hlt2GoodVeloPions])
Hlt2GoodProbeVeloKaons  = bindMembers( 'Good', [ FittedVelo, Hlt2VeloProtos, Hlt2ProbeVeloOnlyKaons, Hlt2GoodVeloKaons])

TagAndProbePions   = bindMembers( None, [ caloProtos		,	Hlt2TagAndProbePions 	] )
TagAndProbeMuons   = bindMembers( None, [ muonWithCaloProtos	, 	Hlt2TagAndProbeMuons	] )
LongAssocMuons     = bindMembers( None, [ muonWithCaloProtos	, 	Hlt2LongAssocParts	] )
ProbeMuonTTMuons	= bindMembers( None, [ muonTTProtos	,	Hlt2MuonTTParts	] )
ProbeVeloMuons   	= bindMembers( None, [ velomuonProtos, Hlt2VeloMuonParts] )
ProbeDownstreamMuons	= bindMembers( None, [ fulldownProtos, Hlt2FullDownParts] )
