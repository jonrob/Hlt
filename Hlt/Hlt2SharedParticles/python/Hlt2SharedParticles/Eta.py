###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
##
from Gaudi.Configuration import *
from HltLine.HltLine import bindMembers
from Configurables import PhotonMaker, ResolvedPi0Maker, MergedPi0Maker
from GaudiKernel.SystemOfUnits import MeV
#
from HltTracking.Hlt2TrackingConfigurations import Hlt2BiKalmanFittedForwardTracking
Hlt2BiKalmanFittedForwardTracking = Hlt2BiKalmanFittedForwardTracking()
##########################################################################
#
# Neutral protoparticles
#
neutralProtos = Hlt2BiKalmanFittedForwardTracking.hlt2NeutralProtos()
##########################################################################
# Make the eta
#
Hlt2ResolvedEtas 			= ResolvedPi0Maker("Hlt2ResolvedEtas")
Hlt2ResolvedEtas.Input      = neutralProtos.outputSelection() 
Hlt2ResolvedEtas.DecayDescriptor 	= "Eta"
Hlt2ResolvedEtas.Particle   = 'eta'
Hlt2ResolvedEtas.Output 	= 'Hlt2/Hlt2ResolvedEtas/Particles'
Hlt2ResolvedEtas.addTool(PhotonMaker)
Hlt2ResolvedEtas.PhotonMaker.Input 	= neutralProtos.outputSelection() 
Hlt2ResolvedEtas.MassWindow 		= 105. * MeV #
Hlt2ResolvedEtas.PhotonMaker.PtCut 	= 200.*MeV
__all__ = ( 'ResolvedEtas' )

ResolvedEtas  = bindMembers( None, [ neutralProtos, Hlt2ResolvedEtas ] ).setOutputSelection(Hlt2ResolvedEtas.Output)

