###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
# @file:   Definition of the electrons used by the DiElectron lines
# @author: Miguel Ramos Pernas miguel.ramos.pernas@cern.ch
# @author: Jessica Prisciandaro jessica.prisciandaro@cern.ch
# @date:   2016-02-12
# =============================================================================

__all__    = ( 'SoftElectrons', 'TrackFittedDetachedDiElectron', 'SoftElectronsNoIP', 'TrackFittedDetachedDiElectronNoIP' )

from Gaudi.Configuration import *
from Configurables import FilterDesktop, BremAdder, DiElectronMaker
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedElectrons
from Hlt2SharedParticles.TrackFittedBasicParticles import BiKalmanFittedBremPhotons as Photons
from HltLine.HltLine import bindMembers, Hlt1Tool, Hlt2Member
from HltTracking.HltPVs import PV3D

Electrons     = Hlt2Member(FilterDesktop, "Electrons",     Inputs = [BiKalmanFittedElectrons], Code = "(MIPDV(PRIMARY) > 0.4 * mm) & (MIPCHI2DV(PRIMARY) > 5) & (TRGHOSTPROB < 0.15) & (PT > 80 * MeV)")
ElectronsNoIP = Hlt2Member(FilterDesktop, "ElectronsNoIP", Inputs = [BiKalmanFittedElectrons], Code = "(TRGHOSTPROB < 0.15) & (PT > 80 * MeV)")

SoftElectrons     = bindMembers('Soft',     [PV3D('Hlt2'), BiKalmanFittedElectrons, Electrons])
SoftElectronsNoIP = bindMembers('SoftNoIP', [              BiKalmanFittedElectrons, ElectronsNoIP])

BA = Hlt1Tool(type = BremAdder, name = 'BremAdder', BremInput = Photons.outputSelection())

Hlt2SharedTrackFittedDetachedDiElectron = Hlt2Member( DiElectronMaker
                                                      , "TrackFittedDetachedDiElectron"
                                                      , Input = "" # Suppress confusing warnings
                                                      , Inputs = [ ] # about missing ProtoParticles
                                                      , ElectronInputs = [ SoftElectrons.outputSelection() ]
                                                      , Particle = 'KS0'
                                                      , DecayDescriptor = "KS0 -> e+ e-"
                                                      , ElectronPtMin=80
                                                      , DiElectronPtMin=200
                                                      , DiElectronMassMax=1000
                                                      , tools = [BA]
                                                      )

# This will allow for photon conversion, but also for possible light resonances with non-zero lifetime going to two electrons
Hlt2SharedTrackFittedDetachedDiElectronNoIP = Hlt2Member( DiElectronMaker
                                                          , "TrackFittedDetachedDiElectronNoIP"
                                                          , Input = "" # Suppress confusing warnings
                                                          , Inputs = [ ] # about missing ProtoParticles
                                                          , ElectronInputs = [ SoftElectronsNoIP.outputSelection() ]
                                                          , Particle = 'KS0'
                                                          , DecayDescriptor = "KS0 -> e+ e-"
                                                          , ElectronPtMin=80
                                                          , DiElectronPtMin=200
                                                          , DiElectronMassMax=1000
                                                          , tools = [BA]
                                                          )

TrackFittedDetachedDiElectron     = bindMembers("Shared", [PV3D('Hlt2'), Photons, SoftElectrons,     Hlt2SharedTrackFittedDetachedDiElectron])
TrackFittedDetachedDiElectronNoIP = bindMembers("Shared", [              Photons, SoftElectronsNoIP, Hlt2SharedTrackFittedDetachedDiElectronNoIP])
