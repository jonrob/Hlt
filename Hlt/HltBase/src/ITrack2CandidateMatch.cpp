/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// Event
// ============================================================================
// ============================================================================
// Local
// ============================================================================
#include "HltBase/ITrack2CandidateMatch.h"
// ============================================================================
/** @file
 *  Implementation file for class Hlt::ITrack2CandidateMatch
 *  @see Hlt::ITrack2CandidateMatch
 *  @date  2011-02-11
 *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
 */
// ============================================================================
// protected & virtual destructor
// ============================================================================
Hlt::ITrack2CandidateMatch::~ITrack2CandidateMatch()
{
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
