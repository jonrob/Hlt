/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#include "HltBase/IHltUnit.h"
// ============================================================================
/** @file
 *  Implementation file for class LoKi::IHltUnit
 *  @date 2008-11-10
 *  @author Vanya BELYAEV Ivan.BELYAEV@nikhef.nl
 */
// ============================================================================
// the unique interface identifier
// ============================================================================
const InterfaceID& Hlt::IUnit::interfaceID()
{
    // the unique interafcx eidentifier
    static const InterfaceID s_IID{"Hlt::IUnit", 1, 0};
    //
    return s_IID;
}
// ============================================================================
// virtual & protected destructor
// ============================================================================
Hlt::IUnit::~IUnit()
{
}
// ============================================================================
// The END
// ============================================================================
