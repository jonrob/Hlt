/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <string>
#include <vector>
// ============================================================================
// HltBase
// ============================================================================
#include "HltBase/IHltInspector.h"
// ============================================================================
/** @file
 *  Implementation file for class Hlt::IInspector
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2009-03-17
 */
// ============================================================================
// virtual & protected destructor
// ============================================================================
Hlt::IInspector::~IInspector()
{
}
// ============================================================================
// Return the unique interface ID
// ============================================================================
const InterfaceID& Hlt::IInspector::interfaceID()
{
    static const InterfaceID s_IID{"Hlt::IInspector", 2, 0};
    return s_IID;
}
// ============================================================================
// The END
// ============================================================================
