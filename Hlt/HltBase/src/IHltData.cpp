/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// HltBase
// ============================================================================
#include "HltBase/IHltData.h"

#ifdef STATUSCODE_ENUM_IMPL
STATUSCODE_ENUM_IMPL( Hlt::IData::Errors )
#endif

// ============================================================================
/** @file
 *  Implementation file for class Hlt::IData
 *  @date 2009-03-17
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 */
// ============================================================================
// virtual & protected destructor
// ============================================================================
Hlt::IData::~IData()
{
}
// ============================================================================
// Return the unique interface ID
// ============================================================================
const InterfaceID& Hlt::IData::interfaceID()
{
    static const InterfaceID s_IID{"Hlt::IData", 2, 0};
    return s_IID;
}

// ============================================================================
// The END
// ============================================================================
