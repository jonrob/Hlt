/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// local
// ============================================================================
#include "Event/HltL0DiMuonCandidate.h"
// ============================================================================
namespace
{
// ==========================================================================
// Class ID definition
const CLID CLID_L0DiMuonCandidate = 7563;
// ==========================================================================
}
// ============================================================================
// Class ID
// ============================================================================
const CLID& Hlt::L0DiMuonCandidate::classID()
{
    return CLID_L0DiMuonCandidate;
}
// ============================================================================
// Retrieve pointer to class definition structure
// ============================================================================
const CLID& Hlt::L0DiMuonCandidate::clID() const
{
    return Hlt::L0DiMuonCandidate::classID();
}
// ============================================================================
std::ostream& Hlt::L0DiMuonCandidate::fillStream( std::ostream& s ) const
{
    return s;
}
// ============================================================================
// The END
// ============================================================================

