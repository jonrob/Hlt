/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "HltBase/HltBaseAlg.h"

#include "Event/RecVertex.h"
#include "Event/Track.h"
#include "Event/Particle.h"

//-----------------------------------------------------------------------------
// Implementation file for class : HltBaseAlg
//
// 2006-06-15 : Jose Angel Hernando Morata
//-----------------------------------------------------------------------------


StatusCode HltBaseAlg::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize();
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if (produceHistos()) {
      for (auto&  i : m_histoDescriptor.value()) {
          book(i.second);
          debug() << " booking histogram from descriptor "
                         << i.second << endmsg;
      }
  }
  return StatusCode::SUCCESS;
}

AIDA::IHistogram1D* HltBaseAlg::initializeHisto(const std::string& title,
                                            double x0, double xf, int nbins) {
  AIDA::IHistogram1D* histo = nullptr;
  if (!produceHistos()) return histo;

  const auto& values = m_histoDescriptor.value();
  auto i = values.find(title);
  Gaudi::Histo1DDef hd = (i!=values.end()) ? i->second
                                           : Gaudi::Histo1DDef{title,x0,xf,nbins};
  debug() << " booking histogram  " << hd << endmsg;
  return book(hd);
}

// ============================================================================
// accessor to Hlt Registration Service
// ============================================================================
Hlt::IRegister* HltBaseAlg::regSvc() const
{
  if ( UNLIKELY(!m_regSvc) ) m_regSvc = service( "Hlt::Service" , true ) ;
  return m_regSvc ;
}
// ============================================================================
// accessor to Hlt Data Service
// ============================================================================
Hlt::IData* HltBaseAlg::hltSvc() const
{
  if ( UNLIKELY(!m_hltSvc) ) m_hltSvc = service( "Hlt::Service" , true ) ;
  return m_hltSvc ;
}

IANNSvc& HltBaseAlg::annSvc() const {
  if ( UNLIKELY(!m_hltANNSvc) ) m_hltANNSvc = service("HltANNSvc",true);
  return *m_hltANNSvc;
}


