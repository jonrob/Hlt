/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// =============================================================================
// GaudiKernel
// =============================================================================
#include "GaudiKernel/ToStream.h"
// =============================================================================
#include "HltBase/Cache.h"
// =============================================================================
namespace Hlt
{
// =============================================================================
// =============================================================================
const std::vector<const Cache::KeyType*> Cache::keys() const
{
    std::vector<const KeyType*> result;
    for ( auto& value : m_boolean_store ) result.push_back( &value.first );
    for ( auto& value : m_integer_store ) result.push_back( &value.first );
    for ( auto& value : m_double_store ) result.push_back( &value.first );
    for ( auto& value : m_string_store ) result.push_back( &value.first );
    return result;
}

std::ostream& Cache::fillStream( std::ostream& s ) const
{
    std::string delim{};
    s << "Cache {";
    for ( auto& value : m_boolean_store ) {
        s << delim;
        Gaudi::Utils::toStream( value.first, s );
        s << ": ";
        Gaudi::Utils::toStream( value.second, s );
        delim = ", ";
    }
    for ( auto& value : m_integer_store ) {
        s << delim;
        Gaudi::Utils::toStream( value.first, s );
        s << ": ";
        Gaudi::Utils::toStream( value.second, s );
        delim = ", ";
    }
    for ( auto& value : m_double_store ) {
        s << delim;
        Gaudi::Utils::toStream( value.first, s );
        s << ": ";
        Gaudi::Utils::toStream( value.second, s );
        delim = ", ";
    }

    for ( auto& value : m_string_store ) {
        s << delim;
        Gaudi::Utils::toStream( value.first, s );
        s << ": ";
        Gaudi::Utils::toStream( value.second, s );
        delim = ", ";
    }
    s << "}";
    s << " CacheKeys [";
    delim = "";
    for ( auto key : keys() ) {
        s << delim;
        Gaudi::Utils::toStream( *key, s );
        delim = ", ";
    }
    s << "]";
    return s;
}
// =============================================================================
} //  Hlt
  // =============================================================================
