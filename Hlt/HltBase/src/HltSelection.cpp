/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "HltBase/HltSelection.h"
#include "Event/L0CaloCandidate.h"
#include "Event/L0MuonCandidate.h"

#include "Event/HltL0DiMuonCandidate.h"
#include "Event/HltMultiTrack.h"
#include "Event/HltStage.h"
#include "Event/HltCandidate.h"

template <typename T>
void Hlt::TSelection<T>::clean()
{
    Selection::clean();
    m_candidates.clear();
}

template <typename T>
Hlt::TSelection<T>::~TSelection()
{
}

// explicityly instantiate allowed instances...
template class Hlt::TSelection<LHCb::Track>;
template class Hlt::TSelection<LHCb::RecVertex>;
template class Hlt::TSelection<LHCb::L0CaloCandidate>;
template class Hlt::TSelection<LHCb::L0MuonCandidate>;
template class Hlt::TSelection<LHCb::Particle>;
template class Hlt::TSelection<LHCb::ProtoParticle>;

template class Hlt::TSelection<Hlt::Candidate>;
template class Hlt::TSelection<Hlt::Stage>;
template class Hlt::TSelection<Hlt::MultiTrack>;
template class Hlt::TSelection<Hlt::L0DiMuonCandidate>;
