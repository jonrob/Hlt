/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HLTBASE_HLTBASE_H
#define HLTBASE_HLTBASE_H 1

// Include files
#include <vector>
#include <map>
#include <string>
#include <iostream>
#include <type_traits>
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/StringKey.h"
#include "GaudiKernel/VectorMap.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "Kernel/IANNSvc.h"
#include "HltBase/IHltRegister.h"
#include "HltBase/IHltData.h"
#include "Event/Track.h"

/** @class HltBaseAlg
 *
 *  Provide services to HLT algorithms and tools
 *
 *  Functionality:
 *       Access to HltSvc, Hlt::Data
 *       Retrieve, register Hlt::Selections
 *       Histogram booking via options
 *       Counters
 *
 *  Options
 *       HistogramDescriptor
 *
 *  @author Hugo Ruiz Perez
 *  @author Jose Angel Hernando Morata
 *  @date   2006-06-15
 */
namespace LHCb
{
class RecVertex;
}

class ISvcLocator;

class HltBaseAlg : public GaudiHistoAlg
{
  public:
    // Algorithm constructor
    using GaudiHistoAlg::GaudiHistoAlg;

    // initialize
    StatusCode initialize() override;

  protected:
    // initialize Histo
    AIDA::IHistogram1D* initializeHisto( const std::string& name, double min = 0.,
                                         double max = 100., int nBins = 100 );

    IANNSvc& annSvc() const;
    Hlt::IRegister* regSvc() const;
    Hlt::IData* hltSvc() const;

  private:

    // Property to book histogram from options
    Gaudi::Property<std::map<std::string, Gaudi::Histo1DDef>> m_histoDescriptor {this, "HistoDescriptor" };

    // hlt data provider service
    mutable SmartIF<Hlt::IData> m_hltSvc;
    //
    // hlt data registration service
    mutable SmartIF<Hlt::IRegister> m_regSvc;

    // assigned names and numbers service...
    mutable SmartIF<IANNSvc> m_hltANNSvc;
};
#endif // HLTBASE_HLTBASE_H
