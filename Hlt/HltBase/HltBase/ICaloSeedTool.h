/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: ICaloSeedTool.h,v 1.4 2010-01-06 08:46:59 albrecht Exp $
#ifndef HLTBASE_ICALOSEEDTOOL_H
#define HLTBASE_ICALOSEEDTOOL_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"

static const InterfaceID IID_ICaloSeedTool( "ICaloSeedTool", 1, 0 );

namespace LHCb
{
class L0CaloCandidate;
}

/** @class ICaloSeedTool ICaloSeedTool.h HltBase/ICaloSeedTool.h
 *
 *
 *  @author Johannes Albrecht
 *  @date   2007-07-03
 */
class ICaloSeedTool : virtual public IAlgTool
{
  public:
    // Return the interface ID
    static const InterfaceID& interfaceID()
    {
        return IID_ICaloSeedTool;
    }

    virtual StatusCode makeTrack( const LHCb::L0CaloCandidate& eL0Cand,
                                  LHCb::Track& seedTrack ) = 0;
};
#endif // HLTBASE_ICALOSEEDTOOL_H
