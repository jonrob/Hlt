/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IMUONSEEDTOOL_H
#define IMUONSEEDTOOL_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"
#include "Event/Track.h"

// forward declarations
namespace LHCb
{
class L0MuonCandidate;
}

static const InterfaceID IID_IMuonSeedTool( "IMuonSeedTool", 1, 0 );

/** @class IMuonSeedTool IMuonSeedTool.h
 *
 *
 *  @author Johannes Albrecht
 *  @date   2007-07-03
 */
class IMuonSeedTool : virtual public IAlgTool
{
  public:
    // Return the interface ID
    static const InterfaceID& interfaceID()
    {
        return IID_IMuonSeedTool;
    }

    /*
     *    this method prepares a L0MuonCandidate to a track at the M2
     *    which can then be used to seed L0 confirmation
     *
     *    input:  a L0MuonCandidate
     *    output: a Track which hold a state at M2
     *            errors of the state define search window size for L0Confirmation
     */
    virtual StatusCode makeTrack( const LHCb::L0MuonCandidate& muonL0Cand,
                                  LHCb::Track& seedTrack ) = 0;

    /*
     *    this method prepares a track from MuonCoords to a track at the M2
     *    which can then be used to seed L0 confirmation
     *
     *    input:  a Track from the muon coords
     *    output: a Track which hold a state at M2
     *            errors of the state define search window size for L0Confirmation
     */
    virtual StatusCode makeTrack( const LHCb::Track& inputTrack,
                                  LHCb::Track& seedTrack ) = 0;
};
#endif // IMUONSEEDTOOL_H
