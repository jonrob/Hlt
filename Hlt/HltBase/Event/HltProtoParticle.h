/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef HltEvent_ProtoParticle_H_
#define HltEvent_ProtoParticle_H_ 1

// ============================================================================
// Just want to define default TES location for Hlt ProtoParticles
// ============================================================================

namespace Hlt
{
  // Namespace for locations in TES
  namespace ProtoParticleLocation
  {
  // =========================================================================
  /** @var Default
   *  The default TES-location of HLT-ProtoParticles
   */
    const std::string Default = "Hlt/ProtoParticles";
  }
}
#endif
