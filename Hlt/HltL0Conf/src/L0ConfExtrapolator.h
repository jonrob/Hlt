/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef L0CONFEXTRAPOLATOR_H
#define L0CONFEXTRAPOLATOR_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "Kernel/ILHCbMagnetSvc.h"

#include "HltBase/IL0ConfExtrapolator.h"            // Interface

#include "Event/Track.h"

/** @class L0ConfExtrapolator L0ConfExtrapolator.h
 *
 *
 *  @author Johannes Albrecht
 *  @date   2008-01-18
 */
class L0ConfExtrapolator : public GaudiTool, virtual public IL0ConfExtrapolator {
public:
  /// Standard constructor
  L0ConfExtrapolator( const std::string& type,
                      const std::string& name,
                      const IInterface* parent);

 /// Initialize method
  StatusCode initialize() override;

  void muon2T( const LHCb::Track& muonTrack, LHCb::State& stateAtT ) const override;

  void ecal2T( const LHCb::Track& ecalTrack,
               LHCb::State& statePosAtT, LHCb::State& stateNegAtT ) const override;

  void hcal2T( const LHCb::Track& ecalTrack,
               LHCb::State& statePosAtT, LHCb::State& stateNegAtT ) const override;

  int getCaloRegion( double stateX, double stateY, double stateZ ) const override;

  StatusCode updateField() ;

  double getBScale() override;

protected:

private:
  SmartIF<ILHCbMagnetSvc>  m_magFieldSvc;
  bool m_fieldOff = false;
  double m_BscaleFactor = 0;

  //for getParabolaHypothesis
  double m_curvFactor;

  std::vector<double> m_fwdSigmaX2;
  std::vector<double> m_fwdSigmaY2;

  double m_ecalKick;

  double m_HECalKick[2], m_HCalKick[2];

  /// @todo FIXME: until this gets into StateParameters.h, we define a constant
  //  static const double zEndT3 = 9315.0;
  double zEndT3 = 9315.0;

  // this routine does the real work once the kick in x direction has
  // been figured out by [eh]cal2T rountines above
  void calo2T( const LHCb::State& aState, double xkick,
               LHCb::State& statePosAtT, LHCb::State& stateNegAtT ) const;
};
#endif // L0CONFEXTRAPOLATOR_H
