If you have a crucial bugfix or feature addition for the current production version, make a merge request with the 2018-patches branch. 

If you want to add a new line, make a merge request with the 2018-patches branch. 

If you have developments which target the Upgrade, make a merge request with master.
